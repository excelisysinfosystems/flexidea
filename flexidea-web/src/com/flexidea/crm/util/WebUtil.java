package com.flexidea.crm.util;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.*;

/**
 *
 * @author akshay
 */
public class WebUtil {
    private static final Logger logger = Logger
	    .getLogger(WebUtil.class.getName());

    public static final String extendedDateFormat = "EEEE, dd MMMM, yyyy";
    public static final String timeFormat = "HH:mm a";
    public static final String dateFormat = "dd/MMM/yyyy";
    public static final String dateTimeFormat = "dd MMM yyyy HH:mm";
    public static final String parseableDateTimeFormat =
	    "dd/MMM/yyyy HH:mm";

    public static final String shortDate = "dd MMM yyyy";
    public static final String shortTime = "HH:mm a";
    public static final String dateTime = "dd MMM yyyy HH:mm a";
    public static final String dateAndMonth = "dd MMMM";
    /*
    public static CrsUsers getCurrentUser(HttpServletRequest request) {
	//return UserDAO.getUserByID(1);
	CrsUsers user = null;
	HttpSession session = request.getSession(false);
	if (session != null) {
	    user = (CrsUsers) session.getAttribute("USER");
	}
	return user;
    }
     *
     */

    public static boolean checkDecimal(Map<String, String> e,
	    String field, String val, double minVal, double maxVal) {
	if (! clean(val).matches("([-])?^[0-9]+(\\.[0-9]{1,2})?$")) {
	    e.put(field, ("errors." + field + ".nan"));
	    return false;
	}

	double value;
	if (minVal > 0) {
	    value = Double.parseDouble(val);
	    if (value < minVal) {
		e.put(field, ("errors." + field + ".minval"));
		return false;
	    }
	}

	if (maxVal > 0) {
	    value = Double.parseDouble(val);
	    if (value > maxVal) {
		e.put(field, ("errors." + field + ".maxval"));
		return false;
	    }
	}
	return true;
    }

    public static boolean checkNumeric(Map<String, String> e,
	    String field, String val, int minVal, int maxVal) {
	if (! clean(val).matches("[0-9]+")) {
	    e.put(field, ("errors." + field + ".nan"));
	    return false;
	}

	int value;
	if (minVal > 0) {
	    value = Integer.parseInt(val);
	    if (value < minVal) {
		e.put(field, ("errors." + field + ".minval"));
		return false;
	    }
	}

	if (maxVal > 0) {
	    value = Integer.parseInt(val);
	    if (value > maxVal) {
		e.put(field, ("errors." + field + ".maxval"));
		return false;
	    }
	}
	return true;
    }

    public static boolean checkMaxLen(Map<String, String> e,
	    String field, String val, int maxLen) {
	if (val.length() > maxLen) {
	    e.put(field, ("errors." + field + ".maxlen"));
	    return false;
	}

	return true;
    }

    public static boolean checkMinLen(Map<String, String> e,
	    String field, String val, int minLen) {
	if (val.length() < minLen) {
	    e.put(field, ("errors." + field + ".minlen"));
	    return false;
	}

	return true;
    }

    public static boolean checkRegEx(Map<String, String> e,
	    String field, String val, String regEx) {
	Pattern p = Pattern.compile(regEx);
	Matcher m = p.matcher(val);
	if (! m.matches()) {
	    e.put(field, ("errors." + field + ".invalidchars"));
	    return false;
	}

	return true;
    }

    public static boolean checkRequired(Map<String, String> e,
	    String field, String val) {
	if (val == null || val.length() == 0) {
	    e.put(field, ("errors." + field + ".required"));
	    return false;
	}

	return true;
    }

    public static boolean checkPresent(String val) {
	if (val == null || val.length() == 0) {
	    return false;
	}

	return true;
    }

    public static String clean(String val) {
	Pattern p = Pattern.compile("\\s+");
	Matcher m = p.matcher(val);
	return m.replaceAll(" ");
    }

    public static boolean checkDate(Map<String, String> e,
	    String field, String format, String date) {
	Date d = toDate(format, date);

	if (d == null) {
	    e.put(field, ("errors." + field + ".nad"));
	    return false;
	}

	return true;
    }

    public static boolean notAfterToday(Map<String, String> e,
	    String field, String format, String date) {

	if (! checkDate(e, field, format, date))
	    return false;

	Date today = new Date();
	Date val = toDate(format, date);

	if (val.after(today)) {
	    e.put(field, "errors." + field + ".nat");
	    return false;
	}
	return true;
    }

    public static boolean notBeforeToday(Map<String, String> e,
	    String field, String format, String date) {

	if (! checkDate(e, field, format, date))
	    return false;

	Date today = new Date();
	Date val = toDate(format, date);

	if (val.before(today)) {
	    e.put(field, ("errors." + field + ".nbt"));
	    return false;
	}
	return true;
    }

    public static Date toDate(String format, String val) {
	try {
	    SimpleDateFormat fmt = new SimpleDateFormat(format);
	    return fmt.parse(val);
	} catch (ParseException ex) {
	    Logger.getLogger(WebUtil.class.getName())
		    .log(Level.SEVERE, null, ex);
	}
	return null;
    }

    public static String formatDate(String format, Date val) {
	if (val == null) {
	    return "-";
	}
	SimpleDateFormat fmt = new SimpleDateFormat(format);
	return fmt.format(val);
    }

    public static void addMessage(HttpServletRequest request,
	String messageKey) {
	List<String> messages = (List<String>)
	    request.getAttribute("MESSAGES");
	if (messages == null)
	    messages = new ArrayList<String>();
	messages.add(messageKey);
	request.setAttribute("MESSAGES", messages);
    }

    public static void addError(HttpServletRequest request,
	String messageKey) {
	List<String> messages = (List<String>)
	    request.getAttribute("ERRORS");
	if (messages == null)
	    messages = new ArrayList<String>();
	messages.add(messageKey);
	request.setAttribute("MESSAGES", messages);
    }

    public static List<String> getMessages(HttpServletRequest request) {
	List<String> messages = (List<String>)
	    request.getAttribute("MESSAGES");
	if (messages == null)
	    messages = new ArrayList<String>();
	return messages;
    }

    public static List<String> getErrors(HttpServletRequest request) {
	List<String> messages = (List<String>)
	    request.getAttribute("ERRORS");
	if (messages == null)
	    messages = new ArrayList<String>();
	return messages;
    }

    public static String smartPrint(Object value) {
	return smartPrint(dateFormat, value);
    }

    public static String smartPrint(String format, Object value) {
	if (value != null) {
	    if (value instanceof Date)
		return smartPrint(format, (Date) value, "-");
	    else
		return value.toString();
	}
	return "-";
    }

    public static String smartPrint(String format, Object value,
	String defaultVal) {
	if (value != null) {
	    if (value instanceof Date)
		return formatDate(format, (Date) value);
	    else
		return value.toString();
	}
	return defaultVal;
    }

    /*
    public static void setOPDDetails(HttpServletRequest request,
	Integer opdRecordId) {
	OpdRecord record = RecordDAO.getOPDRecord(opdRecordId);

	request.setAttribute("OPD_RECORD", record);
	request.setAttribute("PATIENT", record.getPatient());
	request.setAttribute("DOCTORS", DoctorsDAO.getDoctors());
    }

    public static void setIPDDetails(HttpServletRequest request,
	Integer ipdRecordId) {
	IpdRecord record = RecordDAO.getIPDRecord(ipdRecordId);

	request.setAttribute("IPD_RECORD", record);
	request.setAttribute("PATIENT", record.getPatient());
	request.setAttribute("DOCTORS", DoctorsDAO.getDoctors());
    }

    public static void setPatient(HttpServletRequest request,
	Integer patientId) {
	request.setAttribute("PATIENT", PatientDAO.getPatient(patientId));
    }

     *
     */

    public static Map<String, Map<String, String>> getUpdates(
	    Object oldObj, Object newObj, String[] properties) {
	Map<String, Map<String, String>> changes = new
		HashMap<String, Map<String, String>>();
	Map<String, String> values = null;
	for (String property : properties) {
	    values = null;
	    try {
		Object oldVal = oldObj
			.getClass()
			.getMethod("get" + property)
			.invoke(oldObj);
		Object newVal = newObj
			.getClass()
			.getMethod("get" + property)
			.invoke(newObj);
		if (oldVal != null && ! oldVal.equals(newVal)) {
		    values = new HashMap<String, String>();
		    values.put("old", oldVal.toString());
		    values.put("new",
			    (newVal == null? null : newVal.toString()));
		    changes.put(property, values);
		} else if (oldVal == null && newVal != null) {
		    values = new HashMap<String, String>();
		    values.put("old", null);
		    values.put("new", newVal.toString());
		    changes.put(property, values);
		}
		logger.log(Level.FINER, "property={0}, change={1}", new Object[]{property, values});
	    } catch (NoSuchMethodException ex) {
		Logger.getLogger(WebUtil.class.getName()).log(Level.SEVERE, null, ex);
	    } catch (SecurityException ex) {
		Logger.getLogger(WebUtil.class.getName()).log(Level.SEVERE, null, ex);
	    } catch (IllegalAccessException ex) {
		Logger.getLogger(WebUtil.class.getName()).log(Level.SEVERE, null, ex);
	    } catch (IllegalArgumentException ex) {
		Logger.getLogger(WebUtil.class.getName()).log(Level.SEVERE, null, ex);
	    } catch (InvocationTargetException ex) {
		Logger.getLogger(WebUtil.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	logger.log(Level.FINER, "changes={0}", new Object[]{changes});
	return changes;
    }

    public static String randomString(int min, int max) {
	int num = randomInt(min, max);
	byte b[] = new byte[num];
	for (int i = 0; i < num; i++)
	    b[i] = (byte)randomInt('a', 'z');
	return new String(b);
    }

    public static int randomInt(int min, int max) {
	return (int)(Math.random() * (max - min) + min);
    }

    public static void main(String[] args) {
	System.out.println(toDate(dateTime, "2 November 10:10 am"));
    }
}