/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.contacts.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.util.UploadManager;
import java.io.File;
import java.io.Serializable;
import java.util.Map;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.DownloadLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;

/**
 *
 * @author akshay
 */
public final class UploadContactsPanel extends Panel {
    public UploadContactsPanel(String id) {
        super (id);
	WebMarkupContainer uploadContainer =
	    new WebMarkupContainer("uploadContainer");
	uploadContainer.setOutputMarkupId(true);
	uploadContainer.setOutputMarkupPlaceholderTag(true);
	add(uploadContainer);

	Form<UploadModel> form = new Form<UploadModel>("uploadForm",
	    new CompoundPropertyModel<UploadModel>(new UploadModel()));
	uploadContainer.add(form);

	final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");
	uploadFeedback.setOutputMarkupId(true);
	uploadFeedback.setOutputMarkupPlaceholderTag(true);
	form.add(uploadFeedback);

	CheckBox createUsers = new CheckBox("createUsers");

	CrmUser user = UserDAO.getUser(CrmSession.get().getUserId());
	if ("ADMIN".equals(user.getUserRole())) {
	    createUsers.setEnabled(true);
	} else {
	    createUsers.setEnabled(false);
	}
	form.add(createUsers);

	FileUploadField inputFile = new FileUploadField("inputFile");
	form.add(inputFile);

	final WebMarkupContainer results = new WebMarkupContainer("results");
	results.setOutputMarkupId(true);
	results.setOutputMarkupPlaceholderTag(true);
	form.add(results);
	results.add(
	    new Label("read", "0"),
	    new Label("processed", "0"),
	    new Label("written", "0"),
	    new Label("rejected", "0")
	);

	final WebMarkupContainer links = new WebMarkupContainer("links");
	links.setOutputMarkupId(true);
	links.setOutputMarkupPlaceholderTag(true);
	results.add(links);
	links.setVisible(false);

	links.add(
	    new Link("errorLogsLink") {
		@Override
		public void onClick() {
		}
	    },
	    new Link("rejectedFileLink") {
		@Override
		public void onClick() {
		}
	    },
	    new Link("reasonsFileLink") {
		@Override
		public void onClick() {
		}
	    }
	);

	form.add(new AjaxFallbackButton("uploadButton", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (target != null) {
		    target.addComponent(uploadFeedback);
		}

		UploadModel model = (UploadModel) form.getModelObject();
		try {
		    UploadManager um = new UploadManager(
			UserDAO.getUser(CrmSession.get().getUserId()),
			model.isCreateUsers());
		    File rejected = File.createTempFile("upload.", ".csv");
		    File errorLog = File.createTempFile("error.", ".log");
		    File reasons = File.createTempFile("upload.errors.", ".txt");

		    Map<String, Integer> stats = um.upload(
			model.getInputFile().writeToTempFile(),
			reasons, rejected, errorLog);
		    if (stats != null) {
			results.addOrReplace(
			    new Label("read",
				Integer.toString(stats.get("read"))),
			    new Label("processed",
				Integer.toString(stats.get("processed"))),
			    new Label("written",
				Integer.toString(stats.get("written"))),
			    new Label("rejected",
				Integer.toString(stats.get("rejected")))
			);
			links.addOrReplace(
			    new DownloadLink("rejectedFileLink", rejected),
			    new DownloadLink("reasonsFileLink", reasons),
			    new DownloadLink("errorLogsLink", errorLog)
			);
			links.setVisible(true);
			info("Records uploaded successfully, please ensure that"
			    + " you download all the three files before closing"
			    + " this popup.");
			if (target != null) {
			    target.addComponent(results);
			}
		    } else {
			error("Upload failed due to an internal error.");
		    }
		} catch (Exception e) {
		    e.printStackTrace(System.out);
		    error("Upload failed due to an internal error.");
		}
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(uploadFeedback);
		}
	    }
	}
	);
    }

    public static final class UploadModel implements Serializable {
	private boolean createUsers = false;
	private FileUpload inputFile;

	public boolean isCreateUsers() {
	    return createUsers;
	}

	public void setCreateUsers(boolean createUsers) {
	    this.createUsers = createUsers;
	}

	public FileUpload getInputFile() {
	    return inputFile;
	}

	public void setInputFile(FileUpload inputFile) {
	    this.inputFile = inputFile;
	}
    }
}
