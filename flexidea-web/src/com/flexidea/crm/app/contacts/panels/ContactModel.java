package com.flexidea.crm.app.contacts.panels;

import com.flexidea.crm.db.dto.City;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import java.io.Serializable;
import java.util.Date;

public final class ContactModel implements Serializable {

    private Long contactId;
    private Long companyId;
    private String firstName;
    private String lastName;
    private String jobTitle;
    private City contactCity;
    private String contactPhone;
    private String contactCell;
    private String contactEmail;
    private String gift;
    private String creditTerms;
    private Date dob;
    private Date doa;
    private String generalNote;
    private String contactAddress;
    private ContactType contactType;
    private String industry;
    private String companyAddress;
    private City city;
    private String phone;
    private String phone1;
    private String fax;
    private String email;
    private String website;
    private ContactType companyContactType;
    private String companyName;
    private Contact salesRep;

    public City getCity() {
	return city;
    }

    public ContactModel setCity(City city) {
	this.city = city;
	return this;
    }

    public String getCompanyAddress() {
	return companyAddress;
    }

    public ContactModel setCompanyAddress(String companyAddress) {
	this.companyAddress = companyAddress;
	return this;
    }

    public ContactType getCompanyContactType() {
	return companyContactType;
    }

    public ContactModel setCompanyContactType(ContactType companyContactType) {
	this.companyContactType = companyContactType;
	return this;
    }

    public Long getCompanyId() {
	return companyId;
    }

    public ContactModel setCompanyId(Long companyId) {
	this.companyId = companyId;
	return this;
    }

    public String getCompanyName() {
	return companyName;
    }

    public ContactModel setCompanyName(String companyName) {
	this.companyName = companyName;
	return this;
    }

    public String getContactAddress() {
	return contactAddress;
    }

    public ContactModel setContactAddress(String contactAddress) {
	this.contactAddress = contactAddress;
	return this;
    }

    public String getContactCell() {
	return contactCell;
    }

    public ContactModel setContactCell(String contactCell) {
	this.contactCell = contactCell;
	return this;
    }

    public City getContactCity() {
	return contactCity;
    }

    public ContactModel setContactCity(City contactCity) {
	this.contactCity = contactCity;
	return this;
    }

    public String getContactEmail() {
	return contactEmail;
    }

    public ContactModel setContactEmail(String contactEmail) {
	this.contactEmail = contactEmail;
	return this;
    }

    public Long getContactId() {
	return contactId;
    }

    public ContactModel setContactId(Long contactId) {
	this.contactId = contactId;
	return this;
    }

    public String getContactPhone() {
	return contactPhone;
    }

    public ContactModel setContactPhone(String contactPhone) {
	this.contactPhone = contactPhone;
	return this;
    }

    public ContactType getContactType() {
	return contactType;
    }

    public ContactModel setContactType(ContactType contactType) {
	this.contactType = contactType;
	return this;
    }

    public String getCreditTerms() {
	return creditTerms;
    }

    public ContactModel setCreditTerms(String creditTerms) {
	this.creditTerms = creditTerms;
	return this;
    }

    public Date getDoa() {
	return doa;
    }

    public ContactModel setDoa(Date doa) {
	this.doa = doa;
	return this;
    }

    public Date getDob() {
	return dob;
    }

    public ContactModel setDob(Date dob) {
	this.dob = dob;
	return this;
    }

    public String getEmail() {
	return email;
    }

    public ContactModel setEmail(String email) {
	this.email = email;
	return this;
    }

    public String getFax() {
	return fax;
    }

    public ContactModel setFax(String fax) {
	this.fax = fax;
	return this;
    }

    public String getFirstName() {
	return firstName;
    }

    public ContactModel setFirstName(String firstName) {
	this.firstName = firstName;
	return this;
    }

    public String getGeneralNote() {
	return generalNote;
    }

    public ContactModel setGeneralNote(String generalNote) {
	this.generalNote = generalNote;
	return this;
    }

    public String getGift() {
	return gift;
    }

    public ContactModel setGift(String gift) {
	this.gift = gift;
	return this;
    }

    public String getJobTitle() {
	return jobTitle;
    }

    public ContactModel setJobTitle(String jobTitle) {
	this.jobTitle = jobTitle;
	return this;
    }

    public String getLastName() {
	return lastName;
    }

    public ContactModel setLastName(String lastName) {
	this.lastName = lastName;
	return this;
    }

    public String getPhone() {
	return phone;
    }

    public ContactModel setPhone(String phone) {
	this.phone = phone;
	return this;
    }

    public String getPhone1() {
	return phone1;
    }

    public ContactModel setPhone1(String phone1) {
	this.phone1 = phone1;
	return this;
    }

    public String getWebsite() {
	return website;
    }

    public ContactModel setWebsite(String website) {
	this.website = website;
	return this;
    }

    public String getIndustry() {
	return industry;
    }

    public ContactModel setIndustry(String industry) {
	this.industry = industry;
	return this;
    }

    public Contact getSalesRep() {
	return salesRep;
    }

    public ContactModel setSalesRep(Contact salesRep) {
	this.salesRep = salesRep;
	return this;
    }
}
