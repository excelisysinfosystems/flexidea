package com.flexidea.crm.app.contacts.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.IndicatingAutoCompleteTextField;
import com.flexidea.crm.app.common.panel.CommentsPanel;
import com.flexidea.crm.app.common.panel.ContactsAssociationPanel;
import com.flexidea.crm.app.common.panel.MarkAsDeadPanel;
import com.flexidea.crm.app.pages.ContactsPage;
import com.flexidea.crm.db.dao.CompaniesDAO;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.ListsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.City;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.util.WebUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.parse.metapattern.MetaPattern;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * TODO: contactCity drop-down
 * @author akshay
 */
public final class ContactFormPanel extends Panel {
    private Long contactId;
    private Label contactName;
    private String contactNameValue;
    private Fragment commentsFragment;
    private ContactsAssociationPanel associationPanel;

    public ContactFormPanel(String id, boolean edit, Long contactId,
	ContactsAssociationPanel associationPanel) {
        super (id);
	this.contactId = contactId;
	this.associationPanel = associationPanel;

	ContactModel model = new ContactModel();

	if (contactId != null) {
	    Contact contact = ContactsDAO.getContact(contactId);
	    Company company = contact.getCompany();

	    model.setCity(company.getCity())
		.setCompanyAddress(company.getCompanyAddress())
		.setCompanyContactType(company.getContactType())
		.setCompanyId(company.getId())
		.setCompanyName(company.getCompanyName())
		.setContactAddress(contact.getAddress())
		.setContactCell(contact.getCell())
		.setContactCity(contact.getCity())
		.setContactEmail(contact.getEmail())
		.setContactId(contact.getId())
		.setContactPhone(contact.getPhone())
		.setContactType(contact.getContactType())
		.setCreditTerms(contact.getCreditTerms())
		.setDoa(contact.getDoa())
		.setDob(contact.getDob())
		.setEmail(company.getEmail())
		.setFax(company.getFax())
		.setFirstName(contact.getFirstName())
		.setGeneralNote(contact.getGeneralNote())
		.setGift(contact.getGift())
		.setJobTitle(contact.getJobTitle())
		.setLastName(contact.getLastName())
		.setPhone(company.getPhone())
		.setPhone1(company.getPhone1())
		.setWebsite(company.getWebsite())
		.setIndustry(company.getIndustry());

	    setContactNameValue(contact.getFirstName() + " "
		+ contact.getLastName());
	    contactName = new Label("contactLabel", 
		new PropertyModel<String>(this, "contactNameValue"));
	    commentsFragment = new Fragment("commentsContainer", 
		"commentsFragment");
	    commentsFragment.add(
		new CommentsPanel("commentsPanel", CommentsTarget.CONTACTS, 
		company.getId()));
	} else {
	    setContactNameValue("New Contact");
	    contactName = new Label("contactLabel", 
		new PropertyModel<String>(this, "contactNameValue"));
	    commentsFragment = new Fragment("commentsContainer", "gyaanFragment");
	}

	commentsFragment.setOutputMarkupId(true);
	commentsFragment.setOutputMarkupPlaceholderTag(true);

	addOrReplace(commentsFragment);
	addOrReplace(new ContactForm("contactForm",
	    new CompoundPropertyModel<ContactModel>(model),
	    edit));
	contactName.setOutputMarkupId(true);
	addOrReplace(contactName);
	setOutputMarkupId(true);
    }

    public void hideComments(AjaxRequestTarget target) {
	System.out.println("DEBUG: hiding comments contact=" + contactId);
	Fragment fragment = new Fragment("commentsContainer",
	    "gyaanFragment");
	fragment.setOutputMarkupId(true);
	fragment.setOutputMarkupPlaceholderTag(true);

	commentsFragment.replaceWith(fragment);
	commentsFragment = fragment;

	addOrReplace(commentsFragment);

	if (target != null) {
	    target.addComponent(commentsFragment);
	    //target.addComponent(this);
	}
    }

    public void showComments(AjaxRequestTarget target, Long companyId) {
	System.out.println("DEBUG: showing comments contact=" + contactId);
	Fragment fragment = new Fragment("commentsContainer",
	    "commentsFragment");
	fragment.add(
	    new CommentsPanel("commentsPanel", CommentsTarget.CONTACTS,
	    companyId));
	fragment.setOutputMarkupId(true);
	fragment.setOutputMarkupPlaceholderTag(true);

	commentsFragment.replaceWith(fragment);
	commentsFragment = fragment;

	if (target == null)
	    addOrReplace(commentsFragment);

	if (target != null) {
	    target.addComponent(commentsFragment);
	    //target.addComponent(this);
	}
    }

    public final class ContactForm extends Form<ContactModel>
	implements Serializable {
	FeedbackPanel feedback;
	FeedbackPanel feedback1;
	String companyNameValue;
	private MarkAsDeadPanel commentDialog;

	public ContactForm(String id, IModel<ContactModel> imodel, boolean edit) {
	    super(id, imodel);
	    setOutputMarkupId(true);
	    feedback = new FeedbackPanel("feedback");
	    feedback.setOutputMarkupId(true);
	    add(feedback);
	    feedback1 = new FeedbackPanel("feedback1");
	    feedback1.setOutputMarkupId(true);
	    add(feedback1);
	    setCompanyNameValue(imodel.getObject().getCompanyName());
	    createForm(edit, imodel);
	    commentDialog = new MarkAsDeadPanel("commentDialog", "") {

		@Override
		public void onDone(AjaxRequestTarget target, String comment) {
		    ContactModel m = (ContactModel)
			ContactForm.this.getModelObject();
		    if (m.getContactId() == null) {
			return;
		    }
		    Contact contact = ContactsDAO.getContact(m.getContactId());
		    if (contact != null) {
			contact.setModifiedAt(new Date());
			contact.setModifiedBy(UserDAO
			    .getUser(CrmSession.get().getUserId()));
			contact.setIsDeleted(true);
			contact.setComment(comment);
			ContactsDAO.updateContact(null,contact,
			    contact.getCompany(), false, null, null);
			info("Contact deleted!");
		    }

		    ((ContactsPage)getPage()).updateSearchPanel(target);

		    createForm(false, ContactForm.this.getModel());
		    showComments(target,
			ContactForm.this.getModelObject().getCompanyId());
		    if (target != null) {
			target.addComponent(feedback);
			target.addComponent(feedback1);
			target.addComponent(ContactForm.this);
		    }
		}
	    };

	    add(commentDialog);
	}

	private void createForm(boolean edit, final IModel<ContactModel> imodel) {
	    final TextField firstName = new TextField("firstName");
	    final TextField lastName = new TextField("lastName");
	    final TextField jobTitle = new TextField("jobTitle");
	    //TextField contactCity = new TextField("contactCity");
	    IChoiceRenderer<City> cityCR =
		new ChoiceRenderer<City>("cityName", "id");
	    List<City> cities = ListsDAO.getCities();

	    final DropDownChoice contactCity =
		new DropDownChoice("contactCity", cities, cityCR);
	    final TextField contactPhone = new TextField("contactPhone");
	    final TextField cell = new TextField("contactCell");
	    final TextField contactEmail = new TextField("contactEmail");

	    final TextField gift = new TextField("gift");
	    final TextField creditTerms = new TextField("creditTerms");

	    final DateTextField dob = new DateTextField("dob", WebUtil.dateFormat);
	    final DateTextField doa = new DateTextField("doa", WebUtil.dateFormat);
	    /*
	     * BUG ID 4 [START] 
	     */
	    dob.setRequired(false);
	    //dob.setRequired(true);
	    /*
	     * BUG ID 4 [END] 
	     */	    
	    dob.setMarkupId("dob");
	    dob.setOutputMarkupId(true);
	    doa.setMarkupId("doa");
	    doa.setOutputMarkupId(true);

	    final TextArea generalNote = new TextArea("generalNote");
	    generalNote.add(StringValidator.maximumLength(512));

	    final TextArea contactAddress = new TextArea("contactAddress");
	    contactAddress.add(StringValidator.maximumLength(512));

	    addOrReplace(firstName, lastName, jobTitle, contactCity, contactPhone, cell,
		contactEmail, dob, doa, gift, creditTerms, generalNote,
		contactAddress);

	    final TextArea address = new TextArea("companyAddress");
	    //TextField city = new TextField("city");


	    final DropDownChoice city = new DropDownChoice("city", cities, cityCR);

	    final TextField phone = new TextField("phone");
	    final TextField phone1 = new TextField("phone1");
	    final TextField fax = new TextField("fax");
	    final TextField email = new TextField("email");
	    final TextField website = new TextField("website");

	    address.setOutputMarkupId(true);
	    city.setOutputMarkupId(true);
	    phone.setOutputMarkupId(true);
	    phone1.setOutputMarkupId(true);
	    fax.setOutputMarkupId(true);
	    email.setOutputMarkupId(true);
	    website.setOutputMarkupId(true);

	    ChoiceRenderer<ContactType> cr = new ChoiceRenderer<ContactType>(
		"typeName", "id");

	    final DropDownChoice<ContactType> contactType = new
		DropDownChoice<ContactType>("contactType",
		ContactsDAO.listContactTypes(false), cr);

	    contactType.setOutputMarkupPlaceholderTag(true);

	    final WebMarkupContainer salesRepContainer =
		new WebMarkupContainer("salesRepContainer");
	    salesRepContainer
		.setOutputMarkupId(true)
		.setOutputMarkupPlaceholderTag(true)
		.setVisible(false);
	    final DropDownChoice<Contact> salesRep =
		new DropDownChoice<Contact>("salesRep",
		ContactsDAO.listByContactType(
		    ContactsDAO.getContactType("Sales Representative")),
		new ChoiceRenderer<Contact>("fullName", "id"));
	    salesRepContainer.add(salesRep);

	    final DropDownChoice<ContactType> companyContactType = new
		DropDownChoice<ContactType>("companyContactType",
		ContactsDAO.listContactTypes(true), cr);

	    companyContactType.add(new
		AjaxFormComponentUpdatingBehavior("onchange") {

		@Override
		protected void onUpdate(AjaxRequestTarget target) {
			/*
			 * BUG ID 60 [START]
			 */
			if(ContactForm.this.getModelObject()
					.getCompanyContactType()!=null){
			    contactType.setChoices(ContactsDAO.getContactTypes(
				ContactForm.this.getModelObject()
				.getCompanyContactType().isInternal()));
			}
			else{
				contactType.setChoices(ContactsDAO.listContactTypes(false));				
			}
			/*
			 * BUG ID 60 [END]
			 */
		    ContactModel mdl = ContactForm.this.getModelObject();

		    if (mdl.getCompanyContactType()!=null && !mdl.getCompanyContactType().isInternal()) {
			mdl.setContactType(mdl.getCompanyContactType());
			ContactForm.this.setModelObject(mdl);
			contactType.setVisible(false);
			if (mdl.getCompanyId() == null) {
			    salesRepContainer.setVisible(true);
			    salesRep.setRequired(true);
			}
		    } else {
			contactType.setVisible(true);
			salesRepContainer.setVisible(false);
			salesRep.setRequired(false);
		    }

		    if (target != null) {
			target.addComponent(contactType);
			target.addComponent(salesRepContainer);
		    }
		}
	    });

	    contactType.setOutputMarkupId(true);

	    final TextField industry = new TextField("industry");
	    industry.setRequired(true);
	    industry.add(StringValidator.maximumLength(255));

	    final IndicatingAutoCompleteTextField companyName =
		new IndicatingAutoCompleteTextField("companyName",
		new Model(getCompanyNameValue())) {
		//new PropertyModel(this, "companyNameValue")) {

		@Override
		protected Iterator getChoices(String input) {
		    return CompaniesDAO.search(input).iterator();
		}

		@Override
		public void updateModel() {
		    super.updateModel();
		    System.out.println("DEBUG: updateModel model="
			+ getModelObject());
		}
	    };

	    companyName.add(new AjaxFormComponentUpdatingBehavior("onchange") {

		@Override
		protected void onUpdate(AjaxRequestTarget target) {
		    System.out.println("DEBUG: onChangeupdateModel model=" + getModelObject());
		    ContactModel m = getModelObject();
		    if (m != null) {
			m.setCompanyName((String) companyName.getModel().getObject());
			getModel().setObject(m);
			System.out.println("DEBUG: onChangeupdateModel modelValue="
			    + getModelObject().getCompanyName());
		    }
		    if (target != null) {
			target.addComponent(ContactForm.this);
		    }
		}
	    });

	    companyName.add(new AjaxEventBehavior("onblur") {

		@Override
		protected void onEvent(AjaxRequestTarget target) {
		    ContactModel c = getModelObject();
		    c.setCompanyName((String)companyName.getModel().getObject());
		    Company s = CompaniesDAO.getCompany(c.getCompanyName());

		    System.out.println("DEBUG: before c=" + c.getCompanyName() + ", s=" + s);

		    if (s != null && ! s.getCompanyName().equals(c.getCompanyName())) {
			s = null;
		    }

		    System.out.println("DEBUG: after c=" + c.getCompanyName() + ", s=" + s);
		    if (s != null) {
			c.setCity(s.getCity());
			c.setCompanyAddress(s.getCompanyAddress());
			c.setCompanyContactType(s.getContactType());
			c.setCompanyId(s.getId());
			c.setCompanyName(s.getCompanyName());
			c.setEmail(s.getEmail());
			c.setFax(s.getFax());
			c.setWebsite(s.getWebsite());
			c.setPhone(s.getPhone());
			c.setPhone1(s.getPhone1());
			c.setIndustry(s.getIndustry());
			ContactForm.this.setCompanyNameValue(s.getCompanyName());
			companyName.getModel().setObject(companyNameValue);
			associationPanel.update(target, CommentsTarget.CONTACTS,
			    s.getId());
			if (! c.getCompanyContactType().isInternal()) {
			    c.setContactType(c.getCompanyContactType());
			    contactType.setVisible(false);
			} else {
			    contactType.setVisible(true);
			}
			showComments(target, s.getId());
			setModelObject(c);
		    } else {
			c.setCompanyId(null);
			//c.setCompanyName(null);
			c.setIndustry(null);
			c.setCompanyAddress(null);
			c.setCompanyContactType(null);
			c.setPhone(null);
			c.setFax(null);
			c.setEmail(null);
			c.setWebsite(null);
			c.setPhone1(null);
			c.setCity(null);
			setModelObject(c);
			//ContactForm.this.setCompanyNameValue("");
			//companyName.getModel().setObject("");
			associationPanel.update(target, CommentsTarget.CONTACTS,
			    null);
			hideComments(target);
		    }
		    if (target != null) {
			target.addComponent(ContactForm.this);
		    }
		}
	    });


	    address.setRequired(true);
	    address.setOutputMarkupId(true);
	    address.add(StringValidator.maximumLength(512));

	    city.setRequired(true);
	    city.setOutputMarkupId(true);
	    //city.add(StringValidator.maximumLength(50));

	    phone.add(StringValidator.maximumLength(20));
	    phone.add(new PatternValidator("[0-9,/\\s]+"));
	    phone.setRequired(true);

	    phone1.add(StringValidator.maximumLength(20));
	    phone1.add(new PatternValidator("[0-9,/\\s]+"));

	    fax.add(StringValidator.maximumLength(20));
	    fax.add(new PatternValidator("[0-9,/\\s]+"));
	    //fax.add(new PatternValidator(MetaPattern.DIGITS));

	    email.setRequired(true);
	    email.add(StringValidator.maximumLength(300));
	    //email.add(EmailAddressValidator.getInstance());

	    website.add(StringValidator.maximumLength(255));
	    //website.add((new UrlValidator(UrlValidator.ALLOW_ALL_SCHEMES)));

	    addOrReplace(industry, contactType, website, email, fax, phone1, phone, city,
		address, companyName, companyContactType, salesRepContainer);

	    firstName.setRequired(true);
	    firstName.add(StringValidator.maximumLength(100));

	    lastName.setRequired(true);
	    lastName.add(StringValidator.maximumLength(100));

	    //jobTitle.setRequired(true);
	    jobTitle.add(StringValidator.maximumLength(100));

	    contactCity.setRequired(true);
	    contactCity.setOutputMarkupId(true);
	    //contactCity.add(StringValidator.maximumLength(50));

	    contactPhone.add(StringValidator.maximumLength(20));
	    //contactPhone.add(new PatternValidator(MetaPattern.DIGITS));
	    contactPhone.add(new PatternValidator("[0-9,/\\s]+"));
	    //contactPhone.setRequired(true);

	    cell.setRequired(true);
	    cell.add(StringValidator.lengthBetween(10, 12));
	    cell.add(new PatternValidator(MetaPattern.DIGITS));

	    contactEmail.setRequired(true);
	    contactEmail.add(EmailAddressValidator.getInstance());

	    final IndicatingAjaxButton deleteButton = new IndicatingAjaxButton(
		"deleteButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    ContactModel m = ((ContactModel)form.getModelObject());
		    commentDialog.open(target, "Mark Contact '"
			+ m.getFirstName()
			+ " " + m.getLastName() + "' as Dead?"
		    );
		}
	    };

	    final IndicatingAjaxButton saveButton =
		new IndicatingAjaxButton("saveButton", this) {
		private boolean isValid = false;
		private boolean opError = false;

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    isValid = true;
		    boolean showComments = false;
		    Company company = new Company();
		    Contact contact = new Contact();
		    ContactModel m = (ContactModel) form.getModelObject();

		    CrmSession session = CrmSession.get();
		    CrmUser user = UserDAO.getUser(session.getUserId());

		    company.setCity(m.getCity())
			.setCompanyAddress(m.getCompanyAddress())
			.setCompanyName(m.getCompanyName())
			.setContactType(m.getCompanyContactType())
			.setEmail(m.getEmail())
			.setFax(m.getFax())
			.setId(m.getCompanyId())
			.setModifiedAt(new Date())
			.setModifiedBy(user)
			.setPhone(m.getPhone())
			.setPhone1(m.getPhone1())
			.setIndustry(m.getIndustry())
			.setWebsite(m.getWebsite());

		    contact.setAddress(m.getContactAddress())
			.setCell(m.getContactCell())
			.setCity(m.getContactCity())
			.setContactType(m.getContactType())
			.setCreditTerms(m.getCreditTerms())
			.setDoa(m.getDoa())
			.setDob(m.getDob())
			.setEmail(m.getContactEmail())
			.setFirstName(m.getFirstName())
			.setLastName(m.getLastName())
			.setGeneralNote(m.getGeneralNote())
			.setId(m.getContactId())
			.setJobTitle(m.getJobTitle())
			.setPhone(m.getContactPhone())
			.setModifiedAt(new Date())
			.setModifiedBy(user)
			.setGift(m.getGift());

		    boolean associateSalesRep = false;
		    if (contact.getId() == null) {
			contact.setCreatedAt(new Date());
			contact.setCreatedBy(user);
		    }

		    if (company.getId() == null) {
			associateSalesRep = true;
			company.setCreatedAt(new Date());
			company.setCreatedBy(user);
		    }

		    if (company.getId() != null && contact.getId() == null) {
			if (ContactsDAO.checkExists(contact.getFirstName(),
				contact.getLastName(), company)) {
			    opError = true;
			    error("Contact already exists.");
			    return;
			}
		    }

		    ContactsDAO.updateContact(null,contact, company,
			false, null, null);


		    ContactFormPanel.this.setContactId(contact.getId());
		    info("Contact Updated!");
		    ContactFormPanel.this.setContactNameValue(
			contact.getFirstName() + " " + contact.getLastName());
		    m.setCompanyId(contact.getCompany().getId());
		    m.setContactId(contact.getId());
		    ContactForm.this.getModel().setObject(m);

		    if (associateSalesRep) {
			ContactsDAO.associateContact(
			    CommentsTarget.CONTACTS, m.getCompanyId(),
			    m.getSalesRep().getId(), user);
		    }

		    System.out.println("DEBUG: contactId=" + contact.getId());

		    if (m.getContactId() != null) {
			System.out.println("DEBUG: contactId="
			    + m.getContactId());
			showComments = true;
			ContactFormPanel.this.setContactId(m.getContactId());
		    }

		    associationPanel.update(target, CommentsTarget.CONTACTS,
			m.getCompanyId());

		    ((ContactsPage) getPage()).resetMenu(target);
		    ((ContactsPage)getPage()).updateSearchPanel(target);

		    /*
		    if (target != null) {
			target.addComponent(ContactForm.this.feedback);
			target.addComponent(ContactForm.this.feedback1);
			target.addComponent(contactName);
			target.addComponent(ContactForm.this.setEnabled(false));
			target.addComponent(associationPanel);
			target.addComponent(deleteButton.setEnabled(true));
			target.addComponent(firstName.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(lastName.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(jobTitle.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(contactCity.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(contactPhone.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(cell.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(contactEmail.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(dob.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(doa.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(gift.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(creditTerms.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(generalNote.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(contactAddress.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(contactType.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(website.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(email.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(fax.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(phone1.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(phone.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(city.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(address.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(companyName.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(companyContactType.setEnabled(false).setOutputMarkupId(true));
			target.addComponent(industry.setEnabled(false).setOutputMarkupId(true));
		    }

		    if (showComments)
			ContactFormPanel.this.showComments(target, m.getCompanyId());
		     *
		     */
		    createForm(false, imodel);
		    if (target != null) {
			//target.addComponent(feedback);
			//target.addComponent(feedback1);
			target.addComponent(ContactForm.this);
		    }
		}

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);
		    if (target != null) {
			target.addComponent(ContactForm.this.feedback);
			target.addComponent(ContactForm.this.feedback1);
		    }

		    if (isValid && ! opError) {
			error("Well, this is embarrasing. Contact update failed"
			    + " due to an internal error. Kindly report this"
			    + " problem to the support staff.");
		    }
		}

	    };

	    addOrReplace(saveButton, deleteButton);

	    if (imodel.getObject().getCompanyContactType() != null
		&& ! imodel.getObject()
		.getCompanyContactType().isInternal()) {
		contactType.setVisible(false);
	    }

	    if (! edit && imodel.getObject().getContactId() != null) {
		//setEnabled(false);
		firstName.setEnabled(false).setOutputMarkupId(true);
		lastName.setEnabled(false).setOutputMarkupId(true);
		jobTitle.setEnabled(false).setOutputMarkupId(true);
		contactCity.setEnabled(false).setOutputMarkupId(true);
		contactPhone.setEnabled(false).setOutputMarkupId(true);
		cell.setEnabled(false).setOutputMarkupId(true);
		contactEmail.setEnabled(false).setOutputMarkupId(true);
		dob.setEnabled(false).setOutputMarkupId(true);
		doa.setEnabled(false).setOutputMarkupId(true);
		gift.setEnabled(false).setOutputMarkupId(true);
		creditTerms.setEnabled(false).setOutputMarkupId(true);
		generalNote.setEnabled(false).setOutputMarkupId(true);
		contactAddress.setEnabled(false).setOutputMarkupId(true);
		contactType.setEnabled(false).setOutputMarkupId(true);
		website.setEnabled(false).setOutputMarkupId(true);
		email.setEnabled(false).setOutputMarkupId(true);
		fax.setEnabled(false).setOutputMarkupId(true);
		phone1.setEnabled(false).setOutputMarkupId(true);
		phone.setEnabled(false).setOutputMarkupId(true);
		city.setEnabled(false).setOutputMarkupId(true);
		address.setEnabled(false).setOutputMarkupId(true);
		companyName.setEnabled(false).setOutputMarkupId(true);
		companyContactType.setEnabled(false).setOutputMarkupId(true);
		saveButton.setEnabled(false).setOutputMarkupId(true);
		industry.setEnabled(false).setOutputMarkupId(true);
	    }
	    //deleteButton.setEnabled(true);
	    saveButton
		.setOutputMarkupId(true)
		.setOutputMarkupPlaceholderTag(true);
	    deleteButton
		.setOutputMarkupId(true)
		.setOutputMarkupPlaceholderTag(true);

	    if (imodel.getObject().getContactId() != null) {
		if (ContactsDAO.getContact(
		    imodel.getObject().getContactId()).isIsDeleted()) {
		    saveButton.setVisible(false);
		    deleteButton.setVisible(false);
		    setEnabled(false);
		} else {
		    saveButton.setVisible(true);
		    deleteButton.setVisible(true);
		}
	    } else {
		deleteButton.setVisible(false);
	    }
	}

	public String getCompanyNameValue() {
	    return companyNameValue;
	}

	public void setCompanyNameValue(String companyNameValue) {
	    this.companyNameValue = companyNameValue;
	}

    }

    public Label getContactName() {
	return contactName;
    }

    public void setContactName(Label contactName) {
	this.contactName = contactName;
    }

    public Long getContactId() {
	return contactId;
    }

    public void setContactId(Long contactId) {
	this.contactId = contactId;
    }

    public String getContactNameValue() {
	return contactNameValue;
    }

    public void setContactNameValue(String contactNameValue) {
	this.contactNameValue = contactNameValue;
    }

}