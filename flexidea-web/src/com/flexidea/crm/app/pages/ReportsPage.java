package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.reports.panels.ReportsPanel;
import com.flexidea.crm.db.dao.ReportsDAO;
import com.flexidea.crm.db.dto.ReportModel;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.RepeatingView;

/**
 *
 * @author akshay
 */
public final class ReportsPage extends WebPage {
    private WebMarkupContainer listContainer;
    private ReportsPanel reportsPanel;

    public ReportsPage() {
        super ();
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}
	prepareBase();
    }

    public ReportsPage(PageParameters params) {
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}
	prepareBase();
    }

    private void prepareBase() {
	listContainer = new WebMarkupContainer("listContainer");
	listContainer.setOutputMarkupId(true);
	add(listContainer);
	prepareList(null);
	reportsPanel = new ReportsPanel("reportsPanel") {

	    @Override
	    public void onSave(AjaxRequestTarget target) {
		prepareList(target);
	    }
	};
	add(reportsPanel);
	add(new AjaxFallbackLink("contactReport") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		prepareForm(target, true);
	    }
	});

	add(new AjaxFallbackLink("projectReport") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		prepareForm(target, false);
	    }
	});
    }

    private void prepareList(AjaxRequestTarget target) {
	RepeatingView reports = new RepeatingView("reports");
	listContainer.addOrReplace(reports);

	for (ReportModel m : ReportsDAO.getReports()) {
	    final Long reportId = m.getId();
	    WebMarkupContainer container =
		new WebMarkupContainer(reports.newChildId());
	    reports.add(container);

	    AjaxFallbackLink reportLink =
		new AjaxFallbackLink("reportLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    prepareForm(target, reportId);
		}
	    };

	    container.add(reportLink);
	    reportLink.add(new Label("reportName", m.getReportName()));
	}

	if (target != null) {
	    target.addComponent(listContainer);
	}
    }

    private void prepareForm(AjaxRequestTarget target, Long reportId) {
	reportsPanel.updateComponent(target,
	    ReportsDAO.getReportModel(reportId));
    }

    private void prepareForm(AjaxRequestTarget target,
	boolean contactReport) {
	ReportModel model = new ReportModel();

	if (contactReport) {
	    model.setProjectSummaryReport(false);
	} else {
	    model.setProjectSummaryReport(true);
	}

	reportsPanel.updateComponent(target, model);
    }
}

