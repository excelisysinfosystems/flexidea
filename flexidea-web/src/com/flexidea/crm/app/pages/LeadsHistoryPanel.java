package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.common.components.AjaxDataTable;
import com.flexidea.crm.app.common.components.AjaxPagingNavigatorWithImages;
import com.flexidea.crm.app.common.panel.data.LeadsDataProvider;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.util.WebUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.PageParameters;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public final class LeadsHistoryPanel extends Panel {
    public LeadsHistoryPanel(String id, boolean showData) {
        super (id);
	List<IColumn<Lead>> columns = new ArrayList<IColumn<Lead>>();
	columns.add(new AbstractColumn<Lead>(new Model<String>(" "), "status") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Lead>>
		cellItem, String componentId, IModel<Lead> rowModel) {
		Fragment frag = new Fragment(componentId,
		    "statusColor", LeadsHistoryPanel.this);

		Lead p = rowModel.getObject();
		Label statusColor = new Label("statusColor", " ");
		String className = "status_dead";

		if (p.isIsDead()) {
		    className = "status_dead";
		} else if (p.isIsConverted()) {
		    className = "status_recvd";
		}

		statusColor.add(new SimpleAttributeModifier("class",
		    className));
		frag.add(statusColor);
		cellItem.add(frag);
	    }
	});

	columns.add(new AbstractColumn<Lead>(new Model<String>("ID"), "ID") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Lead>>
		cellItem, String componentId, IModel<Lead> rowModel) {
		Fragment frag = new Fragment(componentId,
		    "leadId", LeadsHistoryPanel.this);
		cellItem.add(frag);

		Lead p = rowModel.getObject();
		Long pid = p.getId();

		PageParameters pp = new PageParameters();
		pp.put("lead", pid);

		BookmarkablePageLink leadIDLink =
		    new BookmarkablePageLink("leadIDLink",
		    LeadsPage.class, pp);

		leadIDLink.add(new Label("leadID", Long.toString(pid)));
		frag.add(leadIDLink);
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new AbstractColumn<Lead>(new Model<String>("Lead"), "lead_name") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Lead>>
		cellItem, String componentId, IModel<Lead> rowModel) {
		Fragment frag = new Fragment(componentId,
		    "leadName", LeadsHistoryPanel.this);
		cellItem.add(frag);

		Lead p = rowModel.getObject();
		Long pid = p.getId();

		PageParameters pp = new PageParameters();
		pp.put("lead", pid);

		BookmarkablePageLink leadNameLink =
		    new BookmarkablePageLink("leadNameLink",
		    LeadsPage.class, pp);

		leadNameLink.add(new Label("leadName", p.getLeadName()));
		frag.add(leadNameLink);
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new AbstractColumn<Lead>(new Model<String>("Status"), "status") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Lead>>
		cellItem, String componentId, IModel<Lead> rowModel) {

		Lead p = rowModel.getObject();
		String statusString = "";

		if (p.isIsDead()) {
		    statusString = "Dead";
		} else if (p.isIsConverted()) {
		    statusString = "Converted";
		}

		cellItem.add(new Label(componentId, statusString));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<Lead>(
	    new Model<String>("Created At"), "created_at") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Lead>>
		cellItem, String componentId, IModel<Lead> rowModel) {

		Lead p = rowModel.getObject();

		cellItem.add(new Label(componentId, WebUtil.formatDate(
		    WebUtil.dateTimeFormat, p.getCreatedAt())));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<Lead>(
	    new Model<String>("Modified At"), "modified_at") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Lead>>
		cellItem, String componentId, IModel<Lead> rowModel) {

		Lead p = rowModel.getObject();

		cellItem.add(new Label(componentId, WebUtil.formatDate(
		    WebUtil.dateTimeFormat, p.getModifiedAt())));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	AjaxDataTable<Lead> table =
	    new AjaxDataTable<Lead>("table", columns,
	    new LeadsDataProvider(showData), 25);
	add(table);
    }
}
