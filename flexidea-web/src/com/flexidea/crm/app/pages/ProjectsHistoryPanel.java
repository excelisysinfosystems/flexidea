package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.common.components.AjaxDataTable;
import com.flexidea.crm.app.common.panel.data.ProjectsDataProvider;
import com.flexidea.crm.app.common.panel.data.ProjectsHistoryDataProvider;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.util.WebUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.PageParameters;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public final class ProjectsHistoryPanel extends Panel {
    public ProjectsHistoryPanel(String id, boolean showData) {
        super (id);
	List<IColumn<Project>> columns = new ArrayList<IColumn<Project>>();
	columns.add(new AbstractColumn<Project>(new Model<String>(" "), "status") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Project>>
		cellItem, String componentId, IModel<Project> rowModel) {
		Fragment frag = new Fragment(componentId,
		    "statusColor", ProjectsHistoryPanel.this);

		Project p = rowModel.getObject();

		Label statusColor = new Label("statusColor", " ");
		String className = "status_dead";

		switch(ProjectDAO.getProjectStatus(p)) {
		    case 0:
			className = "status_dead";
			break;
		    case 1:
			className = "status_lost";
			break;
		    case 2:
			className = "status_recvd";
			break;
		}

		statusColor.add(new SimpleAttributeModifier("class",
		    className));
		frag.add(statusColor);
		cellItem.add(frag);
	    }
	});

	columns.add(new AbstractColumn<Project>(new Model<String>("ID"), "id") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Project>>
		cellItem, String componentId, IModel<Project> rowModel) {
		Fragment frag = new Fragment(componentId,
		    "projectId", ProjectsHistoryPanel.this);
		cellItem.add(frag);

		Project p = rowModel.getObject();
		Long pid = p.getId();

		PageParameters pp = new PageParameters();
		pp.put("project", pid);

		BookmarkablePageLink projectIDLink =
		    new BookmarkablePageLink("projectIDLink",
		    ProjectsPage.class, pp);

		projectIDLink.add(new Label("projectID", Long.toString(pid)));
		frag.add(projectIDLink);
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new AbstractColumn<Project>(new Model<String>("Project"), "projectName") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Project>>
		cellItem, String componentId, IModel<Project> rowModel) {
		Fragment frag = new Fragment(componentId,
		    "projectName", ProjectsHistoryPanel.this);
		cellItem.add(frag);

		Project p = rowModel.getObject();
		Long pid = p.getId();

		PageParameters pp = new PageParameters();
		pp.put("project", pid);

		BookmarkablePageLink projectNameLink =
		    new BookmarkablePageLink("projectNameLink",
		    ProjectsPage.class, pp);

		projectNameLink.add(new Label("projectName", p.getProjectName()));
		frag.add(projectNameLink);
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new AbstractColumn<Project>(new Model<String>("Status"), "status") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Project>>
		cellItem, String componentId, IModel<Project> rowModel) {

		Project p = rowModel.getObject();
		String statusString = "";
		switch(ProjectDAO.getProjectStatus(p)) {
		    case 0:
			statusString = "Dead";
			break;
		    case 1:
			statusString = "Lost";
			break;
		    case 2:
			statusString = "Received";
			break;
		}

		cellItem.add(new Label(componentId, statusString));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<Project>(
	    new Model<String>("Created At"), "createdAt") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Project>>
		cellItem, String componentId, IModel<Project> rowModel) {

		Project p = rowModel.getObject();

		cellItem.add(new Label(componentId, WebUtil.formatDate(
		    WebUtil.dateTimeFormat, p.getCreatedAt())));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<Project>(
	    new Model<String>("Modified At"), "modifiedAt") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Project>>
		cellItem, String componentId, IModel<Project> rowModel) {

		Project p = rowModel.getObject();

		cellItem.add(new Label(componentId, WebUtil.formatDate(
		    WebUtil.dateTimeFormat, p.getModifiedAt())));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	AjaxDataTable<Project> table =
	    new AjaxDataTable<Project>("table", columns,
	    new ProjectsHistoryDataProvider(showData), 25);
	add(table);
    }
}
