/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;

/**
 *
 * @author akshay
 */
public final class LandingPage extends WebPage {
    public LandingPage() {
        super ();
	if (! CrmSession.doAuthentication(this)) {
	    setResponsePage(Login.class);
	}
    }

    public LandingPage(PageParameters params) {
	super(params);
	if (! CrmSession.doAuthentication(this)) {
	    setResponsePage(Login.class);
	}
    }
}

