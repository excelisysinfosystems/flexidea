package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.panel.ContactsAssociationPanel;
import com.flexidea.crm.app.common.panel.SearchPanel;
import com.flexidea.crm.app.contacts.panels.ContactFormPanel;
import com.flexidea.crm.app.contacts.panels.UploadContactsPanel;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.LeadsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.Project;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.servlet.AbortWithHttpStatusException;

/**
 *
 *
 * @author akshay
 */
public final class ContactsPage extends WebPage {
    private Long contact;
    private ContactFormPanel formPanel;
    private ContactsAssociationPanel associationPanel;
    private SearchPanel searchPanel;
    private Label newContact;
    private Label editContact;

    private ModalWindow uploadWindow;
    private Label uploadLink;

    public ContactsPage() {
        super ();
	/*
	Contact c = ContactsDAO.getContact(401L);
	ContactFormPanel formPanel = null;

	if (c != null) {
	    formPanel = new ContactFormPanel("contactForm", true, c.getId());
	} else {
	    formPanel = new ContactFormPanel("contactForm", true, null);
	}
	add(formPanel);
	 *
	 */
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	doRender();
    }

    public ContactsPage(PageParameters params) {
	super(params);

	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	this.contact = params.getLong("contact");

	try {
	    if (ContactsDAO.getContact(this.contact) == null) {
		throw new AbortWithHttpStatusException(404, true);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new AbortWithHttpStatusException(404, true);
	}

	doRender();
    }

    public void resetMenu(AjaxRequestTarget target) {
	newContact.add(new AttributeModifier("class", true,
	    new Model<String>("menutab")));
	editContact.add(new AttributeModifier("class", true,
	    new Model<String>("menutab")));
	if (target != null) {
	    if (target != null) {
		target.addComponent(newContact);
		target.addComponent(editContact);
	    }
	}
    }

    public void updateSearchPanel(AjaxRequestTarget aTarget) {
	if (searchPanel != null) {
	    searchPanel.update(aTarget);
	}
    }

    private void doRender() {
	setOutputMarkupId(true);

	if (uploadWindow == null) {
	    uploadWindow = new ModalWindow("uploadWindow");
	    uploadWindow.setOutputMarkupId(true);
	    uploadWindow.setOutputMarkupPlaceholderTag(true);
	    add(uploadWindow);
	}

	if (uploadLink == null) {
	    uploadLink = new Label("uploadLink", "Upload from Excel");
	    uploadLink.setOutputMarkupId(true);
	    uploadLink.add(new AjaxEventBehavior("onclick") {

		@Override
		protected void onEvent(AjaxRequestTarget target) {
		    uploadWindow.setContent(
			new UploadContactsPanel(uploadWindow.getContentId()));
		    uploadWindow.show(target);
		}
	    });
	    add(uploadLink);
	}

	Long companyId = null;
	if (contact != null) {
	    companyId = ContactsDAO.getContact(contact).getCompany().getId();
	}
	associationPanel = new ContactsAssociationPanel("associatedContacts",
	    CommentsTarget.CONTACTS, companyId);
	associationPanel.setOutputMarkupId(true);
	formPanel = new ContactFormPanel("contactForm", false, contact,
		associationPanel);

	if (newContact == null) {
	    newContact = new Label("newContactButton", "New");
	    newContact.setOutputMarkupId(true);
	    newContact.add(new AjaxEventBehavior("onclick") {

		@Override
		protected void onEvent(AjaxRequestTarget target) {
		    associationPanel = new ContactsAssociationPanel(
			"associatedContacts", CommentsTarget.CONTACTS, null);
		    associationPanel.setOutputMarkupId(true);
		    formPanel = new ContactFormPanel("contactForm", true, null,
			    associationPanel);
		    ContactsPage.this.addOrReplace(formPanel);
		    ContactsPage.this.addOrReplace(associationPanel);

                    newContact.add(new AttributeModifier("class", true,
			new Model<String>("menutabH")));
                    editContact.add(new AttributeModifier("class", true,
			new Model<String>("menutab")));
		    if (target != null) {
			target.addComponent(formPanel);
			target.addComponent(associationPanel);
			target.addComponent(newContact);
			target.addComponent(editContact);
		    }
		}
	    });
	    add(newContact);
	}

	if (editContact == null) {
	    editContact = new Label("editContactButton", "Edit");
	    editContact.setOutputMarkupId(true);
	    editContact.add(new AjaxEventBehavior("onclick") {

		@Override
		protected void onEvent(AjaxRequestTarget target) {
		    ContactsPage.this.setContact(formPanel.getContactId());
		    if (ContactsPage.this.getContact() == null) {
			return;
		    }
		    associationPanel = new ContactsAssociationPanel(
			    "associatedContacts", CommentsTarget.CONTACTS,
			    ContactsDAO.getContact(contact).getCompany().getId());
		    associationPanel.setOutputMarkupId(true);
		    formPanel = new ContactFormPanel("contactForm", true,
			    ContactsPage.this.getContact(), associationPanel);
		    ContactsPage.this.addOrReplace(formPanel);
		    ContactsPage.this.addOrReplace(associationPanel);

                    newContact.add(new AttributeModifier("class", true,
			new Model<String>("menutab")));
                    editContact.add(new AttributeModifier("class", true,
			new Model<String>("menutabH")));

		    if (target != null) {
			target.addComponent(formPanel);
			target.addComponent(associationPanel);
			target.addComponent(newContact);
			target.addComponent(editContact);
		    }
		}
	    });
	    add(editContact);
	}

	add(formPanel, associationPanel);

	if (searchPanel == null) {
	    searchPanel = new SearchPanel("searchPanel", CommentsTarget.CONTACTS);
	    searchPanel.setOutputMarkupId(true);
	    add(searchPanel);
	}

	WebMarkupContainer leads = new WebMarkupContainer("associatedLeads");
	leads.setOutputMarkupId(true);
	List<Lead> leadsList = new ArrayList<Lead>();

	Contact ct = null;
	if (contact != null && (ct = ContactsDAO.getContact(contact)) != null) {
	    leadsList.addAll(LeadsDAO.getAssociatedLeads(ct));
	}

	leads.add(new ListView<Lead>("list", leadsList) {

	    @Override
	    protected void populateItem(ListItem<Lead> item) {
		final Lead l = item.getModelObject();
		PageParameters pp = new PageParameters();
		pp.put("lead", l.getId());
		BookmarkablePageLink link = new BookmarkablePageLink("item",
		    LeadsPage.class, pp);

		link.add(new Label("linkLabel", l.getLeadName()));
		item.add(link);
	    }
	});

	WebMarkupContainer projects = new WebMarkupContainer("associatedProjects");
	projects.setOutputMarkupId(true);
	List<Project> projectsList = new ArrayList<Project>();

	if (ct != null) {
	    projectsList.addAll(ProjectDAO.getAssociatedProjects(ct));
	}

	projects.add(new ListView<Project>("list", projectsList) {

	    @Override
	    protected void populateItem(ListItem<Project> item) {
		final Project l = item.getModelObject();
		PageParameters pp = new PageParameters();
		pp.put("project", l.getId());
		BookmarkablePageLink link = new BookmarkablePageLink("item",
		    ProjectsPage.class, pp);
		link.add(new Label("linkLabel", l.getProjectName()));
		item.add(link);
	    }
	});
	add(projects, leads);
    }

    public Long getContact() {
	return contact;
    }

    public void setContact(Long contactId) {
	this.contact = contactId;
    }
}