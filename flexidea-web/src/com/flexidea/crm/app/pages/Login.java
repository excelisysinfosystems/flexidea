/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CrmUser;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;

/**
 *
 * @author akshay
 */
public final class Login extends WebPage {
    public Login() {
        super ();
	doRender();
    }

    public Login(PageParameters params) {
	String username = params.getString("username");
	String password = params.getString("password");

	CrmUser user = UserDAO.getUser(username, password);
	if (user != null) {
	    CrmSession session = CrmSession.get();
	    session.setUserId(user.getUserId());
	    setResponsePage(LandingPage.class);
	}
	doRender();
    }

    private void doRender() {
	Form<CrmUser> form = new Form<CrmUser>("loginForm",
	    new CompoundPropertyModel<CrmUser>(new CrmUser())) {

	    @Override
	    protected void onSubmit() {
		super.onSubmit();
		CrmUser model = getModelObject();
		CrmUser user = UserDAO.getUser(model.getUsername(),
		    model.getPassword());
		if (user != null) {
		    CrmSession session = CrmSession.get();
		    session.setUserId(user.getUserId());
		    setResponsePage(LandingPage.class);
		} else {
		    error("Invalid username/password");
		}
	    }
	};

	add(form);

	FeedbackPanel loginFeedback = new FeedbackPanel("loginFeedback");
	loginFeedback.setOutputMarkupId(true);
	form.add(loginFeedback);

	form.add(new RequiredTextField("username"));
	form.add(new PasswordTextField("password"));
	form.add(new Button("loginButton"));
    }
}

