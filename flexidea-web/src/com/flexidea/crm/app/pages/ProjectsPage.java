package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.AjaxPagingNavigatorWithImages;
import com.flexidea.crm.app.common.components.IndicatingAutoCompleteTextField;
import com.flexidea.crm.app.common.panel.CommentsPanel;
import com.flexidea.crm.app.common.panel.ContactsAssociationPanel;
import com.flexidea.crm.app.common.panel.MarkAsDeadPanel;
import com.flexidea.crm.app.common.panel.SearchPanel;
import com.flexidea.crm.app.common.panel.data.ProjectsHistoryDataProvider;
import com.flexidea.crm.db.dao.CompaniesDAO;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.Project;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.servlet.AbortWithHttpStatusException;

/**
 *
 * @author akshay
 */
public final class ProjectsPage extends WebPage {
    private Long project;
    private SearchPanel searchPanel;
    private ContactsAssociationPanel associationPanel;
    private Fragment commentsFragment;
    private boolean showHistory = false;
    private AjaxLink newProjectButton;
    private AjaxLink historyButton;
    private WebMarkupContainer projectHistory;
    private ProjectForm newProjectForm;
    private MarkAsDeadPanel commentDialog;

    public ProjectsPage() {
        super ();
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	doRender(null);
    }

    public ProjectsPage(PageParameters params) {
	super(params);
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	this.project = params.getLong("project");

	showHistory = false;

	try {
	    if (ProjectDAO.getProject(project) == null) {
		throw new AbortWithHttpStatusException(404, true);
	    }
	} catch (Exception e) {
	    throw new AbortWithHttpStatusException(404, true);
	}

	doRender(null);
    }

    private void showHistory(AjaxRequestTarget target) {

	projectHistory.addOrReplace(
	    new ProjectsHistoryPanel("historyTable", showHistory));

	if (target != null) {
	    target.addComponent(projectHistory);
	}
    }

    private void doRender(AjaxRequestTarget target) {
	setOutputMarkupId(true);

	if (commentDialog == null) {
	    commentDialog = new MarkAsDeadPanel("commentDialog", "") {

		@Override
		public void onDone(AjaxRequestTarget target, String comment) {
		    Project l = null;

		    if (project == null) {
			return;
		    } else {
			l = ProjectDAO.getProject(project);
		    }

		    if (l == null) {
			return;
		    } else {
			l.setComment(comment);
			l.setIsDead(true);
			l.setModifiedAt(new Date());
			l.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
		    }

		    ProjectDAO.update(l);
		    info("Project marked as Dead!");

		    ProjectsPage.this.setProject(l.getId());
		    searchPanel.update(target);
		    ProjectsPage.this.doRender(target);
		}
	    };
	    add(commentDialog);
	}

	if (searchPanel == null) {
	    searchPanel = new SearchPanel("searchPanel", CommentsTarget.PROJECT);
	    searchPanel.setOutputMarkupId(true);
	} else {
	    searchPanel.update(target);
	}

	if (associationPanel == null) {
	    associationPanel = new ContactsAssociationPanel("associatedContacts",
		CommentsTarget.PROJECT, project);
	    associationPanel.setOutputMarkupId(true);
	} else {
	    associationPanel.update(target, CommentsTarget.PROJECT, project);
	}

	if (project != null) {
	    commentsFragment = new Fragment("commentsContainer",
		"commentsFragment");
	    commentsFragment.add(
		new CommentsPanel("commentsPanel", CommentsTarget.PROJECT,
		project));
	} else {
	    commentsFragment = new Fragment("commentsContainer", "gyaanFragment");
	}

	commentsFragment.setOutputMarkupId(true);
	commentsFragment.setOutputMarkupPlaceholderTag(true);

	if (newProjectButton == null) {
	    newProjectButton =  new AjaxLink("newProjectButton") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    newProjectButton.add(
			new SimpleAttributeModifier("class", "menutabH"));
		    historyButton.add(
			new SimpleAttributeModifier("class", "menutab"));
		    ProjectsPage.this.setProject(null);
		    ProjectsPage.this.doRender(target);
		    showHistory = false;
		}
	    };
	    newProjectButton.setOutputMarkupId(true);
	    add(newProjectButton);
	}

	if (historyButton == null) {
	    historyButton =  new AjaxLink("historyButton") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    newProjectButton.add(
			new SimpleAttributeModifier("class", "menutab"));
		    historyButton.add(
			new SimpleAttributeModifier("class", "menutabH"));
		    showHistory = true;
		    ProjectsPage.this.setProject(null);
		    ProjectsPage.this.doRender(target);
		}
	    };
	    newProjectButton.setOutputMarkupId(true);
	    add(historyButton);
	}

	newProjectForm = new ProjectForm("newProjectForm", project);
	newProjectForm.setOutputMarkupId(true);
	newProjectForm.setOutputMarkupPlaceholderTag(true);

	if (projectHistory == null) {
	    projectHistory = new WebMarkupContainer("projectHistory");
	    projectHistory.setOutputMarkupId(true);
	    projectHistory.setOutputMarkupPlaceholderTag(true);
	    projectHistory.setVersioned(false);
	    add(projectHistory);
	}

	if (showHistory) {
	    newProjectForm.setVisible(false);
	    commentsFragment.setVisible(false);
	    projectHistory.setVisible(true);
	} else {
	    projectHistory.setVisible(false);
	    newProjectForm.setVisible(true);
	    commentsFragment.setVisible(true);
	}

	showHistory(target);

	List<Enquiry> enqList = null;

	if (project != null) {
	    enqList = ProjectDAO.getProject(project).getEnquiries();
	} else {
	    enqList = new ArrayList<Enquiry>();
	}

	ListView<Enquiry> enquiryList = new ListView<Enquiry>("enquiryList",
	    enqList) {

	    @Override
	    protected void populateItem(ListItem<Enquiry> item) {
		Enquiry enquiry = item.getModelObject();
		Label contractorName = new Label("contractorName",
		    enquiry.getContractor().getCompany().getCompanyName());
		PageParameters params = new PageParameters();
		params.put("enquiry", enquiry.getId());
		BookmarkablePageLink enquiryLink = new BookmarkablePageLink(
		    "enquiry", EnquiryPage.class, params);
		enquiryLink.add(contractorName);
		item.add(enquiryLink);
	    }
	};
	enquiryList.setOutputMarkupId(true);

	WebMarkupContainer enquiryListContainer =
	    new WebMarkupContainer("enquiryListContainer");
	enquiryListContainer.add(enquiryList);
	enquiryListContainer.setOutputMarkupId(true);

	addOrReplace(newProjectForm, associationPanel,
	    commentsFragment, searchPanel, enquiryListContainer);

	if (target != null) {
	    target.addComponent(newProjectButton);
	    target.addComponent(historyButton);
	    target.addComponent(projectHistory);
	    target.addComponent(newProjectForm);
	    target.addComponent(associationPanel);
	    target.addComponent(commentsFragment);
	    //target.addComponent(searchPanel);
	    target.addComponent(enquiryListContainer);
	}
    }

    private class ProjectForm extends Form implements Serializable {
	private Long project;
	private String projectName;

	public ProjectForm(String id, Long iProject) {
	    super(id);
	    setOutputMarkupId(true);
	    this.project = iProject;

	    Project l = null;
	    String projectId = "Not yet saved";
	    if (project != null) {
		l = ProjectDAO.getProject(project);
		if (l != null) {
		    projectName = l.getProjectName();
		    project = l.getId();
		    projectId = l.getId().toString();
		} else {
		    project = null;
		    projectName = "";
		}
	    }

	    final FeedbackPanel feedback = new FeedbackPanel("feedback");
	    feedback.setOutputMarkupId(true);

	    /*
	     * BUG ID 39 [START]
	     */
	    IndicatingAutoCompleteTextField projectNameField = new IndicatingAutoCompleteTextField("projectName",
		new Model(projectName)){
			@Override
			protected Iterator getChoices(String input) {
				
				List<Project> lstProjects = ProjectDAO.search(input, null, 0, true, false, 0, Integer.MAX_VALUE);
				List<String> lstProjectNames = new ArrayList<String>();
				if(lstProjects!=null){
					for(Project project :lstProjects){
						lstProjectNames.add(project.getProjectName());
					}
				}
				
			    return lstProjectNames.iterator();
			}
			
			@Override
			public void updateModel() {
			    super.updateModel();
			}	    	
	    };
	    /*
	     * BUG ID 39 [END]
	     */	    
	    /*
	    AjaxEditableLabel projectNameField = new AjaxEditableLabel("projectName",
		new PropertyModel<String>(this, "projectName")){

		@Override
		protected void onSubmit(AjaxRequestTarget target) {
		    super.onSubmit(target);
		    info("Label name=" + this.getDefaultModelObjectAsString());
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}
	    };
	     *
	     */

	    projectNameField.setRequired(true);
	    projectNameField.setOutputMarkupId(true);

	    final IndicatingAjaxButton saveButton =
		new IndicatingAjaxButton("saveButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    boolean newProject = false;
		    boolean alertShown = false;
		    Project l = null;
		    
		    setProjectName(String.valueOf(form.get("projectName").getDefaultModel().getObject()));
		    if (project == null) {
			l = new Project();
			l.setProjectName(getProjectName());
			l.setIsDead(false);
			l.setCreatedAt(new Date());
			l.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			newProject = true;
			System.out.println("DEBUG: Fresh Project P1");
		    } else {
			l = ProjectDAO.getProject(project);
			System.out.println("DEBUG: Project=" + l);
		    }

		    if (l == null) {
			l = new Project();
			l.setProjectName(projectName);
			l.setIsDead(false);
			l.setCreatedAt(new Date());
			l.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			newProject = true;
			System.out.println("DEBUG: Fresh Project P2");
		    } else if (ProjectDAO.getProject(projectName) != null) {
			error("Project '" + projectName + "' already exists.");
			l = null;
			alertShown = true;
			System.out.println("DEBUG: Duplicate Project 1");
		    } else {
			l.setProjectName(projectName);
			l.setModifiedAt(new Date());
			l.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
		    }

		    if (newProject && ProjectDAO.getProject(projectName) != null) {
		    	if(!alertShown){
		    		error("Project '" + projectName + "' already exists.");
		    	}
			newProject = false;
			System.out.println("DEBUG: Duplicate Project 2");
		    } else if (l != null){
			System.out.println("DEBUG: Updating project");
			ProjectDAO.update(l);
                        ProjectsPage.this.setProject(l.getId());
			info("Project updated!");
		    }

		    

		    if (target != null) {
			target.addComponent(feedback);
		    }

		    if (newProject) {
			ProjectsPage.this.doRender(target);
		    }
		}
	    };

	    saveButton.setOutputMarkupPlaceholderTag(true);

	    final AjaxSubmitLink createEnquiry =
		new AjaxSubmitLink("createEnquiry", this) {

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    if (target != null) {
			target.addComponent(feedback);
		    }
		    Project l = null;

		    if (project == null) {
			return;
		    } else {
			l = ProjectDAO.getProject(project);
		    }

		    if (l == null) {
			return;
		    }

		    List<Contact> list = ContactsDAO
			.listAssociatedContacts(CommentsTarget.PROJECT,
			l.getId(),
			ContactsDAO.getContactType("Contractor"));

		    if (list.isEmpty()) {
			error("Kindly associate Contractors to the project.");
			return;
		    }

		    list = ContactsDAO
			.listAssociatedContacts(CommentsTarget.PROJECT,
			l.getId(),
			ContactsDAO.getContactType("Sales Representative"));

		    if (list.isEmpty()) {
			error("Kindly associate Sales Representatives to the project.");
			return;
		    }

		    PageParameters params = new PageParameters();
		    params.put("project", l.getId());
		    setResponsePage(EnquiryPage.class, params);
		}
	    };

	    createEnquiry.setOutputMarkupId(true);
	    createEnquiry.setOutputMarkupPlaceholderTag(true);

	    final IndicatingAjaxButton deadButton =
		new IndicatingAjaxButton("deadButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    commentDialog.open(target, "Mark Project '"
			+ projectName + "' as Dead?");
		}
	    };

	    deadButton.setOutputMarkupPlaceholderTag(true);

	    WebMarkupContainer lead = new WebMarkupContainer("lead");
	    lead.setOutputMarkupId(true);
	    lead.setOutputMarkupPlaceholderTag(true);

	    Long leadId = 0L;
	    String leadName = "";

	    if (l != null) {
		if (l.isIsDead()) {
		    deadButton.setVisible(false);
		    saveButton.setVisible(false);
		    createEnquiry.setVisible(false);
		}
		if (l.getLead() != null) {
		    leadId = l.getLead().getId();
		    leadName = l.getLead().getLeadName();
		} else {
		    lead.setVisible(false);
		}
	    } else {
		lead.setVisible(false);
	    }

	    if (iProject == null) {
		deadButton.setVisible(false);
		saveButton.setVisible(true);
		createEnquiry.setVisible(false);
	    }

	    final Long lId = leadId;
	    PageParameters pp = new PageParameters();
	    pp.put("lead", lId);
	    BookmarkablePageLink leadLink = new BookmarkablePageLink("leadLink", 
		LeadsPage.class, pp);
	    /*
	    Link leadLink = new Link("leadLink") {

		@Override
		public void onClick() {
		    PageParameters pp = new PageParameters();
		    pp.put("lead", lId);
		    getPage().setResponsePage(LeadsPage.class, pp);
		}
	    };
	     *
	     */

	    leadLink.add(new Label("leadName", leadName));
	    lead.addOrReplace(leadLink);

	    add(new Label("projectID", projectId));
	    add(feedback, projectNameField, saveButton, deadButton,
		createEnquiry, lead);
	}

	public Long getProject() {
	    return project;
	}

	public void setProject(Long project) {
	    this.project = project;
	}

	public String getProjectName() {
	    return projectName;
	}

	public void setProjectName(String projectName) {
	    this.projectName = projectName;
	}
    }

    public Long getProject() {
	return project;
    }

    public void setProject(Long project) {
	this.project = project;
    }
}

