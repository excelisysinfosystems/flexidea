package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.AttachmentLink;
import com.flexidea.crm.app.common.panel.CommentsPanel;
import com.flexidea.crm.app.common.panel.ContactsAssociationPanel;
import com.flexidea.crm.app.common.panel.MarkAsDeadPanel;
import com.flexidea.crm.app.common.panel.SearchPanel;
import com.flexidea.crm.db.dao.AttachmentDAO;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.EnquiryDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.Attachments;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.EnquiryDocs;
import com.flexidea.crm.db.dto.EnquiryStage;
import com.flexidea.crm.db.dto.EnquiryStatus;
import com.flexidea.crm.util.WebUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.MultiFileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.servlet.AbortWithHttpStatusException;
import org.apache.wicket.validation.validator.MinimumValidator;

/**
 *
 * @author akshay
 */
public final class EnquiryPage extends WebPage {
    private Long project;
    private Long enquiry;
    private EnquiryForm form;
    private SearchPanel searchPanel;
    private ContactsAssociationPanel associationPanel;
    private QuoteStageForm quoteStageForm;
    private QuoteStatusForm quoteStatusForm;
    private Fragment commentsFragment;
    private EnquiryDocsPanel enquiryDocsPanel;
    private MarkAsDeadPanel commentDialog;

    public EnquiryPage() {
        super ();
	throw new AbortWithHttpStatusException(404, true);
    }

    public EnquiryPage(PageParameters params) {
	super(params);
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	project = params.getAsLong("project");
	enquiry = params.getAsLong("enquiry");

	if (project != null && ProjectDAO.getProject(project) == null) {
	    throw new AbortWithHttpStatusException(404, true);
	}

	if (enquiry != null) {
	    Enquiry e =  EnquiryDAO.getEnquiry(enquiry);
	    if (e == null) {
		throw new AbortWithHttpStatusException(404, true);
	    } else {
		this.setProject(e.getProject().getId());
	    }
	}

	doRender(null);
    }

    public void doRender(AjaxRequestTarget target) {

	if (commentDialog == null) {
	    commentDialog = new MarkAsDeadPanel("commentDialog", "") {

		@Override
		public void onDone(AjaxRequestTarget target, String comment) {
		    EnquiryFormModel m = (EnquiryFormModel)
			form.getModelObject();
		    Enquiry eq = null;
		    if (m.getEnquiryId() == null) {
			return;
		    } else {
			eq = EnquiryDAO.getEnquiry(m.getEnquiryId());
			eq.setDead(true);
			eq.setComment(comment);
			eq.setModifiedAt(new Date());
			eq.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
		    }

		    EnquiryDAO.update(eq);
		    info("Enquiry marked as Dead!");
		    EnquiryPage.this.doRender(target);
		}
	    };
	    addOrReplace(commentDialog);
	}

	EnquiryFormModel fModel = new EnquiryFormModel();
	fModel.setEnquiryId(enquiry);
	fModel.setProjectId(project);

	if (enquiry != null) {
	    Enquiry e =  EnquiryDAO.getEnquiry(enquiry);
	    fModel.setBasicValue(e.getBasicValue());
	    fModel.setGrossValue(e.getGrossValue());
	    fModel.setContractor(e.getContractor());
	    fModel.setSalesRep(fModel.getSalesRep());
	    quoteStageForm = new QuoteStageForm("enquiryStage", e.getStage());
	    quoteStatusForm = new QuoteStatusForm("enquiryStatus", e.getStatus());
	} else {
	    quoteStageForm = new QuoteStageForm("enquiryStage", null);
	    quoteStatusForm = new QuoteStatusForm("enquiryStatus", null);
	}

	form = new EnquiryForm("enquiryForm", 
	    new CompoundPropertyModel<EnquiryFormModel>(fModel));
	form.setOutputMarkupId(true);

	if (searchPanel == null) {
	    searchPanel = new SearchPanel("searchPanel", CommentsTarget.ENQUIRY);
	    searchPanel.setOutputMarkupId(true);
	    addOrReplace(searchPanel);
	} else {
	    searchPanel.update(target);
	}

	associationPanel = new ContactsAssociationPanel("associationPanel",
	    CommentsTarget.ENQUIRY, enquiry);
	associationPanel.setOutputMarkupId(true);

	if (enquiry != null) {
	    commentsFragment = new Fragment("commentsContainer",
		"commentsFragment");
	    commentsFragment.add(
		new CommentsPanel("commentsPanel", CommentsTarget.ENQUIRY,
		enquiry));
	} else {
	    commentsFragment = new Fragment("commentsContainer", "gyaanFragment");
	}

	commentsFragment.setOutputMarkupId(true);
	enquiryDocsPanel = new EnquiryDocsPanel("enquiryDocs", "enquiryDocsFragment");
	enquiryDocsPanel.setOutputMarkupId(true);

	addOrReplace(form, associationPanel, quoteStageForm,
	    quoteStatusForm, commentsFragment, enquiryDocsPanel);

	if (target != null) {
	    target.addComponent(form);
	    //target.addComponent(searchPanel);
	    target.addComponent(associationPanel);
	    target.addComponent(quoteStageForm);
	    target.addComponent(quoteStatusForm);
	    target.addComponent(commentsFragment);
	    target.addComponent(enquiryDocsPanel);
	}
    }

    public void updateSearchPanel(AjaxRequestTarget aTarget) {
	if (searchPanel != null) {
	    searchPanel.update(aTarget);
	}
    }

    public class EnquiryForm extends Form implements Serializable {

	public EnquiryForm(String id, final IModel<EnquiryFormModel> imodel) {
	    super(id, imodel);
	    this.setOutputMarkupId(true);
	    final EnquiryFormModel model = imodel.getObject();
	    boolean isDead = false;

	    if (model.getEnquiryId() != null) {
		Enquiry e = EnquiryDAO.getEnquiry(model.getEnquiryId());
		if (e != null) {
		    model.setContractor(e.getContractor());
		    model.setEnquiryId(e.getId());
		    model.setProjectId(e.getProject().getId());
		    model.setSalesRep(e.getSalesRep());
		    isDead = e.isDead();
		    imodel.setObject(model);
		}
	    }

	    final FeedbackPanel feedback = new FeedbackPanel("enquiryFeedback");
	    feedback.setOutputMarkupId(true);

	    final IChoiceRenderer<Contact> cr = new IChoiceRenderer<Contact>() {
		@Override
		public Object getDisplayValue(Contact object) {
		    return object.getFirstName() + " " + object.getLastName();
		}

		@Override
		public String getIdValue(Contact object, int index) {
		    return object.getId().toString();
		}
	    };

	    Link projectLink = new Link("projectLink") {

		@Override
		public void onClick() {
		    PageParameters params = new PageParameters();
		    params.put("project", model.getProjectId());
		    setResponsePage(ProjectsPage.class, params);
		}
	    };
	    projectLink.add(new Label("projectName",
		ProjectDAO.getProject(model.getProjectId()).getProjectName()));

	    List<Contact> list = null;
	    List<Contact> salesRepList = null;

	    if (model.getEnquiryId() == null) {
		list = ContactsDAO.listAssociatedContacts(CommentsTarget.PROJECT,
		    model.getProjectId(),
		    ContactsDAO.getContactType("Contractor"));
	    } else {
		list = ContactsDAO.listAssociatedContacts(CommentsTarget.ENQUIRY,
		    model.getEnquiryId(),
		    ContactsDAO.getContactType("Contractor"));
	    }

	    if (list == null || list.isEmpty()) {
		error("Project '" + ProjectDAO.getProject(
		    model.getProjectId()).getProjectName() 
		    + "' does not have"
		    + " any contractors associated. "
		    + "Please add Contractors in the project.");
		salesRepList = new ArrayList<Contact>();
	    } else if (model.getContractor() != null) {
		salesRepList = ContactsDAO.listAssociatedContacts(
		    CommentsTarget.CONTACTS, 
		    model.getContractor().getCompany().getId(),
		    ContactsDAO.getContactType("Sales Representative"));
	    } else {
		salesRepList = new ArrayList<Contact>();
	    }

	    TextField enquiryIdField = new TextField("enquiryId");
	    enquiryIdField.setEnabled(false);

	    final DropDownChoice<Contact> contractor =
		new DropDownChoice<Contact>("contractor", list,
		new ChoiceRenderer<Contact>(
		    "company.companyName", "id"));

	    final DropDownChoice<Contact> salesRep =
		new DropDownChoice<Contact>("salesRep", salesRepList, cr);

	    salesRep.setOutputMarkupId(true);
	    salesRep.setRequired(true);
	    contractor.setOutputMarkupId(true);
	    contractor.setRequired(true);

	    TextField<Double> basicValue = new TextField<Double>("basicValue");
	    TextField<Double> grossValue = new TextField<Double>("grossValue");

	    basicValue.add(new MinimumValidator<Double>(0.0));
	    grossValue.add(new MinimumValidator<Double>(0.0));

	    contractor.add(new AjaxFormComponentUpdatingBehavior("onchange") {

		@Override
		protected void onUpdate(AjaxRequestTarget target) {
		    if (model.getContractor() != null) {
			if (EnquiryDAO.contactLookup(ProjectDAO.getProject(project),
			    model.getContractor())) {
			    error("Enquiry already exists for contractor "
				+ model.getContractor().toString());
			    model.setContractor(null);
			    imodel.setObject(model);
			} else {
			    List<Contact> l = ContactsDAO
				.listAssociatedContacts(CommentsTarget.CONTACTS,
				model.getContractor().getCompany().getId(),
				ContactsDAO.getContactType("Sales Representative"));
			    if (l != null && ! l.isEmpty()) {
				model.setSalesRep(l.get(0));
				imodel.setObject(model);
				salesRep.setChoices(l);
			    } else {
				error("Contractor \""
				    + model.getContractor().toString()
				    +"\" does not have any Sales Representative "
				    + " associated.");
			    }
			}
		    } else {
			error("The project does not have any contractors "
			    + "associated. Please add Contractors in the "
			    + " project.");
		    }

		    if (target != null) {
			target.addComponent(salesRep);
			target.addComponent(feedback);
		    }
		}
	    });

	    final IndicatingAjaxButton saveButton = new IndicatingAjaxButton(
		"saveButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target,
		    Form<?> form) {
		    EnquiryFormModel m = (EnquiryFormModel)
			form.getModelObject();

		    if (target != null) {
			target.addComponent(feedback);
		    }

		    Enquiry eq = null;
		    EnquiryDocs docs = null;
		    if (m.getEnquiryId() == null) {
			eq = new Enquiry();
			eq.setProject(
			    ProjectDAO.getProject(m.getProjectId()));
			eq.setContractor(m.getContractor());
			eq.setSalesRep(m.getSalesRep());
			eq.setBasicValue(m.getBasicValue());
			eq.setGrossValue(m.getGrossValue());
			eq.setCreatedAt(new Date());
			eq.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			eq.setModifiedAt(new Date());
			eq.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			eq.setStage(EnquiryStage.NOT_QUOTED);
			eq.setStatus(EnquiryStatus.HOT);
			docs = new EnquiryDocs();
			docs.setCreatedAt(new Date());
			docs.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			docs.setRevesionNo(0);
		    } else {
			eq = EnquiryDAO.getEnquiry(m.getEnquiryId());
			eq.setContractor(m.getContractor());
			eq.setSalesRep(m.getSalesRep());
			eq.setBasicValue(m.getBasicValue());
			eq.setGrossValue(m.getGrossValue());
			eq.setModifiedAt(new Date());
			eq.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
		    }

		    EnquiryDAO.update(eq);

		    if (docs != null) {
			docs.setEnquiry(eq);
			EnquiryDAO.revise(docs);
		    }

		    EnquiryPage.this.setEnquiry(eq.getId());
		    EnquiryPage.this.setProject(eq.getProject().getId());
		    m.setEnquiryId(eq.getId());
		    m.setProjectId(eq.getProject().getId());
		    EnquiryPage.this.doRender(target);
		}

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}
	    };

	    final IndicatingAjaxButton deadButton =
		new IndicatingAjaxButton("deadButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    commentDialog.open(target, "Mark Enquiry as Dead?");
		}
	    };

	    deadButton.setOutputMarkupId(true);
	    deadButton.setOutputMarkupPlaceholderTag(true);

	    WebMarkupContainer createQuote =
		new WebMarkupContainer("createQuote");

	    createQuote.setOutputMarkupId(true);
	    createQuote.setOutputMarkupPlaceholderTag(true);

	    if (isDead) {
		saveButton.setVisible(false);
		deadButton.setVisible(false);
		createQuote.setVisible(false);
	    }

	    add(feedback, contractor, enquiryIdField, feedback, projectLink,
		basicValue, grossValue, salesRep, saveButton, deadButton,
		createQuote);
	}
    }

    public class EnquiryFormModel implements Serializable {
	private Long projectId;
	private Long enquiryId;
	private Contact contractor;
	private Contact salesRep;
	private Double basicValue = 0.0;
	private Double grossValue = 0.0;

	public Contact getContractor() {
	    return contractor;
	}

	public void setContractor(Contact contractor) {
	    this.contractor = contractor;
	}

	public Long getEnquiryId() {
	    return enquiryId;
	}

	public void setEnquiryId(Long enquiryId) {
	    this.enquiryId = enquiryId;
	}

	public Long getProjectId() {
	    return projectId;
	}

	public void setProjectId(Long projectId) {
	    this.projectId = projectId;
	}

	public Contact getSalesRep() {
	    return salesRep;
	}

	public void setSalesRep(Contact salesRep) {
	    this.salesRep = salesRep;
	}

	public Double getBasicValue() {
	    if (basicValue == null) {
		basicValue = 0.0;
	    }
	    return basicValue;
	}

	public void setBasicValue(Double basicValue) {
	    this.basicValue = basicValue;
	}

	public Double getGrossValue() {
	    if (grossValue == null) {
		grossValue = 0.0;
	    }
	    return grossValue;
	}

	public void setGrossValue(Double grossValue) {
	    this.grossValue = grossValue;
	}
    }

    public class QuoteStageForm extends Form {
	private EnquiryStage stage;

	public QuoteStageForm(String id, EnquiryStage iStage) {
	    super(id);
	    this.stage = iStage;
	    setOutputMarkupId(true);

	    final FeedbackPanel feedback =
		new FeedbackPanel("enquiryStageFeedback");
	    feedback.setOutputMarkupId(true);
	    add(feedback);
	    DropDownChoice enquiryStageDD =
		new DropDownChoice("enquiryStageDD",
		new PropertyModel(this, "stage"),
		Arrays.asList(EnquiryStage.values()));
	    enquiryStageDD.setOutputMarkupId(true);

	    enquiryStageDD.add(new AjaxFormComponentUpdatingBehavior("onchange") {

		@Override
		protected void onUpdate(AjaxRequestTarget target) {
		    System.out.println("DEBUG: enqStage=" + stage);
		    if (stage != null && enquiry != null) {
			Enquiry eq = EnquiryDAO.getEnquiry(enquiry);
			eq.setStage(stage);
			eq.setModifiedAt(new Date());
			eq.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			EnquiryDAO.update(eq);
			info("Enquiry stage updated to " + stage.toString());
		    }
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}
	    });
	    add(enquiryStageDD);
	}

	public EnquiryStage getStage() {
	    return stage;
	}

	public void setStage(EnquiryStage stage) {
	    this.stage = stage;
	}
    }

    public class QuoteStatusForm extends Form {
	private EnquiryStatus status;

	public QuoteStatusForm(String id, EnquiryStatus iStatus) {
	    super(id);
	    this.status = iStatus;
	    setOutputMarkupId(true);

	    final FeedbackPanel feedback =
		new FeedbackPanel("enquiryStatusFeedback");
	    feedback.setOutputMarkupId(true);
	    add(feedback);

	    DropDownChoice enquiryStatusDD =
		new DropDownChoice("enquiryStatusDD", 
		new PropertyModel(this, "status"),
		Arrays.asList(EnquiryStatus.values()));
	    enquiryStatusDD.setOutputMarkupId(true);

	    enquiryStatusDD.add(new AjaxFormComponentUpdatingBehavior("onchange") {

		@Override
		protected void onUpdate(AjaxRequestTarget target) {
		    System.out.println("DEBUG: enqStatus=" + status);
		    if (status != null && enquiry != null) {
			Enquiry eq = EnquiryDAO.getEnquiry(enquiry);
			eq.setStatus(status);
			eq.setModifiedAt(new Date());
			eq.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			EnquiryDAO.update(eq);
			info("Enquiry status updated to " + status.toString());
		    }
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}
	    });
	    add(enquiryStatusDD);
	}

	public EnquiryStatus getStatus() {
	    return status;
	}

	public void setStatus(EnquiryStatus status) {
	    this.status = status;
	}

    }

    public Long getEnquiry() {
	return enquiry;
    }

    public void setEnquiry(Long enquiry) {
	this.enquiry = enquiry;
    }

    public Long getProject() {
	return project;
    }

    public void setProject(Long project) {
	this.project = project;
    }

    public class EnquiryDocsPanel extends Fragment {
	private Collection<FileUpload> attachments = new ArrayList<FileUpload>();

	public EnquiryDocsPanel(String id, String markupId) {
	    super(id, markupId);
	    renderEnquiryDocs(null);
	}

	private void renderEnquiryDocs(AjaxRequestTarget target) {
	    final FeedbackPanel feedback =
		new FeedbackPanel("enquiryDocsFeedback");
	    feedback.setOutputMarkupId(true);

	    Enquiry eq = null;
	    if (enquiry != null) {
		eq = EnquiryDAO.getEnquiry(enquiry);
	    } else {
		eq = new Enquiry();
	    }

	    Form reviseForm = new Form("reviseForm");
	    reviseForm.setOutputMarkupId(true);
	    String revNo = "1";
	    if (eq.getEnquiryDocs() == null || eq.getEnquiryDocs().isEmpty()) {
		revNo = "1";
	    } else {
		revNo = Integer.toString(eq.getEnquiryDocs().size());
	    }

	    reviseForm.add((new Label("revisionNo", revNo))
		.setOutputMarkupId(true));
	    reviseForm.add((new IndicatingAjaxButton("reviseButton", form) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    if (target != null) {
			target.addComponent(feedback);
		    }

		    if (enquiry == null) {
			return;
		    }

		    Enquiry e = EnquiryDAO.getEnquiry(enquiry);

		    if (e.isDead()) {
			error("Cannot create revisions on dead enquiries.");
			return;
		    }

		    if (e.getEnquiryDocs() == null
			|| e.getEnquiryDocs().isEmpty()) {
			return;
		    }

		    int size = e.getEnquiryDocs().size();
		    int li = size == 0 ? 0 : size - 1;//0
		    if (e.getEnquiryDocs().get(li).getAttachments() == null
			|| e.getEnquiryDocs().get(li).getAttachments().isEmpty()
			) {
			error("Last revision does not have any documents.");
			return;
		    }

		    EnquiryDocs ed = new EnquiryDocs();
		    ed.setEnquiry(e);
		    ed.setRevesionNo(e.getEnquiryDocs()
			.size());
		    ed.setCreatedBy(UserDAO.getUser(
			CrmSession.get().getUserId()));
		    ed.setCreatedAt(new Date());
		    EnquiryDAO.revise(ed);
		    info("Created new revision!");
		    form.addOrReplace(new Label("revisionNo",
			Integer.toString(ed.getRevesionNo() + 1)));
		    if (target != null) {
			//target.addComponent(form);
			EnquiryDocsPanel.this.renderEnquiryDocs(target);
		    }
		}

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}

	    }).setOutputMarkupId(true));

	    List<EnquiryDocs> l = null;
	    int curRevNo = 0;

	    if (eq.getEnquiryDocs() != null) {
		l = eq.getEnquiryDocs();
		for (EnquiryDocs d : l) {
		    if (d.getRevesionNo() > curRevNo) {
			curRevNo = d.getRevesionNo();
		    }
		}
	    } else {
		l = new ArrayList<EnquiryDocs>();
	    }

	    final int curRev = curRevNo;

	    ListView<EnquiryDocs> list = new ListView<EnquiryDocs>("list", l) {
		@Override
		protected void populateItem(ListItem<EnquiryDocs> item) {
		    final EnquiryDocs doc = item.getModelObject();
		    item.add(new ListView<Attachments>("revList", 
			doc.getAttachments()) {

			@Override
			protected void populateItem(ListItem<Attachments> item) {
			    final Attachments a = item.getModelObject();

			    AttachmentLink aLink =
				new AttachmentLink("attachmentLink", a);
			    aLink.add(new Label("attachmentName",
				a.getAttachmentName()));
			    item.add(aLink);
			    item.add(new Label("dateTime", WebUtil
				.formatDate(WebUtil.dateTimeFormat,
				a.getCreatedAt())));
			    item.add(new Label("rep",
				a.getCreatedBy().getFullName()));
			    item.add(new Label("rev", 
				doc.getRevesionNo().toString()));
			    AjaxFallbackLink deleteLink =
				new AjaxFallbackLink("delete") {

				@Override
				public void onClick(AjaxRequestTarget target) {
				    Enquiry eq = EnquiryDAO.getEnquiry(enquiry);
				    AttachmentDAO.deleteAttachment(eq,
					a.getAttachmentId(),
					UserDAO.getUser(
					CrmSession.get().getUserId()));
				    EnquiryDocsPanel.this
					.renderEnquiryDocs(target);
				}
			    };

			    deleteLink.setOutputMarkupId(true);
			    deleteLink.setOutputMarkupPlaceholderTag(true);
			    item.add(deleteLink);

			    if (doc.getRevesionNo() != curRev) {
				deleteLink.setVisible(false);
			    }

			    if (! a.getCreatedBy().equals(
				UserDAO.getUser(
				CrmSession.get().getUserId()))) {
				deleteLink.setVisible(false);
			    }
			}
		    });
		}
	    };

	    Form addDocForm  = new Form("addDocForm");
	    addDocForm.setOutputMarkupId(true);

	    final MultiFileUploadField attachmentsComp =
		new MultiFileUploadField(
		"attachments", new PropertyModel<Collection<FileUpload>>(this,
		"attachments"), 5);

	    addDocForm.add(attachmentsComp);
	    addDocForm.add(new IndicatingAjaxButton("addDocsButton",
		addDocForm) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    if (enquiry == null) {
			return;
		    }

		    Enquiry e = EnquiryDAO.getEnquiry(enquiry);

		    if (e.isDead()) {
			return;
		    }

		    int size = e.getEnquiryDocs().size();
		    int li = size == 0 ? 0 : size - 1;//0
		    EnquiryDocs eDoc = e.getEnquiryDocs().get(li);

		    List<Attachments> attList = new ArrayList<Attachments>();
		    for (FileUpload fu : attachments) {
			Attachments att = new Attachments();
			att.setAttachmentName(fu.getClientFileName());
			att.setContentType(fu.getContentType());
			att.setAttachmentContent(fu.getBytes());
			att.setCreatedAt(new Date());
			att.setEnquiry(eDoc);
			att.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			attList.add(att);
		    }

		    AttachmentDAO.saveAttachment(e, attList);
		    EnquiryDocsPanel.this.renderEnquiryDocs(target);
		}
	    });
	    addOrReplace(feedback, reviseForm, list, addDocForm);
	    if (target != null) {
		EnquiryPage.this.doRender(target);
	    }
	}
    }
}

