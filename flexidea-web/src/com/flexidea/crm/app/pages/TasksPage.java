package com.flexidea.crm.app.pages;

import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.task.panels.TaskForm;
import com.flexidea.crm.app.task.panels.TaskListViewPanel;
import com.flexidea.crm.app.task.panels.TaskTileViewPanel;
import com.flexidea.crm.app.task.panels.TaskViewPanel;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dto.TaskListMode;
import java.util.Arrays;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.servlet.AbortWithHttpStatusException;

/**
 *
 * @author akshay
 */
public final class TasksPage extends WebPage {

    private Long taskId;
    private TaskListMode listMode = TaskListMode.TODAY;
    private boolean history = false;
    private Integer priority = 0;
    private Panel taskPanel;
    private Panel myTasks;
    private Panel monitoringTasks;
    private TaskForm taskForm;
    private ListView<TaskListMode> options;
    private ListView<Integer> priorities;
    private WebMarkupContainer optionsContainer;
    private WebMarkupContainer prioritiesContainer;
    private Label newTaskTab;
    private Label historyTab;

    private boolean tileMyTasks = true;
    private AjaxLink myTasksListMode;
    private String myTasksCaption = "List View";
    private String myTasksCaptionClass = "listViewCaption";
    private boolean tileMonitoring = true;
    private AjaxLink monitoringTasksListMode;
    private String monitoringTasksCaption = "List View";
    private String monitoringTasksCaptionClass = "listViewCaption";
    private Label attentionLabel;
    private String attentionLabelString;

    public TasksPage() {
        super();
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

        update(null);
    }

    public TasksPage(PageParameters params) {
	super(params);
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

        this.taskId = params.getAsLong("task");
        if (taskId != null && TasksDAO.getTask(taskId) == null) {
            throw new AbortWithHttpStatusException(404, true);
        }
        update(null);
    }

    private void updatePriorities(AjaxRequestTarget target) {

	if (prioritiesContainer == null) {
	    prioritiesContainer = new WebMarkupContainer("prioritiesContainer");
	    prioritiesContainer.setOutputMarkupId(true);
	    addOrReplace(prioritiesContainer);
	}

	priorities = new ListView<Integer>("priorities",
	    Arrays.asList(new Integer[]{0,1,2,3,4})) {

	    @Override
	    protected void populateItem(ListItem<Integer> item) {
		//throw new UnsupportedOperationException("Not supported yet.");
		final Integer p = item.getModelObject();
		String pValue = "";
		if (p == 0) {
		    pValue = "All";
		} else {
		    pValue = "Priority " + p;
		}

		Label li = new Label("item", pValue);
		li.add(new AjaxEventBehavior("onclick") {

		    @Override
		    protected void onEvent(AjaxRequestTarget itarget) {
			TasksPage.this.priority = p;
			TasksPage.this.update(itarget);
		    }
		});
		String className = "set_priority_" + p;
		if (p == priority) {
		    className += "priority_item_selected";
		}

		li.add(new AttributeModifier("class", true,
		    new Model<String>(className)));
		item.add(li);
	    }
	};

	priorities.setOutputMarkupId(true);
	prioritiesContainer.addOrReplace(priorities);

	if (target != null) {
	    target.addComponent(prioritiesContainer);
	}
    }

    private void updateOptions(AjaxRequestTarget target) {
	if (optionsContainer == null) {
	    optionsContainer = new WebMarkupContainer("optionsContainer");
	    optionsContainer.setOutputMarkupId(true);
	    addOrReplace(optionsContainer);
	}

	options = new ListView<TaskListMode>("options",
		Arrays.asList(TaskListMode.values())) {

	    @Override
	    protected void populateItem(ListItem<TaskListMode> item) {
		final TaskListMode mode = item.getModelObject();
		Label li = new Label("item", TaskListMode
			.displayValue.get(mode));
		li.add(new AjaxEventBehavior("onclick") {

		    @Override
		    protected void onEvent(AjaxRequestTarget itarget) {
			TasksPage.this.listMode = mode;
			attentionLabelString = TaskListMode
				.displayValue.get(listMode);
			attentionLabel.modelChanged();
			TasksPage.this.update(itarget);
		    }
		});

		String cssClass = "task_option";
		if (listMode.equals(mode)) {
		    cssClass += "task_option_selected";
		}
		li.add(new SimpleAttributeModifier("class", cssClass));
		item.add(li);
	    }
	};
	options.setOutputMarkupId(true);
	optionsContainer.addOrReplace(options);

	if (target != null) {
	    target.addComponent(optionsContainer);
	}
    }

    public void update(AjaxRequestTarget target) {
	attentionLabelString = TaskListMode.displayValue.get(listMode);
	if (attentionLabel == null) {
	    attentionLabel = new Label("attentionLabel",
		new PropertyModel<String>(this, "attentionLabelString"));
	    attentionLabel.setOutputMarkupId(true);
	    add(attentionLabel);
	} else {
	    attentionLabel.modelChanged();
	}

	updateOptions(target);
	updatePriorities(target);

        if (newTaskTab == null) {
            newTaskTab = new Label("newTaskTab", "New Task");
            newTaskTab.add(new AjaxEventBehavior("onclick") {

                @Override
                protected void onEvent(AjaxRequestTarget target) {
                    //throw new UnsupportedOperationException("Not supported yet.");
                    //taskForm = TaskForm.getForm("taskForm", taskId, null, null);
                    //TasksPage.this.addOrReplace(taskForm);
                    TasksPage.this.taskId = null;
                    newTaskTab.add(new AttributeModifier("class", true,
			new Model<String>("menutabH")));
                    historyTab.add(new AttributeModifier("class", true,
			new Model<String>("menutab")));
                    TasksPage.this.history = false;
                    TasksPage.this.update(target);
                }
            });
            //newTaskTab.add(new AttributeModifier("class", true, new Model<String>("menutab")));
            newTaskTab.setOutputMarkupId(true);
        }

        if (historyTab == null) {
            historyTab = new Label("historyTab", "History");
            historyTab.add(new AjaxEventBehavior("onclick") {

                @Override
                protected void onEvent(AjaxRequestTarget target) {
                    //throw new UnsupportedOperationException("Not supported yet.");
                    //taskForm = TaskForm.getForm("taskForm", taskId, null, null);
                    //TasksPage.this.addOrReplace(taskForm);
                    newTaskTab.add(new AttributeModifier("class", true,
			new Model<String>("menutab")));
                    historyTab.add(new AttributeModifier("class", true,
			new Model<String>("menutabH")));
                    TasksPage.this.history = true;
                    TasksPage.this.update(target);
                }
            });
            
            historyTab.setOutputMarkupId(true);
        }

        taskPanel = new EmptyPanel("taskPanel");
        Long userId = CrmSession.get().getUserId();

	myTasksListMode = new AjaxLink("myTasksListMode") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		tileMyTasks = !tileMyTasks;

		if (tileMyTasks && history) {
		    tileMyTasks = false;
		    return;
		}

		update(target);
	    }
	};

        myTasksListMode.setOutputMarkupId(true);
        myTasksListMode.setOutputMarkupPlaceholderTag(true);
	
	if (tileMyTasks && ! history) {
	    myTasks = new TaskTileViewPanel("myTasks", listMode, userId,
		priority, false);
	    myTasksCaption = "List View";
	    myTasksCaptionClass = "listViewCaption";
	} else {
	    tileMyTasks = false;
	    myTasks = new TaskListViewPanel("myTasks", listMode, history, 
		userId, priority, false);
	    myTasksCaption = "Tile View";
	    myTasksCaptionClass = "tileViewCaption";
	}

	Label myTaskCaption = new Label("caption", myTasksCaption);
	myTaskCaption.add(new AttributeModifier("class", true, 
	    new Model<String>(myTasksCaptionClass)));
	myTasksListMode.add(myTaskCaption);

	monitoringTasksListMode = new AjaxLink("monitoringTasksListMode") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		tileMonitoring = !tileMonitoring;

		if (tileMonitoring && history) {
		    tileMonitoring = false;
		    return;
		}

		update(target);
	    }
	};
        monitoringTasksListMode.setOutputMarkupId(true);
        monitoringTasksListMode.setOutputMarkupPlaceholderTag(true);

	if (tileMonitoring && ! history) {
	    monitoringTasks = new TaskTileViewPanel("monitoringTasks",
		listMode, userId, priority, true);
	    monitoringTasksCaption = "List View";
	    monitoringTasksCaptionClass = "listViewCaption";
	} else {
	    tileMonitoring = false;
	    monitoringTasks = new TaskListViewPanel("monitoringTasks", listMode,
                history, userId, priority, true);
	    monitoringTasksCaption = "Tile View";
	    monitoringTasksCaptionClass = "tileViewCaption";
	}

	Label monitoringTaskCaption = new Label("caption", monitoringTasksCaption);
	monitoringTaskCaption.add(new AttributeModifier("class", true,
	    new Model<String>(monitoringTasksCaptionClass)));
	monitoringTasksListMode.add(monitoringTaskCaption);

        taskForm = TaskForm.getForm("taskForm", taskId, null, null);

        taskPanel.setOutputMarkupId(true);
        myTasks.setOutputMarkupId(true);
        monitoringTasks.setOutputMarkupId(true);
        taskForm.setOutputMarkupId(true);

	myTasksListMode.setOutputMarkupId(true);
	monitoringTasksListMode.setOutputMarkupId(true);

        if (history) {
            myTasksListMode.setVisible(false);
            monitoringTasksListMode.setVisible(false);
        } else {
            myTasksListMode.setVisible(true);
            monitoringTasksListMode.setVisible(true);
        }
        addOrReplace(taskPanel, myTasks, monitoringTasks, taskForm, 
	    newTaskTab, historyTab, myTasksListMode, monitoringTasksListMode);

        if (target != null) {
            target.addComponent(taskPanel);
            target.addComponent(myTasks);
            target.addComponent(monitoringTasks);
            target.addComponent(taskForm);
            target.addComponent(newTaskTab);
            target.addComponent(historyTab);
            target.addComponent(myTasksListMode);
            target.addComponent(monitoringTasksListMode);
            target.addComponent(attentionLabel);
        }
    }

    public void showTask(AjaxRequestTarget target, Long iTaskId) {
        this.taskId = iTaskId;
        taskPanel = new TaskViewPanel("taskPanel", iTaskId);
        taskForm = TaskForm.getForm("taskForm", taskId, null, null);

        addOrReplace(taskPanel, taskForm);
        if (target != null) {
            target.addComponent(taskPanel);
            target.addComponent(taskForm);
        }
    }

    public void hideTask(AjaxRequestTarget target) {
        this.taskId = null;
	this.update(target);
    }
}
