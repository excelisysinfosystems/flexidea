package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.AjaxPagingNavigatorWithImages;
import com.flexidea.crm.app.common.panel.CommentsPanel;
import com.flexidea.crm.app.common.panel.ContactsAssociationPanel;
import com.flexidea.crm.app.common.panel.MarkAsDeadPanel;
import com.flexidea.crm.app.common.panel.SearchPanel;
import com.flexidea.crm.app.common.panel.data.LeadsDataProvider;
import com.flexidea.crm.db.dao.LeadsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.Project;
import java.io.Serializable;
import java.util.Date;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.servlet.AbortWithHttpStatusException;

/**
 *
 * @author akshay
 */
public final class LeadsPage extends WebPage {
    private Long lead;
    private SearchPanel searchPanel;
    private ContactsAssociationPanel associationPanel;
    private Fragment commentsFragment;
    private boolean showHistory = false;
    private MarkAsDeadPanel commentDialog;

    public LeadsPage() {
        super ();

	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	doRender(null);
    }

    public LeadsPage(PageParameters params) {
	super(params);
	if (! CrmSession.doAuthentication(this)) {
	    return;
	}

	this.lead = params.getLong("lead");

	try {
	    if (LeadsDAO.getLead(lead) == null)
		throw new AbortWithHttpStatusException(404, true);
	} catch (Exception e) {
	    throw new AbortWithHttpStatusException(404, true);
	}

	doRender(null);
    }

    private void doRender(AjaxRequestTarget target) {
	setOutputMarkupId(true);

	if (commentDialog == null) {
	    commentDialog = new MarkAsDeadPanel("commentDialog", "") {

		@Override
		public void onDone(AjaxRequestTarget target, String comment) {
		    Lead l = null;

		    if (lead == null) {
			return;
		    } else {
			l = LeadsDAO.getLead(lead);
		    }

		    if (l == null) {
			return;
		    } else {
			l.setIsDead(true);
			l.setComment(comment);
			l.setModifiedAt(new Date());
			l.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
		    }

		    LeadsDAO.update(l);
		    info("Lead marked as Dead!");

		    LeadsPage.this.setLead(l.getId());
		    doRender(target);
		}
	    };
	    add(commentDialog);
	}

	if (searchPanel == null) {
	    searchPanel = new SearchPanel("searchPanel", CommentsTarget.LEADS);
	    searchPanel.setOutputMarkupId(true);
	    addOrReplace(searchPanel);
	} else {
	    searchPanel.update(target);
	}

	associationPanel = new ContactsAssociationPanel("associatedContacts",
	    CommentsTarget.LEADS, lead);
	associationPanel.setOutputMarkupId(true);

	if (lead != null) {
	    commentsFragment = new Fragment("commentsContainer",
		"commentsFragment");
	    commentsFragment.add(
		new CommentsPanel("commentsPanel", CommentsTarget.LEADS,
		lead));
	} else {
	    commentsFragment = new Fragment("commentsContainer", "gyaanFragment");
	}

	commentsFragment.setOutputMarkupId(true);
	commentsFragment.setOutputMarkupPlaceholderTag(true);

	AjaxLink newLeadButton =  new AjaxLink("newLeadButton") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		LeadsPage.this.setLead(null);
		LeadsPage.this.showHistory = false;
		LeadsPage.this.doRender(target);
	    }
	};
	newLeadButton.setOutputMarkupId(true);

	AjaxLink showHistoryButton =  new AjaxLink("showHistoryButton") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		LeadsPage.this.setLead(null);
		LeadsPage.this.showHistory = true;
		LeadsPage.this.doRender(target);
	    }
	};
	showHistoryButton.setOutputMarkupId(true);

	LeadForm newLeadForm = new LeadForm("newLeadForm", lead);
	newLeadForm.setOutputMarkupId(true);
	newLeadForm.setOutputMarkupPlaceholderTag(true);

	WebMarkupContainer leadHistory = new WebMarkupContainer("leadsHistory");
	//history begins
	leadHistory.addOrReplace(new LeadsHistoryPanel("historyTable", showHistory));
	leadHistory.setOutputMarkupId(true);
	leadHistory.setOutputMarkupPlaceholderTag(true);
	addOrReplace(leadHistory);

	if (target != null) {
	    target.addComponent(leadHistory);
	}
	// history ends

	if (showHistory) {
	    newLeadButton.add(new SimpleAttributeModifier("class", "menutab"));
	    showHistoryButton.add(new SimpleAttributeModifier("class", "menutabH"));
	    leadHistory.setVisible(true);
	    commentsFragment.setVisible(false);
	    newLeadForm.setVisible(false);
	} else {
	    if (lead != null) {
		newLeadButton.add(new SimpleAttributeModifier("class", "menutab"));
	    } else {
		newLeadButton.add(new SimpleAttributeModifier("class", "menutabH"));
	    }
	    showHistoryButton.add(new SimpleAttributeModifier("class", "menutab"));
	    leadHistory.setVisible(false);
	    commentsFragment.setVisible(true);
	    newLeadForm.setVisible(true);
	}

	Label notConverted = null;
	BookmarkablePageLink converted = null;

	if (lead == null) {
	    converted = new BookmarkablePageLink("converted", LandingPage.class);
	    converted.setOutputMarkupId(true);
	    converted.setOutputMarkupPlaceholderTag(true);
	    converted.setEnabled(false);
	    converted.setVisible(false);
	    notConverted = new Label("notConverted", "Not yet converted");
	    notConverted.setOutputMarkupId(true);
	    notConverted.setOutputMarkupPlaceholderTag(true);
	    notConverted.setVisible(true);
	} else {
	    final Lead lObj = LeadsDAO.getLead(lead);
	    if (lObj.isIsConverted()) {
		PageParameters pp = new PageParameters();
		pp.put("project", lObj.getProject().getId());
		converted = new BookmarkablePageLink("converted", ProjectsPage.class, pp);
		converted.setOutputMarkupId(true);
		converted.setOutputMarkupPlaceholderTag(true);
		notConverted = new Label("notConverted", "Not yet converted");
		notConverted.setOutputMarkupId(true);
		notConverted.setOutputMarkupPlaceholderTag(true);
		notConverted.setVisible(false);
	    } else {
		converted = new BookmarkablePageLink("converted",
		    LandingPage.class);
		converted.setOutputMarkupId(true);
		converted.setOutputMarkupPlaceholderTag(true);
		converted.setEnabled(false);
		converted.setVisible(false);
		notConverted = new Label("notConverted", "Not yet converted");
		notConverted.setOutputMarkupId(true);
		notConverted.setOutputMarkupPlaceholderTag(true);
		notConverted.setVisible(true);
	    }
	}

	addOrReplace(newLeadButton, newLeadForm, associationPanel,
	    commentsFragment, converted, 
	    notConverted, showHistoryButton);

	if (target != null) {
	    target.addComponent(newLeadButton);
	    target.addComponent(newLeadForm);
	    target.addComponent(associationPanel);
	    target.addComponent(commentsFragment);
	    //target.addComponent(searchPanel);
	    target.addComponent(converted);
	    target.addComponent(notConverted);
	}
    }

    private class LeadForm extends Form implements Serializable {
	private Long lead;
	private String leadName;

	public LeadForm(String id, Long iLead) {
	    super(id);
	    setOutputMarkupId(true);
	    this.lead = iLead;

	    String leadId = "Not yet saved";
	    if (lead != null) {
		Lead l = LeadsDAO.getLead(lead);
		if (l != null) {
		    leadName = l.getLeadName();
		    lead = l.getId();
		    leadId = l.getId().toString();
		} else {
		    lead = null;
		    leadName = "";
		}
	    }

	    final FeedbackPanel feedback = new FeedbackPanel("feedback");
	    feedback.setOutputMarkupId(true);

	    TextField leadNameField = new TextField("leadName",
		new PropertyModel<String>(this, "leadName"));

	    /*
	    AjaxEditableLabel leadNameField = new AjaxEditableLabel("leadName",
		new PropertyModel<String>(this, "leadName")){

		@Override
		protected void onSubmit(AjaxRequestTarget target) {
		    super.onSubmit(target);
		    info("Label name=" + this.getDefaultModelObjectAsString());
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}
	    };
	     *
	     */

	    leadNameField.setRequired(true);
	    leadNameField.setOutputMarkupId(true);

	    IndicatingAjaxButton saveButton =
		new IndicatingAjaxButton("saveButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    boolean newLead = false;
		    Lead l = null;

		    if (lead == null) {
			l = new Lead();
			l.setLeadName(leadName);
			l.setIsConverted(false);
			l.setIsDead(false);
			l.setCreatedAt(new Date());
			l.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			newLead = true;
			System.out.println("DEBUG: Fresh Lead P1");
		    } else {
			l = LeadsDAO.getLead(lead);
			System.out.println("DEBUG: Lead=" + l);
		    }

		    boolean dup = false;

		    if (l == null) {
			l = new Lead();
			l.setLeadName(leadName);
			l.setIsConverted(false);
			l.setIsDead(false);
			l.setCreatedAt(new Date());
			l.setCreatedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
			newLead = true;
			System.out.println("DEBUG: Fresh Lead P2");
		    } else if (LeadsDAO.getLead(leadName) != null) {
			error("Lead '" + leadName + "' already exists.");
			l = null;
			System.out.println("DEBUG: Duplicate Lead 1");
			dup = true;
		    } else {
			l.setLeadName(leadName);
			l.setModifiedAt(new Date());
			l.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));
		    }

		    if (newLead && LeadsDAO.getLead(leadName) != null) {
			error("Lead '" + leadName + "' already exists.");
			newLead = false;
			System.out.println("DEBUG: Duplicate Lead 2");
			dup = true;
		    } else if (! dup) {
			System.out.println("DEBUG: Updating lead");
			LeadsDAO.update(l);
			info("Lead updated!");
		    }

		    if (newLead) {
			LeadsPage.this.setLead(l.getId());
		    }

		    if (target != null) {
			target.addComponent(feedback);
		    }

		    if (newLead) {
			LeadsPage.this.doRender(target);
		    }
		}
	    };

	    IndicatingAjaxButton deadButton =
		new IndicatingAjaxButton("deadButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    commentDialog.open(target, "Mark Lead '"
			+ leadName + "' as Dead?");
		}
	    };

	    IndicatingAjaxButton projectButton =
		new IndicatingAjaxButton("projectButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    Lead l = null;

		    if (lead == null) {
			return;
		    } else {
			l = LeadsDAO.getLead(lead);
		    }

		    if (l == null) {
			return;
		    } else if (l.isIsConverted()) {
			error("Lead is already converted into a project");
		    } else if (l.isIsDead()) {
			error("Dead leads cannot be converted into projects");
		    } else if (ProjectDAO.getProject(l.getLeadName()) != null) {
			error("A project with name "
			    + l.getLeadName() + " already exists.");
		    } else {
			l.setIsConverted(true);
			l.setModifiedAt(new Date());
			l.setModifiedBy(UserDAO.getUser(
			    CrmSession.get().getUserId()));

			LeadsDAO.update(l);
			ProjectDAO.update(new Project()
			    .setCreatedAt(new Date())
			    .setCreatedBy(UserDAO
				.getUser(
				    CrmSession.get().getUserId()))
			    .setIsDead(false)
			    .setLead(l)
			    .setProjectName(l.getLeadName()));

			//info("Lead converted to project!");
			l = LeadsDAO.getLead(l.getId());

			LeadsPage.this.setLead(l.getId());

			PageParameters params = new PageParameters();
			params.put("project", l.getProject().getId());
			setResponsePage(ProjectsPage.class, params);
		    }

		    if (target != null) {
			//target.addComponent(feedback);
			LeadsPage.this.doRender(target);
		    }
		}
	    };

	    deadButton.setOutputMarkupPlaceholderTag(true);
	    projectButton.setOutputMarkupPlaceholderTag(true);
	    saveButton.setOutputMarkupPlaceholderTag(true);

	    if (lead == null) {
		deadButton.setVisible(false);
		projectButton.setVisible(false);
	    } else {
		Lead l = LeadsDAO.getLead(lead);
		if (l.isIsConverted() || l.isIsDead()) {
		    deadButton.setVisible(false);
		    projectButton.setVisible(false);
		    saveButton.setVisible(false);
		}
	    }

	    add(new Label("leadID", leadId));
	    add(feedback, leadNameField, saveButton, deadButton, projectButton);
	}

	public Long getLead() {
	    return lead;
	}

	public void setLead(Long lead) {
	    this.lead = lead;
	}

	public String getLeadName() {
	    return leadName;
	}

	public void setLeadName(String leadName) {
	    this.leadName = leadName;
	}
    }

    public Long getLead() {
	return lead;
    }

    public void setLead(Long lead) {
	this.lead = lead;
    }
}

