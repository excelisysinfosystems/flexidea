package com.flexidea.crm.app.pages;

import com.flexidea.crm.app.admin.panels.ManageOptionFormPanel;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.panel.ContactsAssociationPanel;
import com.flexidea.crm.app.common.panel.SearchPanel;
import com.flexidea.crm.app.contacts.panels.ContactFormPanel;
import com.flexidea.crm.app.contacts.panels.UploadContactsPanel;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.LeadsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.Project;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.servlet.AbortWithHttpStatusException;

/**
 * 
 * 
 * @author akshay
 */
public final class AdminPage extends WebPage {

	private ManageOptionFormPanel manageOptionFormPanel;

	private Label mngOptions;

	public AdminPage() {
		super();
		if (!CrmSession.doAuthentication(this)) {
			return;
		}

		doRender();
	}

	public AdminPage(PageParameters params) {
		super(params);

		if (!CrmSession.doAuthentication(this)) {
			return;
		}

		doRender();
	}

	private void doRender() {

		setOutputMarkupId(true);
		
		AdminPage.this.manageOptionFormPanel = new ManageOptionFormPanel("manageOptionFormPanel");

		AdminPage.this.add(manageOptionFormPanel);		

		if (mngOptions == null) {
			mngOptions = new Label("mngOptions", "Manage Options");
			mngOptions.setOutputMarkupId(true);
			mngOptions.add(new AjaxEventBehavior("onclick") {
				@Override
				protected void onEvent(AjaxRequestTarget target) {

					AdminPage.this.manageOptionFormPanel = new ManageOptionFormPanel("manageOptionFormPanel");

					AdminPage.this.addOrReplace(manageOptionFormPanel);
				}
			});

			add(mngOptions);
		}

	}

	public ManageOptionFormPanel getManageOptionFormPanel() {
		return manageOptionFormPanel;
	}

	public void setManageOptionFormPanel(ManageOptionFormPanel manageOptionFormPanel) {
		this.manageOptionFormPanel = manageOptionFormPanel;
	}

	public Label getMngOptions() {
		return mngOptions;
	}

	public void setMngOptions(Label mngOptions) {
		this.mngOptions = mngOptions;
	}
}