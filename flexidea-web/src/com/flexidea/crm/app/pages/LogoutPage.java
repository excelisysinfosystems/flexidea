/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.pages;
import com.flexidea.crm.app.base.CrmSession;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;

/**
 *
 * @author akshay
 */
public final class LogoutPage extends WebPage {
    public LogoutPage() {
        super ();
	CrmSession.get().invalidate();
	setRedirect(true);
	setResponsePage(Login.class);
    }

    public LogoutPage(PageParameters params) {
        //TODO:  process page parameters
	CrmSession.get().invalidate();
	setRedirect(true);
	setResponsePage(Login.class);
    }
}

