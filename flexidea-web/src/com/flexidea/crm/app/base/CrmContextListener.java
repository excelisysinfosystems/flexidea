/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.base;

import com.flexidea.crm.db.util.ReminderService;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 * @author akshay
 */
public class CrmContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
	ReminderService.start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
	ReminderService.stop();
    }
}
