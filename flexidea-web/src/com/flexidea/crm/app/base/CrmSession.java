/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.base;

import com.flexidea.crm.app.pages.Login;
import java.io.Serializable;
import org.apache.wicket.Application;
import org.apache.wicket.Request;
import org.apache.wicket.RequestCycle;
import org.apache.wicket.Session;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WebSession;

/**
 *
 * @author akshay
 */
public class CrmSession extends WebSession implements Serializable {
    private Long userId;

    public CrmSession(Request request) {
	super(request);
	System.out.println("DEBUG: Creating new session. id=" + this.getId());
    }

    public CrmSession(WebApplication application, Request request) {
	super(application, request);
	System.out.println("DEBUG: Creating new session. id=" + this.getId());
    }

    public CrmSession(Application application, Request request) {
	super(application, request);
	System.out.println("DEBUG: Creating new session. id=" + this.getId());
    }


    public Long getUserId() {
	return userId;
    }

    public CrmSession setUserId(Long userId) {
	this.userId = userId;
	return this;
    }

    public boolean isUserLoggedIn() {
	System.out.println("DEBUG: user=" + userId + ", session=" + getId());
	return (userId != null);
    }

    public static CrmSession get() {
	Session.get().bind();
	CrmSession session = (CrmSession) Session.get();
	if (session == null) {
	    System.out.println("DEBUG: GET: NULL Session.");
	    RequestCycle.get().setRedirect(true);
	    RequestCycle.get().setResponsePage(Login.class);
	}
	return session;
    }

    public static boolean doAuthentication(WebPage page) {
	CrmSession session = null;
	if ((session = get()) == null
	    || ! session.isUserLoggedIn()) {
	    page.setResponsePage(Login.class);
	    return false;
	}
	return true;
    }
}
