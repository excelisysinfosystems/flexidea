/*
 * CRMApplication.java
 *
 * Created on 5 October, 2010, 6:00 PM
 */
 
package com.flexidea.crm.app.base;           

import com.flexidea.crm.app.pages.ContactsPage;
import com.flexidea.crm.app.pages.EnquiryPage;
import com.flexidea.crm.app.pages.LandingPage;
import com.flexidea.crm.app.pages.LeadsPage;
import com.flexidea.crm.app.pages.Login;
import com.flexidea.crm.app.pages.LogoutPage;
import com.flexidea.crm.app.pages.ProjectsPage;
import com.flexidea.crm.app.pages.ReportsPage;
import com.flexidea.crm.app.pages.TasksPage;
import org.apache.wicket.Request;
import org.apache.wicket.Response;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.time.Duration;
/** 
 *
 * @author akshay
 * @version 
 */

public class CRMApplication extends WebApplication {

    public CRMApplication() {
	mountBookmarkablePage("/browse/enquiries", EnquiryPage.class);
	mountBookmarkablePage("/browse/contacts", ContactsPage.class);
	mountBookmarkablePage("/browse/leads", LeadsPage.class);
	mountBookmarkablePage("/browse/projects", ProjectsPage.class);
	mountBookmarkablePage("/browse/tasks", TasksPage.class);
	mountBookmarkablePage("/reports", ReportsPage.class);
	mountBookmarkablePage("/login", Login.class);
	mountBookmarkablePage("/logout", LogoutPage.class);

    }

    @Override
    protected void init() {
	super.init();
	getMarkupSettings().setStripWicketTags(true);
	if (DEVELOPMENT.equalsIgnoreCase(getConfigurationType())) {
	    getResourceSettings().setResourcePollFrequency(Duration.ONE_SECOND);
	    getDebugSettings().setComponentUseCheck(true);
	    getMarkupSettings().setStripWicketTags(false);
	} else if (DEPLOYMENT.equalsIgnoreCase(getConfigurationType())) {
	    getResourceSettings().setResourcePollFrequency(null);
	    getDebugSettings().setComponentUseCheck(false);
	    getMarkupSettings().setStripWicketTags(true);
	    getDebugSettings().setAjaxDebugModeEnabled(false);
	}
	//getDebugSettings().setComponentUseCheck(true);
	//getDebugSettings().setAjaxDebugModeEnabled(true);
    }

    @Override
    public Class getHomePage() {
	return LandingPage.class;
    }

    @Override
    public Session newSession(Request request, Response response) {
	return new CrmSession(request);
    }

    @Override
    public void sessionDestroyed(String sessionId) {
	super.sessionDestroyed(sessionId);
	System.out.println("DEBUG: session destroyed! id=" + sessionId);
    }
}
