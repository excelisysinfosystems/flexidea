package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.EnquiryDAO;
import com.flexidea.crm.db.dao.LeadsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.TaskStatus;
import com.flexidea.crm.db.dto.Tasks;
import com.flexidea.crm.util.WebUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

/**
 *
 * @author akshay
 */
public final class FollowupTaskPanel extends Panel {
    private CommentsTarget target;
    private Long targetId;

    private Date on;
    private Contact targetContact;
    private String description;
    private CrmUser owner;
    private CrmUser to;

    public FollowupTaskPanel(String id, CommentsTarget iTarget, Long iTargetId,
	CrmUser iOwner, CrmUser iTo, final ModalWindow parentWindow) {

        super (id);
	this.target = iTarget;
	this.targetId = iTargetId;
	this.owner = iOwner;
	this.to = iTo;

	Form taskForm = new Form("taskForm");

	final FeedbackPanel feedback = new FeedbackPanel("taskFeedback");
	feedback.setOutputMarkupId(true);

	Label toName = new Label("toName", to.getFullName());

	List<Contact> tc = ContactsDAO.listContacts(1);
	//	.listAssociatedContacts(target, targetId);
	DropDownChoice<Contact> targetContactDD =
	    new DropDownChoice<Contact>("target",
	    new PropertyModel<Contact>(FollowupTaskPanel.this,
	    "targetContact"), tc,
	    new ChoiceRenderer<Contact>("contactName", "id"));

	if (target.equals(CommentsTarget.CONTACTS)) {
	    targetContactDD.setRequired(true);
	} else {
	    targetContactDD.setRequired(false);
	}

	TextArea desc = new TextArea("description",
	    new PropertyModel<String>(FollowupTaskPanel.this, "description"));
	desc.setRequired(true);

	DateTextField onDate = new DateTextField("on",
	    new PropertyModel<Date>(FollowupTaskPanel.this, "on"),
	    WebUtil.parseableDateTimeFormat
	   //new PatternDateConverter("dd MMM yyyy", true)
	);

	onDate.setMarkupId("on");
	onDate.setOutputMarkupId(true);
	onDate.setRequired(true);

	AjaxFallbackButton saveButton = new AjaxFallbackButton("saveButton",
	    taskForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget atarget, Form<?> form) {
		if (atarget != null) {
		    atarget.addComponent(feedback);
		}
		Tasks t = new Tasks();
		t.setActive(true);
		t.setAssignedBy(owner);
		t.setCreatedAt(new Date());
		t.setCreatedBy(owner);
		t.setDelegatedTo(to);
		t.setDescription(description);


		Calendar cal = Calendar.getInstance();
		cal.setTime(on);
		//cal.set(Calendar.HOUR, cal.getMinimum(Calendar.HOUR));
		//cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
		//cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
		t.setStart(cal.getTime());
		cal.set(Calendar.HOUR, cal.getMaximum(Calendar.HOUR));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		t.setEnd(cal.getTime());

		t.setOwner(owner);
		t.setPriority(3);
		t.setSource(target.toString());
		t.setStatus(TaskStatus.OPEN);
		t.setTargetContact(targetContact);

		switch (target) {
		    case CONTACTS:
			t.setTitle("Follow up "
			    + targetContact.getContactName());
			/*
			t.setTitle(to.getFullName() + " to follow up " 
			    + ContactsDAO.getContact(targetId).getContactName()
			    + (targetContact != null ? 
				" for " + targetContact.getContactName() : ""));
			 *
			 */
			break;
		    case ENQUIRY:
			t.setTargetEnquiry(EnquiryDAO.getEnquiry(targetId));
			t.setTitle("Follow up Enquiry "
			    + t.getTargetEnquiry().getEnquiryName()
			    + (targetContact != null ?
				" with " + targetContact.getContactName() : ""));
			break;
		    case LEADS:
			t.setTargetLead(LeadsDAO.getLead(targetId));
			t.setTitle("Follow up Lead "
			    + t.getTargetLead().getLeadName()
			    + (targetContact != null ?
				" with " + targetContact.getContactName() : ""));
			break;
		    case PROJECT:
			t.setTargetProject(ProjectDAO.getProject(targetId));
			t.setTitle("Follow up Project "
			    + t.getTargetProject().getProjectName()
			    + (targetContact != null ?
				" with " + targetContact.getContactName() : ""));
			break;
		}

		TasksDAO.update(t, new ArrayList<TaskReminders>());
		info("Follow up saved!");
		parentWindow.close(atarget);
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(feedback);
		}
	    }

	};

	taskForm.add(feedback, saveButton, onDate, desc, targetContactDD, toName);
	add(taskForm);
    }
}
