package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.app.pages.TasksPage;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dto.Tasks;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public final class TaskLinkPanel extends Panel {
    boolean showColor = true;

    public TaskLinkPanel(String id, final String taskTitle,
	final Long taskId, boolean showColor) {
        super (id);
	this.showColor = showColor;

	AjaxFallbackLink taskLink = new AjaxFallbackLink("taskLink") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		if (getPage() instanceof TasksPage) {
		    TasksPage tasksPage = (TasksPage) getPage();
		    tasksPage.showTask(target, taskId);
		}
	    }
	};

	Label title = new Label("title", taskTitle);
        Tasks t = TasksDAO.getTask(taskId);
	if (showColor) {
	    switch (t.getStatus()) {
		case COMPLETE:
		    title.add(new AttributeModifier("class", true,
			new Model<String>("task_complete")));
		    break;
		case OPEN:
		    title.add(new AttributeModifier("class", true,
			new Model<String>("task_open_priority" + t.getPriority())));
		    break;
		case REJECTED:
		    title.add(new AttributeModifier("class", true,
			new Model<String>("task_rejected")));
		    break;
	    }
	}

	taskLink.add(title);
	add(taskLink);
    }

    public TaskLinkPanel(String id, final String taskTitle, final Long taskId) {
        this(id, taskTitle, taskId, true);
    }
}
