package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.panel.CommentsPanel;
import com.flexidea.crm.app.pages.TasksPage;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.TaskStatus;
import com.flexidea.crm.db.dto.Tasks;
import java.util.ArrayList;
import java.util.Date;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public final class TaskViewPanel extends Panel {
    private Long taskId;

    public TaskViewPanel(String id, Long iTaskId) {
        super (id);
	this.taskId = iTaskId;
	setOutputMarkupId(true);
	update(null);
    }

    public void update(AjaxRequestTarget target) {
	Tasks t = TasksDAO.getTask(taskId);

	Label assignmentLabel = null;
	Label owner = null;
	Label description = new Label("description", t.getDescription());
	Label title = new Label("title", t.getTitle());
	Label status = null;

	if (t.getDelegatedTo() != null) {
	    assignmentLabel = new Label("assignmentLabel", "Delegated To");
	    owner = new Label("owner", t.getDelegatedTo().getFullName());
	} else {
	    assignmentLabel = new Label("assignmentLabel", "Assigned To");
	    owner = new Label("owner", t.getOwner().getFullName());
	}

	ListView<TaskReminders> reminders = new ListView<TaskReminders>(
	    "reminders", t.getReminders()) {

	    @Override
	    protected void populateItem(ListItem<TaskReminders> item) {
		final TaskReminders r = item.getModelObject();
		String l = "";
		String rClass = "";

		switch (r.getReminderType()) {
		    case SMS:
			l = "Message ";
			rClass = "r_sms";
			break;
		    case EMAIL:
			l = "Email ";
			rClass = "r_email";
			break;
		}

		l += r.getDuration();

		l += " " + r.getDurationMeasure().toString();

		Label reminder = new Label("reminder", l);
                Form form = new Form("form");
		reminder.add(new AttributeModifier("class", true,
		    new Model<String>(rClass)));

		form.add(reminder);
		AjaxFallbackButton removeButton = new AjaxFallbackButton(
		    "removeButton", form) {

		    @Override
		    protected void onSubmit(AjaxRequestTarget target,
			Form<?> form) {
			TasksDAO.removeReminder(r);
			TaskViewPanel.this.update(target);
		    }
		};

		removeButton.setOutputMarkupId(true);
		removeButton.setOutputMarkupPlaceholderTag(true);
		form.add(removeButton);
                item.add(form);
	    }
	};

	final ModalWindow commentsWindow = new ModalWindow("commentsWindow");
	commentsWindow.setOutputMarkupId(true);
	commentsWindow.setOutputMarkupPlaceholderTag(true);

	/*
	commentsWindow.setCloseButtonCallback(new ModalWindow.CloseButtonCallback() {

	    @Override
	    public boolean onCloseButtonClicked(AjaxRequestTarget target) {
		update(target);
		return true;
	    }
	});
	 *
	 */
	commentsWindow.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {

	    @Override
	    public void onClose(AjaxRequestTarget target) {
		//update(target);
		((TasksPage) target.getPage()).update(target);
	    }
	});

	Form taskViewForm = new Form("taskViewForm");

	AjaxButton rejectButton = new AjaxButton("rejectButton", taskViewForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		CrmUser me = UserDAO
		    .getUser(CrmSession.get().getUserId());
		Tasks t = TasksDAO.getTask(taskId);

		if (t != null) {
		    if (t.getOwner().getUserId()
			.equals(me.getUserId())
			&& t.getDelegatedTo() == null) {

			t.setStatus(TaskStatus.REJECTED);
			t.setModifiedAt(new Date());
			t.setModifiedBy(me);
			TasksDAO.update(t,
			    new ArrayList<TaskReminders>());
			return;
		    }
		} else {
		    return;
		}

		commentsWindow.setContent(new TaskCommentPanel(
		    commentsWindow.getContentId(), taskId,
		    TaskViewPanel.this, true, commentsWindow));
		commentsWindow.setTitle("Rejection comments");
		commentsWindow.show(target);
	    }
	};

	AjaxButton doneButton = new AjaxButton("doneButton", taskViewForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		commentsWindow.setContent(new TaskCommentPanel(
		    commentsWindow.getContentId(), taskId,
		    TaskViewPanel.this, false, commentsWindow));
		commentsWindow.setTitle("Completion comments");
		commentsWindow.show(target);
	    }
	};

	doneButton.setOutputMarkupPlaceholderTag(true);
	rejectButton.setOutputMarkupPlaceholderTag(true);

	taskViewForm.add(doneButton, rejectButton);

	status = new Label("status", t.getStatusString());

	switch (t.getStatus()) {
	    case COMPLETE:
	    case REJECTED:
		doneButton.setVisible(false);
		rejectButton.setVisible(false);
		break;
	}

	CommentsPanel comments = new CommentsPanel("comments",
	    CommentsTarget.TASK, taskId);

	AjaxFallbackLink close = new AjaxFallbackLink("close") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		((TasksPage) getPage()).hideTask(target);
	    }
	};

	addOrReplace(comments, taskViewForm, status, title, commentsWindow,
	    assignmentLabel, owner, description, reminders, close);

	if (target != null) {
	    target.addComponent(this);
	}
    }
}
