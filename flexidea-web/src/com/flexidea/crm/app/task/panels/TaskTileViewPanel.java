package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.AjaxPagingNavigatorWithImages;
import com.flexidea.crm.app.common.panel.data.TasksDataProvider;
import com.flexidea.crm.app.pages.TasksPage;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.TaskListMode;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.TaskStatus;
import com.flexidea.crm.db.dto.Tasks;
import com.flexidea.crm.util.WebUtil;
import java.util.ArrayList;
import java.util.Date;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.GridView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

/**
 *
 * @author akshay
 */
public final class TaskTileViewPanel extends Panel {
    private TaskListMode mode;
    private Long user;
    private Integer priority;
    private boolean monitoring;

    public TaskTileViewPanel(String id, TaskListMode mode,
	Long user, Integer priority, boolean monitoring) {
	super(id);
	this.mode = mode;
	this.user = user;
	this.priority = priority;
	this.monitoring = monitoring;
	update();
    }

    private void update() {
	/*
	List<IColumn<Tasks>> columns = new ArrayList<IColumn<Tasks>>();

	columns.add(new AbstractColumn<Tasks>(new Model<String>("")) {

	    @Override
	    public void populateItem(Item<ICellPopulator<Tasks>> cellItem,
		String componentId, IModel<Tasks> rowModel) {
		Tasks t = rowModel.getObject();
		cellItem.add(new ViewFragment(componentId, "viewFragment", t));
	    }

            @Override
            public String getCssClass() {
                return "taskTileViewItem";
            }

	});

	AjaxFallbackDefaultDataTable<Tasks> dataTable =
	    new AjaxFallbackDefaultDataTable<Tasks>("dataTable", columns,
	    new TasksDataProvider(mode, false, user, priority, monitoring),
	    20);
	 *
	 */
	GridView<Tasks> dataTable = new GridView<Tasks>("dataTable",
	    new TasksDataProvider(mode, false, user, priority, monitoring)) {

	    @Override
	    protected void populateEmptyItem(Item<Tasks> item) {
		item.add(new EmptyPanel("item"));
	    }

	    @Override
	    protected void populateItem(Item<Tasks> item) {
		Tasks t = item.getModelObject();
		item.add(new ViewFragment("item", "viewFragment", t));
	    }
	};
	dataTable.setColumns(2);
	dataTable.setRows(3);

	WebMarkupContainer container = new WebMarkupContainer("tileView");
	container.setOutputMarkupId(true);
	container.add(dataTable);
	container.add(new AjaxPagingNavigatorWithImages("paging", dataTable));
	container.setVersioned(false);
	addOrReplace(container);
    }

    public TaskListMode getMode() {
	return mode;
    }

    public void setMode(TaskListMode mode) {
	this.mode = mode;
    }

    public boolean isMonitoring() {
	return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
	this.monitoring = monitoring;
    }

    public Integer getPriority() {
	return priority;
    }

    public void setPriority(Integer priority) {
	this.priority = priority;
    }

    public Long getUser() {
	return user;
    }

    public void setUser(Long user) {
	this.user = user;
    }

    public final class ViewFragment extends Fragment {

	public ViewFragment(String id, String markupId, final Tasks task) {
	    super(id, markupId);
	    setOutputMarkupId(true);
	    updateFragment(null, task);
	}

	public void updateFragment(AjaxRequestTarget target, Tasks task) {
	    StatusForm form = new StatusForm("statusForm", task);
	    form.setOutputMarkupId(true);
	    addOrReplace(form);
	    if (target != null) {
		target.addComponent(form);
	    }
	}

	private class StatusForm extends Form {
	    private boolean complete;
	    private ModalWindow commentsWindow;

	    public StatusForm(String id, Tasks task) {
		super(id);

		setOutputMarkupId(true);
		final Long itask = task.getId();
		commentsWindow = new ModalWindow("commentsWindow");
		commentsWindow.setOutputMarkupId(true);
		commentsWindow.setOutputMarkupPlaceholderTag(true);
		commentsWindow.setWindowClosedCallback(
			new ModalWindow.WindowClosedCallback() {
		    @Override
		    public void onClose(AjaxRequestTarget target) {
			//updateFragment(target, TasksDAO.getTask(itask));
			((TasksPage) target.getPage()).update(target);
		    }
		});

		add(commentsWindow);

		//CheckBox completeCB = new CheckBox("complete",
		//   new PropertyModel<Boolean>(this, "complete"));
		AjaxCheckBox completeCB = new AjaxCheckBox("complete",
			new PropertyModel<Boolean>(this, "complete")) {

		    @Override
		    protected void onUpdate(AjaxRequestTarget target) {
			commentsWindow.setTitle("Completion comments");
			commentsWindow.setContent(new TaskCommentPanel(
				commentsWindow.getContentId(),
				itask, null, false,
				commentsWindow));
			commentsWindow.show(target);
		    }
		};

		completeCB.setOutputMarkupId(true);
		completeCB.setMarkupId("complete");
		completeCB.setOutputMarkupPlaceholderTag(true);

		AjaxFallbackButton reject = new AjaxFallbackButton("reject",
			this) {

		    @Override
		    protected void onSubmit(AjaxRequestTarget target,
			    Form<?> form) {
			CrmUser me = UserDAO
			    .getUser(CrmSession.get().getUserId());
			Tasks t = TasksDAO.getTask(itask);

			if (t != null) {
			    if (t.getOwner().getUserId()
				.equals(me.getUserId())
				&& t.getDelegatedTo() == null) {

				t.setStatus(TaskStatus.REJECTED);
				t.setModifiedAt(new Date());
				t.setModifiedBy(me);
				TasksDAO.update(t,
				    new ArrayList<TaskReminders>());
				return;
			    }
			} else {
			    return;
			}

			commentsWindow.setTitle("Rejection comments");
			commentsWindow.setContent(new TaskCommentPanel(
				commentsWindow.getContentId(),
				itask, null, true,
				commentsWindow));
			commentsWindow.show(target);
		    }
		};

		switch(task.getStatus()) {
		    case ACCEPTED:
		    case REJECTED:
			completeCB.setVisible(false);
			reject.setVisible(false);
			this.setEnabled(false);
			break;
		}

		add(completeCB, reject);
		add(new AttributeModifier("class", true,
			new Model<String>("taskView_p" + task.getPriority())));
		add(new AjaxEventBehavior("onclick") {

		    @Override
		    protected void onEvent(AjaxRequestTarget target) {
			((TasksPage)getPage()).showTask(target, itask);
		    }
		});

		Label title = new Label("title", task.getTitle());

		String statusClass = "task";
		if (TasksDAO.getDaysToGo(task) > 0) {
		    statusClass += "_pending";
		} else {
		    statusClass += "_overdue";
		}

		Label daysToGo = new Label("daysToGo",
			Integer.toString(task.getCommentsCount()));
		//Long.toString(TasksDAO.getDaysToGo(task)));
		daysToGo.add(new AttributeModifier("class", true,
			new Model<String>(statusClass + "_short")));
		Label statusString = new Label("statusString",
			task.getStatusString());
		statusString.add(new AttributeModifier("class", true,
			new Model<String>(statusClass)));

		Label start = new Label("start",
			WebUtil.formatDate(WebUtil.dateAndMonth, task.getStart()));
		Label end = new Label("end",
			WebUtil.formatDate(WebUtil.dateAndMonth, task.getEnd()));

		String delegationCaption = "";
		String ownerName = "";

		CrmUser me = UserDAO.getUser(user);

		if (task.getAssignedBy().equals(me)) {
		    if (! task.getOwner().equals(me)) {
			delegationCaption = "Assigned To";
			ownerName = task.getOwner().getFullName();
		    } else if (! me.equals(task.getDelegatedTo())
			    && task.getDelegatedTo() != null
			    && task.getOwner().equals(me)) {
			delegationCaption = "Delegated To";
			ownerName = task.getDelegatedTo().getFullName();
		    } else if (me.equals(task.getDelegatedTo())
			    || task.getOwner().equals(me)) {
			delegationCaption = "";
			ownerName = "Mine";
		    } else {
			delegationCaption = "Delegated To";
			ownerName = task.getDelegatedTo().getFullName();
		    }
		} else {
		    if (! task.getOwner().equals(me)) {
			delegationCaption = "Assigned By";
			ownerName = task.getAssignedBy().getFullName();
		    } else {
			delegationCaption = "Delegated By";
			ownerName = task.getAssignedBy().getFullName();
		    }
		}

		Label delegationTitle = new Label("delegationTitle",
			delegationCaption);
		Label owner = new Label("owner",
			ownerName);

		add(title, daysToGo, statusString, start, end,
			delegationTitle, owner);
	    }

	    public boolean isComplete() {
		return complete;
	    }

	    public void setComplete(boolean complete) {
		this.complete = complete;
	    }
	}
    }
}
