/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.db.dao.CommentsDAO;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.CrmComments;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.TaskStatus;
import com.flexidea.crm.db.dto.Tasks;
import java.util.ArrayList;
import java.util.Date;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.StringValidator;

/**
 *
 * @author akshay
 */
public final class TaskCommentPanel extends Panel {
    private String comments;

    public TaskCommentPanel(String id, final Long taskId,
	final TaskViewPanel viewPanel, final boolean isRejected,
	final ModalWindow commentsWindow) {
        super (id);
	setOutputMarkupId(true);

	final FeedbackPanel feedback = new FeedbackPanel("taskCommentFeedback");
	feedback.setOutputMarkupId(true);
	Form taskCommentForm = new Form("taskCommentForm");
	TextArea comment = new TextArea("comment",
	    new PropertyModel<String>(this, "comments"));
	comment.setRequired(true);
	comment.add(StringValidator.maximumLength(1024));
	taskCommentForm.add(comment);
	taskCommentForm.add(feedback);

	AjaxFallbackButton postButton = new AjaxFallbackButton("postButton", 
		taskCommentForm) {
	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (target != null) {
		    target.addComponent(feedback);
		}

		Tasks task = TasksDAO.getTask(taskId);

		CrmComments comment = new CrmComments();
		comment.setAssociatedWith(CommentsTarget.TASK.toString());
		comment.setCommentText(TaskCommentPanel
		    .this.getComments().getBytes());
		comment.setCreatedAt(new Date());
		comment.setCreatedBy(
		    UserDAO.getUser(CrmSession.get().getUserId()));
		CommentsDAO.addComments(CommentsTarget.TASK, taskId, task,
		    comment, new ArrayList<CommentsDAO.Attachment>());

		if (isRejected) {
		    task.setStatus(TaskStatus.REJECTED);
		} else {
		    task.setStatus(TaskStatus.COMPLETE);
		}
		task.setModifiedAt(new Date());
		task.setModifiedBy(comment.getCreatedBy());
		TasksDAO.update(task, new ArrayList<TaskReminders>());

		//viewPanel.update(target);
		info("Task status saved!");
		commentsWindow.close(target);
		this.setEnabled(false);
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(feedback);
		}
	    }

	};
	taskCommentForm.add(postButton);
	add(taskCommentForm);
    }

    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

}
