package com.flexidea.crm.app.task.panels;

import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.pages.TasksPage;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.EnquiryDAO;
import com.flexidea.crm.db.dao.LeadsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.AssociatedContacts;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.DurationMeasure;
import com.flexidea.crm.db.dto.EnquiryContacts;
import com.flexidea.crm.db.dto.LeadContacts;
import com.flexidea.crm.db.dto.ProjectContacts;
import com.flexidea.crm.db.dto.TaskReminderType;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.TaskStatus;
import com.flexidea.crm.db.dto.Tasks;
import com.flexidea.crm.util.WebUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.MaximumValidator;
import org.apache.wicket.validation.validator.RangeValidator;
import org.apache.wicket.validation.validator.StringValidator;

/**
 *
 * @author akshay
 */
public class TaskForm extends Form<TaskForm.TaskFormModel> {
    private Long task;
    private CommentsTarget target;
    private Long targetId;
    private WebMarkupContainer priorityMarkupContainer;
    private HiddenField priority;

    public static TaskForm getForm(String id, Long task, CommentsTarget iTarget,
	Long iTargetid) {

	TaskForm form = null;
	TaskFormModel model = new TaskFormModel();
	model.setTaskId(task);
	model.setAssignmentType("Delegate");
	CommentsTarget ct = iTarget;
	Long tId = iTargetid;

	if (ct == null) {
	    ct = CommentsTarget.CONTACTS;
	}

	Tasks t = null;
	if (task != null) {
	    t = TasksDAO.getTask(task);
	    if (! t.getOwner().equals(t.getDelegatedTo())) {
		model.setAssignmentType("Delegate");
		model.setOwner(t.getDelegatedTo());
	    } else {
		model.setAssignmentType("Assign");
		model.setOwner(t.getOwner());
	    }

	    model.setDescription(t.getDescription());
	    model.setTarget(t.getTargetContact());
	    model.setTitle(t.getTitle());
	    model.setStart(t.getStart());
	    model.setEnd(t.getEnd());
	    model.setPriority(t.getPriority());

	    if (t.getSource() != null)
		ct = CommentsTarget.valueOf(t.getSource());

	    switch(ct) {
		case CONTACTS:
		    if (t.getTargetContact() != null)
			tId = t.getTargetContact().getId();
		    break;
		case ENQUIRY:
		    if (t.getTargetEnquiry() != null)
			tId = t.getTargetEnquiry().getId();
		    break;
		case LEADS:
		    if (t.getTargetLead() != null)
			tId = t.getTargetLead().getId();
		    break;
		case PROJECT:
		    if (t.getTargetProject() != null)
			tId = t.getTargetProject().getId();
		    break;
	    }
	}

	form = new TaskForm(id, new CompoundPropertyModel<TaskFormModel>(model),
	    ct, tId);
	if (t != null && ! TaskStatus.OPEN.equals(t.getStatus())) {
	    form.setEnabled(false);
	}
	return form;
    }

    protected TaskForm(String id, IModel<TaskFormModel> imodel,
	CommentsTarget iTarget, Long iTargetId) {
	super(id, imodel);

	this.target = iTarget;
	this.targetId = iTargetId;
	this.task = imodel.getObject().getTaskId();

	List<CrmUser> users = UserDAO.listUsers();
	List<Contact> contacts = new ArrayList<Contact>();
	switch (target) {
	    case CONTACTS:
		if (targetId != null) {
		    for(AssociatedContacts c :
			ContactsDAO.getContact(targetId)
			    .getCompany().getAssociatedContacts()) {
			if (c.getChild().getContactType().isInternal()) {
			    contacts.add(c.getChild());
			}
		    }
		} else {
		    contacts.addAll(ContactsDAO.listContacts(1));
		}
		break;
	    case ENQUIRY:
		for (EnquiryContacts c :
		    EnquiryDAO.getEnquiry(targetId).getContacts()) {
		    contacts.add(c.getContact());
		}
		break;
	    case LEADS:
		for (LeadContacts c 
		    : LeadsDAO.getLead(targetId).getLeadContacts()) {
		    contacts.add(c.getContact());
		}
		break;
	    case PROJECT:
		for (ProjectContacts c
		    : ProjectDAO.getProject(targetId).getProjectContacts()) {
		    contacts.add(c.getContact());
		}
		break;
	}

	final FeedbackPanel feedback = new FeedbackPanel("tasksFeedback");
	feedback.setOutputMarkupId(true);

	TextField title = new TextField("title");
	title.setRequired(true);
	title.add(StringValidator.maximumLength(255));

	TextArea description = new TextArea("description");
	description.setRequired(true);
	description.add(StringValidator.maximumLength(1024));

	DateTextField start = new DateTextField("start", 
	    WebUtil.parseableDateTimeFormat);
	start.setMarkupId("start");
	start.setOutputMarkupId(true);
	//start.setRequired(true);
	DateTextField end = new DateTextField("end", 
	    WebUtil.parseableDateTimeFormat);
	end.setMarkupId("end");
	end.setOutputMarkupId(true);
	//end.setRequired(true);

	/*
	TextField startTime = new TextField("startTime");//, WebUtil.shortTime);
	//startTime.setRequired(true);
        //startTime.add(DateValidator.minimum(new Date(), WebUtil.shortTime));
        startTime.add(new PatternValidator("[\\d]{1,2}:[\\d]{1,2} (am|AM|pm|PM)"));
	TextField endTime = new TextField("endTime");//, WebUtil.shortTime);
	endTime.setRequired(true);
        endTime.add(new PatternValidator("[\\d]{1,2}:[\\d]{1,2} (am|AM|pm|PM)"));
        //endTime.add(DateValidator.minimum(new Date(), WebUtil.shortTime));
	 *
	 */

	TextField messageDuration = new TextField("messageDuration");
	//messageDuration.add(new PatternValidator(MetaPattern.DIGITS));
	messageDuration.add(new MaximumValidator(60));

	DropDownChoice<Contact> targetContact =
	    new DropDownChoice<Contact>("target", contacts,
	    new ChoiceRenderer<Contact>("contactName", "id"));

	DropDownChoice<CrmUser> owner = new DropDownChoice<CrmUser>("owner", users,
	    new ChoiceRenderer<CrmUser>("fullName", "userId"));
	//owner.setRequired(true);

	IChoiceRenderer<DurationMeasure> dcr =
	    new IChoiceRenderer<DurationMeasure>() {

	    @Override
	    public Object getDisplayValue(DurationMeasure object) {
		switch(object) {
		    case DAYS:
			return "Daily";
		    case HOURS:
			return "Hourly";
		    case MINUTES:
			return "Minutes";
		    case MONTHS:
			return "Monthly";
		    case WEEKS:
			return "Weekly";
		}

		return null;
	    }

	    @Override
	    public String getIdValue(DurationMeasure object, int index) {
		return object.toString();
	    }
	};

	List<DurationMeasure> smsDurationType = Arrays.asList(
	    new DurationMeasure[] {
		DurationMeasure.MINUTES,
		DurationMeasure.HOURS,
		DurationMeasure.DAYS,
		DurationMeasure.WEEKS,
		DurationMeasure.MONTHS
	});

	List<DurationMeasure> emailDurationType = Arrays.asList(
	    new DurationMeasure[] {
		DurationMeasure.HOURS,
		DurationMeasure.DAYS,
		DurationMeasure.WEEKS,
		DurationMeasure.MONTHS
	});

	DropDownChoice<DurationMeasure> smsDurationMeasure =
	    new DropDownChoice<DurationMeasure>("smsDurationMeasure",
	    smsDurationType, dcr);
	DropDownChoice<DurationMeasure> emailDurationMeasure =
	    new DropDownChoice<DurationMeasure>("emailDurationMeasure",
	    emailDurationType, dcr);

	RadioChoice assignmentType = new RadioChoice("assignmentType",
	    Arrays.asList(new String[] {"Assign", "Delegate"}));
	assignmentType.setRequired(true);
	assignmentType.setSuffix("&nbsp; &nbsp;");

	//RadioChoice priority = new RadioChoice("priority",
	 //   Arrays.asList(new Integer[]{1, 2, 3, 4, 5}));
        //DropDownChoice<Integer> priority = new DropDownChoice<Integer>("priority",
         //       Arrays.asList(new Integer[]{1, 2, 3, 4, 5}));
             //contacts);
	priority = new HiddenField("priority");
	priority.add(new RangeValidator(0, 4));
	priority.setOutputMarkupId(true);

	AjaxFallbackButton saveButton = new AjaxFallbackButton("saveButton",
	    this) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    if (target != null) {
                        target.addComponent(feedback);
                    }
                    TaskFormModel m = (TaskFormModel) form.getModelObject();
                    Tasks t = null;
                    if (m.getTaskId() != null) {
                        t = TasksDAO.getTask(m.getTaskId());
                    } else {
                        t = new Tasks();
                    }
                    t.setId(m.getTaskId());
                    t.setActive(true);
                    t.setAssignedBy(UserDAO.getUser(CrmSession.get().getUserId()));
                    t.setOwner(UserDAO.getUser(CrmSession.get().getUserId()));
		    if (m.getOwner() == null) {
			m.setOwner(t.getOwner());
		    }
                    if ("Assign".equals(m.getAssignmentType())) {
                        t.setOwner(m.getOwner());
                    }
                    if ("Delegate".equals(m.getAssignmentType())) {
                        t.setDelegatedTo(m.getOwner());
                    }
                    t.setCreatedAt(new Date());
                    t.setCreatedBy(UserDAO.getUser(CrmSession.get().getUserId()));
                    t.setDescription(m.getDescription());
		    if (m.getStart() == null) {
			m.setStart(new Date());
			//m.setStartTime(WebUtil.formatDate(WebUtil.timeFormat,
			  //  m.getStart()));
		    }
		    /*
                    t.setStart(WebUtil.toDate(WebUtil.parseableDateTimeFormat,
                        WebUtil.formatDate(WebUtil.dateFormat, m.getStart())
                        + " "
                        + m.getStartTime()));
                    t.setEnd(WebUtil.toDate(WebUtil.parseableDateTimeFormat, 
                         WebUtil.formatDate(WebUtil.dateFormat, m.getEnd())
                         + " "
                         + m.getEndTime()
                         ));
		     *
		     */
		    t.setStart(m.getStart());
		    if (m.getEnd() == null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(m.getStart());
			cal.set(Calendar.HOUR, cal.getMaximum(Calendar.HOUR));
			cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
			cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
			m.setEnd(cal.getTime());
		    }
		    t.setEnd(m.getEnd());
                    t.setId(m.getTaskId());

		    if (m.getPriority() == null || m.getPriority() == 0) {
			//default priority
			m.setPriority(3);
		    }

                    t.setPriority(m.getPriority());
                    t.setSource(getTarget().toString());
                    t.setTargetContact(m.getTarget());
                    
                    switch (getTarget()) {
                        case ENQUIRY:
                            t.setTargetEnquiry(EnquiryDAO.getEnquiry(targetId));
                            break;
                        case LEADS:
                            t.setTargetLead(LeadsDAO.getLead(targetId));
                            break;
                        case PROJECT:
                            t.setTargetProject(ProjectDAO.getProject(targetId));
                            break;
                    }
                    t.setStatus(TaskStatus.OPEN);
                    t.setTitle(m.getTitle());
                    List<TaskReminders> rems = new ArrayList<TaskReminders>();
                    if (m.getMessageDuration() != null) {
                        TaskReminders smsReminder = new TaskReminders();
                        smsReminder.setActive(true);
                        smsReminder.setCreatedAt(new Date());
                        smsReminder.setCreatedBy(UserDAO.getUser(CrmSession.get().getUserId()));
                        smsReminder.setDuration(m.getMessageDuration());
                        smsReminder.setDurationMeasure(m.getSmsDurationMeasure());
                        smsReminder.setReminderType(TaskReminderType.SMS);
                        rems.add(smsReminder);
                    }
                    if (m.getEmailDurationMeasure() != null) {
                        TaskReminders smsReminder = new TaskReminders();
                        smsReminder.setActive(true);
                        smsReminder.setCreatedAt(new Date());
                        smsReminder.setCreatedBy(UserDAO.getUser(CrmSession.get().getUserId()));
                        smsReminder.setDurationMeasure(m.getEmailDurationMeasure());
                        smsReminder.setReminderType(TaskReminderType.EMAIL);
			if (DurationMeasure.MINUTES.equals(smsReminder.getDurationMeasure())) {
			    smsReminder.setDuration(10);
			} else {
			    smsReminder.setDuration(1);
			}
                        rems.add(smsReminder);
                    }
                    if (t.getEnd().before(t.getStart())) {
                        error("Task begin date should fall before start date.");
                        return;
                    }
                    TasksDAO.update(t, rems);
                    setTask(t.getId());
                    m.setTaskId(task);
                    TaskForm.this.setModelObject(m);
                    info("Task saved!");
		    TasksPage page = (TasksPage) getPage();
		    page.update(target);
                } catch (Exception ex) {
                    Logger.getLogger(TaskForm.class.getName())
			.log(Level.SEVERE, null, ex);
                }
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(feedback);
		}
	    }
	};

	updatePrioritySelect(null);
	add(feedback, title, description, owner, assignmentType, start,
	    end, messageDuration, smsDurationMeasure,
	    emailDurationMeasure, targetContact, priority, saveButton);
    }

    private void updatePrioritySelect(AjaxRequestTarget target) {
	if (priorityMarkupContainer == null) {
	    priorityMarkupContainer =
		new WebMarkupContainer("priorityMarkupContainer");
	    priorityMarkupContainer.setOutputMarkupId(true);
	    addOrReplace(priorityMarkupContainer);
	}

	ListView<Integer> priorities =
		new ListView<Integer>("priorities",
	    Arrays.asList(new Integer[]{1, 2, 3, 4, 5})) {

	    @Override
	    protected void populateItem(ListItem<Integer> item) {
		TaskFormModel model = (TaskFormModel)
		    TaskForm.this.getModelObject();
		final Integer i = item.getModelObject();

		AjaxFallbackLink li = new AjaxFallbackLink("item") {

		    @Override
		    public void onClick(AjaxRequestTarget target) {
			TaskFormModel m = (TaskFormModel)
			    TaskForm.this.getModelObject();
			m.setPriority(i);
			TaskForm.this.modelChanged();
			updatePrioritySelect(target);
		    }
		};

		String cssClass = "priority_" + i;

		if (model.getPriority() == i) {
		    cssClass += "priority_selected";
		}

		li.add(new SimpleAttributeModifier("class", cssClass));
		item.add(li);
	    }
	};

	priorities.setOutputMarkupId(true);
	priorityMarkupContainer.addOrReplace(priorities);
	if (target != null) {
	    target.addComponent(priorityMarkupContainer);
	    target.addComponent(priority);
	}
    }

    public static class TaskFormModel implements Serializable {
	private	Long taskId;
	private	String title;
	private	String description;
	private	CrmUser owner;
	private	String assignmentType = "Delegate";
	private	Date start = new Date();
	//private	String startTime;
	private	Date end;
	//private	String endTime;
	private	Integer messageDuration;
	private DurationMeasure smsDurationMeasure;
	private DurationMeasure emailDurationMeasure;
	private	String durationType;
	private	String emailDuration;
	private Contact target;
	private Integer priority;

	public TaskFormModel() {
	    Calendar cal = Calendar.getInstance();
	    /*
	     * BUG ID 11[START]
	     */
	    //cal.set(Calendar.HOUR_OF_DAY, 9);
	    //cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
	    /*
	     * BUG ID 11[END]
	     */	    
	    start = cal.getTime();
	    cal.add(Calendar.DAY_OF_MONTH, 1);
	    cal.set(Calendar.HOUR_OF_DAY, 17);
	    cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
	    end = cal.getTime();
	}

	public String getAssignmentType() {
	    return assignmentType;
	}

	public void setAssignmentType(String assignmentType) {
	    this.assignmentType = assignmentType;
	}

	public String getDescription() {
	    return description;
	}

	public void setDescription(String description) {
	    this.description = description;
	}

	public String getDurationType() {
	    return durationType;
	}

	public void setDurationType(String durationType) {
	    this.durationType = durationType;
	}

	public String getEmailDuration() {
	    return emailDuration;
	}

	public void setEmailDuration(String emailDuration) {
	    this.emailDuration = emailDuration;
	}

	public Date getEnd() {
	    return end;
	}

	public void setEnd(Date end) {
	    this.end = end;
	}

	/*
	@Deprecated
	public String getEndTime() {
	    return endTime;
	}

	@Deprecated
	public void setEndTime(String endTime) {
	    this.endTime = endTime;
	}
	 *
	 */

	public Integer getMessageDuration() {
	    return messageDuration;
	}

	public void setMessageDuration(Integer messageDuration) {
	    this.messageDuration = messageDuration;
	}

	public CrmUser getOwner() {
	    return owner;
	}

	public void setOwner(CrmUser owner) {
	    this.owner = owner;
	}

	public Date getStart() {
	    return start;
	}

	public void setStart(Date start) {
	    this.start = start;
	}

	/*
	@Deprecated
	public String getStartTime() {
	    return startTime;
	}

	@Deprecated
	public void setStartTime(String startTime) {
	    this.startTime = startTime;
	}
	 *
	 */

	public Long getTaskId() {
	    return taskId;
	}

	public void setTaskId(Long taskId) {
	    this.taskId = taskId;
	}

	public String getTitle() {
	    return title;
	}

	public void setTitle(String title) {
	    this.title = title;
	}

	public Contact getTarget() {
	    return target;
	}

	public void setTarget(Contact target) {
	    this.target = target;
	}

	public DurationMeasure getEmailDurationMeasure() {
	    return emailDurationMeasure;
	}

	public void setEmailDurationMeasure(DurationMeasure emailDurationMeasure) {
	    this.emailDurationMeasure = emailDurationMeasure;
	}

	public DurationMeasure getSmsDurationMeasure() {
	    return smsDurationMeasure;
	}

	public void setSmsDurationMeasure(DurationMeasure smsDurationMeasure) {
	    this.smsDurationMeasure = smsDurationMeasure;
	}

	public Integer getPriority() {
	    return priority;
	}

	public void setPriority(Integer priority) {
	    this.priority = priority;
	}
    }

    public Long getTask() {
	return task;
    }

    public void setTask(Long task) {
	this.task = task;
    }

    public CommentsTarget getTarget() {
	return target;
    }

    public void setTarget(CommentsTarget target) {
	this.target = target;
    }

    public Long getTargetId() {
	return targetId;
    }

    public void setTargetId(Long targetId) {
	this.targetId = targetId;
    }

}