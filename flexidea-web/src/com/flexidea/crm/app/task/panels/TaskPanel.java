/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.app.common.panel.CommentsPanel;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Tasks;
import com.flexidea.crm.util.WebUtil;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author akshay
 */
public final class TaskPanel extends Panel {
    public TaskPanel(String id, final Long taskId) {
        super (id);
	setOutputMarkupId(true);
	Tasks t = TasksDAO.getTask(taskId);

	Label title = new Label("title", t.getTitle());
	Label startEnd = new Label("startEnd",
	    WebUtil.formatDate("dd MMM", t.getStart())
	    + "/"
	    + WebUtil.formatDate("dd MMM", t.getEnd())
	    );
	TaskViewPanel viewPanel = new TaskViewPanel("taskViewPanel", taskId);
	CommentsPanel commentsPanel = new CommentsPanel("taskCommentsPanel",
	    CommentsTarget.TASK, taskId);

	add(title, startEnd, viewPanel, commentsPanel);
    }
}
