package com.flexidea.crm.app.task.panels;
import com.flexidea.crm.app.common.panel.data.TasksDataProvider;
import com.flexidea.crm.db.dto.TaskListMode;
import com.flexidea.crm.db.dto.Tasks;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public final class TaskListViewPanel extends Panel {
    private TaskListMode mode;
    private boolean showHistory;
    private Long user;
    private Integer priority;
    private boolean monitoring;

    public TaskListViewPanel(String id, TaskListMode mode, boolean showHistory,
	Long user, Integer priority, boolean monitoring) {
	super(id);
	this.mode = mode;
	this.showHistory = showHistory;
	this.user = user;
	this.priority = priority;
	this.monitoring = monitoring;
	update();
    }

    private void update() {
	List<IColumn<Tasks>> columns = new ArrayList<IColumn<Tasks>>();

	columns.add(new AbstractColumn<Tasks>(
	    new Model<String>("ID"), "id") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Tasks>> cellItem,
		String componentId, IModel<Tasks> rowModel) {
		Tasks t = rowModel.getObject();
		cellItem.add(new TaskLinkPanel(componentId,
		    Long.toString(t.getId()),
		    t.getId()));
	    }

            @Override
            public String getCssClass() {
                return "w250";
            }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<Tasks>(
	    new Model<String>("Title"), "title") {

	    @Override
	    public void populateItem(Item<ICellPopulator<Tasks>> cellItem,
		String componentId, IModel<Tasks> rowModel) {
		Tasks t = rowModel.getObject();
		cellItem.add(new TaskLinkPanel(componentId, t.getTitle(),
		    t.getId(), false));
	    }

            @Override
            public String getCssClass() {
                return "w250";
            }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	if (! showHistory) {
	    columns.add(new PropertyColumn<Tasks>(new Model<String>(
		"Delegated By"), "creatorName", "owner.fullName"){

                @Override
                public String getCssClass() {
                    return "w100";
                }

		@Override
		public boolean isSortable() {
		    return true;
		}

            });

	    columns.add(new AbstractColumn<Tasks>(new Model<String>(
		"Status"), "end")
            {

                @Override
                public String getCssClass() {
                    return "w100";
                }

		@Override
		public void populateItem(Item<ICellPopulator<Tasks>> 
		    cellItem, String componentId,
		    IModel<Tasks> rowModel) {
		    cellItem.add(new Label(componentId,
			rowModel.getObject().getStatusString()));
		}
            });

	    columns.add(new AbstractColumn<Tasks>(new Model<String>(
		"Comments"), "commentsCount") {

		@Override
		public void populateItem(Item<ICellPopulator<Tasks>>
		    cellItem, String componentId,
		    IModel<Tasks> rowModel) {
		    cellItem.add(new Label(componentId,
			Integer.toString(rowModel.getObject()
			.getCommentsCount())));
		}

		@Override
		public boolean isSortable() {
		    return false;
		}

	    });
	} else {
	    columns.add(new PropertyColumn<Tasks>(new Model<String>(
		"Delegated By"), "creatorName", "assignedBy.fullName"));
	    columns.add(new PropertyColumn<Tasks>(new Model<String>(
		"Completion/Rejection date"), "modifiedAt", "modifiedAt"));
	}

	AjaxFallbackDefaultDataTable<Tasks> dataTable =
	    new AjaxFallbackDefaultDataTable<Tasks>("dataTable", columns,
	    new TasksDataProvider(mode, showHistory, user, priority, monitoring),
	    20);
	addOrReplace(dataTable);
    }

    public TaskListMode getMode() {
	return mode;
    }

    public void setMode(TaskListMode mode) {
	this.mode = mode;
    }

    public boolean isMonitoring() {
	return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
	this.monitoring = monitoring;
    }

    public Integer getPriority() {
	return priority;
    }

    public void setPriority(Integer priority) {
	this.priority = priority;
    }

    public boolean isShowHistory() {
	return showHistory;
    }

    public void setShowHistory(boolean showHistory) {
	this.showHistory = showHistory;
    }

    public Long getUser() {
	return user;
    }

    public void setUser(Long user) {
	this.user = user;
    }
}
