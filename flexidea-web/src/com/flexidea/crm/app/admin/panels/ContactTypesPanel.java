package com.flexidea.crm.app.admin.panels;
import com.flexidea.crm.app.common.components.YesNoHandler;
import com.flexidea.crm.app.common.components.YesNoPanel;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dto.ContactType;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public final class ContactTypesPanel extends Panel {
    private WebMarkupContainer container;
    private WebMarkupContainer formContainer;
    private WebMarkupContainer tableContainer;
    private YesNoPanel confirmationPanel;

    public ContactTypesPanel(String id) {
        super (id);
	container = new WebMarkupContainer("container");
	container
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	add(container);
	container.add(confirmationPanel = 
	    new YesNoPanel("confirmationPanel"));

	formContainer = new WebMarkupContainer("formContainer");
	formContainer
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	container.add(formContainer);

	tableContainer = new WebMarkupContainer("tableContainer");
	tableContainer
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	container.add(tableContainer);
	makeForm(null, null);
	makeTable(null);
    }

    protected void makeForm(AjaxRequestTarget target, Long id) {
	ContactType ct = null;

	if (id == null) {
	    ct = new ContactType();
	} else {
	    ct = ContactsDAO.getContactType(id);
	}

	StatelessForm<ContactType> form =
	    new StatelessForm<ContactType>("ctForm",
	    new CompoundPropertyModel<ContactType>(ct));
	formContainer.addOrReplace(form);

	form.add(new FeedbackPanel("feedback")
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true));

	form.add(new RequiredTextField<String>("typeName"));
	form.add(new CheckBox("internal"));
	form.add(new CheckBox("forCompany"));
	form.add(new CheckBox("forContact"));

	form.add(new AjaxFallbackButton("saveButton", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (target != null) {
		    target.addComponent(form.get("feedback"));
		}

		ContactType ct = (ContactType) form.getModelObject();

		if (! (ct.isForCompany() && ct.isForContact())) {
		    error("You have to choose whether this contact"
			+ " type is applicable to Companies or"
			+ " Contacts or both.");
		    return;
		}

		ContactsDAO.save(ct);
		info("Contact type saved");
		makeTable(target);
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(form.get("feedback"));
		}
	    }
	});

	form.add(new AjaxFallbackButton("newButton", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		makeForm(target, null);
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		makeForm(target, null);
	    }
	});

	form.add(new AjaxFallbackButton("cancelButton", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		makeForm(target, null);
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		makeForm(target, null);
	    }
	});

	if (target != null) {
	    target.addComponent(formContainer);
	}
    }

    protected void makeTable(AjaxRequestTarget target) {
	List<IColumn<ContactType>> columns =
	    new ArrayList<IColumn<ContactType>>();

	columns.add(new PropertyColumn<ContactType>(
	    new Model<String>("ID"), "id", "id") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new PropertyColumn<ContactType>(
	    new Model<String>("Name"), "typeName", "typeName") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new PropertyColumn<ContactType>(
	    new Model<String>("Internal"), "internal", "internal") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new PropertyColumn<ContactType>(
	    new Model<String>("For Company"), "forCompany", "forCompany") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new PropertyColumn<ContactType>(
	    new Model<String>("For Contact"), "forContact", "forContact") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }
	});

	columns.add(new AbstractColumn<ContactType>(
	    new Model<String>("")) {

	    @Override
	    public void populateItem(Item<ICellPopulator<ContactType>>
		cellItem, String componentId,
		IModel<ContactType> rowModel) {
		cellItem.add(new LinksFragment(componentId,
		    rowModel.getObject().getId()) {

		    @Override
		    protected void onEdit(AjaxRequestTarget target,
			Long id) {
			ContactTypesPanel.this.makeForm(target, id);
		    }

		    @Override
		    protected void onDelete(AjaxRequestTarget target, 
			Long id) {
			ContactsDAO.deleteContactType(id);
			ContactTypesPanel.this.makeTable(target);
		    }
		});
	    }
	});
    }

    public abstract class LinksFragment extends Fragment {
	private Long typeId;

	public LinksFragment(String id, Long typeId) {
	    super(id, "linksFragment", ContactTypesPanel.this);
	    this.typeId = typeId;
	    add(new AjaxFallbackLink("editLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    onEdit(target, LinksFragment.this.typeId);
		}
	    });

	    add(new AjaxFallbackLink("deleteLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    confirmationPanel.show(target, new YesNoHandler() {

			@Override
			public void onYes(AjaxRequestTarget target) {
			    onDelete(target, LinksFragment.this.typeId);
			}

			@Override
			public void onNo(AjaxRequestTarget target) {
			}
		    });
		}
	    });
	}

	public Long getTypeId() {
	    return typeId;
	}

	public void setTypeId(Long typeId) {
	    this.typeId = typeId;
	}

	protected abstract void onEdit(AjaxRequestTarget target, Long id);
	protected abstract void onDelete(AjaxRequestTarget target, Long id);
    }
}
