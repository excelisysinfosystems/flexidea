package com.flexidea.crm.app.admin.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.panel.data.UserDataProvider;
import com.flexidea.crm.db.dao.AuthDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.AuthRoles;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.UserRoles;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.GridView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

/**
 *
 * @author akshay
 */
public final class UserRolesPanel extends Panel {
    private WebMarkupContainer container;
    private WebMarkupContainer formContainer;
    private WebMarkupContainer tableContainer;

    public UserRolesPanel(String id) {
        super (id);
	container = new WebMarkupContainer("container");
	container.setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	add(container);

	formContainer = new WebMarkupContainer("formContainer");
	formContainer.setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	container.add(formContainer);

	tableContainer = new WebMarkupContainer("tableContainer");
	tableContainer.setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	container.add(tableContainer);
	update(null, null);
	updateTable(null);
    }

    private void update(AjaxRequestTarget target, Long userId) {
	if (userId == null) {
	    formContainer.addOrReplace(new EmptyPanel("assignmentForm"));
	} else {
	    formContainer.addOrReplace(new FormFragment(
		"assignmentForm", "formFragment", userId));
	}

	if (target != null) {
	    target.addComponent(formContainer);
	}
    }

    private void updateTable(AjaxRequestTarget target) {
	List<IColumn<CrmUser>> columns =
	    new ArrayList<IColumn<CrmUser>>();

	columns.add(new PropertyColumn<CrmUser>(
	    new Model<String>("ID"), "userId", "userId") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new PropertyColumn<CrmUser>(
	    new Model<String>("Name"), "fullName", "fullName") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<CrmUser>(new Model<String>("Roles")) {

	    @Override
	    public void populateItem(
		Item<ICellPopulator<CrmUser>> cellItem, 
		String componentId, IModel<CrmUser> rowModel) {
		String roles = "";
		CrmUser user = rowModel.getObject();
		int i = 0;
		int lastIndex = user.getUserRoles().size() - 1;

		for (UserRoles role : user.getUserRoles()) {
		    roles += role.getRole().getRoleName();
		    i++;
		    if (i < lastIndex) {
			roles += ", ";
		    }
		}
		cellItem.add(new Label(componentId, roles));
	    }
	});

	columns.add(new AbstractColumn<CrmUser>(new Model<String>("Roles")) {

	    @Override
	    public void populateItem(
		Item<ICellPopulator<CrmUser>> cellItem,
		String componentId, IModel<CrmUser> rowModel) {
		cellItem.add(new LinksFragment(componentId,
		    "linksFragment", rowModel.getObject().getUserId()));
	    }
	});

	tableContainer.addOrReplace(
	    new AjaxFallbackDefaultDataTable<CrmUser>(
	    "dataTable", columns, new UserDataProvider(), 50));

	if (target != null) {
	    target.addComponent(tableContainer);
	}
    }

    private class LinksFragment extends Fragment {
	private Long userId;

	public LinksFragment(String id, String markupId, Long iuserId) {
	    super(id, markupId, UserRolesPanel.this);
	    this.userId = iuserId;
	    add(new AjaxFallbackLink("changeLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    update(target, userId);
		}
	    });
	}

	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}
    }

    private class FormFragment extends Fragment {
	private Long userId;

	public FormFragment(String id, String markupId, Long iuserId) {
	    super(id, markupId, UserRolesPanel.this);
	    this.userId = iuserId;

	    Form<UserRoleModel> form =
		new Form<UserRoleModel>("roleForm",
		new CompoundPropertyModel<UserRoleModel>(
		new UserRoleModel(userId)));
	    add(form);
	    final FeedbackPanel feedback = new FeedbackPanel("feedback");
	    form.add(feedback
		.setOutputMarkupId(true)
		.setOutputMarkupPlaceholderTag(true));

	    form.add(new Label("userName"));

	    final CheckGroup<AuthRoles> group = new CheckGroup<AuthRoles>(
		"checkGroup", form.getModelObject().getRoles());
	    form.add(group);
	    GridView<AuthRoles> dataTable =
		new GridView<AuthRoles>("dataTable",
		new ListDataProvider<AuthRoles>(
		form.getModelObject().getRoles())) {

		@Override
		protected void populateEmptyItem(Item<AuthRoles> item) {
		    AuthRoles role = new AuthRoles();
		    WebMarkupContainer c = new WebMarkupContainer("item");
		    c.setOutputMarkupId(true)
			.setOutputMarkupPlaceholderTag(true)
			.setVisible(false);
		    c.add(new Check("selected",
			new PropertyModel<Boolean>(role, "selected"),
			group));
		    c.add(new Label("roleName", ""));
		    item.add(c);
		}

		@Override
		protected void populateItem(Item<AuthRoles> item) {
		    AuthRoles role = item.getModelObject();
		    WebMarkupContainer c = new WebMarkupContainer("item");
		    item.add(c);
		    c.add(new Check("selected",
			new PropertyModel<Boolean>(role, "selected"),
			group));
		    c.add(new Label("roleName", role.getRoleName()));
		}
	    }.setColumns(4);

	    group.add(dataTable);

	    form.add(new AjaxFallbackButton("saveButton", form) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    if (target != null) {
			target.addComponent(feedback);
		    }
		    UserRoleModel model =
			(UserRoleModel) form.getModelObject();
		    List<Long> roles = new ArrayList<Long>();

		    for (AuthRoles role : model.getRoles()) {
			if (role.isSelected()) {
			    roles.add(role.getId());
			}
		    }

		    AuthDAO.assignRoles(UserDAO.getUser(
			CrmSession.get().getUserId()), userId, roles);
		    updateTable(target);
		}

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);
		    if (target != null) {
			target.addComponent(feedback);
		    }
		}
	    });

	    form.add(new AjaxFallbackButton("cancelButton", form) {

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    update(target, null);
		}
	    });
	}

	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	private class UserRoleModel implements Serializable {
	    private Long userId;
	    private String userName;
	    private List<AuthRoles> roles = new ArrayList<AuthRoles>();

	    public UserRoleModel(Long userId) {
		this.userId = userId;
		CrmUser user = UserDAO.getUser(userId);
		Map<Long, Boolean> map = new HashMap<Long, Boolean>();
		this.userName = user.getFullName();

		for (UserRoles role : user.getUserRoles()) {
		    map.put(role.getRole().getId(), Boolean.TRUE);
		}

		for (AuthRoles role :
		    AuthDAO.list(null, true, 0, Integer.MAX_VALUE)) {
		    if (map.containsKey(role.getId())) {
			role.setSelected(true);
		    } else {
			role.setSelected(false);
		    }
		    roles.add(role);
		}
	    }

	    public String getUserName() {
		return userName;
	    }

	    public void setUserName(String userName) {
		this.userName = userName;
	    }

	    public List<AuthRoles> getRoles() {
		return roles;
	    }

	    public void setRoles(List<AuthRoles> roles) {
		this.roles = roles;
	    }

	    public Long getUserId() {
		return userId;
	    }

	    public void setUserId(Long userId) {
		this.userId = userId;
	    }
	}
    }
}
