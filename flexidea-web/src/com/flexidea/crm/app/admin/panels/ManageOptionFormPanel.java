package com.flexidea.crm.app.admin.panels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import com.flexidea.crm.db.dao.ListsDAO;
import com.flexidea.crm.db.dto.AdminOptionsModel;
import com.flexidea.crm.db.dto.Combo;
import com.flexidea.crm.db.dto.ComboOption;

/**
 * TODO: contactCity drop-down
 * 
 * @author akshay
 */
public final class ManageOptionFormPanel extends Panel {

	public ManageOptionFormPanel(String id) {
		super(id);

		AdminOptionsModel model = new AdminOptionsModel();

		addOrReplace(new AdminOptionsForm("adminOptionForm", new CompoundPropertyModel<AdminOptionsModel>(model), false));
		setOutputMarkupId(true);
	}

	public final class AdminOptionsForm extends Form<AdminOptionsModel> implements Serializable {

		public AdminOptionsForm(String id, IModel<AdminOptionsModel> imodel, boolean edit) {
			super(id, imodel);
			setOutputMarkupId(true);

			createForm(edit, imodel);

		}

		private void createForm(boolean edit, final IModel<AdminOptionsModel> imodel) {

			IChoiceRenderer<Combo> combos = new ChoiceRenderer<Combo>("comboName", "comboId");
			List<Combo> lstCombos = ListsDAO.getCombos();
			
			IChoiceRenderer<ComboOption> optionsCombo = new ChoiceRenderer<ComboOption>("comboOptionName", "comboOptionId");

			final DropDownChoice comboOptions = new DropDownChoice("comboOptions", new ArrayList(), optionsCombo);			

			final DropDownChoice city = new DropDownChoice("comboName", lstCombos, combos);

			city.setOutputMarkupId(true);
			city.setRequired(true);

			city.add(new AjaxFormComponentUpdatingBehavior("onchange") {
				@Override
				protected void onUpdate(AjaxRequestTarget event) {

					Combo cmbSelected = AdminOptionsForm.this.getModelObject().getComboName();

					if (cmbSelected != null) {						
						comboOptions.setChoices(cmbSelected.getComboOptions());
						addOrReplace(comboOptions);
					}
				}
			});

			addOrReplace(city);


			//comboOptions.setVisible(false);
			addOrReplace(comboOptions);

		}

	}

}