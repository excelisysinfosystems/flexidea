package com.flexidea.crm.app.admin.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.YesNoHandler;
import com.flexidea.crm.app.common.components.YesNoPanel;
import com.flexidea.crm.app.common.panel.data.AuthRolesDataProvider;
import com.flexidea.crm.db.dao.AuthDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.AuthAction;
import com.flexidea.crm.db.dto.AuthRoles;
import com.flexidea.crm.db.dto.AuthRules;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.Operations;
import com.flexidea.crm.db.dto.Principles;
import com.flexidea.crm.util.WebUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

/**
 *
 * @author akshay
 */
public final class RolesPanel extends Panel {
    private WebMarkupContainer container;
    private WebMarkupContainer formContainer;
    private YesNoPanel confirmationDialog;
    private WebMarkupContainer tableContainer;

    public RolesPanel(String id) {
        super (id);
	container = new WebMarkupContainer("container");
	container
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	add(container);

	formContainer = new WebMarkupContainer("formContainer");
	formContainer
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	container.add(formContainer);

	tableContainer = new WebMarkupContainer("tableContainer");
	tableContainer
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	container.add(tableContainer);
	container.add(confirmationDialog =
	    new YesNoPanel("confirmationDialog"));
	prepareForm(null, null);
	prepareTable(null);
    }

    private void prepareForm(AjaxRequestTarget target,
	IModel<AuthRoles> imodel) {
	IModel<AuthRoles> model = imodel;

	if (model == null) {
	    AuthRoles role = new AuthRoles();
	    role.setAuthRules(new ArrayList<AuthRules>());
	    for (Operations op : Operations.values()) {
		AuthRules rule = new AuthRules();
		for (Principles pr : Principles.values()) {
		    rule.setOperation(op);
		    rule.setPrinciple(pr);
		}
		role.getAuthRules().add(rule);
	    }
	    model = new CompoundPropertyModel<AuthRoles>(role);
	}

	Form<AuthRoles> rolesForm = 
	    new Form<AuthRoles>("rolesForm", model);
	formContainer.addOrReplace(rolesForm);

	final FeedbackPanel rolesFeedback = new FeedbackPanel("rolesFeedback");
	rolesFeedback.setOutputMarkupId(true);
	rolesFeedback.setOutputMarkupPlaceholderTag(true);
	rolesForm.add(rolesFeedback);

	rolesForm.add(new RequiredTextField("roleName")
	    .setLabel(new Model<String>("Role Name")));

	RepeatingView heads = new RepeatingView("heads");
	rolesForm.add(heads);

	for (Principles pr : Principles.values()) {
	    heads.add(new Label("item", pr.toString()));
	}

	RepeatingView rows = new RepeatingView("rows");
	rolesForm.add(rows);

	Map<Operations, List<AuthRules>> map = new LinkedHashMap<Operations, List<AuthRules>>();

	for (AuthRules rule : model.getObject().getAuthRules()) {

	    if (! map.containsKey(rule.getOperation())) {
		map.put(rule.getOperation(), new ArrayList<AuthRules>());
	    }

	    map.get(rule.getOperation()).add(rule);
	}

	for (Operations op : map.keySet()) {
	    WebMarkupContainer c = new WebMarkupContainer(rows.newChildId());
	    rows.add(c);
	    c.add(new Label("opName",
		op.toString()));

	    RepeatingView  opRules = new RepeatingView("opRules");
	    c.add(opRules);
	    for (AuthRules rule : map.get(op)) {
		WebMarkupContainer ic = new WebMarkupContainer(
		    opRules.newChildId());
		opRules.add(ic);
		DropDownChoice<AuthAction> action =
		    new DropDownChoice<AuthAction>("action", 
		    new PropertyModel<AuthAction>(rule, "action"),
		    Arrays.asList(AuthAction.values()));
		ic.add(action);
	    }
	}

	AjaxFallbackButton saveButton = new AjaxFallbackButton(
	    "saveButton", rolesForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (target != null) {
		    target.addComponent(rolesFeedback);
		}

		AuthRoles role = (AuthRoles) form.getModelObject();

		AuthRoles oldRole = AuthDAO.getRole(role.getRoleName());

		if (role.getId() == null
		    && oldRole != null) {
		    error("Duplicate rule name");
		    return;
		}

		if (role.getId() != null
		    && oldRole != null
		    && ! oldRole.getId().equals(role.getId())
		) {
		    error("Duplicate rule name");
		    return;
		}

		List<AuthRules> rules = role.getAuthRules();
		role.setAuthRules(null);
		CrmUser me = UserDAO.getUser(CrmSession
		    .get().getUserId());
		AuthDAO.save(null, role, rules, me);

		info("Role saved successfully!");
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(rolesFeedback);
		}
	    }
	};

	rolesForm.add(saveButton);

	AjaxFallbackButton newButton = new AjaxFallbackButton(
	    "newButton", rolesForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		prepareForm(target, null);
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		prepareForm(target, null);
	    }
	};

	rolesForm.add(newButton);

	if (target != null) {
	    target.addComponent(formContainer);
	}
    }

    private void prepareTable(AjaxRequestTarget target) {
	List<IColumn<AuthRoles>> columns =
	    new ArrayList<IColumn<AuthRoles>>();
	columns.add(new PropertyColumn<AuthRoles>(
	    new Model<String>("ID"), "id", "id") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new PropertyColumn<AuthRoles>(
	    new Model<String>("Role Name"), "roleName", "roleName") {

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<AuthRoles>(
	    new Model<String>("Created At"), "createdAt") {

	    @Override
	    public void populateItem(
		Item<ICellPopulator<AuthRoles>> cellItem,
		String componentId, IModel<AuthRoles> rowModel) {
		cellItem.add(new Label(componentId,
		    WebUtil.formatDate(WebUtil.dateTimeFormat,
		    rowModel.getObject().getCreatedAt())));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<AuthRoles>(
	    new Model<String>("Modified At"), "modifiedAt") {

	    @Override
	    public void populateItem(
		Item<ICellPopulator<AuthRoles>> cellItem,
		String componentId, IModel<AuthRoles> rowModel) {
		cellItem.add(new Label(componentId,
		    WebUtil.formatDate(WebUtil.dateTimeFormat,
		    rowModel.getObject().getModifiedAt())));
	    }

	    @Override
	    public boolean isSortable() {
		return true;
	    }

	});

	columns.add(new AbstractColumn<AuthRoles>(
	    new Model<String>("")) {

	    @Override
	    public void populateItem(
		Item<ICellPopulator<AuthRoles>> cellItem,
		String componentId, IModel<AuthRoles> rowModel) {
		cellItem.add(new LinksFragment(componentId, "links", 
		    tableContainer, rowModel.getObject().getId()));
	    }

	    @Override
	    public boolean isSortable() {
		return false;
	    }

	});

	tableContainer.addOrReplace(
	    new AjaxFallbackDefaultDataTable("dataTable",
	    columns, new AuthRolesDataProvider(), 50));
	if (target != null) {
	    target.addComponent(tableContainer);
	}
    }

    private class LinksFragment extends Fragment {
	private Long ruleId;

	public LinksFragment(String id, String markupId, 
	    MarkupContainer markupProvider, Long iruleId) {
	    super(id, markupId, markupProvider);
	    this.ruleId = iruleId;

	    AjaxFallbackLink editLink = new AjaxFallbackLink("editLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    prepareForm(target,
			new CompoundPropertyModel<AuthRoles>(
			AuthDAO.getRole(null, ruleId))
		    );
		}
	    };

	    add(editLink);

	    AjaxFallbackLink deleteLink = new AjaxFallbackLink("deleteLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    confirmationDialog.show(target, new YesNoHandler() {

			@Override
			public void onYes(AjaxRequestTarget target) {
			    AuthDAO.delete(null,
				AuthDAO.getRole(null, ruleId));
			    prepareTable(target);
			}

			@Override
			public void onNo(AjaxRequestTarget target) {
			}
		    });
		}
	    };

	    add(deleteLink);
	}
    }
}
