package com.flexidea.crm.app.reports.panels;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.ReportsDAO;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ReportModel;
import com.flexidea.crm.db.dto.ReportsSchedule;
import com.flexidea.crm.util.WebUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

/**
 *
 * @author akshay
 */
public abstract class SaveReportPanel extends Panel {
    private WebMarkupContainer container;
    private Form<ReportModel> form;

    public SaveReportPanel(String id, ReportModel model) {
        super (id);
	form =
	    new Form<ReportModel>("reportForm",
	    new CompoundPropertyModel<ReportModel>(model));
	form.setOutputMarkupId(true);
	form.setOutputMarkupPlaceholderTag(true);
	add(form);

	container = new WebMarkupContainer("saveContainer");
	container.setOutputMarkupId(true);
	container.setOutputMarkupPlaceholderTag(true);
	form.addOrReplace(container);

	update(null, model);
    }

    private void update(AjaxRequestTarget target, ReportModel model) {
	if (model.getReportsSchedule() == null) {
	    model.setReportsSchedule(new ArrayList<ReportsSchedule>());
	}

	form.modelChanging();
	form.setModel(new CompoundPropertyModel<ReportModel>(model));
	form.modelChanged();

	container.addOrReplace(new FeedbackPanel("feedback")
	    .setOutputMarkupId(true));
	TextField reportName = new TextField("reportName");
	reportName
	    //.setRequired(true)
	    .add(MaximumLengthValidator.maximumLength(128))
	    ;
	container.addOrReplace(reportName);

	container.addOrReplace(new AjaxFallbackButton("addSchedule", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target,
		Form<?> form) {
		ReportModel m = (ReportModel) form.getModelObject();
		ReportsSchedule schedule = new ReportsSchedule();
		schedule.setModel(m);

		m.getReportsSchedule().add(schedule);
		update(target, m);
	    }
	});

	RepeatingView sched = new RepeatingView("schedule");
	sched.setOutputMarkupId(true);
	container.addOrReplace(sched);
	int i = 0;
	for (ReportsSchedule schedule : model.getReportsSchedule()) {
	    schedule.setIndex(i);

	    if (! schedule.isActive()) {
		i++;
		continue;
	    }

	    WebMarkupContainer sContainer =
		new WebMarkupContainer(sched.newChildId());
	    sContainer.setOutputMarkupId(true);
	    sContainer.setOutputMarkupPlaceholderTag(true);
	    sched.add(sContainer);
	    Form<ReportsSchedule> scheduleForm =
		new Form<ReportsSchedule>("scheduleForm",
		new CompoundPropertyModel<ReportsSchedule>(schedule));

	    sContainer.add(scheduleForm);

	    scheduleForm.add(new FeedbackPanel("scheduleFeedback")
		.setOutputMarkupId(true));

	    DropDownChoice<Contact> to = new DropDownChoice<Contact>(
		"to",
		//new PropertyModel<Contact>(schedule, "to"),
		ContactsDAO.listContacts(1),
		new ChoiceRenderer<Contact>("toString", "id"));
	    //to.setRequired(true);
	    scheduleForm.add(to);

	    if (schedule.getStartDate() == null) {
		schedule.setStartDate(new Date());
	    }

	    DateTextField startDate = new DateTextField("startDate", 
		//new PropertyModel<Date>(schedule, "startDate"),
		WebUtil.parseableDateTimeFormat);
	    startDate.setMarkupId("startDate" + i);
	    startDate.setOutputMarkupId(true);
	    scheduleForm.add(startDate);

	    Label startDatePicker = new Label("startDatePicker",
		"$('#" + startDate.getMarkupId() + "')"
		+ ".datetimepicker({"
		+ "dateFormat: 'dd/M/yy',"
		+ "changeMonth: true,"
		+ "changeYear: true"
		+ "});");
	    startDatePicker.setEscapeModelStrings(false);
	    scheduleForm.add(startDatePicker);

	    DropDownChoice<ReportsSchedule.ScheduleDuration> duration =
		new DropDownChoice<ReportsSchedule.ScheduleDuration>(
		"duration",
		//new PropertyModel<ReportsSchedule.ScheduleDuration>(
		 //   schedule, "duration"),
		Arrays.asList(ReportsSchedule.ScheduleDuration.values()));
	    //duration.setRequired(true);
	    scheduleForm.add(duration);
	    scheduleForm.add(new RemoveButton("remove",
		scheduleForm, i));
	    i++;
	}

	container.addOrReplace(new AjaxFallbackButton("save", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target,
		Form<?> form) {
		if (target != null) {
		    target.addComponent(container.get("feedback"));
		}

		try {
		    ReportModel m = (ReportModel) form.getModelObject();
		    ReportsDAO.save(m, m.getReportsSchedule());

		    info("Report Saved Successfully!");

		    ((Form<ReportModel>)form)
			.modelChanging();
		    ((Form<ReportModel>)form)
			.setModelObject(m);
		    ((Form<ReportModel>)form)
			.modelChanged();
		    onSave(target, m);
		} catch (Exception e) {
		    e.printStackTrace(System.err);
		    error("Could not save report due to an internal error");
		}
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(container.get("feedback"));
		}
	    }

	});

	if (target != null) {
	    target.addComponent(container);
	}
    }

    public final class RemoveButton extends AjaxFallbackButton {
	private int index;

	public RemoveButton(String id,
	    Form<?> form, int index) {
	    super(id, form);
	    this.index = index;
	}

	@Override
	protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
	    System.out.println("DEBUG: removing idx=" + index);
	    if (target != null) {
		target.addComponent(form.get("scheduleFeedback"));
	    }

	    ReportsSchedule m = (ReportsSchedule) form.getModelObject();
	    m.setActive(false);
	    update(target, m.getModel());
	}

	@Override
	protected void onError(AjaxRequestTarget target, Form<?> form) {
	    super.onError(target, form);
	    if (target != null) {
		target.addComponent(form.get("scheduleFeedback"));
	    }
	    System.out.println("ERROR: removing idx=" + index);
	}
    }

    public abstract void onSave(AjaxRequestTarget target,
	ReportModel model);
}
