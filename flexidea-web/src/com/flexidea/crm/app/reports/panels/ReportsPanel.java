package com.flexidea.crm.app.reports.panels;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.db.dao.CompaniesDAO;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.ListsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dao.ReportsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.City;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.EnquiryStatus;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.db.dto.ReportModel;
import com.flexidea.crm.util.WebUtil;
import java.io.File;
import java.util.Arrays;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.behavior.AbstractAjaxBehavior;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxFallbackLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.target.resource.ResourceStreamRequestTarget;
import org.apache.wicket.util.resource.FileResourceStream;

/**
 *
 * @author akshay
 */
public abstract class ReportsPanel extends Panel {
    private WebMarkupContainer reportsPanelContainer;
    private ModalWindow saveWindow;

    public ReportsPanel(String id) {
        super (id);
	reportsPanelContainer =
	    new WebMarkupContainer("reportsPanelContainer");
	reportsPanelContainer.setOutputMarkupId(true);
	add(reportsPanelContainer);
	updateComponent(null, new ReportModel());
    }

    public void updateComponent(AjaxRequestTarget target,
	ReportModel model) {
	final StatelessForm<ReportModel> form =
	    new StatelessForm<ReportModel>("reportsForm",
	    new CompoundPropertyModel<ReportModel>(model));

	saveWindow = new ModalWindow("saveWindow");
	saveWindow.setOutputMarkupId(true);
	saveWindow.setOutputMarkupPlaceholderTag(true);
        
	form.add(saveWindow);

	reportsPanelContainer.addOrReplace(form);

	final WebMarkupContainer filtersContainer =
	    new WebMarkupContainer("filtersContainer");
	filtersContainer.setOutputMarkupId(true);
	filtersContainer.setOutputMarkupPlaceholderTag(true);
	form.add(filtersContainer);

	if (model.getId() == null) {
	    form.add(new Label("reportName", "New Report"));
	} else {
	    form.add(new Label("reportName"));
	}

	FeedbackPanel feedback =
	    new FeedbackPanel("reportsFeedback");
	feedback.setOutputMarkupId(true);
	form.addOrReplace(feedback);

	WebMarkupContainer groups = new WebMarkupContainer("groups");
	groups.setOutputMarkupPlaceholderTag(true);
	groups.setOutputMarkupId(true);
	form.addOrReplace(groups);

	DropDownChoice<ReportModel.Level1Group> level1Group =
	    new DropDownChoice<ReportModel.Level1Group>("level1Group",
	    Arrays.asList(ReportModel.Level1Group.values()));
	groups.add(level1Group);

	DropDownChoice<ReportModel.Level2Group> level2Group =
	    new DropDownChoice<ReportModel.Level2Group>("level2Group",
	    Arrays.asList(ReportModel.Level2Group.values()));
	groups.add(level2Group);

	DropDownChoice<ReportModel.Level3Group> level3Group =
	    new DropDownChoice<ReportModel.Level3Group>("level3Group",
	    Arrays.asList(ReportModel.Level3Group.values()));
	groups.add(level3Group);

	groups.setVisible(model.isProjectSummaryReport());

	// -- Contractor
	 WebMarkupContainer contractorFilter =
	    new WebMarkupContainer("contractorFilter");
	contractorFilter.setOutputMarkupId(true);
	contractorFilter.setOutputMarkupPlaceholderTag(true);
	contractorFilter.setVisible(model.isFilterByContractor());
	filtersContainer.add(contractorFilter);

	DropDownChoice<Company> filterContractor =
	    new DropDownChoice<Company>("filterContractor",
	    CompaniesDAO.listByContactType(
		ContactsDAO.getContactType("Contractor")),
	    new ChoiceRenderer<Company>("companyName", "id"));
	contractorFilter.add(filterContractor);
	contractorFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByContractor(false);
		m.setFilterContractor(null);
		updateComponent(target, m);
	    }
	});
	// -- Contractor

	// -- Consultant
	 WebMarkupContainer consultantFilter =
	    new WebMarkupContainer("consultantFilter");
	consultantFilter.setOutputMarkupId(true);
	consultantFilter.setOutputMarkupPlaceholderTag(true);
	consultantFilter.setVisible(model.isFilterByConsultant());
	filtersContainer.add(consultantFilter);

	DropDownChoice<Company> filterConsultant =
	    new DropDownChoice<Company>("filterConsultant",
	    CompaniesDAO.listByContactType(
		ContactsDAO.getContactType("Consultant")),
	    new ChoiceRenderer<Company>("companyName", "id"));
	consultantFilter.add(filterConsultant);
	consultantFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByConsultant(false);
		m.setFilterConsultant(null);
		updateComponent(target, m);
	    }
	});
	// -- Consultant

	// -- SalesRep
	 WebMarkupContainer salesRepFilter =
	    new WebMarkupContainer("salesRepFilter");
	salesRepFilter.setOutputMarkupId(true);
	salesRepFilter.setOutputMarkupPlaceholderTag(true);
	salesRepFilter.setVisible(model.isFilterBySalesRep());
	filtersContainer.add(salesRepFilter);

	DropDownChoice<Contact> filterSalesRep =
	    new DropDownChoice<Contact>("filterSalesRep",
	    ContactsDAO.listByContactType(
		ContactsDAO.getContactType("Sales Representative")),
	    new ChoiceRenderer<Contact>("toString", "id"));
	salesRepFilter.add(filterSalesRep);
	salesRepFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterBySalesRep(false);
		m.setFilterSalesRep(null);
		updateComponent(target, m);
	    }
	});
	// -- SalesRep

	// -- Branch Filter
	WebMarkupContainer branchFilter =
	    new WebMarkupContainer("branchFilter");
	branchFilter.setOutputMarkupId(true);
	branchFilter.setOutputMarkupPlaceholderTag(true);
	branchFilter.setVisible(model.isFilterByBranch());
	filtersContainer.add(branchFilter);

	DropDownChoice<City> filterBranch =
	    new DropDownChoice<City>("filterBranch",
	    ListsDAO.getCities(),
	    new ChoiceRenderer<City>("cityName", "id"));
	branchFilter.add(filterBranch);
	branchFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByBranch(false);
		m.setFilterBranch(null);
		updateComponent(target, m);
	    }
	});
	// -- Branch Filter

	// -- Project Filter
	 WebMarkupContainer projectFilter =
	    new WebMarkupContainer("projectFilter");
	projectFilter.setOutputMarkupId(true);
	projectFilter.setOutputMarkupPlaceholderTag(true);
	projectFilter.setVisible(model.isFilterByProject());
	filtersContainer.add(projectFilter);

	DropDownChoice<Project> filterProject =
	    new DropDownChoice<Project>("filterProject",
	    ProjectDAO.list(),
	    new ChoiceRenderer<Project>("projectName", "id"));
	projectFilter.add(filterProject);
	projectFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByProject(false);
		m.setFilterProject(null);
		updateComponent(target, m);
	    }
	});
	// -- Project Filter

	// -- Job Status Filter
	WebMarkupContainer jobStatusFilter =
	    new WebMarkupContainer("jobStatusFilter");
	jobStatusFilter.setOutputMarkupId(true);
	jobStatusFilter.setOutputMarkupPlaceholderTag(true);
	jobStatusFilter.setVisible(model.isFilterByJobStatus());
	filtersContainer.add(jobStatusFilter);

	DropDownChoice<EnquiryStatus> filterJobStatus =
	    new DropDownChoice<EnquiryStatus>("filterJobStatus",
	    Arrays.asList(EnquiryStatus.values()));
	jobStatusFilter.add(filterJobStatus);
	jobStatusFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByJobStatus(false);
		m.setFilterJobStatus(null);
		updateComponent(target, m);
	    }
	});
	// -- Job Status Filter

	WebMarkupContainer valueFilter =
	    new WebMarkupContainer("valueFilter");
	valueFilter
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	valueFilter.setVisible(model.getFilterByValue());
	filtersContainer.add(valueFilter);
	WebMarkupContainer valueBetweenFilter =
	    new WebMarkupContainer("valueBetweenFilter");
	valueBetweenFilter
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true)
	    .setVisible("between".equals(model
	    .getFilterValueOperator()));
	valueFilter.add(valueBetweenFilter);
	valueFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByValue(false);
		m.setFilterValue1(0.00);
		m.setFilterValue2(0.00);
		updateComponent(target, m);
	    }
	});

	DropDownChoice<String> filterValueOperator =
	    new DropDownChoice<String>("filterValueOperator",
	    Arrays.asList(new String[] {"=", ">", "<", "between"}));
	valueFilter.add(filterValueOperator);
	TextField filterValue1 = new TextField("filterValue1");
	valueFilter.add(filterValue1);
	TextField filterValue2 = new TextField("filterValue2");
	valueBetweenFilter.add(filterValue2);

	filterValueOperator.add(
	    new AjaxFormComponentUpdatingBehaviorImpl("onchange",
	    form, valueBetweenFilter));

	WebMarkupContainer timeFilter =
	    new WebMarkupContainer("timeFilter");
	timeFilter.setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	timeFilter.setVisible(model.isFilterByDateRange());
	filtersContainer.add(timeFilter);
	timeFilter.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByDateRange(false);
		m.setFilterStartDate(null);
		m.setFilterStartDate(null);
		updateComponent(target, m);
	    }
	});

	DateTextField filterStartDate = new DateTextField(
	    "filterStartDate", WebUtil.parseableDateTimeFormat);
	filterStartDate.setMarkupId("filterStartDate");
	filterStartDate.setOutputMarkupId(true);
	DateTextField filterEndDate = new DateTextField(
	    "filterEndDate", WebUtil.parseableDateTimeFormat);
	filterEndDate.setMarkupId("filterEndDate");
	filterEndDate.setOutputMarkupId(true);
	timeFilter.add(filterStartDate, filterEndDate);

	WebMarkupContainer contactFilters =
	    new WebMarkupContainer("contactFilters");
	contactFilters.setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	filtersContainer.add(contactFilters);
	contactFilters.add(new IndicatingAjaxFallbackLink("remove") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		ReportModel m = form.getModelObject();
		m.setFilterByContactType(false);
		m.setFilterContactType(null);
		updateComponent(target, m);
	    }
	});

	DropDownChoice<ContactType> filterContactType =
	    new DropDownChoice<ContactType>("filterContactType",
	    ContactsDAO.getContactTypes(),
	    new ChoiceRenderer<ContactType>("typeName", "id"));
	contactFilters.add(filterContactType);
	contactFilters.setVisible(model.isFilterByContactType());

	String[] projectReportFilters = new String[] {
	    "Contractor", "Consultant", "Sales Representative",
	    "Branch", "Value", "Job Status", "Time Period",
	    "Project"
	};

	String[] contactReportFilters = new String[] {
	    "Branch", "Contact Type"
	};

	WebMarkupContainer filters = new WebMarkupContainer("filters");
	filters.setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	filtersContainer.add(filters);

	DropDownChoice<String> filtersDD = new DropDownChoice<String>(
	    "filtersDD", Arrays.asList(model.isProjectSummaryReport()
	    ? projectReportFilters : contactReportFilters));
	filters.add(filtersDD);

	filters.add(new IndicatingAjaxButton("addFilter", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		ReportModel m = (ReportModel) form.getModelObject();

		if ("Contractor".equals(m.getFiltersDD())) {
		    m.setFilterByContractor(true);
		} else if ("Consultant".equals(m.getFiltersDD())) {
		    m.setFilterByConsultant(true);
		} else if ("Sales Representative"
		    .equals(m.getFiltersDD())) {
		    m.setFilterBySalesRep(true);
		} else if ("Branch"
		    .equals(m.getFiltersDD())) {
		    m.setFilterByBranch(true);
		} else if ("Value"
		    .equals(m.getFiltersDD())) {
		    m.setFilterByValue(true);
		} else if ("Job Status"
		    .equals(m.getFiltersDD())) {
		    m.setFilterByJobStatus(true);
		} else if ("Time Period"
		    .equals(m.getFiltersDD())) {
		    m.setFilterByDateRange(true);
		} else if ("Project"
		    .equals(m.getFiltersDD())) {
		    m.setFilterByProject(true);
		} else if ("Contact Type"
		    .equals(m.getFiltersDD())) {
		    m.setFilterByContactType(true);
		}

		updateComponent(target, m);
	    }
	});

	final ReportDownload rd = new ReportDownload();
	form.add(rd);

	form.add(new AjaxFallbackButton("submit", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (target != null) {
		    target.addComponent(form.get("reportsFeedback"));
		}

		try {
		    File file = ReportsDAO.export((ReportModel)
			form.getModelObject());
		    if (file != null) {
			rd.initiate(target, file);
		    } else {
			error("Report generation failed");
		    }
		} catch (Exception e) {
		    e.printStackTrace(System.err);
		    error("Could not generate report due to an internal error");
		}

		if (target != null) {
		    target.addComponent(reportsPanelContainer);
		}
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(form.get("reportsFeedback"));
		}
	    }
	});

	AjaxFallbackButton save = new AjaxFallbackButton("save", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target,
		Form<?> form) {
		ReportModel m = (ReportModel) form.getModelObject();
		saveWindow.setContent(
		    new SaveReportPanel(saveWindow.getContentId(), m) {

		    @Override
		    public void onSave(AjaxRequestTarget target, 
			ReportModel modl) {
			saveWindow.close(target);
			ReportsPanel.this.onSave(target);
		    }
		});
		saveWindow.show(target);
	    }
	};

	save.setOutputMarkupId(true);
	save.setOutputMarkupPlaceholderTag(true);

	if (CrmSession.get().isUserLoggedIn() &&
	    "ADMIN".equals(
	    UserDAO.getUser(CrmSession.get().getUserId())
	    .getUserRole())) {
	    save.setVisible(true);
	} else {
	    save.setVisible(false);
	}

	form.add(save);

	if (target != null) {
	    target.addComponent(reportsPanelContainer);
	}
    }

    private static class AjaxFormComponentUpdatingBehaviorImpl
	extends AjaxFormComponentUpdatingBehavior {

	private  StatelessForm<ReportModel> form;
	private  WebMarkupContainer valueBetweenFilter;

	public AjaxFormComponentUpdatingBehaviorImpl(String event, 
	    StatelessForm<ReportModel> form,
	    WebMarkupContainer valueBetweenFilter) {
	    super(event);
	    this.form = form;
	    this.valueBetweenFilter = valueBetweenFilter;
	}

	@Override
	protected void onUpdate(AjaxRequestTarget target) {
	    String value = form.getModelObject().getFilterValueOperator();
	    if ("between".equals(value)) {
		valueBetweenFilter.setVisible(true);
	    } else {
		valueBetweenFilter.setVisible(false);
	    }
	    if (target != null) {
		target.addComponent(valueBetweenFilter);
	    }
	}
    }

    public static final class ReportDownload
	extends AbstractAjaxBehavior {
	private File file;

	public void initiate(AjaxRequestTarget target, File file) {
	    this.file = file;
	    target.appendJavascript("window.location.href='" +
		getCallbackUrl() + "'");
	}

	@Override
	public void onRequest() {
	    getComponent()
		.getRequestCycle()
		.setRequestTarget(
		new ResourceStreamRequestTarget(
		new FileResourceStream(file)));
	}
    }

    public abstract void onSave(AjaxRequestTarget target);
}
