package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ListMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;

/**
 *
 * @author akshay
 */
public class ContactsDataProvider extends SortableDataProvider<Contact>{
    private ListMode mode;
    private Long companyId;
    private String criteria;
    private String alphaGroup;

    private static final Map<String, String> reMap =
	new HashMap<String, String>();

    static {
	reMap.clear();
	reMap.put("A-F", "^[a-f]");
	reMap.put("G-M", "^[g-m]");
	reMap.put("N-S", "^[n-s]");
	reMap.put("T-Z", "^[t-z]");
    }

    public ContactsDataProvider(ListMode mode, Long companyId, 
	String criteria, String alphaGroup) {
	this.mode = mode;
	this.companyId = companyId;
	this.criteria = criteria;
	this.alphaGroup = alphaGroup;
    }

    private List<Contact> list(int first, int count) {
	switch (mode) {
	    case ALPAHABATICAL:
		return ContactsDAO
		    .listForAlphaGroup(reMap.get(alphaGroup), criteria, first, count);
	    case INDUSTRY_WISE:
		return ContactsDAO
		    .listForCompany(companyId, null, null, first, count)
		    ;
	    case COMPANY_WISE:
		return ContactsDAO
		    .listForCompany(companyId, null, null, first, count) ;
	    case RECENT:
		return ContactsDAO
		    .searchContacts(CommentsTarget.CONTACTS, null,
			criteria, null, first, count, false, false, true, null, true)
		    ;
	    case CONSULTANT_WISE:
		return ContactsDAO
		    .listForCompany(companyId, criteria,
			ContactsDAO.getContactType("Consultant"),
			first, count) ;
	    case SUPPLIER_WISE:
		return ContactsDAO
		    .listForCompany(companyId, criteria,
			ContactsDAO.getContactType("Supplier"),
			first, count) ;
	    case CITY_WISE:
		return ContactsDAO.listByCity(companyId, criteria, first, count);
	    case ID:
		List<Contact> list = new ArrayList<Contact>();
		Long id = null;
		try {
		    id = Long.parseLong(criteria);
		    if (id != null) {
			Contact c = ContactsDAO.getContact(id);
			if (c != null) {
			    list.add(c);
			}
		    }
		} catch (Exception e) {

		}
		return list;
	}
	return null;
    }

    @Override
    public Iterator<? extends Contact> iterator(int first, int count) {
	return list(first, count).iterator();
    }

    @Override
    public int size() {
	return list(0, Integer.MAX_VALUE).size();
    }

    @Override
    public IModel<Contact> model(Contact object) {
	return new ContactDetachableModel(object);
    }
}
