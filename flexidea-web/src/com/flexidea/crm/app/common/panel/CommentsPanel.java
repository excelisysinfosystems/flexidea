package com.flexidea.crm.app.common.panel;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.components.AttachmentLink;
import com.flexidea.crm.db.dao.CommentsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.Attachments;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.CrmComments;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.util.WebUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.MultiFileUploadField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.lang.Bytes;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * TODO: Filter and Search
 * @author akshay
 */
public final class CommentsPanel extends Panel implements Serializable {
    private CommentsTarget target;
    private Long targetId;
    private String commentsText;
    private CommentsFragment commentsContainer;
    private SearchForm searchForm;
    private WebMarkupContainer container;

    public CommentsPanel(String id, CommentsTarget cTarget, Long cTargetId) {
	super(id);
	this.target = cTarget;
	this.targetId = cTargetId;
	container = new WebMarkupContainer("container");
	container.setOutputMarkupId(true);
	add(container);
	setOutputMarkupId(true);
	doRender(null);
    }

    private void doRender(AjaxRequestTarget target) {
	AddCommentsForm form = new AddCommentsForm("addCommentsForm",
		new CompoundPropertyModel<CommentsPanel>(this));

	List<CrmComments> list = CommentsDAO.getComments(this.getTarget(),
	    targetId);
	Set<CrmUser> users = new LinkedHashSet<CrmUser>();

	for (CrmComments c : list) {
	    users.add(c.getCreatedBy());
	}

	List<CrmUser> lUsers = new ArrayList<CrmUser>();
	//lUsers.add(new CrmUser(0L, "All Comments", id, id));
	lUsers.addAll(new ArrayList<CrmUser>(users));

	searchForm = new SearchForm("searchForm", lUsers);
	searchForm.setOutputMarkupId(true);
	commentsContainer = new CommentsFragment("commentsContainer",
	    "commentsListFragment", getTarget(), list);
	commentsContainer.setOutputMarkupId(true);

	container.addOrReplace(commentsContainer);
	addOrReplace(searchForm, form);

	if (target != null) {
	    target.addComponent(form);
	    target.addComponent(searchForm);
	    target.addComponent(container);
	}
    }

    private void onAddComment(AjaxRequestTarget target, List<CrmComments> ilist) {
	List<CrmComments> list = ilist;

	if (list == null) {
	    list = CommentsDAO.getComments(this.getTarget(), targetId);
	}

	CommentsFragment replacement = new CommentsFragment("commentsContainer",
	    "commentsListFragment", getTarget(), list);

	//commentsContainer.replaceWith(replacement);
	commentsContainer = replacement;
	//commentsContainer.setOutputMarkupId(true);
	container.addOrReplace(commentsContainer);

	if (target != null) {
	    target.addComponent(container);
	    //target.addComponent(this);
	}

	searchForm.update(target, list);
    }

    public class SearchForm extends Form implements Serializable {
	private String criteria;
	private CrmUser filter;
	private DropDownChoice<CrmUser> filterDD;

	public SearchForm(String id, List<CrmUser> users) {
	    super(id);
	    setOutputMarkupId(true);

	    AjaxFallbackLink showAll = new AjaxFallbackLink("showAll") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    setFilter(null);
		    setCriteria(null);
		    SearchForm.this.modelChanged();
		    List<CrmComments> list = CommentsDAO.getComments(
			getTarget(), getTargetId());
		    CommentsPanel.this.onAddComment(target, list);
		}
	    };

	    IChoiceRenderer<CrmUser> cr = new IChoiceRenderer<CrmUser>() {

		@Override
		public Object getDisplayValue(CrmUser object) {
		    if (object == null || object.getUserId() == 0L) {
			return "All Comments";
		    }

		    return object.getFullName();
		}

		@Override
		public String getIdValue(CrmUser object, int index) {
		    return object.getUserId().toString();
		}
	    };

	    filterDD = new DropDownChoice<CrmUser>("filter",
		new PropertyModel<CrmUser>(this, "filter"), users , cr);
	    filterDD.setOutputMarkupId(true);

	    TextField criteriaText = new TextField("criteria",
		new PropertyModel<String>(this, "criteria"));

	    IndicatingAjaxButton searchButton =
		new IndicatingAjaxButton("searchButton", this) {

		@Override
		protected void onSubmit(AjaxRequestTarget target,
		    Form<?> form) {
		    List<CrmComments> list = CommentsDAO.searchComments(
			CommentsPanel.this.target, CommentsPanel.this.targetId,
			SearchForm.this.filter, SearchForm.this.criteria);

		    CommentsPanel.this.onAddComment(target, list);
		}
	    };

	    add(filterDD, criteriaText, searchButton, showAll);
	}

	public void update(AjaxRequestTarget target, List<CrmComments> list) {
	    Set<CrmUser> users = new LinkedHashSet<CrmUser>();
	    for (CrmComments c : list) {
		users.add(c.getCreatedBy());
	    }

	    List<CrmUser> lUsers = new ArrayList<CrmUser>();
	    //lUsers.add(new CrmUser(0L, "All Comments", "", ""));
	    lUsers.addAll(new ArrayList<CrmUser>(users));

	    filterDD.setChoices(lUsers);


	    if (target != null) {
		target.addComponent(filterDD);
	    }
	}

	public String getCriteria() {
	    return criteria;
	}

	public void setCriteria(String criteria) {
	    this.criteria = criteria;
	}

	public CrmUser getFilter() {
	    return filter;
	}

	public void setFilter(CrmUser filter) {
	    this.filter = filter;
	}
    }

    public static class CommentsFragment extends Fragment {
	public CommentsFragment(String id, String markupId,
	    final CommentsTarget target, List<CrmComments> comments) {
	    super(id, markupId);
	    setOutputMarkupId(true);
	    setOutputMarkupPlaceholderTag(true);
	    add(new ListView<CrmComments>("commentItem", comments) {

		@Override
		protected void populateItem(ListItem<CrmComments> item) {
		    CrmComments ci = item.getModelObject();
		    System.out.println("DEBUG: comment=" + ci.getCommentId());
		    Label text = new Label("commentText",
			new String(ci.getCommentText()));

		    if (CommentsTarget.PROJECT.equals(target)) {
			text.add(new SimpleAttributeModifier("class",
			    ci.getAssociatedWith() + "_comment"));
		    }

		    item.add(
			new Label("sentOn", 
			    WebUtil.formatDate(WebUtil.extendedDateFormat, 
			    ci.getCreatedAt())),
			new Label("userName", ci.getCreatedBy().getFullName()),
			new Label("sentAt", 
			    WebUtil.formatDate(WebUtil.timeFormat, 
			    ci.getCreatedAt())),
			text
		    );

		    item.add(new Label("attLabel", 
			ci.getAttachments().isEmpty() ? "" : "Attachments"));

		    item.add(new ListView<Attachments>("attachments",
			ci.getAttachments()) {

			@Override
			protected void populateItem(ListItem<Attachments> item) {
			    Attachments att = item.getModelObject();
			    AttachmentLink link = new AttachmentLink("attachmentLink", att);
			    link.add(new Label("attachmentName", att.getAttachmentName()));
			    item.add(link);
			}
		    });
		}
	    });
	}
    }

    public static class AddCommentsForm extends Form
	implements Serializable {

	private FeedbackPanel feedback;
	private Collection<FileUpload> attachments = new ArrayList<FileUpload>();

	public AddCommentsForm(String id, IModel<CommentsPanel> model) {
	    super(id, model);

	    setMultiPart(true);
	    setMaxSize(Bytes.megabytes(10));

	    feedback = new FeedbackPanel("addCommentsFeedback");
	    feedback.setOutputMarkupId(true);
	    final TextArea comments = new TextArea("commentsText");
	    //comments.setRequired(true);
	    comments.setOutputMarkupId(true);
	    comments.add(StringValidator.maximumLength(511));

	    final MultiFileUploadField attachmentsComp =
		new MultiFileUploadField(
		"attachments", new PropertyModel<Collection<FileUpload>>(this,
		"attachments"), 5);

	    IndicatingAjaxButton submit = new IndicatingAjaxButton(
		    "addComments") {
		private boolean isValid = false;
		private boolean opError = false;

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

		    isValid = true;
		    CommentsPanel m = (CommentsPanel) form.getModelObject();

		    if (m.getCommentsText() == null
			|| "".equals(m.getCommentsText().trim())) {
			isValid = false;
			error("Comments are required.");
			return;
		    }

		    CrmComments comment = new CrmComments();
		    comment.setCommentText(m.getCommentsText().getBytes());
		    comment.setCreatedAt(new Date());
		    CrmSession session = CrmSession.get();
		    comment.setCreatedBy(UserDAO.getUser(session.getUserId()));

		    List<CommentsDAO.Attachment> files =
			new ArrayList<CommentsDAO.Attachment>();

		    Iterator<FileUpload> iterator = attachments.iterator();

		    while (iterator.hasNext()) {
			FileUpload fu = iterator.next();
			CommentsDAO.Attachment attachment =
			    new CommentsDAO.Attachment();
			attachment.setContentType(fu.getContentType());
			attachment.setFileName(fu.getClientFileName());
			try {
			    attachment.setInputStream(fu.getInputStream());
			} catch (IOException ex) {
			    Logger.getLogger(CommentsPanel.class.getName())
				.log(Level.SEVERE, null, ex);
			    throw new RuntimeException(ex);
			}
			files.add(attachment);
		    }

		    CommentsDAO.addComments(m.getTarget(), m.getTargetId(),
			null, comment, files);

		    comments.clearInput();
		    attachmentsComp.clearInput();
                    attachmentsComp.setOutputMarkupId(true);
                    comments.setOutputMarkupId(true);
		    m.setCommentsText("");
		    form.modelChanged();
		    if (target != null) {
			//target.addComponent(AddCommentsForm.this.feedback);
			target.addComponent(comments);
			target.addComponent(attachmentsComp);
		    }

		    info("Comments added successfully");
		    m.onAddComment(target, null);
		}

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);

		    if (target != null) {
			target.addComponent(AddCommentsForm.this.feedback);
		    }

		    if (isValid && !opError) {
			error("Well this is embarrassing, add comments failed"
			    + " due to an internal error. Please report this"
			    + " instance to the application administrator.");
		    }
		}
	    };

	    IndicatingAjaxButton attach = new IndicatingAjaxButton(
		    "attachFiles") {
		private boolean isValid = false;
		private boolean opError = false;

		@Override
		protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

		    isValid = true;
		    if (attachments.isEmpty()) {
			isValid = false;
			error("Please provide files to be attached.");
			return;
		    }

		    CommentsPanel m = (CommentsPanel) form.getModelObject();
		    CrmComments comment = new CrmComments();
		    if (m.getCommentsText() == null ||
			"".equals(m.getCommentsText().trim())) {
			m.setCommentsText("");
		    }
		    comment.setCommentText(m.getCommentsText().getBytes());
		    comment.setCreatedAt(new Date());
		    CrmSession session = CrmSession.get();
		    comment.setCreatedBy(UserDAO.getUser(session.getUserId()));

		    List<CommentsDAO.Attachment> files =
			new ArrayList<CommentsDAO.Attachment>();

		    Iterator<FileUpload> iterator = attachments.iterator();

		    while (iterator.hasNext()) {
			FileUpload fu = iterator.next();
			CommentsDAO.Attachment attachment =
			    new CommentsDAO.Attachment();
			attachment.setContentType(fu.getContentType());
			attachment.setFileName(fu.getClientFileName());
			try {
			    attachment.setInputStream(fu.getInputStream());
			} catch (IOException ex) {
			    Logger.getLogger(CommentsPanel.class.getName())
				.log(Level.SEVERE, null, ex);
			    throw new RuntimeException(ex);
			}
			files.add(attachment);
		    }

		    CommentsDAO.addComments(m.getTarget(), m.getTargetId(),
			null, comment, files);

		    comments.clearInput();
		    attachmentsComp.clearInput();
                    attachmentsComp.setOutputMarkupId(true);
                    comments.setOutputMarkupId(true);
		    m.setCommentsText("");
		    form.modelChanged();
		    if (target != null) {
		    	/*
		    	 * BUG ID 44 [START]
		    	 */
		    	target.addComponent(AddCommentsForm.this.feedback);
		    	/*
		    	 * BUG ID 44 [END]
		    	 */		    	
			target.addComponent(comments);
			target.addComponent(attachmentsComp);
		    }

	    	/*
	    	 * BUG ID 44 [START]
	    	 */
		    //info("Comments added successfully");
	    	/*
	    	 * BUG ID 44 [END]
	    	 */		    
		    m.onAddComment(target, null);
	    	/*
	    	 * BUG ID 44 [START]
	    	 */		    
		    info("File has been attached successfully.");
	    	/*
	    	 * BUG ID 44 [START]
	    	 */
		}

		@Override
		protected void onError(AjaxRequestTarget target, Form<?> form) {
		    super.onError(target, form);

		    if (target != null) {
			target.addComponent(AddCommentsForm.this.feedback);
		    }

		    if (isValid && !opError) {
			error("Well this is embarrassing, add comments failed"
			    + " due to an internal error. Please report this"
			    + " instance to the application administrator.");
		    }
		}
	    };

	    add(attach, feedback, comments, submit, attachmentsComp);
	}

	public Collection<FileUpload> getAttachments() {
	    return attachments;
	}

	public void setAttachments(Collection<FileUpload> attachments) {
	    this.attachments = attachments;
	}

    }

    public String getCommentsText() {
	return commentsText;
    }

    public void setCommentsText(String commentsText) {
	this.commentsText = commentsText;
    }

    public CommentsTarget getTarget() {
	return target;
    }

    public void setTarget(CommentsTarget target) {
	this.target = target;
    }

    public Long getTargetId() {
	return targetId;
    }

    public void setTargetId(Long targetId) {
	this.targetId = targetId;
    }
}
