package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.TaskListMode;
import com.flexidea.crm.db.dto.Tasks;
import java.util.Iterator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class TasksDataProvider extends SortableDataProvider<Tasks> {
    private TaskListMode mode;
    private boolean showHistory;
    private Long user;
    private Integer priority;
    private boolean monitoring;

    public TasksDataProvider(TaskListMode mode, boolean showHistory, Long user,
	Integer priority, boolean monitoring) {
	this.mode = mode;
	this.showHistory = showHistory;
	this.user = user;
	this.priority = priority;
	this.monitoring = monitoring;
    }

    @Override
    public Iterator<? extends Tasks> iterator(int first, int count) {
	String orderBy = null;
	boolean asc = true;

	if (getSort() != null) {
	    orderBy = getSort().getProperty();
	    asc = getSort().isAscending();
	}

	return TasksDAO.listTasks(mode, priority, UserDAO.getUser(user),
		showHistory, monitoring, first, count, orderBy, asc)
		.iterator();
    }

    @Override
    public int size() {
	return TasksDAO.listTasks(mode, priority, UserDAO.getUser(user),
	    showHistory, monitoring, 0, Integer.MAX_VALUE, null, false).size();
    }

    public static class TaskModel extends LoadableDetachableModel<Tasks> {
	private Long taskId;

	public TaskModel(Tasks object) {
	    this.taskId = object.getId();
	}

	public TaskModel(Long object) {
	    this.taskId = object;
	}

	@Override
	protected Tasks load() {
	    return TasksDAO.getTask(taskId);
	}

	public Long getTaskId() {
	    return taskId;
	}

	public void setTaskId(Long taskId) {
	    this.taskId = taskId;
	}

    }

    @Override
    public IModel<Tasks> model(Tasks object) {
	return new TaskModel(object);
    }
}
