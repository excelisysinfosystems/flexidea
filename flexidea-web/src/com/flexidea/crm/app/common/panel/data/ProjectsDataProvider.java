package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.db.dto.ListMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class ProjectsDataProvider extends SortableDataProvider<Project> {
    private ListMode mode;
    private String criteria;
    private String alphaGroup;
    private Company company;

    private static final Map<String, String> reMap =
	new HashMap<String, String>();

    static {
	reMap.clear();
	reMap.put("A-F", "^[a-f]");
	reMap.put("G-M", "^[g-m]");
	reMap.put("N-S", "^[n-s]");
	reMap.put("T-Z", "^[t-z]");
    }

    public ProjectsDataProvider(ListMode mode, String criteria, 
	String alphaGroup, Company company) {
	this.mode = mode;
	this.criteria = criteria;
	this.alphaGroup = alphaGroup;
	this.company = company;
    }

    public List<Project> getList(int first, int count) {
	switch (mode) {
	    case ALPAHABATICAL:
		return ProjectDAO
		    .search(criteria, reMap.get(alphaGroup), 0,
		    false, false, first, count);
		    //.search(criteria, reMap.get(alphaGroup), false,
		//	first, count, false);
	    case RECENT:
		return ProjectDAO
		    .search(criteria, null, 0, true, false, first, count);
		    //.search(criteria, null, true, first, count, false);
	    case Q1:
		return ProjectDAO
		    .search(criteria, reMap.get(alphaGroup), 1, true, false,
		    first, count);
	    case Q2:
		return ProjectDAO
		    .search(criteria, reMap.get(alphaGroup), 2, true, false,
		    first, count);
	    case Q3:
		return ProjectDAO
		    .search(criteria, reMap.get(alphaGroup), 3, true, false,
		    first, count);
	    case Q4:
		return ProjectDAO
		    .search(criteria, reMap.get(alphaGroup), 4, true, false,
		    first, count);
	    case CONSULTANT_WISE:
		return ProjectDAO
		    .search(criteria, ContactsDAO.getContactType("Consultant"),
		    company,
		    first, count);
	    case SALES_REP_WISE:
		return ProjectDAO
		    .search(criteria,
		    ContactsDAO.getContactType("Sales Representative"),
		    company,
		    first, count);
	    case COMPANY_WISE:
		return ProjectDAO
		    .listByCompany(company, first, count);
	    case ID:
		List<Project> list = new ArrayList<Project>();
		Long id = null;
		try {
		    id = Long.parseLong(criteria);
		    if (id != null) {
			Project p = ProjectDAO.getProject(id);
			if (p != null) {
			    list.add(p);
			}
		    }
		} catch (Exception e) {

		}
		return list;
	}
	return null;
    }

    @Override
    public Iterator<? extends Project> iterator(int first, int count) {
	return getList(first, count).iterator();
    }

    @Override
    public int size() {
	return getList(0, Integer.MAX_VALUE).size();
    }

    public static class ProjectModel extends LoadableDetachableModel<Project> {
	private Long projectId;

	public ProjectModel(Project project) {
	    if (project != null)
		this.projectId = project.getId();
	}

	public ProjectModel(Long projectId) {
	    this.projectId = projectId;
	}

	@Override
	protected Project load() {
	    return ProjectDAO.getProject(projectId);
	}

    }

    @Override
    public IModel<Project> model(Project object) {
	return new ProjectModel(object);
    }

}
