package com.flexidea.crm.app.common.panel;
import java.io.Serializable;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author akshay
 */
public abstract class MarkAsDeadPanel extends Panel
    implements Serializable {
    private ModalWindow dialogWindow;

    public MarkAsDeadPanel(String id, String title) {
        super (id);
	add(dialogWindow = new ModalWindow("dialogWindow"));
	dialogWindow.setTitle(title);
	dialogWindow.setOutputMarkupId(true);
	dialogWindow.setOutputMarkupPlaceholderTag(true);
    }

    public abstract void onDone(AjaxRequestTarget target, String comment);

    public void open(AjaxRequestTarget target, String title) {
	dialogWindow.setTitle(title);
	dialogWindow.setContent(
	    new CommentFormPanel(dialogWindow.getContentId()) {

	    @Override
	    public void onDone(AjaxRequestTarget target, String comment) {
		dialogWindow.close(target);
		MarkAsDeadPanel.this.onDone(target, comment);
	    }
	});
	dialogWindow.show(target);
    }
}
