package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.LeadsDAO;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.ListMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class LeadsDataProvider extends SortableDataProvider<Lead> {
    private boolean history = false;
    private ListMode mode;
    private String criteria;
    private String alphaGroup;
    private boolean showDead = false;
    private boolean giveData = true;

    private static final Map<String, String> reMap =
	new HashMap<String, String>();

    static {
	reMap.clear();
	reMap.put("A-F", "^[a-f]");
	reMap.put("G-M", "^[g-m]");
	reMap.put("N-S", "^[n-s]");
	reMap.put("T-Z", "^[t-z]");
    }

    public LeadsDataProvider(boolean giveData) {
	this.history = true;
	this.mode = ListMode.RECENT;
	this.criteria = "";
	this.alphaGroup = null;
	showDead = true;
	this.giveData = giveData;
    }

    public LeadsDataProvider(ListMode mode, String criteria, String alphaGroup) {
	this.mode = mode;
	this.criteria = criteria;
	this.alphaGroup = alphaGroup;
    }

    private List<Lead> getList(int first, int count) {
	switch (mode) {
	    case ALPAHABATICAL:
		return LeadsDAO
		    .search(criteria, reMap.get(alphaGroup), 0, false, false,
		    false, first, count, null);
		    //.search(criteria, reMap.get(alphaGroup), false,
		//	first, count, false)
	    case RECENT:
		if (giveData) {
		    if (! history) {
			return LeadsDAO
			    .search(criteria, null, 0, true, showDead,
			    showDead, first, count, null);
		    } else {
			if (getSort() == null) {
			    return LeadsDAO
				.search(criteria, null, 0, true, 
				showDead, showDead, first, count, null);
			} else {
			    return LeadsDAO
				.search(criteria, null, 0, true, showDead,
				showDead, first, count,
				getSort().getProperty()
				+ (getSort().isAscending() ? " asc" : " desc")
				);
			}
		    }
		} else {
		    return new ArrayList<Lead>();
		}
		    //.search(criteria, null, true, first, count, false)
	    case Q1:
		return LeadsDAO
		    .search(criteria, reMap.get(alphaGroup), 1, true, false,
		    false, first, count, null);
		    //.search(criteria, null, true, first, count, false)
	    case Q2:
		return LeadsDAO
		    .search(criteria, reMap.get(alphaGroup), 2, true, false,
		    false, first, count, null);
	    case Q3:
		return LeadsDAO
		    .search(criteria, reMap.get(alphaGroup), 3, true, false,
		    false, first, count, null);
	    case Q4:
		return LeadsDAO
		    .search(criteria, reMap.get(alphaGroup), 4, true, false,
		    false, first, count, null);
	    case CONVERTED:
		return LeadsDAO
		    .search(criteria, reMap.get(alphaGroup), 0, true, false,
		    true, first, count, null);
	    case SALES_REP_WISE:
		return LeadsDAO
		    .search(criteria,
		    ContactsDAO.getContactType("Sales Representative"),
		    first, count);
	    case ID:
		List<Lead> list = new ArrayList<Lead>();
		Long id = null;
		try {
		    id = Long.parseLong(criteria);
		    if (id != null) {
			Lead p = LeadsDAO.getLead(id);
			if (p != null) {
			    list.add(p);
			}
		    }
		} catch (Exception e) {

		}
		return list;
	}
	return null;
    }

    @Override
    public Iterator<? extends Lead> iterator(int first, int count) {
	return getList(first, count).iterator();
    }

    @Override
    public int size() {
	return getList(0, Integer.MAX_VALUE).size();
    }

    public static class LeadModel extends LoadableDetachableModel<Lead> {
	private Long leadId;

	public LeadModel(Lead lead) {
	    this.leadId = lead.getId();
	}

	public LeadModel(Long leadId) {
	    this.leadId = leadId;
	}

	@Override
	protected Lead load() {
	    return LeadsDAO.getLead(leadId);
	}

    }

    @Override
    public IModel<Lead> model(Lead object) {
	return new LeadModel(object);
    }

}
