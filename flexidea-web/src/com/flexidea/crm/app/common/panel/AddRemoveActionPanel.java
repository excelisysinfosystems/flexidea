package com.flexidea.crm.app.common.panel;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author akshay
 */
public abstract class AddRemoveActionPanel extends Panel {
    public AddRemoveActionPanel(String id, final CommentsTarget cTarget,
	    final Long targetId, final Long contactId, boolean isAdded) {
        super (id);
	setOutputMarkupId(true);
	setOutputMarkupPlaceholderTag(true);

	Form addRemoveForm = new Form("addRemoveForm");
	addRemoveForm.setOutputMarkupId(true);

	AjaxButton addButton = new AjaxButton("addButton", addRemoveForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (targetId == null) {
		    return;
		}
		form.get("addButton").setVisible(false);
		form.get("removeButton").setVisible(true);
		//ContactsDAO.associateContact(cTarget, targetId, contactId,
		 //   UserDAO.getUser(CrmSession.get().getUserId()));
		AddRemoveActionPanel.this.onAdd(
		    cTarget, targetId, contactId);
		if (target != null) {
		    target.addComponent(form.get("addButton"));
		    target.addComponent(form.get("removeButton"));
		}
	    }
	};

	addButton.setOutputMarkupId(true);
	addButton.setOutputMarkupPlaceholderTag(true);

	AjaxButton removeButton = new AjaxButton("removeButton", addRemoveForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (targetId == null) {
		    return;
		}
		form.get("addButton").setVisible(true);
		form.get("removeButton").setVisible(false);

		//ContactsDAO.dissociateContact(cTarget, targetId, contactId,
		 //   UserDAO.getUser(CrmSession.get().getUserId()));
		AddRemoveActionPanel.this.onRemove(
		    cTarget, targetId, contactId);
		if (target != null) {
		    target.addComponent(form.get("addButton"));
		    target.addComponent(form.get("removeButton"));
		}
	    }
	};

	removeButton.setOutputMarkupId(true);
	removeButton.setOutputMarkupPlaceholderTag(true);

	if (isAdded) {
	    removeButton.setVisible(true);
	    addButton.setVisible(false);
	} else {
	    removeButton.setVisible(false);
	    addButton.setVisible(true);
	}

	addRemoveForm.add(addButton, removeButton);
	add(addRemoveForm);
    }

    public abstract void onAdd(CommentsTarget target,
	Long targetId, Long contactId);

    public abstract void onRemove(CommentsTarget target,
	Long targetId, Long contactId);
}
