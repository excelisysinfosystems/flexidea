package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.AuthDAO;
import com.flexidea.crm.db.dto.AuthRoles;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class AuthRolesDataProvider extends SortableDataProvider<AuthRoles> {

    private List<AuthRoles> list(int first, int count) {
	String orderBy = null;
	boolean asc = false;
	if (getSort() != null) {
	    orderBy = getSort().getProperty();
	    asc = getSort().isAscending();
	}
	return AuthDAO.list(orderBy, asc, first, count);
    }

    @Override
    public Iterator<? extends AuthRoles> iterator(int first, int count) {
	return list(first, count).iterator();
    }

    @Override
    public int size() {
	return list(0, Integer.MAX_VALUE).size();
    }

    public static class AuthRolesModel
	extends LoadableDetachableModel<AuthRoles> {
	private Long id;

	@Override
	protected AuthRoles load() {
	    return AuthDAO.getRole(null, id);
	}

	public Long getId() {
	    return id;
	}

	public void setId(Long id) {
	    this.id = id;
	}

	public AuthRolesModel(Long id) {
	    this.id = id;
	}

	public AuthRolesModel(AuthRoles role) {
	    this.id = role.getId();
	}
    }

    @Override
    public IModel<AuthRoles> model(AuthRoles object) {
	return new AuthRolesModel(object);
    }
}
