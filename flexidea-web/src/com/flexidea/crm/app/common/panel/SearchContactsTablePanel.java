package com.flexidea.crm.app.common.panel;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.common.panel.data.SortableContactsDataProvider;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public abstract class SearchContactsTablePanel extends Panel
    implements Serializable {

    private List<Long> toAdd = new ArrayList<Long>();
    private ContactType contactType;
    private CommentsTarget target;
    private Long targetId;
    private String criteria;
    private DataTable dataTable;
    private WebMarkupContainer tableContainer;

    public SearchContactsTablePanel(String id, ContactType contactType,
	CommentsTarget target, Long targetId) {
	super(id);

	this.contactType = contactType;
	this.target = target;
	this.targetId = targetId;

	setOutputMarkupId(true);

	Label education = null;
	if (targetId == null) {
	    switch (target) {
		case CONTACTS:
		    education = new Label("education",
			"Kindly save the contact first");
		    break;
		case ENQUIRY:
		    education = new Label("education",
			"Kindly save the enquiry first");
		    break;
		case LEADS:
		    education = new Label("education",
			"Kindly save the lead first");
		    break;
		case PROJECT:
		    education = new Label("education",
			"Kindly save the project first");
		    break;
		case TASK:
		    education = new Label("education",
			"Kindly save the task first");
		    break;
	    }
	} else {
	    education = new Label("education", "");
	    education.setOutputMarkupPlaceholderTag(true);
	    education.setVisible(false);
	}

	add(education);

	Form searchForm = new Form("searchForm", new CompoundPropertyModel(this));
	TextField criteriaText = new TextField("criteria");

	IChoiceRenderer<ContactType> cr = new IChoiceRenderer<ContactType>() {

	    @Override
	    public Object getDisplayValue(ContactType object) {
		return object.getTypeName();
	    }

	    @Override
	    public String getIdValue(ContactType object, int index) {
		return object.getId().toString();
	    }
	};

	DropDownChoice<ContactType> contactTypeDD = 
	    new DropDownChoice<ContactType>("contactType",
	    ContactsDAO.getContactTypes(), cr);

	IndicatingAjaxButton searchButton = new IndicatingAjaxButton("searchButton") {
	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		prepareDataTable(target);
	    }
	};

	IndicatingAjaxButton doneButton = new IndicatingAjaxButton("doneButton") {
	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		for (Long contactId : toAdd) {
		    ContactsDAO.associateContact(
			SearchContactsTablePanel.this.target,
			SearchContactsTablePanel.this.targetId,
			contactId, UserDAO.getUser(
			CrmSession.get().getUserId()));
		}
		onDone(target);
	    }
	};

	searchForm.add(doneButton, criteriaText, contactTypeDD, searchButton);
	add(searchForm);
	tableContainer = new WebMarkupContainer("tableContainer");
	tableContainer.setOutputMarkupId(true);
	add(tableContainer);
	prepareDataTable(null);
    }

    private void prepareDataTable(AjaxRequestTarget itarget) {
	List<IColumn<Contact>> columns = new ArrayList<IColumn<Contact>>();

	columns.add(new AbstractColumn<Contact>(new Model<String>("Actions")) {

	    @Override
	    public void populateItem(Item<ICellPopulator<Contact>> cellItem,
		    String componentId, IModel<Contact> rowModel) {
		cellItem.add(new AddRemoveActionPanel(componentId,
		    target, targetId, rowModel.getObject().getId(),
		    toAdd.contains(rowModel.getObject().getId())) {

		    @Override
		    public void onAdd(CommentsTarget target, Long targetId, Long contactId) {
			List<Long> list = 
			    SearchContactsTablePanel.this.toAdd;
			if (! list.contains(contactId)) {
			    list.add(contactId);
			}
		    }

		    @Override
		    public void onRemove(CommentsTarget target, Long targetId, Long contactId) {
			List<Long> list =
			    SearchContactsTablePanel.this.toAdd;
			if (list.contains(contactId)) {
			    list.remove(contactId);
			}
		    }
		});
	    }
	});

	columns.add(new PropertyColumn<Contact>(new Model<String>("First Name"),
		"firstName", "firstName"));
	columns.add(new PropertyColumn<Contact>(new Model<String>("Last Name"),
		"lastName", "lastName"));
	columns.add(new PropertyColumn<Contact>(new Model<String>("Company"),
		"company.companyName", "company.companyName"));
	columns.add(new PropertyColumn<Contact>(new Model<String>("Type"),
		"contactType", "contactType.typeName"));

	dataTable =  new AjaxFallbackDefaultDataTable<Contact>("table",
		columns,
		new SortableContactsDataProvider(targetId, this.target,
		criteria, contactType),
		//new ContactsDataProvider(ListMode.RECENT, targetId, criteria, criteria),

		20);
	//new AjaxFallbackDefaultDataTable("table", columns, null, FLAG_RESERVED1);

	tableContainer.addOrReplace(dataTable);

	if (itarget != null) {
	    itarget.addComponent(tableContainer);
	}
    }

    public ContactType getContactType() {
	return contactType;
    }

    public void setContactType(ContactType contactType) {
	this.contactType = contactType;
    }

    public String getCriteria() {
	return criteria;
    }

    public void setCriteria(String criteria) {
	this.criteria = criteria;
    }

    public CommentsTarget getTarget() {
	return target;
    }

    public void setTarget(CommentsTarget target) {
	this.target = target;
    }

    public Long getTargetId() {
	return targetId;
    }

    public void setTargetId(Long targetId) {
	this.targetId = targetId;
    }

    public abstract void onDone(AjaxRequestTarget target);
}
