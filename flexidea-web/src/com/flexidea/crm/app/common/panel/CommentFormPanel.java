/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.common.panel;
import java.io.Serializable;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;

/**
 *
 * @author akshay
 */
public abstract class CommentFormPanel extends Panel
    implements Serializable {
    private String comment;

    public CommentFormPanel(String id) {
	super(id);
	StatelessForm<CommentFormPanel> form =
		new StatelessForm<CommentFormPanel>("commentForm",
		new CompoundPropertyModel<CommentFormPanel>(this)
		);
	add(form);
	form.add(new TextArea("comment").setRequired(true));
	form.add(new FeedbackPanel("commentFeedback")
		.setOutputMarkupId(true));
	form.add(new IndicatingAjaxButton("submitButton", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		if (target != null) {
		    target.addComponent(form.get("commentFeedback"));
		}

		CommentFormPanel m = (CommentFormPanel) form.getModelObject();
		onDone(target, m.getComment());
	    }

	    @Override
	    protected void onError(AjaxRequestTarget target, Form<?> form) {
		super.onError(target, form);
		if (target != null) {
		    target.addComponent(form.get("commentFeedback"));
		}
	    }
	});
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public abstract void onDone(AjaxRequestTarget target,
	String comment);
}
