package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CrmUser;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class UserDataProvider extends SortableDataProvider<CrmUser> {

    private List<CrmUser> list(int first, int count) {
	String orderBy = null;
	boolean asc = false;

	if (getSort() != null) {
	    orderBy = getSort().getProperty();
	    asc = getSort().isAscending();
	}

	return UserDAO.listUsers(orderBy, asc, first, count);
    }

    @Override
    public Iterator<? extends CrmUser> iterator(int first, int count) {
	return list(first, count).iterator();
    }

    @Override
    public int size() {
	return list(0, Integer.MAX_VALUE).size();
    }

    public static class UserDataModel extends LoadableDetachableModel<CrmUser> {
	private Long id;

	@Override
	protected CrmUser load() {
	    return UserDAO.getUser(id);
	}

	public Long getId() {
	    return id;
	}

	public void setId(Long id) {
	    this.id = id;
	}

	public UserDataModel(Long id) {
	    this.id = id;
	}

	public UserDataModel(CrmUser user) {
	    this.id = user.getUserId();
	}
    }

    @Override
    public IModel<CrmUser> model(CrmUser object) {
	return new UserDataModel(object);
    }

}
