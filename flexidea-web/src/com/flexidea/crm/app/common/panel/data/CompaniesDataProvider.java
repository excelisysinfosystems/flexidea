/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.CompaniesDAO;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.ListMode;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class CompaniesDataProvider extends SortableDataProvider<Company> {
    private String criteria;
    private boolean recent;
    private String city;
    private String industry;
    private ListMode mode;

    public CompaniesDataProvider(ListMode mode, String criteria, String city,
	String industry, boolean recent) {

	this.criteria = criteria;
	this.recent = recent;
	this.city = city;
	this.industry = industry;
	this.mode = mode;
    }

    private List<Company> list(int first, int count) {
	switch (mode) {
	    case INDUSTRY_WISE:
		return CompaniesDAO
		    .search(criteria, city, industry, first, count, recent)
		//return ContactsDAO
		 //   .listCompanies(null, null, null, first, count)
		    ;
	    case COMPANY_WISE:
		return CompaniesDAO
		    .search(criteria, city, industry, first, count, recent)
		//return ContactsDAO
		 //   .listForCompany(companyId, null, null, first, count)
		    ;
	    case CONSULTANT_WISE:
		return ContactsDAO
		    .listCompanies(null, criteria,
			ContactsDAO.getContactType("Consultant"),
			first, count) ;
	    case SUPPLIER_WISE:
		return ContactsDAO
		    .listCompanies(null, criteria,
			ContactsDAO.getContactType("Supplier"),
			first, count) ;
	    case SALES_REP_WISE:
		return ContactsDAO
		    .listCompanies(null, criteria,
			ContactsDAO.getContactType("Sales Representative"),
			first, count) ;
	    case CITY_WISE:
		return ContactsDAO.listCompanyByCity(null, city, first, count);

	}
	return null;
    }

    @Override
    public Iterator<? extends Company> iterator(int first, int count) {
	return list(first, count)
	    .iterator();
    }

    @Override
    public int size() {
	return list(0, Integer.MAX_VALUE)
	    .size();
    }

    public class CompanyModel extends LoadableDetachableModel<Company>  {
	Long companyId;

	public CompanyModel(Company object) {
	    super(object);
	    this.companyId = object.getId();
	}

	@Override
	protected Company load() {
	    return CompaniesDAO.getCompany(companyId);
	}

	public Long getCompanyId() {
	    return companyId;
	}

	public void setCompanyId(Long companyId) {
	    this.companyId = companyId;
	}
    }

    @Override
    public IModel<Company> model(Company object) {
	return new CompanyModel(object);
    }

    public String getCriteria() {
	return criteria;
    }

    public void setCriteria(String criteria) {
	this.criteria = criteria;
    }

    public boolean isRecent() {
	return recent;
    }

    public void setRecent(boolean recent) {
	this.recent = recent;
    }

}
