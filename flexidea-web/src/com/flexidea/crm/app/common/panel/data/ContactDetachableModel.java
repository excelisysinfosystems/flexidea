package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dto.Contact;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class ContactDetachableModel extends LoadableDetachableModel<Contact> {
    private Long id;

    public ContactDetachableModel(Contact object) {
	super(object);
	this.id = object.getId();
    }

    public ContactDetachableModel(Long id) {
	this.id = id;
    }

    @Override
    protected Contact load() {
	return ContactsDAO.getContact(id);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final ContactDetachableModel other = (ContactDetachableModel) obj;
	if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public int hashCode() {
	return Long.valueOf(id).hashCode();
    }

}
