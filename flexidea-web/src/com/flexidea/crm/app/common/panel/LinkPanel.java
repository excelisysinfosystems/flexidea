package com.flexidea.crm.app.common.panel;
import com.flexidea.crm.app.pages.ContactsPage;
import com.flexidea.crm.app.pages.EnquiryPage;
import com.flexidea.crm.app.pages.LeadsPage;
import com.flexidea.crm.app.pages.ProjectsPage;
import com.flexidea.crm.db.dto.CommentsTarget;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author akshay
 */
public final class LinkPanel extends Panel {
    public LinkPanel(String id, final CommentsTarget linkTarget,
	final Long targetId, final String linkCaption) {
        super (id);

	PageParameters params = new PageParameters();
	Class page = null;

	switch (linkTarget) {
	    case CONTACTS:
		params.put("contact", targetId);
		page = ContactsPage.class;
		break;
	    case LEADS:
		params.put("lead", targetId);
		page = LeadsPage.class;
		break;
	    case PROJECT:
		params.put("project", targetId);
		page = ProjectsPage.class;
		break;
	    case ENQUIRY:
		params.put("enquiry", targetId);
		page = EnquiryPage.class;
		break;
	}

	BookmarkablePageLink link =
	    new BookmarkablePageLink("itemLink", page, params);

	link.add(new Label("itemLabel", linkCaption));
	add(link);
    }
}
