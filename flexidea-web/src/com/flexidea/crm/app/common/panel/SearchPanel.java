package com.flexidea.crm.app.common.panel;

import com.flexidea.crm.app.common.components.AjaxPagingNavigatorWithImages;
import com.flexidea.crm.app.common.panel.data.AlphabaticalDataProvider;
import com.flexidea.crm.app.common.panel.data.CompaniesDataProvider;
import com.flexidea.crm.app.common.panel.data.ContactsDataProvider;
import com.flexidea.crm.app.common.panel.data.EnquiryDataProvider;
import com.flexidea.crm.app.common.panel.data.LeadsDataProvider;
import com.flexidea.crm.app.common.panel.data.ProjectsDataProvider;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.ListMode;
import com.flexidea.crm.db.dto.Project;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.PropertyModel;
/**
 *
 * @author akshay
 */
public final class SearchPanel extends Panel {
    private Mode mode;
    private String criteria;
    private CommentsTarget searchTarget;
    private Label modeLabel;
    private List<Mode> modes = new ArrayList<Mode>();

    private String selectedAlphaGroup;
    private Long selectedCompany;
    private WebMarkupContainer table;
    private DropDownChoice<Mode> modeDD;
    private Form searchForm;
    private WebMarkupContainer container;
    /*
     * BUG ID 48 [START] 
     */
    private int currentPage = 0;
    private static int previousSearchTargetCurrentPage = 0;
    private static Long previousSelectedCompany = null; 
    private static CommentsTarget previousSearchTarget;
    /*
     * BUG ID 48 [END] 
     */
    public SearchPanel(String id, CommentsTarget cTarget) {
        super (id);
	this.searchTarget = cTarget;
	
	
    /*
     * BUG ID 48 [START] 
     */
	if(SearchPanel.previousSearchTarget!=null && SearchPanel.previousSearchTarget.equals(cTarget)){
		setCurrentPage(SearchPanel.previousSearchTargetCurrentPage);
	}
	SearchPanel.previousSearchTarget = cTarget;
	selectedCompany = SearchPanel.previousSelectedCompany;
    /*
     * BUG ID 48 [END] 
     */
    
    
	container = new WebMarkupContainer("container");
	container
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true)
	    .setMarkupId("midDataLeft");
	add(container);
	Mode companyWise = new Mode(ListMode.COMPANY_WISE, "Company");
	Mode all = new Mode(ListMode.ALPAHABATICAL, "All");
	Mode recent = new Mode(ListMode.RECENT, "Recent");
	Mode industryWise = new Mode(ListMode.INDUSTRY_WISE, "Industry");
	Mode supplierWise = new Mode(ListMode.SUPPLIER_WISE, "Supplier");
	Mode cityWise = new Mode(ListMode.CITY_WISE, "City");
	Mode consultantWise = new Mode(ListMode.CONSULTANT_WISE, "Consultant");

	Mode salesRepWise = new Mode(ListMode.SALES_REP_WISE, "Sales Rep.");
	Mode converted = new Mode(ListMode.CONVERTED, "All Converted");
	Mode q1 = new Mode(ListMode.Q1, "Q1");
	Mode q2 = new Mode(ListMode.Q2, "Q2");
	Mode q3 = new Mode(ListMode.Q3, "Q3");
	Mode q4 = new Mode(ListMode.Q4, "Q4");
	Mode byId = new Mode(ListMode.ID, "ID");

	modes.add(recent);
	modes.add(all);

	if (CommentsTarget.CONTACTS.equals(cTarget)) {
	    modes.add(companyWise);
	    modes.add(industryWise);
	    modes.add(supplierWise);
	    modes.add(cityWise);
	    modes.add(consultantWise);
	}

	if (CommentsTarget.LEADS.equals(cTarget)) {
	    modes.add(salesRepWise);
	    modes.add(converted);
	    modes.add(q1);
	    modes.add(q2);
	    modes.add(q3);
	    modes.add(q4);
	}

	if (CommentsTarget.PROJECT.equals(cTarget)) {
	    modes.add(companyWise);
	    modes.add(consultantWise);
	    modes.add(salesRepWise);
	    modes.add(q1);
	    modes.add(q2);
	    modes.add(q3);
	    modes.add(q4);
	}

	if (CommentsTarget.ENQUIRY.equals(cTarget)) {
	    modes.add(companyWise);
	    modes.add(consultantWise);
	    modes.add(salesRepWise);
	    modes.add(q1);
	    modes.add(q2);
	    modes.add(q3);
	    modes.add(q4);
	}

	modes.add(byId);
	// Set default
	if (CommentsTarget.LEADS.equals(cTarget)) {
	    mode = recent;
	} else if (CommentsTarget.PROJECT.equals(cTarget)) {
	    mode = recent;
	} else if (CommentsTarget.ENQUIRY.equals(cTarget)) {
	    mode = recent;
	} else {
	    mode = companyWise;
	}

	modeLabel = new Label("modeLabel",
	    new PropertyModel<String>(this, "mode.displayValue"));
	modeLabel.setOutputMarkupId(true);

	IChoiceRenderer<Mode> cr = new IChoiceRenderer<Mode>() {

	    @Override
	    public Object getDisplayValue(Mode object) {
		return object.getDisplayValue();
	    }

	    @Override
	    public String getIdValue(Mode object, int index) {
		return object.getListMode().toString();
	    }
	};

	modeDD = new DropDownChoice<Mode>(
	    "mode", new PropertyModel<Mode>(this, "mode"), modes, cr);

	modeDD.add(new AjaxFormComponentUpdatingBehavior("onchange") {

	    @Override
	    protected void onUpdate(AjaxRequestTarget target) {
		if (mode == null) {
		    update(target, null);
		} else {
		    switch (mode.listMode) {
			case CONVERTED:
			case Q1:
			case Q2:
			case Q3:
			case Q4:
			case ALPAHABATICAL:
			    update(target, selectedAlphaGroup);
			    break;
			case SALES_REP_WISE:
			case CONSULTANT_WISE:
			case SUPPLIER_WISE:
			case INDUSTRY_WISE:
			case CITY_WISE:
			case COMPANY_WISE:
			    update(target, selectedCompany);
			    break;
			case ID:
			case RECENT:
			    update(target, null);
			    break;
		    }
		}
	    }
	});

	searchForm = new Form("searchForm");
	TextField<String> criteriaText =
	    new TextField<String>("criteria", new PropertyModel<String>(
	    this, "criteria"));

	IndicatingAjaxButton searchButton =
	    new IndicatingAjaxButton("searchButton", searchForm) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		update(target, null);
	    }
	};

	searchForm.add(criteriaText, searchButton);
	searchForm.setOutputMarkupId(true);

	table = new WebMarkupContainer("listContainer");
	table.setOutputMarkupId(true);
	table.setOutputMarkupPlaceholderTag(true);
	container.addOrReplace(modeDD.setOutputMarkupId(true),
	    searchForm.setOutputMarkupId(true), table);
	update();
	setOutputMarkupId(true);
    }
    
    /*
     * BUG ID 48 [START] 
     */
    public void setCurrentPage(int page){
    	this.currentPage = page;
    	SearchPanel.previousSearchTargetCurrentPage = page;
    }
    /*
     * BUG ID 48 [END] 
     */

    public void update(AjaxRequestTarget target, Object object) {

	if (object != null) {
	    if (object instanceof Long) {
		this.selectedCompany = (Long) object;
	    /*
	     * BUG ID 48 [START] 
	     */
		SearchPanel.previousSelectedCompany = selectedCompany;
	    /*
	     * BUG ID 48 [END] 
	     */
	    } else if (object instanceof String) {
		this.selectedAlphaGroup = (String) object;
	    }
	}

	update();
	if (target != null) {
	    /*
	    target.addComponent(modeLabel.setOutputMarkupId(true));
	    target.addComponent(table.setOutputMarkupId(true));
	    target.addComponent(modeDD.setOutputMarkupId(true));
	    target.addComponent(searchForm.setOutputMarkupId(true));
	     *
	     */
	    target.addComponent(container);
	}
    }

    public void update(AjaxRequestTarget target) {
	update();
	if (target != null) {
	    /*
	    target.addComponent(modeLabel.setOutputMarkupId(true));
	    target.addComponent(table.setOutputMarkupId(true));
	    target.addComponent(modeDD.setOutputMarkupId(true));
	    target.addComponent(searchForm.setOutputMarkupId(true));
	     *
	     */
	    target.addComponent(container);
	}
    }

    private void update() {
	if (mode == null) {
	    mode = modes.get(0);
	}

	modeLabel = new Label("modeLabel",
	    new PropertyModel<String>(this, "mode.displayValue"));
	modeLabel.setOutputMarkupId(true);

	container.addOrReplace(modeLabel);

	switch (mode.getListMode()) {
	    case CONVERTED:
	    case Q1:
	    case Q2:
	    case Q3:
	    case Q4:
	    case ALPAHABATICAL:
		doAlphabats();
		break;
	    case CONSULTANT_WISE:
	    case SUPPLIER_WISE:
	    case SALES_REP_WISE:
	    case INDUSTRY_WISE:
	    case CITY_WISE:
	    case COMPANY_WISE:
		doCompany();
		break;
	    case ID:
	    case RECENT:
		doRecent();
		break;
	}
    }

    private void doAlphabats() {
	table.addOrReplace(new GroupList<String>("list",
	    new AlphabaticalDataProvider(), 15, table,currentPage));
    }

    private void doCompany() {
	switch (mode.getListMode()) {
	    case CITY_WISE:
		table.addOrReplace(new GroupList<Company>
		    ("list", new CompaniesDataProvider(mode.getListMode(), 
		    criteria, criteria, null, false), 15, table,currentPage));
		break;
	    case INDUSTRY_WISE:
		table.addOrReplace(new GroupList<Company>
		    ("list", new CompaniesDataProvider(mode.getListMode(),
		    null, null, criteria, false), 15, table,currentPage));
		break;
	    case COMPANY_WISE:
		table.addOrReplace(new GroupList<Company>
		    ("list", new CompaniesDataProvider(mode.getListMode(),
		    criteria, null, null, false), 15, table,currentPage));
		break;
	    case CONSULTANT_WISE:
	    case SUPPLIER_WISE:
	    case SALES_REP_WISE:
		table.addOrReplace(new GroupList<Company>
		    ("list", new CompaniesDataProvider(mode.getListMode(),
		    criteria, null, null, false), 15, table,currentPage));
		break;
	}


	table.setOutputMarkupId(true);
	container.addOrReplace(table);
    }

    private void doRecent() {

	switch (searchTarget) {
	    case CONTACTS:
		table.addOrReplace(new GroupList<Contact>("list",
		    new ContactsDataProvider(mode.getListMode(), null,
		    criteria, null), 15, table,currentPage));
	    break;
	    case LEADS:
		table.addOrReplace(new GroupList<Lead>("list",
		    new LeadsDataProvider(mode.getListMode(),
		    criteria, null), 15, table,currentPage));
	    break;
	    case PROJECT:
		table.addOrReplace(new GroupList<Project>("list",
		    new ProjectsDataProvider(mode.getListMode(),
		    criteria, null, null), 15, table,currentPage));
	    break;
	    case ENQUIRY:
		table.addOrReplace(new GroupList<Enquiry>("list",
		    new EnquiryDataProvider(mode.getListMode(),
		    criteria, null, null),
		    15, table,currentPage));
	    break;
	}

	table.setOutputMarkupId(true);
	container.addOrReplace(table);
    }

    private static class Mode implements Serializable {
	private ListMode listMode;
	private String displayValue;

	public Mode(ListMode listMode, String displayValue) {
	    this.listMode = listMode;
	    this.displayValue = displayValue;
	}

	public String getDisplayValue() {
	    return displayValue;
	}

	public void setDisplayValue(String displayValue) {
	    this.displayValue = displayValue;
	}

	public ListMode getListMode() {
	    return listMode;
	}

	public void setListMode(ListMode listMode) {
	    this.listMode = listMode;
	}
    }

    private final class GroupList<T> extends DataView<T> {
	private WebMarkupContainer parentContainer;

	public GroupList(String id, IDataProvider<T> dataProvider,
	    int itemsPerPage, WebMarkupContainer iparentContainer,int currentPage) {
	    super(id, dataProvider, itemsPerPage);
	    this.parentContainer = iparentContainer;

	    AjaxPagingNavigatorWithImages navigator =
		new AjaxPagingNavigatorWithImages("navigator", this) {

		@Override
		protected void onAjaxEvent(AjaxRequestTarget target) {
		    //super.onAjaxEvent(target);
		    target.addComponent(parentContainer);
		}

	    };
	    parentContainer.addOrReplace(navigator);
	    /*
	     * BUG ID 48 [START] 
	     */	    
	    setCurrentPage(currentPage);
	    /*
	     * BUG ID 48 [END] 
	     */
	}

    /*
     * BUG ID 48 [START] 
     */
	@Override
	protected void onAfterRender() {
		SearchPanel.this.setCurrentPage(getCurrentPage());
		super.onAfterRender();
	}
    /*
     * BUG ID 48 [END] 
     */
	
	@Override
	protected void populateItem(Item<T> item) {
	    T modelObject = item.getModelObject();
	    if (modelObject instanceof Company) {
		Company c = (Company) modelObject;
		boolean expand = c.getId().equals(selectedCompany);

		item.add(new ListGroupPanel(
		    "item", mode.getListMode(),
		    searchTarget, c.getId(), c.getCompanyName(),
		    null, criteria, SearchPanel.this, expand));
	    } else if (modelObject instanceof String) {
		String groupTitle = (String) modelObject;
		boolean expand = groupTitle.equals(selectedAlphaGroup);

		item.add(new ListGroupPanel(
		    "item", mode.getListMode(), //ListMode.ALPAHABATICAL,
		    searchTarget, null, groupTitle, groupTitle, criteria,
		    SearchPanel.this, expand));
	    } else if (modelObject instanceof Contact) {
		Contact c = (Contact) modelObject;
		item.add(new LinkPanel("item",
		    CommentsTarget.CONTACTS, c.getId(),
		    c.getFirstName() + " " + c.getLastName()));
	    } else if (modelObject instanceof Lead) {
		Lead c = (Lead) modelObject;
		item.add(new LinkPanel("item",
		    CommentsTarget.LEADS, c.getId(),
		    c.getLeadName()));
	    } else if (modelObject instanceof Project) {
		Project c = (Project) modelObject;
		item.add(new LinkPanel("item",
		    CommentsTarget.PROJECT, c.getId(),
		    c.getProjectName()));
	    }  else if (modelObject instanceof Enquiry) {
		Enquiry c = (Enquiry) modelObject;
		item.add(new LinkPanel("item",
		    CommentsTarget.ENQUIRY, c.getId(),
		    c.getProject().getProjectName()));
	    }
	}
    }
}
