package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dto.ContactType;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class ContactTypesDataProvider
    extends SortableDataProvider<ContactType> {
    private List<ContactType> list(int first, int count) {
	String orderBy = null;
	boolean asc = true;
	if (getSort() != null) {
	    orderBy = getSort().getProperty();
	    asc = getSort().isAscending();
	}
	return ContactsDAO.list(orderBy, asc, first, count);
    }

    @Override
    public Iterator<? extends ContactType> iterator(int first, int count) {
	return list(first, count).iterator();
    }

    @Override
    public int size() {
	return list(0, Integer.MAX_VALUE).size();
    }

    public static class ContactTypeModel extends LoadableDetachableModel<ContactType> {
	private Long id;

	public ContactTypeModel(Long id) {
	    this.id = id;
	}

	public ContactTypeModel(ContactType object) {
	    this.id = object.getId();
	}

	@Override
	protected ContactType load() {
	    throw new UnsupportedOperationException("Not supported yet.");
	}

	public Long getId() {
	    return id;
	}

	public void setId(Long id) {
	    this.id = id;
	}
    }

    @Override
    public IModel<ContactType> model(ContactType object) {
	return new ContactTypeModel(object);
    }

}
