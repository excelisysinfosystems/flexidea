package com.flexidea.crm.app.common.panel;
import com.flexidea.crm.app.base.CrmSession;
import com.flexidea.crm.app.pages.ContactsPage;
import com.flexidea.crm.app.task.panels.FollowupTaskPanel;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import java.util.List;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author akshay
 */
public final class ContactsAssociationPanel extends Panel {
    private Long targetId;
    private CommentsTarget target;
    private ModalWindow followupWindow;
    private ModalWindow searchContacts;

    public void update(AjaxRequestTarget aTarget, 
	CommentsTarget itarget, Long itargetId) {
	update(itarget, itargetId);
	if (aTarget != null) {
	    aTarget.addComponent(this);
	}
    }

    public void update(CommentsTarget itarget, Long itargetId) {

	this.target = itarget;
	this.targetId = itargetId;

	List<Contact> list = null;

	list = ContactsDAO.listAssociatedContacts(target, targetId);

	addContactList(list);

	searchContacts = new ModalWindow("searchContacts");

	searchContacts.setOutputMarkupId(true);
	searchContacts.setOutputMarkupPlaceholderTag(true);
	searchContacts.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {

	    @Override
	    public void onClose(AjaxRequestTarget mtarget) {
		addContactList(ContactsDAO
		    .listAssociatedContacts(target, targetId));
		if (mtarget != null) {
		    mtarget.addComponent(ContactsAssociationPanel.this);
		}
	    }
	});

	Form form = new Form("form");
	form.add(searchContacts);
	form.add(new AjaxButton("associateContacts", form) {

	    @Override
	    protected void onSubmit(AjaxRequestTarget itarget, Form<?> form) {
		searchContacts.setContent(new SearchContactsTablePanel(
		    searchContacts.getContentId(), null, target, targetId) {

		    @Override
		    public void onDone(AjaxRequestTarget target) {
			ContactsAssociationPanel.this.onDone(target);
		    }
		});
		searchContacts.show(itarget);
		if (itarget != null) {
		    itarget.addComponent(searchContacts);
		}
	    }
	});

	this.followupWindow = new ModalWindow("followupWindow");
	followupWindow.setOutputMarkupId(true);
	followupWindow.setOutputMarkupPlaceholderTag(true);

	setOutputMarkupId(true);
	addOrReplace(form, followupWindow);
    }

    public ContactsAssociationPanel(String id, CommentsTarget itarget,
	Long itargetId) {
        super (id);
	update(itarget, itargetId);
    }

    private void addContactList(List<Contact> list) {
	ListView<Contact> contactsContainer =
	    new ListView<Contact>("contactsContainer", list) {

	    @Override
	    protected void populateItem(ListItem<Contact> item) {
		Contact contact = item.getModelObject();
		if (contact.getContactType().isInternal()) {
		    item.add(new ContactFragment("contact", "internalContact", contact));
		} else {
		    item.add(new ContactFragment("contact", "externalContact", contact));
		}
	    }
	};
	contactsContainer.setOutputMarkupId(true);
	contactsContainer.setOutputMarkupPlaceholderTag(true);
	addOrReplace(contactsContainer);
    }

    private final class ContactFragment extends Fragment {

	public ContactFragment(String id, String markupId, final Contact contact) {
	    super(id, markupId);
	    setOutputMarkupId(true);

	    if (contact.getContactType().isInternal()) {
		//TODO: add link
	    }
	    Label ctype = new Label("contactType",
		contact.getContactType().getTypeName());

	    if (contact.getContactType().isInternal()) {
		ctype.add(new AjaxEventBehavior("onclick") {

		    @Override
		    protected void onEvent(AjaxRequestTarget itarget) {
			followupWindow.setContent(new FollowupTaskPanel(
			    followupWindow.getContentId(), target, targetId, 
			    UserDAO.getUser(CrmSession.get().getUserId()),
			    contact.getUser(), followupWindow));
			followupWindow.show(itarget);
		    }
		});
	    }

	    add(ctype);
	    add(new Label("contactJobTitle", contact.getJobTitle()));
	    add(new Label("contactCompanyName", contact.getCompany().getCompanyName()));
	    add(new Label("contactEmail", contact.getEmail()));
	    add(new Label("contactCell", contact.getCell()));
	    Link contactLink = new Link("contactLink") {

		@Override
		public void onClick() {
		    PageParameters params = new PageParameters();
		    params.put("contact", contact.getId());
		    setResponsePage(ContactsPage.class, params);
		}
	    };
	    contactLink.add(new Label("contactName",
		contact.getFirstName() + " " + contact.getLastName()));

	    AjaxLink removeLink = new AjaxLink("removeLink") {

		@Override
		public void onClick(AjaxRequestTarget target) {
		    ContactsDAO.dissociateContact(
			ContactsAssociationPanel.this.target, targetId,
			contact.getId(), UserDAO
			.getUser(CrmSession.get().getUserId()));
		    ContactsAssociationPanel.this.update(
			ContactsAssociationPanel.this.target, targetId);
		    if (target != null) {
			target.addComponent(ContactsAssociationPanel.this);
		    }
		}
	    };

	    removeLink.setOutputMarkupId(true);
	    removeLink.setOutputMarkupPlaceholderTag(true);

	    if (CommentsTarget.ENQUIRY.equals(target)
		&& contact.getContactType()
		.getTypeName().equals("Sales Representative")
	    ) {
		removeLink.setVisible(false);
	    }

	    add(contactLink, removeLink);
	}
    }

    public CommentsTarget getTarget() {
	return target;
    }

    public void setTarget(CommentsTarget target) {
	this.target = target;
    }

    public Long getTargetId() {
	return targetId;
    }

    public void setTargetId(Long targetId) {
	this.targetId = targetId;
    }

    private void onDone(AjaxRequestTarget target) {
	if (searchContacts != null) {
	    searchContacts.close(target);
	}
    }
}
