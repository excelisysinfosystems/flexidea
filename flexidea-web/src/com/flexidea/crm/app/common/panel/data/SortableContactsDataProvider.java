package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import java.util.Iterator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;

/**
 *
 * @author akshay
 */
public class SortableContactsDataProvider extends SortableDataProvider<Contact>{
    private Long targetId;
    private CommentsTarget target;
    private String criteria;
    private ContactType contactType;

    public SortableContactsDataProvider(Long targetId, CommentsTarget target,
	String criteria, ContactType contactType) {
	this.targetId = targetId;
	this.target = target;
	this.criteria = criteria;
	this.contactType = contactType;
	/*
	System.out.println("DEBUG: sortableContacts t="
	    + target.toString() + ", tid=" + targetId + ", c=" + criteria
	    + ", ct=" + contactType);
	 *
	 */
    }

    @Override
    public Iterator<? extends Contact> iterator(int first, int count) {
	String sortCol = null;
	boolean asc = false;

	if (this.getSort() != null) {
	    sortCol = getSort().getProperty();
	    asc = getSort().isAscending();
	}

	return ContactsDAO
	    .searchContacts(target, targetId, criteria, contactType, first, count,
	    false, true, true, sortCol, asc).iterator();
    }

    @Override
    public int size() {
	return ContactsDAO
	    .searchContacts(target, targetId, criteria, contactType, 
	    0, Integer.MAX_VALUE, false, true, true, null, true).size();
    }

    @Override
    public IModel<Contact> model(Contact object) {
	return new ContactDetachableModel(object);
    }

    public CommentsTarget getTarget() {
	return target;
    }

    public void setTarget(CommentsTarget target) {
	this.target = target;
    }

    public Long getTargetId() {
	return targetId;
    }

    public void setTargetId(Long targetId) {
	this.targetId = targetId;
    }

    public ContactType getContactType() {
	return contactType;
    }

    public void setContactType(ContactType contactType) {
	this.contactType = contactType;
    }

    public String getCriteria() {
	return criteria;
    }

    public void setCriteria(String criteria) {
	this.criteria = criteria;
    }
}
