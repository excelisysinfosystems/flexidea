package com.flexidea.crm.app.common.panel;
import com.flexidea.crm.app.common.components.AjaxPagingNavigatorWithImages;
import com.flexidea.crm.app.common.panel.data.AlphabaticalDataProvider;
import com.flexidea.crm.app.common.panel.data.ContactsDataProvider;
import com.flexidea.crm.app.common.panel.data.EnquiryDataProvider;
import com.flexidea.crm.app.common.panel.data.LeadsDataProvider;
import com.flexidea.crm.app.common.panel.data.ProjectsDataProvider;
import com.flexidea.crm.app.pages.ContactsPage;
import com.flexidea.crm.app.pages.EnquiryPage;
import com.flexidea.crm.app.pages.LeadsPage;
import com.flexidea.crm.app.pages.ProjectsPage;
import com.flexidea.crm.db.dao.CompaniesDAO;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.ListMode;
import com.flexidea.crm.db.dto.Project;
import java.io.Serializable;
import org.apache.wicket.PageParameters;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;

/**
 *
 * @author akshay
 */
public final class ListGroupPanel extends Panel implements Serializable {
    private ListMode mode;
    private CommentsTarget linkTarget;
    private Long targetId;
    private String groupTitle;
    private String groupRegex;
    private String criteria;
    private SearchPanel parentPanel;
    private boolean expand;

    public ListGroupPanel(String id, ListMode mode, CommentsTarget linkTarget,
	Long targetId, String groupTitle, String groupRegex, 
	String criteria, SearchPanel iparentPanel, boolean expand) {
	super(id);
	this.mode = mode;
	this.linkTarget = linkTarget;
	this.targetId = targetId;
	this.groupTitle = groupTitle;
	this.groupRegex = groupRegex;
	this.criteria = criteria;
	this.parentPanel = iparentPanel;
	this.expand = expand;

	System.out.println("DEBUG: ListGroupPanel m=" + mode.toString()
	    + ", lt=" + linkTarget.toString() + ", t=" + targetId
	    + ", gT=" + groupTitle + ", re=" + groupRegex + ", c=" + criteria
	    );

	setOutputMarkupId(true);
	populate();
    }

    public void populate() {
	AjaxLink titleLink = null;
	Label titleLabel = null;

	titleLabel = new Label("titleLabel", groupTitle);
	titleLink = new AjaxLink("titleLink") {
	    @Override
	    public void onClick(AjaxRequestTarget target) {
		switch (mode) {
		    case CONVERTED:
		    case Q1:
		    case Q2:
		    case Q3:
		    case Q4:
		    case ALPAHABATICAL:
			parentPanel.update(target,
				ListGroupPanel.this.groupTitle);
			break;
		    case SALES_REP_WISE:
		    case CONSULTANT_WISE:
		    case SUPPLIER_WISE:
		    case CITY_WISE:
		    case INDUSTRY_WISE:
		    case COMPANY_WISE:
			parentPanel.update(target, targetId);
			break;
		    case RECENT:
			PageParameters params = new PageParameters();
			switch(linkTarget) {
			    case CONTACTS:
				params.put("contact", targetId);
				setResponsePage(ContactsPage.class, params);
				break;
			    case LEADS:
				params.put("lead", targetId);
				setResponsePage(LeadsPage.class, params);
				break;
			    case PROJECT:
				params.put("project", targetId);
				setResponsePage(ProjectsPage.class, params);
				break;
			    case ENQUIRY:
				params.put("enquiry", targetId);
				setResponsePage(EnquiryPage.class, params);
				break;
			}
			break;
		}
	    }
	};

	titleLink.addOrReplace(titleLabel);
	add(titleLink);
	if (! expand) {
	    WebMarkupContainer cc =
		new WebMarkupContainer("groupContainer");
	    cc.setOutputMarkupId(true);
	    cc.setOutputMarkupPlaceholderTag(true);
	    cc.add(new GroupList<String>("rows",
		new AlphabaticalDataProvider(), 15, linkTarget, cc));
	    add(cc);
	    cc.setVisible(false);
	} else {
	    switch (mode) {
		case CONVERTED:
		case SALES_REP_WISE:
		case Q1:
		case Q2:
		case Q3:
		case Q4:
		case CONSULTANT_WISE:
		case SUPPLIER_WISE:
		case CITY_WISE:
		case INDUSTRY_WISE:
		case COMPANY_WISE:
		case ALPAHABATICAL:
		    switch (linkTarget) {
			case CONTACTS:
			    WebMarkupContainer cc =
				new WebMarkupContainer("groupContainer");
			    cc.setOutputMarkupId(true);
			    cc.setOutputMarkupPlaceholderTag(true);
			    cc.setVisible(true);
			    cc.add(new GroupList<Contact>("rows",
				new ContactsDataProvider(mode, targetId, 
				criteria, groupTitle), 15, linkTarget, cc));
			    add(cc);
			    break;
			case LEADS:
			    WebMarkupContainer lc =
				new WebMarkupContainer("groupContainer");
			    lc.setOutputMarkupId(true);
			    lc.setOutputMarkupPlaceholderTag(true);
			    lc.setVisible(true);
			    lc.add(new GroupList<Lead>("rows",
				new LeadsDataProvider(mode, criteria,
				groupTitle), 15, linkTarget, lc));
			    add(lc);
			    break;
			case PROJECT:
			    WebMarkupContainer pc =
				new WebMarkupContainer("groupContainer");
			    pc.setOutputMarkupId(true);
			    pc.setOutputMarkupPlaceholderTag(true);
			    pc.setVisible(true);
			    pc.add(new GroupList<Project>("rows",
				new ProjectsDataProvider(mode, criteria,
				groupTitle, CompaniesDAO.getCompany(targetId)),
				15, linkTarget, pc));
			    add(pc);
			    break;
			case ENQUIRY:
			    WebMarkupContainer ec =
				new WebMarkupContainer("groupContainer");
			    ec.setOutputMarkupId(true);
			    ec.setOutputMarkupPlaceholderTag(true);
			    ec.setVisible(true);
			    ec.add(new GroupList<Enquiry>("rows",
				new EnquiryDataProvider(mode, criteria,
				groupTitle, CompaniesDAO.getCompany(targetId)),
				15, linkTarget, ec));
			    add(ec);
			    break;
		    }
		    break;
	    }
	}
    }

    public CommentsTarget getLinkTarget() {
	return linkTarget;
    }

    public void setLinkTarget(CommentsTarget linkTarget) {
	this.linkTarget = linkTarget;
    }

    public Long getTargetId() {
	return targetId;
    }

    public void setTargetId(Long targetId) {
	this.targetId = targetId;
    }

    public String getGroupRegex() {
	return groupRegex;
    }

    public void setGroupRegex(String groupRegex) {
	this.groupRegex = groupRegex;
    }

    public String getGroupTitle() {
	return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
	this.groupTitle = groupTitle;
    }

    public ListMode getMode() {
	return mode;
    }

    public void setMode(ListMode mode) {
	this.mode = mode;
    }

    public String getCriteria() {
	return criteria;
    }

    public void setCriteria(String criteria) {
	this.criteria = criteria;
    }

    private final class GroupList<T> extends DataView<T> {

	private CommentsTarget target;
	private WebMarkupContainer parentContainer;

	public GroupList(String id,
	    IDataProvider<T> dataProvider,
	    int itemsPerPage, CommentsTarget target,
	    WebMarkupContainer iparentContainer) {
	    super(id, dataProvider, itemsPerPage);
	    this.target = target;
	    this.parentContainer = iparentContainer;

	    AjaxPagingNavigatorWithImages navigator = 
		new AjaxPagingNavigatorWithImages("groupNavigator", this) {

		@Override
		protected void onAjaxEvent(AjaxRequestTarget target) {
		    //super.onAjaxEvent(target);
		    target.addComponent(parentContainer);
		}

	    };
	    parentContainer.addOrReplace(navigator);
	}


	@Override
	protected void populateItem(Item<T> item) {
	    switch (target) {
		case CONTACTS:
		    Contact c = (Contact) item.getModelObject();
		    LinkPanel f = new LinkPanel("col",
			CommentsTarget.CONTACTS, c.getId(),
			c.getFirstName() + " " + c.getLastName());
		    item.add(f);
		break;
		case LEADS:
		    Lead l = (Lead) item.getModelObject();
		    LinkPanel lf = new LinkPanel("col",
			CommentsTarget.LEADS, l.getId(),
			l.getLeadName());
		    item.add(lf);
		break;
		case PROJECT:
		    Project p = (Project) item.getModelObject();
		    LinkPanel pf = new LinkPanel("col",
			CommentsTarget.PROJECT, p.getId(),
			p.getProjectName());
		    item.add(pf);
		break;
		case ENQUIRY:
		    Enquiry e = (Enquiry) item.getModelObject();
		    LinkPanel ef = new LinkPanel("col",
			CommentsTarget.ENQUIRY, e.getId(),
			e.getProject().getProjectName());
		    item.add(ef);
		break;
	    }
	}
    }
}
