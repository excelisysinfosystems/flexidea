/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.common.panel.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 *
 * @author akshay
 */
public class AlphabaticalDataProvider extends SortableDataProvider<String> {
    private List<String> list;

    public AlphabaticalDataProvider() {
	list = new ArrayList<String>();
	list.add("A-F");
	list.add("G-M");
	list.add("N-S");
	list.add("T-Z");
    }

    @Override
    public Iterator<? extends String> iterator(int first, int count) {
	return list.iterator();
    }

    @Override
    public int size() {
	return list.size();
    }

    @Override
    public IModel<String> model(String object) {
	return new Model<String>(object);
    }

}
