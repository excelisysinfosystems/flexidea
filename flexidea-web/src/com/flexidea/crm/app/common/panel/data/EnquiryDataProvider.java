package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.EnquiryDAO;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.ListMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 *
 * @author akshay
 */
public class EnquiryDataProvider extends SortableDataProvider<Enquiry> {
    private ListMode mode;
    private String criteria;
    private String alphaGroup;
    private Company company;

    private static final Map<String, String> reMap =
	new HashMap<String, String>();

    static {
	reMap.clear();
	reMap.put("A-F", "^[a-f]");
	reMap.put("G-M", "^[g-m]");
	reMap.put("N-S", "^[n-s]");
	reMap.put("T-Z", "^[t-z]");
    }

    public List<Enquiry> getList(int first, int count) {
	switch (mode) {
	    case ALPAHABATICAL:
		return EnquiryDAO
		    .search(criteria, reMap.get(alphaGroup), 0,
		    false, false, first, count);
		    //.search(criteria, reMap.get(alphaGroup), false,
		//	first, count, false);
	    case RECENT:
		return EnquiryDAO
		    .search(criteria, null, 0, true, false, first, count);
		    //.search(criteria, null, true, first, count, false);
	    case Q1:
		return EnquiryDAO
		    .search(criteria, reMap.get(alphaGroup), 1, true, false,
		    first, count);
	    case Q2:
		return EnquiryDAO
		    .search(criteria, reMap.get(alphaGroup), 2, true, false,
		    first, count);
	    case Q3:
		return EnquiryDAO
		    .search(criteria, reMap.get(alphaGroup), 3, true, false,
		    first, count);
	    case Q4:
		return EnquiryDAO
		    .search(criteria, reMap.get(alphaGroup), 4, true, false,
		    first, count);
	    case CONSULTANT_WISE:
		return EnquiryDAO
		    .search(criteria, ContactsDAO.getContactType("Consultant"),
		    company,
		    first, count);
	    case SALES_REP_WISE:
		return EnquiryDAO
		    .search(criteria,
		    ContactsDAO.getContactType("Sales Representative"),
		    company,
		    first, count);
	    case COMPANY_WISE:
		return EnquiryDAO
		    .listByCompany(company, first, count);
	    case ID:
		List<Enquiry> list = new ArrayList<Enquiry>();
		Long id = null;
		try {
		    id = Long.parseLong(criteria);
		    if (id != null) {
			Enquiry e = EnquiryDAO.getEnquiry(id);
			if (e != null) {
			    list.add(e);
			}
		    }
		} catch (Exception e) {

		}
		return list;
	}
	return null;
    }

    public EnquiryDataProvider(ListMode mode, String criteria, 
	String alphaGroup, Company company) {
	this.mode = mode;
	this.criteria = criteria;
	this.alphaGroup = alphaGroup;
	this.company = company;
    }

    @Override
    public Iterator<? extends Enquiry> iterator(int first, int count) {
	return getList(first, count).iterator();
    }

    @Override
    public int size() {
	return getList(0, Integer.MAX_VALUE).size();
    }

    public static class EnquiryModel extends LoadableDetachableModel<Enquiry> {
	private Long enquiryId;

	public EnquiryModel(Enquiry enquiry) {
	    this.enquiryId = enquiry.getId();
	}

	public EnquiryModel(Long enquiryId) {
	    this.enquiryId = enquiryId;
	}

	@Override
	protected Enquiry load() {
	    return EnquiryDAO.getEnquiry(enquiryId);
	}

    }

    @Override
    public IModel<Enquiry> model(Enquiry object) {
	return new EnquiryModel(object);
    }

}
