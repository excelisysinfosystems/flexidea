package com.flexidea.crm.app.common.panel.data;

import com.flexidea.crm.db.dao.ProjectDAO;
import com.flexidea.crm.db.dto.Project;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 *
 * @author akshay
 */
public class ProjectsHistoryDataProvider extends SortableDataProvider<Project> {
    private boolean showHistory;
    private SortableComparator comparator = new SortableComparator();

    public ProjectsHistoryDataProvider(boolean history) {
	this.showHistory = history;
    }

    private List<Project> list(int first , int max) {
	if (! showHistory) {
	    return new ArrayList<Project>();
	}

	List<Project> data = ProjectDAO.history(first, max);
	Collections.sort(data, comparator);

	int count = max;

	if (count == Integer.MAX_VALUE) {
	    count = Integer.MAX_VALUE;
	} else {
	    count += first;
	}

	if (count > data.size()) {
	    count = data.size();
	}

	return data.subList(first, count);
    }

    @Override
    public Iterator<? extends Project> iterator(int first, int count) {
	List<Project> list = list(first, count);
	return list.iterator();
    }

    @Override
    public int size() {
	int size = list(0, Integer.MAX_VALUE).size();
	return size;
    }

    @Override
    public IModel<Project> model(Project object) {
	return new ProjectsDataProvider.ProjectModel(object);
    }

    private class SortableComparator implements Comparator<Project>, 
	Serializable {

	@Override
	public int compare(Project o1, Project o2) {
	    if (getSort() == null) {
		return 0;
	    }

	    PropertyModel<Comparable> m1 =
		new PropertyModel<Comparable>(o1, 
		getSort().getProperty());
	    PropertyModel<Comparable> m2 =
		new PropertyModel<Comparable>(o2,
		getSort().getProperty());
	    int score = 0;

	    if (m1.getObject() == null && m2.getObject() != null) {
		score = 1;
	    } else if (m1.getObject() != null && m2.getObject() == null) {
		score = -1;
	    } else if (m1.getObject() != null && m2.getObject() != null) {
		score = m1.getObject().compareTo(m2.getObject());
	    }

	    if (! getSort().isAscending()) {
		score = -score;
	    }

	    return score;
	}
    }
}
