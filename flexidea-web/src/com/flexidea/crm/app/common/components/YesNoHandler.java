package com.flexidea.crm.app.common.components;

import org.apache.wicket.ajax.AjaxRequestTarget;

/**
 *
 * @author akshay
 */
public interface YesNoHandler {
    void onYes(AjaxRequestTarget target);
    void onNo(AjaxRequestTarget target);
}
