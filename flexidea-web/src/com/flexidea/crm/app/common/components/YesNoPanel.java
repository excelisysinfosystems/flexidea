package com.flexidea.crm.app.common.components;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author akshay
 */
public class YesNoPanel extends Panel {
    ModalWindow dialogWindow;

    public YesNoPanel(String id) {
        super (id);
	dialogWindow = new ModalWindow("dialogWindow");
	dialogWindow
	    .setOutputMarkupId(true)
	    .setOutputMarkupPlaceholderTag(true);
	add(dialogWindow);
    }

    public void show(AjaxRequestTarget target, final YesNoHandler handler) {
	Fragment frag = new Fragment(dialogWindow.getContentId(),
	    "dialogForm");
	AjaxFallbackLink yesLink = new AjaxFallbackLink("yesLink") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		dialogWindow.close(target);
		if (handler != null) {
		    handler.onYes(target);
		}
	    }
	};

	AjaxFallbackLink noLink = new AjaxFallbackLink("noLink") {

	    @Override
	    public void onClick(AjaxRequestTarget target) {
		dialogWindow.close(target);
		if (handler != null) {
		    handler.onNo(target);
		}
	    }
	};

	frag.add(yesLink, noLink);

	dialogWindow.setContent(frag);
	dialogWindow.show(target);
    }

}
