/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.common.components;

import java.util.Iterator;
import org.apache.wicket.ajax.IAjaxIndicatorAware;
import org.apache.wicket.extensions.ajax.markup.html.AjaxIndicatorAppender;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.model.IModel;

/**
 *
 * @author akshay
 */
public class IndicatingAutoCompleteTextField<T> extends AutoCompleteTextField<T>
    implements IAjaxIndicatorAware {
    private AjaxIndicatorAppender indicatorAppender;

    public IndicatingAutoCompleteTextField(String id, IModel<T> object) {
	super(id, object);
	indicatorAppender = new AjaxIndicatorAppender();
	add(indicatorAppender);
    }

    @Override
    protected Iterator<T> getChoices(String input) {
	return null;
    }

    @Override
    public String getAjaxIndicatorMarkupId() {
	return indicatorAppender.getMarkupId();
    }

}
