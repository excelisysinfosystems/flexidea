package com.flexidea.crm.app.common.components;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;

/**
 *
 * @author akshay
 */
public class AjaxPagingNavigatorWithImages extends AjaxPagingNavigator {

    public AjaxPagingNavigatorWithImages(String id, IPageable pageable,
	IPagingLabelProvider labelProvider) {
	super(id, pageable, labelProvider);
    }

    public AjaxPagingNavigatorWithImages(String id, IPageable pageable) {
	super(id, pageable, null);
    }

}
