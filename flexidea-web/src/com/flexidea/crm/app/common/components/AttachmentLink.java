/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.app.common.components;

import com.flexidea.crm.db.dao.AttachmentDAO;
import com.flexidea.crm.db.dto.Attachments;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.target.resource.ResourceStreamRequestTarget;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.resource.ResourceStreamNotFoundException;
import org.apache.wicket.util.time.Time;

/**
 *
 * @author akshay
 */
public class AttachmentLink extends Link {
    private Attachments attachment;

    public AttachmentLink(String id, Attachments attachment) {
	super(id);
	this.attachment = attachment;
    }

    @Override
    public void onClick() {
	final IResourceStream stream = new ByteStreamResource(getAttachment());
	getRequestCycle().setRequestTarget(new
	    ResourceStreamRequestTarget(stream) {

	    @Override
	    public String getFileName() {
		return ((ByteStreamResource)stream)
			.getAttachment()
			.getAttachmentName();
	    }
	});
    }

    private static class ByteStreamResource implements IResourceStream {
	private Attachments attachment;
	private Locale locale;
	private ByteArrayInputStream stream;

	public ByteStreamResource(Attachments attachment) {
	    this.attachment = attachment;
	    stream = new ByteArrayInputStream(attachment.getAttachmentContent());
	}

	public Attachments getAttachment() {
	    return attachment;
	}

	public void setAttachment(Attachments attachment) {
	    this.attachment = attachment;
	}

	@Override
	public String getContentType() {
	    return attachment.getContentType();
	}

	@Override
	public long length() {
	    return attachment.getAttachmentContent().length;
	}

	@Override
	public InputStream getInputStream() throws ResourceStreamNotFoundException {
	    return this.stream;
	}

	@Override
	public void close() throws IOException {
	    this.stream.close();
	}

	@Override
	public Locale getLocale() {
	    return locale;
	}

	@Override
	public void setLocale(Locale locale) {
	    this.locale = locale;
	}

	@Override
	public Time lastModifiedTime() {
	    return Time.valueOf(attachment.getComment().getCreatedAt());
	}
    }

    public Attachments getAttachment() {
	return attachment;
    }

    public void setAttachment(Attachments attachment) {
	this.attachment = attachment;
    }
}
