package com.flexidea.crm.app.common.components;

import com.flexidea.crm.app.common.panel.data.LeadsDataProvider;
import com.flexidea.crm.db.dto.Lead;
import java.util.List;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackHeadersToolbar;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.extensions.markup.html.repeater.data.table.NoRecordsToolbar;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.model.IModel;

/**
 *
 * @author akshay
 */
public class AjaxDataTable<T> extends DataTable<T> {

    public AjaxDataTable(String id, final List<IColumn<T>> columns,
                ISortableDataProvider<T> dataProvider, int rowsPerPage)
    {
	this(id, columns.toArray((IColumn<T>[])new IColumn[columns.size()]),
	    dataProvider, rowsPerPage);
    }


    public AjaxDataTable(String id, IColumn<T>[] columns, ISortableDataProvider<T> dataProvider, int rowsPerPage) {
	super(id, columns, dataProvider, rowsPerPage);
	setOutputMarkupId(true);
	setVersioned(false);
	addTopToolbar(new AjaxNavigationToolbar(this) {

	    @Override
	    protected PagingNavigator newPagingNavigator(String navigatorId, DataTable<?> table) {
		return new AjaxPagingNavigatorWithImages(navigatorId, table);
	    }

	    @Override
	    protected WebComponent newNavigatorLabel(String navigatorId, DataTable<?> table) {
		return new Label(navigatorId);
	    }

	});
	addTopToolbar(new AjaxFallbackHeadersToolbar(this, dataProvider));
	addBottomToolbar(new NoRecordsToolbar(this));
    }

    @Override
    protected Item<T> newRowItem(String id, int index, IModel<T> model)
    {
	return new OddEvenItem<T>(id, index, model);
    }

}
