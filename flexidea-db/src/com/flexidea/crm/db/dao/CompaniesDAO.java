package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class CompaniesDAO {

    public static Company getCompany(Long id) {
	return (Company) DBUtil.getEntity(Company.class, id);
    }

    public static List<Company> search(String criteria) {
	return search(criteria, 0, Integer.MAX_VALUE, false);
    }

    public static List<Company> search(String criteria, boolean isRegex) {
	return search(criteria, 0, Integer.MAX_VALUE, false);
    }

    @Deprecated
    public static int searchCount(String criteria, int first, 
	int max, boolean sortByTime) {
	return search(criteria, first, max, sortByTime).size();
    }

    @Deprecated
    public static List<Company> search(String icriteria, 
	int first, int max, boolean sortByTime) {
	List<Company> list = new ArrayList<Company>();
	EntityManager em = DBUtil.getEM();

	String criteria = icriteria == null ? "" : icriteria;

	try {
	    Query q = em.createQuery("select Object(o) from Company o"
		+ " where o.companyName = :name"
		+ " or o.companyName like :criteria"
		+ (sortByTime ? " order by o.createdAt asc" : "")
		);
	    q.setParameter("name", criteria);
	    q.setParameter("criteria", "%" + criteria + "%");

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);
	    else
		q.setMaxResults(max);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    list.addAll(q.getResultList());
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Company> search(String icriteria, String icity,
	String iIndustry, int first, int max, boolean sortByTime) {

	List<Company> list = new ArrayList<Company>();
	EntityManager em = DBUtil.getEM();

	String criteria = icriteria == null ? "" : icriteria;
	String city = icity == null ? "" : icity;
	String industry = iIndustry == null ? "" : iIndustry;

	try {
	    Query q = null;
	    if (iIndustry != null) {
		q = em.createQuery("select Object(o)"
		    + " from Company o"
		    + " where"
		    + " o.industry = :industry"
		    + " or o.industry like :indCriteria"
		    + (sortByTime ? " order by o.createdAt asc" : "")
		    );
		q.setParameter("industry", industry);
		q.setParameter("indCriteria", "%" + industry + "%");
	    } else if (icity != null) {
		q = em.createQuery("select DISTINCT co"
		    + " from Contact c JOIN c.city ct JOIN c.company co"
		    + " where ct.cityName = :city or ct.cityName like :criteria"
		    //+ " group by c.company"
		    + (sortByTime ? " order by c.createdAt asc" : "")
		    );
		q.setParameter("city", city);
		q.setParameter("criteria", "%" + city + "%");
	    } else {
		q = em.createQuery("select Object(o)"
		    + " from Company o"
		    + " where"
		    + " o.companyName = :name"
		    + " or o.companyName like :criteria"
		    + (sortByTime ? " order by o.createdAt asc" : "")
		    );
		q.setParameter("name", criteria);
		q.setParameter("criteria", "%" + criteria + "%");
	    }

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);
	    else
		q.setMaxResults(max);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    em.close();
	}

	return list;
    }

    public static Company getCompany(String companyName) {
	List<Company> list = search(companyName);

	for (Company c : list) {
	    if (c.getCompanyName().equalsIgnoreCase(companyName))
		return c;
	}

	return null;
    }

    public static List<Company> searchIndustries (String iCriteria, int first,
	int max) {
	List<Company> list = new ArrayList<Company>();

	String criteria = iCriteria == null ? "" : iCriteria;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select DISTINCT c from Company c"
		+ " where c.industry = :name or c.industry like :criteria",
		Company.class);
	    q.setParameter("name", criteria);
	    q.setParameter("criteria", "%" + criteria + "%");

	    q.setFirstResult(first == 0 ? first : first - 1);

	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return list;
    }

    public static List<Company> listByContactType(ContactType contactType) {
	List<Company> list = new ArrayList<Company>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c) from Company c"
		+ " where c.contactType = :contactType",
		Company.class);
	    q.setParameter("contactType", contactType);

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return list;
    }
}
