package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.EnquiryContacts;
import com.flexidea.crm.db.dto.EnquiryDocs;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.db.dto.ProjectContacts;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class EnquiryDAO {

    public static Enquiry getEnquiry(Long targetId) {
	return (Enquiry) DBUtil.getEntity(Enquiry.class, targetId);
    }

    public static Enquiry update(Enquiry enquiry) {
	EntityManager em = DBUtil.getEM();

	try {
	    em.getTransaction().begin();

	    if (enquiry.getId() == null) {
		enquiry.setDead(false);
		em.persist(enquiry);
		for (ProjectContacts pc :
		    enquiry.getProject().getProjectContacts()) {
		    EnquiryContacts ec = new EnquiryContacts();
		    ec.setContact(pc.getContact());
		    ec.setEnquiry(enquiry);
		    em.persist(ec);
		}

		CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
		    enquiry.getId(), enquiry, enquiry.getCreatedBy(),
		    "Created enquiry");
	    } else {
		Enquiry oldEnquiry = getEnquiry(enquiry.getId());
		em.merge(enquiry);
		if (! oldEnquiry.isDead() && enquiry.isDead()) {
		    CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
			enquiry.getId(), enquiry, enquiry.getModifiedBy(),
			"Enquiry marked as dead\nReason:\n"
			+ enquiry.getComment()
			);
		} else if (! oldEnquiry.getSalesRep()
		    .equals(enquiry.getSalesRep())) {
		    CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
			enquiry.getId(), enquiry, enquiry.getModifiedBy(),
			"Changed sales rep. to " + 
			enquiry.getSalesRep().getFirstName()
			+ " " +
			enquiry.getSalesRep().getFirstName()
			);
		} else if (! oldEnquiry.getContractor()
		    .equals(enquiry.getContractor())) {
		    CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
			enquiry.getId(), enquiry, enquiry.getModifiedBy(),
			"Changed contractor to " +
			enquiry.getContractor().getFirstName()
			+ " " +
			enquiry.getContractor().getFirstName()
			);
		}

	    }

	    em.getTransaction().commit();
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return enquiry;
    }

    public static EnquiryDocs revise(EnquiryDocs docs) {
	EntityManager em = DBUtil.getEM();

	try {
	    em.getTransaction().begin();

	    em.persist(docs);

	    CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
		docs.getEnquiry().getId(), docs.getEnquiry(), 
		docs.getCreatedBy(), "Created revision #" + docs.getRevesionNo());
	    em.getTransaction().commit();
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}
	return docs;
    }

    public static boolean contactLookup (Project project, Contact contact) {
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(e) from Enquiry e"
		+ " where e.project = :project and e.contractor = :contractor",
		Enquiry.class);
	    q.setParameter("project", project);
	    q.setParameter("contractor", contact);
	    if (! q.getResultList().isEmpty()) {
		return true;
	    }
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}
	return false;
    }

    @Deprecated
    public static List<Enquiry> search(String iCriteria, String iRegex,
	    boolean recent, int min, int max, boolean listDead) {
	List<Enquiry> list = new ArrayList<Enquiry>();

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	String query = "select e.* from enquiry e, project p"
	    + " where e.project = p.ID and p.project_name LIKE ?1";

	if (iRegex != null) {
	    query = "select e.* from enquiry e, project p"
		+ " where e.project = p.ID and p.project_name REEXP ?2 "
		+ " and p.project_name LIKE ?1";
	}

	/*
	if (! listDead) {
	    query += " and is_dead = 0";
	}
	 *
	 */

	if (recent) {
	    query += " order by created_at desc";
	}

	try {
	    Query q = em.createNativeQuery(query, Enquiry.class);
	    q.setParameter(1, "%" + criteria + "%");
	    if (iRegex != null) {
		q.setParameter(2, iRegex);
	    }

	    q.setFirstResult(min == 0 ? min : min - 1);
	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Enquiry> listByCompany(Company company, int first, int max) {
	List<Enquiry> list = new ArrayList<Enquiry>();

	String query = "select distinct l from"
	    + " EnquiryContacts lc JOIN lc.enquiry l JOIN lc.contact c"
	    + " where "
	    + " c.company = :company";
	    //+ " and l.isDead = :dead";

	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery(query, Enquiry.class);
	    //q.setParameter("dead", Boolean.FALSE);
	    q.setParameter("company", company);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
	return list;
    }

    public static List<Enquiry> search(String iCriteria,
	ContactType contactType, Company company, int first, int max) {
	List<Enquiry> list = new ArrayList<Enquiry>();

	String query = "select Object(l) from"
	    + " EnquiryContacts lc JOIN lc.enquiry l JOIN lc.contact c"
	    + " where c.contactType = :contactType"
	    + " and (c.firstName LIKE :fName or c.lastName LIKE :lName)"
	    + (company != null ? " and c.company = :company" : "")
	    ;//+ " and l.isDead = :dead";

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	try {
	    Query q = em.createQuery(query, Enquiry.class);
	    //q.setParameter("name", criteria);
	    q.setParameter("fName", "%" + criteria + "%");
	    q.setParameter("lName", "%" + criteria + "%");
	    //q.setParameter("dead", Boolean.FALSE);
	    if (company != null) {
		q.setParameter("company", company);
	    }
	    q.setParameter("contactType", contactType);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
	return list;
    }

    public static List<Enquiry> search(String iCriteria,
	    String iRegex, int quarter,
	    boolean recent,
	    boolean listDead,
	    int min, int max) {
	List<Enquiry> list = new ArrayList<Enquiry>();

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	String query = "select e.* from enquiry e, project p"
	    + " where e.project = p.ID and p.project_name LIKE ?2";

	if (iRegex != null) {
	    query = "select e.* from enquiry e, project p"
		+ " where e.project = p.ID and "
		//+ " (e.enquiry_name REGEXP ?2 and e.enquiry_name LIKE ?1)"
		+ " (p.project_name REGEXP ?3 and p.project_name LIKE ?4)";
	}

	if (! listDead) {
	    //query += " and e.is_dead = 0";
	}

	Date startQuarter = null;
	Date endQuarter = null;

	if (quarter > 0) {

	    Calendar cal = Calendar.getInstance();
	    int month = Calendar.APRIL;
	    switch(quarter) {
		case 1:
		    month = Calendar.APRIL;
		    break;
		case 2:
		    month = Calendar.JULY;
		    break;
		case 3:
		    month = Calendar.OCTOBER;
		    break;
		case 4:
		    month = Calendar.JANUARY;
		    break;
	    }

	    if (cal.get(Calendar.MONTH) < month) {
		cal.add(Calendar.YEAR, -1);
	    }

	    cal.set(Calendar.HOUR_OF_DAY,
		cal.getMinimum(Calendar.HOUR_OF_DAY));
	    cal.set(Calendar.MINUTE,
		cal.getMinimum(Calendar.MINUTE));
	    cal.set(Calendar.SECOND,
		cal.getMinimum(Calendar.SECOND));
	    cal.set(Calendar.DAY_OF_MONTH,
		cal.getMinimum(Calendar.DAY_OF_MONTH));
	    cal.set(Calendar.MONTH, month);

	    startQuarter = cal.getTime();

	    cal.set(Calendar.HOUR_OF_DAY,
		cal.getMaximum(Calendar.HOUR_OF_DAY));
	    cal.set(Calendar.MINUTE,
		cal.getMaximum(Calendar.MINUTE));
	    cal.set(Calendar.SECOND,
		cal.getMaximum(Calendar.SECOND));
	    cal.set(Calendar.DAY_OF_MONTH,
		cal.getMaximum(Calendar.DAY_OF_MONTH));
	    cal.add(Calendar.MONTH, 3);
	    endQuarter = cal.getTime();

	    query += " and e.created_at between ?5 AND ?6";
	}

	if (recent) {
	    query += " order by e.created_at desc";
	}

	try {
	    Query q = em.createNativeQuery(query, Enquiry.class);
	    q.setParameter(1, "%" + criteria + "%");
	    if (iRegex != null) {
		q.setParameter(2, iRegex);
		q.setParameter(3, iRegex);
		q.setParameter(4, "%" + criteria + "%");
	    } else {
		q.setParameter(2, "%" + criteria + "%");
	    }

	    if (quarter > 0) {
		q.setParameter(5, startQuarter);
		q.setParameter(6, endQuarter);
	    }

	    q.setFirstResult(min == 0 ? min : min - 1);
	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return list;
    }
}
