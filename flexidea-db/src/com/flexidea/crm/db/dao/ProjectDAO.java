package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.EnquiryStatus;
import com.flexidea.crm.db.dto.LeadContacts;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.db.dto.ProjectContacts;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class ProjectDAO {
    public static Project getProject(Long id) {
	return (Project) DBUtil.getEntity(Project.class, id);
    }

    public static Project getProject(String projectName) {
	Project project = null;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("Select Object(l) from Project l where"
		+ " l.projectName = :name and l.isDead = :dead");
	    q.setParameter("name", projectName);
	    q.setParameter("dead", false);
	    if (! q.getResultList().isEmpty()) {
		project = (Project) q.getResultList().get(0);
	    }
	} catch (Exception e) {

	} finally {
	    em.close();
	}

	return project;
    }

    public static List<Project> getProjects() {
	List<Project> projects = new ArrayList<Project>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("Select Object(l) from Project l where"
		+ " l.isDead = :dead", Project.class);
	    q.setParameter("dead", false);
	    if (! q.getResultList().isEmpty()) {
		projects.addAll(q.getResultList());
	    }
	} catch (Exception e) {

	} finally {
	    em.close();
	}

	return projects;
    }
    public static Project update(Project project) {
	EntityManager em = DBUtil.getEM();

	try {
	    em.getTransaction().begin();

	    if (project.getId() == null) {
		em.persist(project);
		CommentsDAO.addComment(em, CommentsTarget.PROJECT,
		    project.getId(), project, project.getCreatedBy(),
		    "Created project");
		if (project.getLead() != null) {
		    for (LeadContacts lc : project.getLead().getLeadContacts()) {
			ProjectContacts pc = new ProjectContacts();
			pc.setContact(lc.getContact());
			pc.setProject(project);
			em.persist(pc);
		    }
		}
	    } else {
		Project oldProject = getProject(project.getId());
		em.merge(project);

		if (! oldProject.isIsDead() && project.isIsDead()) {
		    CommentsDAO.addComment(em, CommentsTarget.PROJECT,
			project.getId(), project, project.getModifiedBy(),
			"Project marked as dead\nReason\n:"
			+ project.getComment());
		} else {
		    CommentsDAO.addComment(em, CommentsTarget.PROJECT,
			project.getId(), project, project.getModifiedBy(),
			"Project name updated to " + project.getProjectName());
		}
	    }

	    em.getTransaction().commit();
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return project;
    }

    @Deprecated
    public static List<Project> search(String iCriteria, String iRegex,
	    boolean recent, int min, int max, boolean listDead) {
	List<Project> list = new ArrayList<Project>();

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	String query = "select * from project"
	    + " where project_name LIKE ?1";

	if (iRegex != null) {
	    query = "select * from project"
		+ " where project_name REGEXP ?2 and project_name LIKE ?1";
	}

	if (! listDead) {
	    query += " and is_dead = 0";
	}

	if (recent) {
	    query += " order by created_at desc";
	}

	try {
	    Query q = em.createNativeQuery(query, Project.class);
	    q.setParameter(1, "%" + criteria + "%");
	    if (iRegex != null) {
		q.setParameter(2, iRegex);
	    }

	    q.setFirstResult(min == 0 ? min : min - 1);
	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Project> listByCompany(Company company, int first, int max) {
	List<Project> list = new ArrayList<Project>();

	String query = "select distinct l from"
	    + " ProjectContacts lc JOIN lc.project l JOIN lc.contact c"
	    + " where "
	    + " c.company = :company"
	    + " and l.isDead = :dead";

	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery(query, Project.class);
	    q.setParameter("dead", Boolean.FALSE);
	    q.setParameter("company", company);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
	return list;
    }

    public static List<Project> search(String iCriteria,
	ContactType contactType, Company company, int first, int max) {
	List<Project> list = new ArrayList<Project>();

	String query = "select Object(l) from"
	    + " ProjectContacts lc JOIN lc.project l JOIN lc.contact c"
	    + " where c.contactType = :contactType"
	    + " and (c.firstName LIKE :fName or c.lastName LIKE :lName)"
	    + (company != null ? " and c.company = :company" : "")
	    + " and l.isDead = :dead";

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	try {
	    Query q = em.createQuery(query, Project.class);
	    //q.setParameter("name", criteria);
	    q.setParameter("fName", "%" + criteria + "%");
	    q.setParameter("lName", "%" + criteria + "%");
	    q.setParameter("dead", Boolean.FALSE);
	    if (company != null) {
		q.setParameter("company", company);
	    }
	    q.setParameter("contactType", contactType);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
	return list;
    }

    public static List<Project> search(String iCriteria,
	    String iRegex, int quarter,
	    boolean recent,
	    boolean listDead,
	    int min, int max) {
	List<Project> list = new ArrayList<Project>();

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	String query = "select * from project"
	    + " where project_name LIKE ?1";

	if (iRegex != null) {
	    query = "select * from project"
		+ " where (project_name REGEXP ?2 and project_name LIKE ?1)";
	}

	if (! listDead) {
	    query += " and is_dead = 0";
	}

	Date startQuarter = null;
	Date endQuarter = null;

	if (quarter > 0) {

	    Calendar cal = Calendar.getInstance();
	    int month = Calendar.APRIL;
	    switch(quarter) {
		case 1:
		    month = Calendar.APRIL;
		    break;
		case 2:
		    month = Calendar.JULY;
		    break;
		case 3:
		    month = Calendar.OCTOBER;
		    break;
		case 4:
		    month = Calendar.JANUARY;
		    break;
	    }

	    if (cal.get(Calendar.MONTH) < month) {
		cal.add(Calendar.YEAR, -1);
	    }

	    cal.set(Calendar.HOUR_OF_DAY,
		cal.getMinimum(Calendar.HOUR_OF_DAY));
	    cal.set(Calendar.MINUTE,
		cal.getMinimum(Calendar.MINUTE));
	    cal.set(Calendar.SECOND,
		cal.getMinimum(Calendar.SECOND));
	    cal.set(Calendar.DAY_OF_MONTH,
		cal.getMinimum(Calendar.DAY_OF_MONTH));
	    cal.set(Calendar.MONTH, month);

	    startQuarter = cal.getTime();

	    cal.set(Calendar.HOUR_OF_DAY,
		cal.getMaximum(Calendar.HOUR_OF_DAY));
	    cal.set(Calendar.MINUTE,
		cal.getMaximum(Calendar.MINUTE));
	    cal.set(Calendar.SECOND,
		cal.getMaximum(Calendar.SECOND));
	    cal.set(Calendar.DAY_OF_MONTH,
		cal.getMaximum(Calendar.DAY_OF_MONTH));
	    cal.add(Calendar.MONTH, 3);
	    endQuarter = cal.getTime();

	    query += " and created_at between ?3 AND ?4";
	}

	if (recent) {
		/*
		 * BUG ID 63 [START]
		 */
	    //query += " order by created_at desc";
		query += " order by project_name asc";
		/*
		 * BUG ID 63 [END]
		 */		
	}

	try {
	    Query q = em.createNativeQuery(query, Project.class);
	    q.setParameter(1, "%" + criteria + "%");
	    if (iRegex != null) {
		q.setParameter(2, iRegex);
	    }

	    if (quarter > 0) {
		q.setParameter(3, startQuarter);
		q.setParameter(4, endQuarter);
	    }

	    q.setFirstResult(min == 0 ? min : min - 1);
	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Project> getAssociatedProjects(Contact contact) {
	List<Project> list = new ArrayList<Project>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("Select DISTINCT (l) "
		+ " from ProjectContacts lc JOIN lc.project l where"
		+ " lc.contact = :contact and l.isDead = :dead", Project.class);
	    q.setParameter("contact", contact);
	    q.setParameter("dead", false);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}
	return list;
    }

    public static List<Project> history(int first, int count) {
	List<Project> list = new ArrayList<Project>();

	EntityManager em = null;
	try {
	    em = DBUtil.getEM();
	    Query q = em.createQuery("Select Object(p) from Project p", 
		Project.class);

	    //q.setFirstResult(first == 0 ? first : first - 1);

	    /*
	    if (count < Integer.MAX_VALUE) {
		q.setMaxResults(count + 1);
	    }
	     *
	     */

	    int c = 0;
	    int f = 0;
	    for (Project p : (List<Project>) q.getResultList()) {
		if (getProjectStatus(p) < 3) {
		    if (f++ < first) {
			continue;
		    }

		    if (c++ > count) {
			break;
		    }

		    list.add(p);
		}
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return list;
    }

    public static List<Project> list() {
	List<Project> list = new ArrayList<Project>();

	EntityManager em = null;
	try {
	    em = DBUtil.getEM();
	    Query q = em.createQuery("Select Object(p) from Project p",
		Project.class);

	    for (Project p : (List<Project>) q.getResultList()) {
		if (! p.isIsDead()) {
		    list.add(p);
		}
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return list;
    }

    public static int getProjectStatus(Project p) {
	int status = 3;
	if (p.isIsDead()) {
	    return 0;
	} else {
	    boolean allLost = false;
	    boolean received = false;
	    int undeadCount = 0;
	    int lostCount = 0;
	    for (Enquiry e : p.getEnquiries()) {

		if (! e.isDead()) {
		    /*
		    System.out.println("DEBUG: p="
			+ p.getProjectName()
			+ ", s=" + e.getStage().toString()
			+ ", st=" + e.getStatus().toString());
		     *
		     */
		    undeadCount++;
		    if (e.getStatus().equals(EnquiryStatus.LOST)) {
			lostCount++;
		    }
		    if (e.getStatus().equals(EnquiryStatus.RECEIVED)) {
			received = true;
		    }
		}
	    }

	    if (lostCount == undeadCount) {
		allLost = true;
	    }

	    if (p.getEnquiries().isEmpty()) {
		p.setStatus(4);
		status = 4;
	    } else {
		if (allLost) {
		    p.setStatus(1);
		    status = 1;
		} else if (received) {
		    p.setStatus(2);
		    status = 2;
		}
	    }
	}
	return status;
    }
}
