package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class LeadsDAO {
    public static Lead getLead(Long id) {
	return (Lead) DBUtil.getEntity(Lead.class, id);
    }

    public static Lead getLead(String leadName) {
	Lead lead = null;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("Select Object(l) from Lead l where"
		+ " l.leadName = :name and l.isDead = :dead");
	    q.setParameter("name", leadName);
	    q.setParameter("dead", false);
	    if (! q.getResultList().isEmpty()) {
		lead = (Lead) q.getResultList().get(0);
	    }
	} catch (Exception e) {

	} finally {
	    em.close();
	}

	return lead;
    }

    public static Lead update(Lead lead) {
	EntityManager em = DBUtil.getEM();

	try {
	    em.getTransaction().begin();

	    if (lead.getId() == null) {
		em.persist(lead);
		CommentsDAO.addComment(em, CommentsTarget.LEADS,
		    lead.getId(), lead, lead.getCreatedBy(), "Generated Lead");
	    } else {
		Lead oldLead = getLead(lead.getId());
		em.merge(lead);
		if (! oldLead.isIsConverted() && lead.isIsConverted()) {
		    CommentsDAO.addComment(em, CommentsTarget.LEADS,
			lead.getId(), lead, lead.getModifiedBy(),
			"Lead converted to project");
		} else if(! oldLead.isIsDead() && lead.isIsDead()) {
		    CommentsDAO.addComment(em, CommentsTarget.LEADS,
			lead.getId(), lead, lead.getModifiedBy(),
			"Lead marked as dead\nReason:\n"
			+ lead.getComment()
			);
		} else {
		    CommentsDAO.addComment(em, CommentsTarget.LEADS,
			lead.getId(), lead, lead.getModifiedBy(),
			"Updated lead name as " + lead.getLeadName());
		}
	    }

	    em.getTransaction().commit();
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return lead;
    }

    public static List<Lead> search(String iCriteria,
	ContactType contactType, int first, int max) {
	List<Lead> list = new ArrayList<Lead>();

	String query = "select Object(l) from"
	    + " LeadContacts lc JOIN lc.lead l JOIN lc.contact c"
	    + " where c.contactType = :contactType"
	    + " and (c.firstName LIKE :fName or c.lastName LIKE :lName)"
	    + " and l.isDead = :dead";

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	try {
	    Query q = em.createQuery(query, Lead.class);
	    //q.setParameter("name", criteria);
	    q.setParameter("fName", "%" + criteria + "%");
	    q.setParameter("lName", "%" + criteria + "%");
	    q.setParameter("dead", Boolean.FALSE);
	    q.setParameter("contactType", contactType);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
	return list;
    }

    public static List<Lead> search(String iCriteria,
	    String iRegex, int quarter,
	    boolean recent, 
	    boolean listDead,
	    boolean listConverted,
	    int min, int max, String orderBy) {
	List<Lead> list = new ArrayList<Lead>();

	EntityManager em = DBUtil.getEM();
	String criteria = iCriteria == null ? "" : iCriteria;

	String query = "select *"
	    + "from lead"
	    + " where lead_name LIKE ?1";

	if (iRegex != null) {
	    query = "select * from lead"
		+ " where (lead_name REGEXP ?2 and lead_name LIKE ?1)";
	}

	if (! listDead) {
	    query += " and is_dead = 0";
	} else {
	    query += " and is_dead = 1";
	}

	if (listConverted && listDead) {
	    query += " or is_converted = 1";
	} else if (listConverted) {
	    query += " and is_converted = 1";
	}

	Date startQuarter = null;
	Date endQuarter = null;

	if (quarter > 0) {

	    Calendar cal = Calendar.getInstance();
	    int month = Calendar.APRIL;
	    switch(quarter) {
		case 1:
		    month = Calendar.APRIL;
		    break;
		case 2:
		    month = Calendar.JULY;
		    break;
		case 3:
		    month = Calendar.OCTOBER;
		    break;
		case 4:
		    month = Calendar.JANUARY;
		    break;
	    }

	    if (cal.get(Calendar.MONTH) < month) {
		cal.add(Calendar.YEAR, -1);
	    }

	    cal.set(Calendar.HOUR_OF_DAY,
		cal.getMinimum(Calendar.HOUR_OF_DAY));
	    cal.set(Calendar.MINUTE,
		cal.getMinimum(Calendar.MINUTE));
	    cal.set(Calendar.SECOND,
		cal.getMinimum(Calendar.SECOND));
	    cal.set(Calendar.DAY_OF_MONTH,
		cal.getMinimum(Calendar.DAY_OF_MONTH));
	    cal.set(Calendar.MONTH, month);

	    startQuarter = cal.getTime();

	    cal.set(Calendar.HOUR_OF_DAY,
		cal.getMaximum(Calendar.HOUR_OF_DAY));
	    cal.set(Calendar.MINUTE,
		cal.getMaximum(Calendar.MINUTE));
	    cal.set(Calendar.SECOND,
		cal.getMaximum(Calendar.SECOND));
	    cal.set(Calendar.DAY_OF_MONTH,
		cal.getMaximum(Calendar.DAY_OF_MONTH));
	    cal.add(Calendar.MONTH, 3);
	    endQuarter = cal.getTime();

	    query += " and created_at between ?3 AND ?4";
	}

	if (recent && orderBy == null) {
	    query += " order by created_at desc";
	} else if (orderBy != null) {
	    if (! orderBy.startsWith("status")) {
		query += " order by " + orderBy;
	    } else {
		if (orderBy.endsWith("asc")) {
		    query += " order by is_dead asc, is_converted asc";
		} else {
		    query += " order by is_dead desc, is_converted desc";
		}
	    }
	}

	try {
	    Query q = em.createNativeQuery(query, Lead.class);
	    q.setParameter(1, "%" + criteria + "%");
	    if (iRegex != null) {
		q.setParameter(2, iRegex);
	    }

	    if (quarter > 0) {
		q.setParameter(3, startQuarter);
		q.setParameter(4, endQuarter);
	    }

	    q.setFirstResult(min == 0 ? min : min - 1);
	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Lead> getAssociatedLeads(Contact contact) {
	List<Lead> list = new ArrayList<Lead>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("Select DISTINCT (l) "
		+ " from LeadContacts lc JOIN lc.lead l where"
		+ " lc.contact = :contact and l.isDead = :dead", Lead.class);
	    q.setParameter("contact", contact);
	    q.setParameter("dead", false);

	    list.addAll(q.getResultList());
	} catch (Exception e) {

	} finally {
	    em.close();
	}
	return list;
    }

    public static List<Lead> list(boolean dead) {
	List<Lead> list = new ArrayList<Lead>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("Select Object(l) "
		+ " from Lead l where l.isDead = :dead", Lead.class);
	    q.setParameter("dead", dead);

	    list.addAll(q.getResultList());
	} catch (Exception e) {

	} finally {
	    em.close();
	}
	return list;
    }
}
