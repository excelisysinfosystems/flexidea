package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.AuthRoles;
import com.flexidea.crm.db.dto.AuthRules;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.UserRoles;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class AuthDAO {
    public static AuthRoles getRole(EntityManager iem, Long id) {
	AuthRoles res = null;
	EntityManager em = null;
	try {
	    if (iem == null) {
		em = DBUtil.getEM();
	    } else {
		em = iem;
	    }

	    res = em.find(AuthRoles.class, id);
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
	return res;
    }

    public static AuthRules getRule(EntityManager iem, Long id) {
	AuthRules res = null;
	EntityManager em = null;
	try {
	    if (iem == null) {
		em = DBUtil.getEM();
	    } else {
		em = iem;
	    }

	    res = em.find(AuthRules.class, id);
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
	return res;
    }

    public static void save(EntityManager iem, List<AuthRules> rules,
	CrmUser user) {
	EntityManager em = null;
	try {
	    if (iem == null) {
		em = DBUtil.getEM();
		em.getTransaction().begin();
	    } else {
		em = iem;
	    }

	    for (AuthRules rule : rules) {
		save(iem, rule, user);
	    }

	    if (iem == null) {
		em.getTransaction().commit();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
    }

    public static void save(EntityManager iem, AuthRules role,
	CrmUser user) {
	EntityManager em = null;
	try {
	    if (iem == null) {
		em = DBUtil.getEM();
		em.getTransaction().begin();
	    } else {
		em = iem;
	    }

	    if (role.getId() == null) {
		role.setCreatedAt(new Date());
		role.setCreatedBy(user);
		em.persist(role);
	    } else {
		role.setModifiedAt(new Date());
		role.setModifiedBy(user);
		em.persist(role);
	    }

	    if (iem == null) {
		em.getTransaction().commit();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
    }

    public static void save(EntityManager iem, AuthRoles role,
	List<AuthRules> rules, CrmUser user) {
	EntityManager em = null;
	try {
	    if (iem == null) {
		em = DBUtil.getEM();
		em.getTransaction().begin();
	    } else {
		em = iem;
	    }

	    if (role.getId() == null) {
		role.setCreatedAt(new Date());
		role.setCreatedBy(user);
		em.persist(role);
	    } else {
		role.setModifiedAt(new Date());
		role.setModifiedBy(user);
		em.persist(role);
	    }

	    for (AuthRules rule : rules) {
		rule.setRole(role);
		save(em, rule, user);
	    }

	    if (iem == null) {
		em.getTransaction().commit();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
    }

    public static void delete(EntityManager iem, AuthRoles role) {
	EntityManager em = null;
	try {
	    if (iem == null) {
		em = DBUtil.getEM();
		em.getTransaction().begin();
	    } else {
		em = iem;
	    }

	    for (AuthRules rule : role.getAuthRules()) {
		em.remove(rule);
	    }
	    em.remove(role);

	    if (iem == null) {
		em.getTransaction().commit();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
    }

    public static List<AuthRoles> list(String orderBy, boolean asc,
	int first, int count) {
	List<AuthRoles> list = new ArrayList<AuthRoles>();
	EntityManager em = DBUtil.getEM();

	try {
	    String query = "Select Object(r) from AuthRoles r";
	    if (orderBy != null) {
		query = query + " order by " + orderBy
		    + (asc ? " ASC" : " DESC");
	    }

	    Query q = em.createQuery(query);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (count < Integer.MAX_VALUE) {
		q.setMaxResults(count + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return list;
    }

    public static AuthRoles getRole(String roleName) {
	AuthRoles role = null;
	EntityManager em = DBUtil.getEM();

	try {
	    String query = "Select Object(r) from AuthRoles r"
		+ " where r.roleName = :roleName";
	    Query q = em.createQuery(query);
	    q.setParameter("roleName", roleName);

	    if (! q.getResultList().isEmpty()) {
		role = (AuthRoles) q.getResultList().get(0);
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return role;
    }

    public static void assignRoles(CrmUser admin, Long userId,
	List<Long> roles) {
	EntityManager em = null;
	try {
	    em = DBUtil.getEM();
	    em.getTransaction().begin();

	    CrmUser user = em.find(CrmUser.class, userId);
	    // Remove
	    for (UserRoles role : user.getUserRoles()) {
		em.remove(role);
	    }
	    // Reassign
	    for (Long roleId : roles) {
		AuthRoles role = em.find(AuthRoles.class, roleId);
		UserRoles ur = new UserRoles();
		ur.setCreatedAt(new Date());
		ur.setModifiedAt(new Date());
		ur.setCreatedBy(admin);
		ur.setModifiedBy(admin);
		ur.setRole(role);
		ur.setUser(user);
		em.persist(ur);
	    }
	    em.getTransaction().commit();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
    }
}
