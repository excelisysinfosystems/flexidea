package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.Attachments;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.util.DBUtil;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author akshay
 */
public class AttachmentDAO {
    public static Attachments getAttachments(Long id) {
	Attachments contact = null;
	EntityManager em = DBUtil.getEM();
	contact = em.find(Attachments.class, id);
	em.close();
	return contact;
    }

    public static void saveAttachment(Enquiry enquiry,
	List<Attachments> attachments) {
	EntityManager em = null;

	try {
	    em = DBUtil.getEM();
	    em.getTransaction().begin();
	    for (Attachments a : attachments) {
		em.persist(a);
		CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
		    enquiry.getId(), enquiry, a.getCreatedBy(),
		    "Added enquiry document " + a.getAttachmentName());
	    }
	    em.getTransaction().commit();
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
    }

    public static void deleteAttachment(Enquiry enquiry,
	Long attachment, CrmUser user) {
	EntityManager em = null;

	try {
	    em = DBUtil.getEM();
	    em.getTransaction().begin();
	    Attachments a = em.find(Attachments.class, attachment);
	    em.clear();
	    a.setEnquiry(null);
	    em.remove(em.merge(a));
	    CommentsDAO.addComment(em, CommentsTarget.ENQUIRY,
		enquiry.getId(), enquiry, user,
		"Deleted enquiry document " + a.getAttachmentName());
	    em.getTransaction().commit();
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
    }
}
