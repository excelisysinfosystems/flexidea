package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.AssociatedContacts;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.EnquiryContacts;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.LeadContacts;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.db.dto.ProjectContacts;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class ContactsDAO {
    public static Contact getContact(Long id) {
	Contact contact = null;
	EntityManager em = DBUtil.getEM();
	contact = em.find(Contact.class, id);
	em.close();
	return contact;
    }

    public static ContactType getContactType(Long id) {
	ContactType contact = null;
	contact = (ContactType) DBUtil.getEntity(
	    ContactType.class, id);
	return contact;
    }

    public static List<ContactType> getContactTypes() {
	List<ContactType> list = null;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c) "
		+ " from ContactType c"
		+ " order by c.typeName asc");
	    list = q.getResultList();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static void deleteContactType(Long id) {
	EntityManager em = DBUtil.getEM();

	try {
	    em.getTransaction().begin();
	    ContactType ct = (ContactType)
		em.find(ContactType.class, id);

	    if (ct != null) {
		ct.setForCompany(false);
		ct.setForContact(false);
		em.merge(ct);
	    }

	    em.getTransaction().commit();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}
    }

    public static List<ContactType> list(String orderBy,
	boolean asc, int first, int count) {

	List<ContactType> list = null;
	EntityManager em = DBUtil.getEM();

	try {
	    String query = "select Object(c) "
		+ " from ContactType c"
		+ " where not (c.forContact = false "
		+ " and c.forCompany = false)";

	    if (orderBy != null) {
		query = query + " order by c."
		    + orderBy
		    + (asc ? " asc" : " desc")
		;
	    } else {
		query = query + " order by c.typeName asc";
	    }

	    Query q = em.createQuery(query);

	    q.setFirstResult(first == 0 ? 0 : first - 1);
	    if (count < Integer.MAX_VALUE) {
		q.setMaxResults(count + 1);
	    }

	    list = q.getResultList();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<ContactType> listContactTypes(boolean forCompany) {
	List<ContactType> list = new ArrayList<ContactType>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c)"
		+ " from ContactType c"
		+ " order by c.typeName asc");
	    for (ContactType ct : (List<ContactType>) q.getResultList()) {
		if (forCompany && ct.isForCompany()) {
		    list.add(ct);
		} else if (! forCompany && ct.isForContact()) {
		    list.add(ct);
		}
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static ContactType getContactType(String type) {

	for (ContactType t : getContactTypes()) {
	    if (t.getTypeName().equals(type)) {
		return t;
	    }
	}

	return null;
    }

    public static List<ContactType> getContactTypes(boolean internal) {
	List<ContactType> list = null;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c) from ContactType c"
		+ " where c.internal = :state"
		+ " order by c.typeName asc");
	    q.setParameter("state", internal);
	    list = q.getResultList();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static void save(ContactType ct) {
	EntityManager em = DBUtil.getEM();

	try {
	    em.getTransaction().begin();

	    if (ct.getId() == null) {
		em.persist(ct);
	    } else {
		em.merge(ct);
	    }

	    em.getTransaction().commit();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

    }

    public static List<Contact> listContacts(int type) {
	List<Contact> list = new ArrayList<Contact>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c) from"
		+ " Contact c JOIN c.contactType cc"
		+ " where c.isDeleted = :deleted "
		+ (type != 0 ? " and cc.internal = :state" : "")
		);

	    q.setParameter("deleted", false);
	    if (type != 0) {
		q.setParameter("state", type == 1 ? true : false);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}
	return list;
    }

    public static void updateContact(EntityManager iem,
	Contact contact, Company company, boolean createUser,
	String password, String username) {
	EntityManager em = null;

	if (iem == null) {
	    em = DBUtil.getEM();
	} else {
	    em = iem;
	}

	try {
	    if (iem == null) {
		em.getTransaction().begin();
	    }
	    if (company.getId() == null) {
		em.persist(company);
		CommentsDAO.addComment(em, CommentsTarget.CONTACTS,
		    company.getId(), company, company.getCreatedBy(),
		    "Created company");
	    } else {
		Company oldCompany = CompaniesDAO.getCompany(company.getId());
		em.merge(company);
		CommentsDAO.addUpdateComment(em, 
		    CommentsTarget.CONTACTS, company.getId(), 
		    oldCompany, company, company.getModifiedBy(),
		    new String[] {"companyName", "industry", "companyAddress",
		    "phone", "phone1", "fax", "email", "website", "city"
		    },
		    new String[] {"name", "industry", "address", "phone",
		    "phone1", "fax", "email", "website", "city"}, "company");
	    }

	    if (contact.getId() == null) {
		contact.setIsDeleted(false);
		contact.setCompany(company);
		em.persist(contact);
		if (createUser && contact.getContactType().isInternal()) {
		    CrmUser user = new CrmUser();
		    user.setContact(contact);
		    user.setFullName(contact.getContactName());
		    user.setPassword(password);
		    user.setUserRole(contact.getContactType().getTypeName());
		    user.setUsername(username);
		    user.setUserRole(contact.getContactType().getTypeName());
		    em.persist(user);
		    contact.setUser(user);
		}
		CommentsDAO.addComment(em, CommentsTarget.CONTACTS,
		    company.getId(), company, contact.getCreatedBy(),
		    "Created contact " + contact.getFirstName() + " "
		    + contact.getLastName());
	    } else {
		Contact oldContact = getContact(contact.getId());
		contact.setCompany(company);
		em.merge(contact);
		if (! oldContact.isIsDeleted() && contact.isIsDeleted()) {
		    CommentsDAO.addComment(em, CommentsTarget.CONTACTS,
			company.getId(), company, contact.getModifiedBy(),
			"Deleted contact " + contact.getFirstName() + " "
			+ contact.getLastName()
			+ "\nReason:\n"
			+ contact.getComment()
		    );
		} else {
		    CommentsDAO.addUpdateComment(em,
			CommentsTarget.CONTACTS, company.getId(),
			oldContact, contact, contact.getModifiedBy(),
			new String[] {"firstName", "lastName", "jobTitle",
			"address", "phone", "cell", "dob", "doa", "gift",
			"creditTerms", "company", "city"
			},
			new String[] {"first name", "last name", "job title",
			"address", "phone", "cell no.", "date of birth",
			"anniversary", "gift", "credit terms", "company", "city"}, null
		    );
		}
	    }

	    if (iem == null) {
		em.getTransaction().commit();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
    }

    public static List<CrmUser> listAssociatedUsers(CommentsTarget target,
	Long targetid) {
	List<CrmUser> list = new ArrayList<CrmUser>();
	for (Contact contact : listAssociatedContacts(target, targetid)) {
	    if (contact.getContactType().isInternal()
		&& contact.getUser() != null) {
		list.add(contact.getUser());
	    }
	}
	return list;
    }

    public static List<Contact> listAssociatedContacts(CommentsTarget target,
	Long targetId, ContactType type) {

	List<Contact> list = new ArrayList<Contact>();

	if (targetId == null) {
	    return list;
	}

	EntityManager em = DBUtil.getEM();
	try {
	    switch (target) {
		case CONTACTS:
		    //Contact contact = getContact(targetId);
		    Company company = CompaniesDAO.getCompany(targetId);

		    for (AssociatedContacts ac :
			company.getAssociatedContacts()) {
			if(type == null || type.equals(ac
			    .getChild().getContactType())) {
			    list.add(ac.getChild());
			}
		    }

		    break;
		case LEADS:
		    Lead lead = LeadsDAO.getLead(targetId);

		    for (LeadContacts ac :
			lead.getLeadContacts()) {
			if(type == null || type.equals(ac
			    .getContact().getContactType())) {
			    list.add(ac.getContact());
			}
		    }
		    break;
		case PROJECT:
		    Project project = ProjectDAO.getProject(targetId);

		    for (ProjectContacts ac :
			project.getProjectContacts()) {
			if(type == null || type.equals(ac
			    .getContact().getContactType())) {
			    list.add(ac.getContact());
			}
		    }
		    break;
		case ENQUIRY:
		    Enquiry enquiry = EnquiryDAO.getEnquiry(targetId);

		    for (EnquiryContacts ac :
			enquiry.getContacts()) {
			if(type == null || type.equals(ac
			    .getContact().getContactType())) {
			    list.add(ac.getContact());
			}
		    }
		    break;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Contact> listAssociatedContacts(CommentsTarget target,
	Long targetId) {
	return listAssociatedContacts(target, targetId, null);
    }

    public static List<Contact> searchContacts(CommentsTarget target,
	Long targetId, String icriteria, ContactType contactType,
	int first, int max, boolean isRegex, boolean checkOwner,
	boolean sortRecent, String orderBy, boolean isAscending) {
	List<Contact> list = new ArrayList<Contact>();

	if (CommentsTarget.CONTACTS.equals(target)
	    && targetId == null && checkOwner) {
	    return list;
	}

	EntityManager em = DBUtil.getEM();
	String criteria = icriteria == null ? "" : icriteria;
	try {
	    //switch (target) {
	//	case CONTACTS:
	    Query q;
	    System.out.println("DEBUG: search t=" + contactType
		+ ", c=" + criteria + ", f=" + first + ", l=" + max
		);
	    String qs = "select Object(c) from"
		+ " Contact c JOIN c.company comp"
		+ " JOIN comp.contactType ct"
		+ " where c.isDeleted = :deleted"
		+ (contactType != null ?
		    " and c.contactType = :contactType" : "")
		+ " and (c.firstName like :criteria"
		+ " or c.lastName like :criteria"
		+ " or comp.companyName like :criteria)"
		//+ (sortRecent ? " order by c.createdAt desc" : "")
		;

	    if (sortRecent && orderBy == null) {
		qs += " order by c.createdAt desc";
	    } else if (orderBy != null) {
		String ord = orderBy;
		if (ord.startsWith("company.")) {
		    ord = "comp.companyName";
		} else if (ord.startsWith("contactType")) {
		    ord = "ct.typeName";
		} else {
		    ord = "c." + ord;
		}
		qs += " order by " + ord + " "
		    + (isAscending ? "asc" : "desc") ;
	    }

	    q = em.createQuery(qs);
	    q.setParameter("criteria",
		isRegex? criteria : "%" + criteria + "%");
	    if (contactType != null) {
		q.setParameter("contactType", contactType);
	    }
	    q.setParameter("deleted", false);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    int count = 0;
	    for (Contact c :
		(List<Contact>) q.getResultList()) {
		boolean found = false;

		if (checkOwner && lookupAssociatedContact(target, targetId,
		    c)) {
		    found = true;
		}

		if (! found && count <= max && ! c.isIsDeleted()) {
		    count++;
		    list.add(c);
		}
	    }
	//	    break;
	 //   }
	} catch (Exception e) {
	    e.printStackTrace(System.err);
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Company> searchCompanies(CommentsTarget target,
	Long targetId, String icriteria, ContactType contactType,
	int first, int max, boolean isRegex, boolean checkOwner,
	boolean sortRecent) {
	List<Company> list = new ArrayList<Company>();

	if (CommentsTarget.CONTACTS.equals(target)
	    && targetId == null && checkOwner) {
	    return list;
	}

	EntityManager em = DBUtil.getEM();
	String criteria = icriteria == null ? "" : icriteria;
	try {
	    //switch (target) {
	//	case CONTACTS:
	    Query q;
	    System.out.println("DEBUG: search t=" + contactType
		+ ", c=" + criteria + ", f=" + first + ", l=" + max
		);
	    q = em.createQuery("select DISTINCT comp from"
		    + " Contact c JOIN c.company comp"
		    + " where c.isDeleted = :deleted"
		    + (contactType != null ?
			" and c.contactType = :contactType" : "")
		    + " and (c.firstName like :criteria"
		    + " or c.lastName like :criteria"
		    + " or comp.companyName like :criteria)"
		    + (sortRecent ? " order by c.createdAt desc" : ""),
		    Company.class
		    );

	    q.setParameter("criteria",
		isRegex? criteria : "%" + criteria + "%");

	    if (contactType != null) {
		q.setParameter("contactType", contactType);
	    }

	    q.setParameter("deleted", false);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	//	    break;
	 //   }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Contact> listByCity(Long companyId, String icriteria,
	int first, int max) {
	List<Contact> list = new ArrayList<Contact>();

	String criteria = icriteria == null ? "" : icriteria;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c)"
		+ " from Contact c JOIN c.city ct JOIN c.company cm"
		+ " JOIN cm.city cmc"
		+ " where"
		+ " (ct.cityName = :city or ct.cityName like :criteria"
		+ " or cmc.cityName = :city or cmc.cityName like :criteria)"
		+ " and c.isDeleted = :deleted"
		+ " and c.company = :company");
	    q.setParameter("city", criteria);
	    q.setParameter("company", CompaniesDAO.getCompany(companyId));
	    q.setParameter("criteria", "%" + criteria + "%");
	    q.setParameter("deleted", false);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Company> listCompanyByCity(Long companyId,
	String icriteria, int first, int max) {
	List<Company> list = new ArrayList<Company>();

	System.out.println("DEBUG: lcbc: c=" + companyId + ", i=" + icriteria);

	String criteria = icriteria == null ? "" : icriteria;
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select DISTINCT comp"
		+ " from Contact c JOIN c.city ct JOIN c.company comp"
		+ " where (ct.cityName = :city or ct.cityName like :criteria)"
		+ " and c.isDeleted = :deleted"
		+ (companyId != null ? " and c.company = :company" : ""),
		Company.class
		);
	    q.setParameter("city", criteria);
	    if (companyId != null) {
		q.setParameter("company", CompaniesDAO.getCompany(companyId));
	    }
	    q.setParameter("criteria", "%" + criteria + "%");
	    q.setParameter("deleted", false);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Contact> listForAlphaGroup(String regex,
	String icriteria, int first, int max) {
	List<Contact> list = new ArrayList<Contact>();
	String criteria = icriteria == null ? "" : icriteria;

	System.out.println("DEBUG: AGS r=" + regex + ", c=" + criteria);
	EntityManager em = DBUtil.getEM();
	try {
	    String query = "select c.* from "
		+ " contact c, company cc "
		+ " where c.company = cc.ID and"
		+ " (c.first_name REGEXP ?1"
		+ " or c.last_name REGEXP ?1)"
		//+ " or cc.company_name REGEXP ?1)"
		+ " and (c.first_name LIKE ?2"
		+ " or c.last_name LIKE ?2)"
		+ " and c.is_deleted = 0";
		//+ " or cc.company_name LIKE ?2)";

	    Query q = em.createNativeQuery(query, Contact.class);

	    q.setParameter(2, "%" + criteria + "%");
	    q.setParameter(1, regex);

	    q.setFirstResult(first == 0 ? first : first - 1);

	    if (max != Integer.MAX_VALUE)
		q.setMaxResults(max + 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Contact> listForCompany(Long companyId,
	String icriteria, ContactType cType,
	int first, int max) {
	List<Contact> list = new ArrayList<Contact>();
	String criteria = icriteria == null ? "" : icriteria;

	System.out.println("DEBUG: cs c=" + criteria + ", cid=" + companyId);
	EntityManager em = DBUtil.getEM();
	try {
	    Query q;
	    String likeQuery = "select c.* from contact c "
		    + " ,company comp "
		    + " where c.company = comp.ID "
		    + (companyId != null ? " and comp.ID = ?1" : "")
		    //+ " comp.companyName LIKE :criteria"
		    + " and (c.first_name LIKE ?2"
		    + " or c.last_name LIKE ?2)"
		    + " and c.is_deleted = 0"
		    + (cType == null ? "" : 
			" and (c.contact_type = ?3 or comp.contact_type = ?3)")
		    ;

	    q = em.createNativeQuery(likeQuery, Contact.class);
	    q.setParameter(2, "%" + criteria + "%");
	    if (cType != null) {
		q.setParameter(3, cType.getId());
	    }
	    if (companyId != null) {
		q.setParameter(1, companyId);
	    }

	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    q.setFirstResult(first == 0 ? first : first - 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static List<Company> listCompanies(Long companyId,
	String icriteria, ContactType cType,
	int first, int max) {
	List<Company> list = new ArrayList<Company>();
	String criteria = icriteria == null ? "" : icriteria;

	System.out.println("DEBUG: cs c=" + criteria + ", cid=" + companyId);
	EntityManager em = DBUtil.getEM();
	try {
	    Query q;
	    String likeQuery = "select DISTINCT(comp) from Contact c "
		    + " JOIN c.company comp "
		    + " where "
		    + (companyId != null ? " c.company = :company and" : "")
		    //+ " comp.companyName LIKE :criteria"
		    + " (c.firstName LIKE :fName"
		    + " or c.lastName LIKE :lName)"
		    + " and c.isDeleted = :deleted"
		    + (cType == null ? "" : 
			" and (c.contactType = :contactType "
			+ " or comp.contactType = :compContactType)")
		    ;
	    
	    /*
	     * BUG ID 27 [START]
	     */
	    if(cType!=null && cType.getTypeName().equalsIgnoreCase("Consultant")){
	    	likeQuery = likeQuery + " and c.contactType=comp.contactType";
	    }
	    /*
	     * BUG ID 27 [END]
	     */
	    
	    q = em.createQuery(likeQuery, Company.class);
	    q.setParameter("fName", "%" + criteria + "%");
	    q.setParameter("lName", "%" + criteria + "%");
	    if (cType != null) {
		q.setParameter("contactType", cType);
		q.setParameter("compContactType", cType);
	    }
	    if (companyId != null) {
		q.setParameter("company", CompaniesDAO.getCompany(companyId));
	    }

	    q.setParameter("deleted", false);

	    if (max != Integer.MAX_VALUE) {
		q.setMaxResults(max + 1);
	    }

	    q.setFirstResult(first == 0 ? first : first - 1);

	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }

    public static void dissociateContact(CommentsTarget target, Long targetId,
	Long contactId, CrmUser user) {
	EntityManager em = DBUtil.getEM();

	try {
	    switch (target) {
		case CONTACTS:
		    //Contact contact = getContact(targetId);
		    Company company = CompaniesDAO.getCompany(targetId);
		    Contact newContact = getContact(contactId);
		    AssociatedContacts associatedContact = null;

		    System.out.println("DEBUG: Dissociating: p=" + company
			+ ", c=" + newContact);
		    for (AssociatedContacts c
			: company.getAssociatedContacts()) {
			System.out.println("DEBUG: Dissociate: p="
			    + c.getCompany() + ", c=" + c.getChild()
			    + ", n=" + newContact);
			if (c.getChild().getId().equals(newContact.getId())) {
			    associatedContact = c;
			    break;
			}
		    }

		    if (associatedContact != null) {
			System.out.println("DEBUG: killing a=" + associatedContact);
			em.getTransaction().begin();
			AssociatedContacts dr = em.find(AssociatedContacts.class,
			    associatedContact.getId());
			em.remove(dr);
			CommentsDAO.addComment(em, target,
			    targetId, company, user, "Dissociated contact "
			    + dr.getChild().getFirstName() + " " +
			    dr.getChild().getLastName() + " from "
			    + dr.getChild().getCompany().getCompanyName());
			em.getTransaction().commit();
		    }

		    break;
		case LEADS:
		    Lead lead = LeadsDAO.getLead(targetId);
		    LeadContacts lc = null;
		    for (LeadContacts c
			: lead.getLeadContacts()) {
			if (c.getContact().getId().equals(contactId)) {
			    lc = c;
			    break;
			}
		    }

		    if (lc != null) {
			em.getTransaction().begin();
			LeadContacts dr = em.find(LeadContacts.class,
			    lc.getId());
			em.remove(dr);
			CommentsDAO.addComment(em, target,
			    targetId, lead, user, "Dissociated contact "
			    + dr.getContact().getFirstName() + " " +
			    dr.getContact().getLastName() + " from "
			    + dr.getContact().getCompany().getCompanyName());
			em.getTransaction().commit();
		    }

		    break;
		case PROJECT:
		    Project project = ProjectDAO.getProject(targetId);
		    ProjectContacts pc = null;
		    for (ProjectContacts c
			: project.getProjectContacts()) {
			if (c.getContact().getId().equals(contactId)) {
			    pc = c;
			    break;
			}
		    }

		    if (pc != null) {
			em.getTransaction().begin();
			ProjectContacts dr = em.find(ProjectContacts.class,
			    pc.getId());
			em.remove(dr);
			CommentsDAO.addComment(em, target,
			    targetId, project, user, "Dissociated contact "
			    + dr.getContact().getFirstName() + " " +
			    dr.getContact().getLastName() + " from "
			    + dr.getContact().getCompany().getCompanyName());
			em.getTransaction().commit();
		    }

		    break;
		case ENQUIRY:
		    Enquiry enquiry = EnquiryDAO.getEnquiry(targetId);
		    EnquiryContacts ec = null;
		    for (EnquiryContacts c
			: enquiry.getContacts()) {
			if (c.getContact().getId().equals(contactId)) {
			    ec = c;
			    break;
			}
		    }

		    if (ec != null) {
			em.getTransaction().begin();
			EnquiryContacts dr = em.find(EnquiryContacts.class,
			    ec.getId());
			em.remove(dr);
			CommentsDAO.addComment(em, target,
			    targetId, enquiry, user, "Dissociated contact "
			    + dr.getContact().getFirstName() + " " +
			    dr.getContact().getLastName() + " from "
			    + dr.getContact().getCompany().getCompanyName());
			em.getTransaction().commit();
		    }

		    break;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}
    }

    public static boolean lookupAssociatedContact(CommentsTarget target,
	Long targetId, Contact contact) {
	boolean found = false;

	EntityManager em = DBUtil.getEM();

	if (target == null || contact == null) {
	    return false;
	}

	try {
	    Query q = null;
	    switch (target) {
		case CONTACTS:
		    if (! contact.getCompany().getId().equals(targetId)) {
			q = em.createQuery("select Object(o) "
			    + " from AssociatedContacts o"
			    + " where o.company = :company and o.child=:child");
			q.setParameter("company", CompaniesDAO.getCompany(targetId));
			q.setParameter("child", contact);
		    } else {
			found = true;
		    }
		    break;
		case LEADS:
		    q = em.createQuery("select Object(o) "
			+ " from LeadContacts o"
			+ " where o.lead = :lead and o.contact=:contact");
		    q.setParameter("lead", LeadsDAO.getLead(targetId));
		    q.setParameter("contact", contact);
		    break;
		case PROJECT:
		    q = em.createQuery("select Object(o) "
			+ " from ProjectContacts o"
			+ " where o.project = :project and o.contact=:contact");
		    q.setParameter("project", ProjectDAO.getProject(targetId));
		    q.setParameter("contact", contact);
		    break;
		case ENQUIRY:
		    q = em.createQuery("select Object(o) "
			+ " from EnquiryContacts o"
			+ " where o.enquiry = :enquiry and o.contact=:contact");
		    q.setParameter("enquiry", EnquiryDAO.getEnquiry(targetId));
		    q.setParameter("contact", contact);
		    break;
	    }

	    if (q != null) {
		found = ! q.getResultList().isEmpty();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return found;
    }

    public static void associateContact(CommentsTarget target, Long targetId,
	Long contactId, CrmUser user) {
	EntityManager em = DBUtil.getEM();

	try {
	    Contact newContact = getContact(contactId);
	    switch (target) {
		case CONTACTS:
		    //Contact contact = getContact(targetId);
		    Company company = CompaniesDAO.getCompany(targetId);

		    if (! lookupAssociatedContact(target, targetId, newContact)) {
			AssociatedContacts c = new AssociatedContacts();
			//c.setParent(contact);
			c.setCompany(company);
			c.setChild(newContact);
			em.getTransaction().begin();
			em.persist(c);
			em.merge(company);
			CommentsDAO.addComment(em, target,
			    targetId, company, user, "Associated contact "
			    + newContact.getFirstName() + " " +
			    newContact.getLastName() + " from "
			    + newContact.getCompany().getCompanyName());
			em.getTransaction().commit();
		    }
		    break;
		case LEADS:
		    Lead lead = LeadsDAO.getLead(targetId);

		    if (! lookupAssociatedContact(target, targetId, newContact)) {
			LeadContacts c = new LeadContacts();
			c.setContact(newContact);
			c.setLead(lead);
			em.getTransaction().begin();
			em.persist(c);

			CommentsDAO.addComment(em, target,
			    targetId, lead, user, "Associated contact "
			    + newContact.getFirstName() + " " +
			    newContact.getLastName() + " from "
			    + newContact.getCompany().getCompanyName());

			for (AssociatedContacts child
			    : newContact.getCompany()
				.getAssociatedContacts()) {
			    if (! lookupAssociatedContact(target, targetId, 
				child.getChild())) {
				LeadContacts ch = new LeadContacts();
				ch.setContact(child.getChild());
				ch.setLead(lead);
				em.persist(ch);
			    }
			}

			em.merge(lead);
			em.getTransaction().commit();
		    }
		    break;
		case PROJECT:
		    Project project = ProjectDAO.getProject(targetId);

		    if (! lookupAssociatedContact(target, targetId, newContact)) {
			ProjectContacts c = new ProjectContacts();
			c.setContact(getContact(contactId));
			c.setProject(project);
			em.getTransaction().begin();
			em.persist(c);

			CommentsDAO.addComment(em, target,
			    targetId, project, user, "Associated contact "
			    + newContact.getFirstName() + " " +
			    newContact.getLastName() + " from "
			    + newContact.getCompany().getCompanyName());

			for (AssociatedContacts child
			    : newContact.getCompany()
				.getAssociatedContacts()) {
			    if (! lookupAssociatedContact(target, targetId,
				child.getChild())) {
				ProjectContacts ch = new ProjectContacts();
				ch.setContact(child.getChild());
				ch.setProject(project);
				em.persist(ch);
			    }
			}

			em.merge(project);
			em.getTransaction().commit();
		    }
		    break;
		case ENQUIRY:
		    Enquiry enquiry = EnquiryDAO.getEnquiry(targetId);

		    if (! lookupAssociatedContact(target, targetId, newContact)) {
			EnquiryContacts c = new EnquiryContacts();
			c.setContact(getContact(contactId));
			c.setEnquiry(enquiry);
			em.getTransaction().begin();
			em.persist(c);

			CommentsDAO.addComment(em, target,
			    targetId, enquiry, user, "Associated contact "
			    + newContact.getFirstName() + " " +
			    newContact.getLastName() + " from "
			    + newContact.getCompany().getCompanyName());

			for (AssociatedContacts child
			    : newContact
				.getCompany().getAssociatedContacts()) {
			    if (! lookupAssociatedContact(target, targetId,
				child.getChild())) {
				EnquiryContacts ch = new EnquiryContacts();
				ch.setContact(child.getChild());
				ch.setEnquiry(enquiry);
				em.persist(ch);
			    }
			}

			em.merge(enquiry);
			em.getTransaction().commit();
		    }
		    break;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}
    }

    public static boolean checkExists(String firstName,
	    String lastName, Company company) {
	EntityManager em = null;
	try {
	    em = DBUtil.getEM();
	    Query q = em.createQuery(
		"select Object(c) from Contact c where"
		+ " UPPER(c.firstName) = :firstName "
		+ " and UPPER(c.lastName) = :lastName"
		+ " and c.company = :company",
		Contact.class);

	    q.setParameter("firstName", firstName.trim().toUpperCase());
	    q.setParameter("lastName", lastName.trim().toUpperCase());
	    q.setParameter("company", company);

	    if (q.getResultList().isEmpty()) {
		return false;
	    } else {
		return true;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
    }

    public static List<Contact> listByContactType(ContactType contactType) {
	List<Contact> list = new ArrayList<Contact>();
	EntityManager em = DBUtil.getEM();

	try {
	    Query q = em.createQuery("select Object(c) from Contact c"
		+ " where c.contactType = :contactType",
		Contact.class);
	    q.setParameter("contactType", contactType);

	    list.addAll(q.getResultList());
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}

	return list;
    }
}
