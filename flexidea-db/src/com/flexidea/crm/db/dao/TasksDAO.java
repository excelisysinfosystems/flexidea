package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.TaskListMode;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.TaskStatus;
import com.flexidea.crm.db.dto.Tasks;
import com.flexidea.crm.db.util.DBUtil;
import com.flexidea.crm.db.util.ReminderService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class TasksDAO {

    public static Tasks getTask(Long id) {
        return (Tasks) DBUtil.getEntity(Tasks.class, id);
    }

    public static TaskReminders getReminder(Long id) {
        return (TaskReminders) DBUtil.getEntity(TaskReminders.class, id);
    }

    public static long getDaysToGo(Tasks task) {
        long days = 0;
        long diff = task.getEnd().getTime() - (new Date()).getTime();
        diff = diff - (diff % 86400000L);
        days = diff / 86400000L;
        return days;
    }

    public static long getCompletionDays(Tasks task) {
        long days = 0;
        long diff = task.getEnd().getTime() - task.getModifiedAt().getTime();
        diff = diff - (diff % 86400000L);
        days = diff / 86400000L;
        return days;
    }

    public static void update(Tasks task, List<TaskReminders> reminders) {
        EntityManager em = DBUtil.getEM();
        try {
            em.getTransaction().begin();

            if (task.getId() == null) {
                em.persist(task);
		if (task.getTargetEnquiry() != null) {
		    CommentsDAO.addComment(em,
			CommentsTarget.ENQUIRY, task.getTargetEnquiry().getId(),
			task.getTargetEnquiry(), task.getCreatedBy(),
			"Task assigned to "
			+ (task.getDelegatedTo() != null ?
			    task.getDelegatedTo().getFullName()
			    : task.getOwner().getFullName())
		    );
		} else if(task.getTargetProject() != null) {
		    CommentsDAO.addComment(em,
			CommentsTarget.PROJECT, task.getTargetProject().getId(),
			task.getTargetProject(), task.getCreatedBy(),
			"Task assigned to "
			+ (task.getDelegatedTo() != null ?
			    task.getDelegatedTo().getFullName()
			    : task.getOwner().getFullName())
		    );
		} else if(task.getTargetLead() != null) {
		    CommentsDAO.addComment(em,
			CommentsTarget.LEADS, task.getTargetLead().getId(),
			task.getTargetLead(), task.getCreatedBy(),
			"Task assigned to "
			+ (task.getDelegatedTo() != null ?
			    task.getDelegatedTo().getFullName()
			    : task.getOwner().getFullName())
		    );
		} else if(task.getTargetContact() != null) {
		    CommentsDAO.addComment(em,
			CommentsTarget.CONTACTS, task.getTargetContact()
			.getCompany()
			.getId(), task.getTargetContact().getCompany(),
			task.getCreatedBy(),
			"Task assigned to "
			+ (task.getDelegatedTo() != null ?
			    task.getDelegatedTo().getFullName()
			    : task.getOwner().getFullName())
		    );
		}
            } else {
                em.merge(task);
		switch (task.getStatus()) {
		    case REJECTED:
		    case COMPLETE:
			for (TaskReminders r : task.getReminders()) {
			    removeReminder(em, r);
			}
			break;
		}
            }

            for (TaskReminders r : reminders) {
                r.setTask(task);
                em.persist(r);
            }

            em.getTransaction().commit();

	    for (TaskReminders r : reminders) {
		ReminderService.schedule(r);
	    }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
    }

    public static void removeReminder(EntityManager iem, TaskReminders reminder) {
        EntityManager em = null;
        try {
	    if (iem == null) {
		em = DBUtil.getEM();
		em.getTransaction().begin();
	    } else {
		em = iem;
	    }
            TaskReminders rem = (TaskReminders) em.find(TaskReminders.class,
                    reminder.getId());
            em.remove(rem);

	    if (iem == null && em != null) {
		em.getTransaction().commit();
	    }
	    ReminderService.removeSchedule(reminder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (iem == null && em != null) {
                em.close();
            }
        }
    }

    public static void removeReminder(TaskReminders reminder) {
	removeReminder(null, reminder);
    }

    public static List<Tasks> listTasks(TaskListMode mode, Integer priority,
            CrmUser owner, boolean history, boolean monitoring,
	    int start, int max, String orderBy, boolean isAscending) {
        List<Tasks> list = new ArrayList<Tasks>();
        String query = "Select Object(t) from Tasks t "
	    + " JOIN t.owner o JOIN t.assignedBy a where";

        System.out.println("DEBUG: listTasks mode={" + mode
                + "}, priority={" + priority + "}, owner={" + owner + "},"
                + " history={" + history + "}, m={" + monitoring + "}, s={"
                + start + "}, max={" + max + "}");
        //, mode, priority, owner,
        //    history, monitoring, start, max);
        if (monitoring) {
            query += " t.owner <> t.delegatedTo"
		    + " and t.owner = :owner"
		    + " and t.delegatedTo IS NOT NULL";
        } else {
            query += " (t.delegatedTo = :owner OR"
                    + " (t.delegatedTo IS NULL AND t.owner=:owner))";
        }

        if (priority > 0) {
            query += " and t.priority = :priority";
        } else {
            query += " and t.priority > 0";
        }

        boolean found = false;
        switch (mode) {
            case AFTER_THAT:
                found = true;
                query += " and (t.end >= :nextMonth";
                if (history) {
                    query += " and t.status <> :open)";
                } else {
                    query += " and t.status = :open)";
                }
                break;
            case THIS_MONTH:
                found = true;
                query += " and (t.end <= :monthEnd and t.end >= :monthStart";
                if (history) {
                    query += " and t.status <> :open)";
                } else {
                    query += " and t.status = :open)";
                }
                break;
            case THIS_WEEK:
                found = true;
                query += " and (t.end <= :weekEnd and t.end >= :weekStart";
                if (history) {
                    query += " and t.status <> :open)";
                } else {
                    query += " and t.status = :open)";
                }
                break;
            case TODAY:
                found = true;
                query += " and (t.end <= :dayEnd";// and t.end >= :dayStart";
                if (history) {
                    query += " and t.status <> :open)";
                } else {
                    query += " and t.status = :open)";
                }
                break;
        }

        if (!found) {
            if (history) {
                query += " and t.status <> :open";
            } else {
                query += " and t.status = :open";
            }
        }

	if (orderBy == null) {
	    query += " order by t.end desc";
	} else {
	    String ord = orderBy;
	    if (ord.startsWith("statusString")) {
		ord = "t.end";
	    } else if (ord.startsWith("owner.fullName")) {
		ord = "o.fullName";
	    } else if (ord.startsWith("assignedBy.fullName")) {
		ord = "a.fullName";
	    } else if (ord.startsWith("creatorName")) {
		ord = "o.fullName";
	    } else {
		ord = "t." + ord;
	    }
	    query += " order by " + ord
		+ (isAscending ? " asc" : " desc");
	}

        //System.out.println("DEBUG: QUERY" + query);

        EntityManager em = DBUtil.getEM();
        try {
            Query q = em.createQuery(query, Tasks.class);

            q.setFirstResult(start == 0 ? start : start - 1);

            if (max != Integer.MAX_VALUE) {
                q.setMaxResults(max + 1);
            }

            q.setParameter("owner", owner);

            if (priority > 0) {
                q.setParameter("priority", priority);
            }

            q.setParameter("open", TaskStatus.OPEN);

            Calendar cal = Calendar.getInstance();
            cal.setLenient(true);

            switch (mode) {
                case AFTER_THAT:
                    //query += " and t.end >= :nextMonth";
                    cal.set(Calendar.DAY_OF_MONTH,
                            cal.getMinimum(Calendar.DAY_OF_MONTH));
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMinimum(Calendar.HOUR_OF_DAY));
                    cal.add(Calendar.MONTH, 1);
                    cal.set(Calendar.MINUTE,
                            cal.getMinimum(Calendar.MINUTE));
                    q.setParameter("nextMonth", cal.getTime());
                    break;
                case THIS_MONTH:
                    //query += " and t.end <= :monthEnd and t.end >= monthStart";
                    cal.set(Calendar.DAY_OF_MONTH,
                            cal.getMinimum(Calendar.DAY_OF_MONTH));
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMinimum(Calendar.HOUR_OF_DAY));
                    cal.set(Calendar.MINUTE,
                            cal.getMinimum(Calendar.MINUTE));
                    q.setParameter("monthStart", cal.getTime());
                    cal.add(Calendar.MONTH, 1);
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMaximum(Calendar.HOUR_OF_DAY));
                    cal.set(Calendar.MINUTE,
                            cal.getMaximum(Calendar.MINUTE));
                    q.setParameter("monthEnd", cal.getTime());
                    break;
                case THIS_WEEK:
                    //query += " and t.end <= :weekEnd and t.end >= :weekStart";
                    cal.set(Calendar.DAY_OF_WEEK,
                            cal.getMinimum(Calendar.DAY_OF_WEEK));
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMinimum(Calendar.HOUR_OF_DAY));
                    cal.set(Calendar.MINUTE,
                            cal.getMinimum(Calendar.MINUTE));
                    q.setParameter("weekStart", cal.getTime());
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMaximum(Calendar.HOUR_OF_DAY));
                    cal.set(Calendar.DAY_OF_WEEK,
                            cal.getMaximum(Calendar.DAY_OF_WEEK));
                    cal.set(Calendar.MINUTE,
                            cal.getMaximum(Calendar.MINUTE));
                    q.setParameter("weekEnd", cal.getTime());
                    break;
                case TODAY:
                    //query += " and t.end <= :dayEnd and t.end >= :dayStart";
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMinimum(Calendar.HOUR_OF_DAY));
                    cal.set(Calendar.MINUTE,
                            cal.getMinimum(Calendar.MINUTE));
                    //q.setParameter("dayStart", cal.getTime());
                    cal.set(Calendar.HOUR_OF_DAY,
                            cal.getMaximum(Calendar.HOUR_OF_DAY));
                    cal.set(Calendar.MINUTE,
                            cal.getMaximum(Calendar.MINUTE));
                    q.setParameter("dayEnd", cal.getTime());
                    break;
            }
            list.addAll(q.getResultList());
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            em.close();
        }

        return list;
    }
}
