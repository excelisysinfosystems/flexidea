package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.util.DBUtil;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author akshay
 */
public class UserDAO {
    public static CrmUser getUser(Long id) {
	CrmUser user = null;
	EntityManager em = DBUtil.getEM();
	user = em.find(CrmUser.class, id);
	em.close();
	return user;
    }

    public static CrmUser getUser(String username, String password) {
	CrmUser user = null;

	EntityManager em = DBUtil.getEM();
	try {
	    Query q = em.createQuery("select Object(u) from CrmUser u"
		+ " where u.username=:username and u.password=:password");
	    q.setParameter("username", username);
	    q.setParameter("password", DigestUtils.md5Hex(password));

	    if (q.getResultList().size() > 0) {
		user = (CrmUser) q.getResultList().get(0);
	    }
	} catch (Exception e) {

	} finally {
	    em.close();
	}

	return user;
    }

    public static List<CrmUser> listUsers() {
	List<CrmUser> list = new ArrayList<CrmUser>();
	EntityManager em = DBUtil.getEM();
	try {
	    Query q = em.createQuery("select Object(u) from CrmUser u");
	    list.addAll(q.getResultList());
	} catch (Exception e) {

	} finally {
	    em.close();
	}
	return list;
    }

    public static List<CrmUser> listUsers(String orderBy, boolean asc,
	int first, int count) {
	List<CrmUser> list = new ArrayList<CrmUser>();
	EntityManager em = DBUtil.getEM();
	try {
	    String query = "select Object(u) from CrmUser u";

	    if (orderBy != null) {
		query += " order by " + orderBy
		    + (asc ? " asc" : " desc");
	    }

	    Query q = em.createQuery(query);

	    q.setFirstResult(first == 0 ? first : first - 1);
	    if (count < Integer.MAX_VALUE) {
		q.setMaxResults(count + 1);
	    }

	    list.addAll(q.getResultList());
	} catch (Exception e) {

	} finally {
	    em.close();
	}
	return list;
    }
}
