/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.flexidea.crm.db.dto.City;
import com.flexidea.crm.db.dto.Combo;
import com.flexidea.crm.db.util.DBUtil;

/**
 * 
 * @author akshay
 */
public class ListsDAO {
	public static List<City> getCities() {
		List<City> list = new ArrayList<City>();
		EntityManager em = DBUtil.getEM();

		try {
			Query q = em.createQuery("select Object(c) from City c" + " order by c.cityName");
			list.addAll((List<City>) q.getResultList());
		} catch (Exception e) {

		} finally {
			em.close();
		}

		return list;
	}

	public static City getCity(String cityName) {
		City city = null;
		EntityManager em = DBUtil.getEM();

		try {
			Query q = em.createQuery("select Object(c) from City c" + " where UPPER(c.cityName)=:cityName");
			q.setParameter("cityName", cityName.toUpperCase());
			if (!q.getResultList().isEmpty()) {
				city = (City) q.getResultList().get(0);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			em.close();
		}

		return city;
	}

	public static List<Combo> getCombos() {
		List<Combo> list = new ArrayList<Combo>();
		EntityManager em = DBUtil.getEM();

		try {
			Query q = em.createQuery("select Object(c) from Combo c" + " order by c.comboName");
			list.addAll((List<Combo>) q.getResultList());
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			em.close();
		}

		return list;
	}
}
