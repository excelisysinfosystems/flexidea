package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.Attachments;
import com.flexidea.crm.db.dto.CommentsTarget;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.ContactComments;
import com.flexidea.crm.db.dto.CrmComments;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.Enquiry;
import com.flexidea.crm.db.dto.EnquiryComments;
import com.flexidea.crm.db.dto.Lead;
import com.flexidea.crm.db.dto.LeadComments;
import com.flexidea.crm.db.dto.Project;
import com.flexidea.crm.db.dto.ProjectComments;
import com.flexidea.crm.db.dto.TaskComments;
import com.flexidea.crm.db.dto.Tasks;
import com.flexidea.crm.db.util.DBUtil;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author akshay
 */
public class CommentsDAO {

    public static class Attachment {
	private String fileName;
	private String contentType;
	private InputStream inputStream;

	public String getContentType() {
	    return contentType;
	}

	public void setContentType(String contentType) {
	    this.contentType = contentType;
	}

	public String getFileName() {
	    return fileName;
	}

	public void setFileName(String fileName) {
	    this.fileName = fileName;
	}

	public InputStream getInputStream() {
	    return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
	    this.inputStream = inputStream;
	}
    }

    public static void addUpdateComment(EntityManager em,
	CommentsTarget target, Long targetId,
	Object oldEntity, Object newEntity, CrmUser user,
	String[] properties, String[] display, String prefix) {
	List<String> changed = DBUtil.compareModels(oldEntity, newEntity,
	    properties, display);
	StringBuilder sb = new StringBuilder();

	sb.append("Updated ");
	if (prefix != null) {
	    sb.append(prefix);
	}
	int i = 0;
	for (String c : changed) {
	    if (changed.size() > 1 && i < changed.size() - 2) {
		sb.append(c);
		sb.append(", ");
	    } else if (changed.size() > 1 && i == changed.size() - 1) {
		sb.append(" and ");
		sb.append(c);
	    } else {
		sb.append(c);
	    }
	    i++;
	}
	addComment(em, target, targetId, null, user, sb.toString());
    }

    public static void addComment(EntityManager em, CommentsTarget target, 
	Long targetId, Object targetObj, CrmUser user, String comment) {
	CrmComments c = new CrmComments();
	c.setAssociatedWith(target.toString());
	c.setCommentText(comment.getBytes());
	c.setCreatedAt(new Date());
	c.setCreatedBy(user);
	addComments(em, target, targetId, targetObj, c);
    }

    public static void addComments(EntityManager em, CommentsTarget target, 
	Long targetId, Object targetObj, CrmComments comment) {
	addComments(em, target, targetId, targetObj, comment, new ArrayList<Attachment>());
    }

    public static void addComments(EntityManager iem, CommentsTarget target,
	Long targetId, Object targetObj, CrmComments comment, List<Attachment> attachments) {
	EntityManager em = iem;
	try {
	    if (em == null) {
		em = DBUtil.getEM();
	    }

	    if (iem == null) {
		em.getTransaction().begin();
	    }
	    comment.setAssociatedWith(target.toString());
	    switch (target) {
		case CONTACTS:
		    em.persist(comment);

		    ContactComments cm = new ContactComments();
		    cm.setComment(comment);

		    if (targetObj == null) {
			cm.setCompany(CompaniesDAO.getCompany(targetId));
		    } else {
			cm.setCompany((Company) targetObj);
		    }

		    em.persist(cm);

		    for (Attachment a : attachments) {
			Attachments att = new Attachments();
			att.setAttachmentName(a.getFileName());
			att.setContentType(a.getContentType());
			att.setComment(comment);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int ch = 0;
			InputStream stream = a.getInputStream();
			while ((ch = stream.read()) != -1) {
			    bos.write(ch);
			}
			att.setAttachmentContent(bos.toByteArray());

			em.persist(att);
		    }

		    break;
		case LEADS:
		    em.persist(comment);

		    LeadComments lm = new LeadComments();
		    lm.setComment(comment);
		    if (targetObj == null) {
			lm.setLead(LeadsDAO.getLead(targetId));
		    } else {
			lm.setLead((Lead) targetObj);
		    }
		    em.persist(lm);

		    for (Attachment a : attachments) {
			Attachments att = new Attachments();
			att.setAttachmentName(a.getFileName());
			att.setContentType(a.getContentType());
			att.setComment(comment);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int ch = 0;
			InputStream stream = a.getInputStream();
			while ((ch = stream.read()) != -1) {
			    bos.write(ch);
			}
			att.setAttachmentContent(bos.toByteArray());

			em.persist(att);
		    }

		    break;
		case PROJECT:
		    em.persist(comment);

		    ProjectComments pm = new ProjectComments();
		    pm.setComment(comment);

		    if (targetObj == null) {
			pm.setProject(ProjectDAO.getProject(targetId));
		    } else {
			pm.setProject((Project) targetObj);
		    }

		    em.persist(pm);

		    for (Attachment a : attachments) {
			Attachments att = new Attachments();
			att.setAttachmentName(a.getFileName());
			att.setContentType(a.getContentType());
			att.setComment(comment);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int ch = 0;
			InputStream stream = a.getInputStream();
			while ((ch = stream.read()) != -1) {
			    bos.write(ch);
			}
			att.setAttachmentContent(bos.toByteArray());

			em.persist(att);
		    }

		    break;
		case ENQUIRY:
		    em.persist(comment);

		    EnquiryComments qm = new EnquiryComments();
		    qm.setComment(comment);
		    if (targetObj == null) {
			qm.setEnquiry(EnquiryDAO.getEnquiry(targetId));
		    } else {
			qm.setEnquiry((Enquiry) targetObj);
		    }
		    em.persist(qm);

		    for (Attachment a : attachments) {
			Attachments att = new Attachments();
			att.setAttachmentName(a.getFileName());
			att.setContentType(a.getContentType());
			att.setComment(comment);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int ch = 0;
			InputStream stream = a.getInputStream();
			while ((ch = stream.read()) != -1) {
			    bos.write(ch);
			}
			att.setAttachmentContent(bos.toByteArray());

			em.persist(att);
		    }
		    break;
		case TASK:
		    em.persist(comment);

		    TaskComments tm = new TaskComments();
		    tm.setComment(comment);
		    if (targetObj == null) {
			tm.setTask(TasksDAO.getTask(targetId));
		    } else {
			tm.setTask((Tasks) targetObj);
		    }

		    em.persist(tm);

		    for (Attachment a : attachments) {
			Attachments att = new Attachments();
			att.setAttachmentName(a.getFileName());
			att.setContentType(a.getContentType());
			att.setComment(comment);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int ch = 0;
			InputStream stream = a.getInputStream();
			while ((ch = stream.read()) != -1) {
			    bos.write(ch);
			}
			att.setAttachmentContent(bos.toByteArray());

			em.persist(att);
		    }
		    break;
	    }
	    if (iem == null) {
		em.getTransaction().commit();
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (iem == null && em != null) {
		em.close();
	    }
	}
    }

    public static void addComments(CommentsTarget target, Long targetId,
	Object targetObj, CrmComments comment, List<Attachment> attachments) {
	addComments(null, target, targetId, targetObj, comment, attachments);
    }

    public static List<CrmComments> getComments(CommentsTarget target,
	Long targetId) {
	List<CrmComments> comments = new ArrayList<CrmComments>();
	EntityManager em = DBUtil.getEM();
	try {
	    Query qry;
	    switch (target) {
		case CONTACTS:
		    qry = em.createQuery("select Object(cm) from"
			+ " Company c JOIN c.comments o"
			+ " JOIN o.comment cm"
			+ " where c = :company"
			+ " order by cm.createdAt desc",
			CrmComments.class);
		    qry.setParameter("company", CompaniesDAO.getCompany(targetId));
			//.getContact(targetId).getCompany());
		    comments.addAll(qry.getResultList());
		    break;
		case LEADS:
		    qry = em.createQuery("select Object(c) from"
			+ " Lead l JOIN l.leadComments lc "
			+ " JOIN lc.comment c"
			+ " where l = :lead"
			+ " order by c.createdAt desc",
			CrmComments.class);
		    qry.setParameter("lead", LeadsDAO.getLead(targetId));
		    comments.addAll(qry.getResultList());
		    /*
		    for (LeadComments c :
			LeadsDAO.getLead(targetId).getLeadComments()) {
			comments.add(c.getComment());
		    }
		     *
		     */
		    break;
		case PROJECT:
		    Project p = ProjectDAO.getProject(targetId);

		    String nquery = "(select c.* from crm_comments c, "
			+ "project_comments pc where pc.project_comment = "
			+ "c.comment_id and pc.project=?1) union "
			+ "(select c1.* from crm_comments c1, lead_comments "
			+ "lc where lc.lead_comment = c1.comment_id and "
			+ "lc.lead=?2) "
			+ " union "
			+ " (select c3.* from crm_comments c3, "
			+ " enquiry_comments ec, enquiry e "
			+ " where ec.enquiry_comment = "
			+ " c3.comment_id and ec.enquiry=e.ID"
			+ " and e.project = ?3)"

			+ " order by created_at desc";

		    Query nq = em.createNativeQuery(nquery, CrmComments.class);
		    nq.setParameter(1, p.getId());

		    if (p.getLead() != null)
			nq.setParameter(2, p.getLead().getId());
		    else
			nq.setParameter(2, 0L);

		    nq.setParameter(3, p.getId());
		    comments.addAll(nq.getResultList());
		    break;
		case ENQUIRY:
		    Enquiry e = EnquiryDAO.getEnquiry(targetId);

		    if (e.getProject() != null) {
			String query = "select c.* from crm_comments c, "
			    + "enquiry_comments pc where pc.enquiry_comment = "
			    + "c.comment_id and pc.enquiry=?1"
			    /*+ " union "
			    + "(select c1.* from crm_comments c1,"
			    + " project_comments "
			    + "lc where lc.project_comment = c1.comment_id and "
			    + "lc.project=?2) "
			     */
			    + " order by created_at desc";

			Query q = em.createNativeQuery(query, CrmComments.class);
			q.setParameter(1, e.getId());
			//q.setParameter(2, e.getProject().getId());
			comments.addAll(q.getResultList());
		    } else {
			for (EnquiryComments c : e.getComments()) {
			    comments.add(c.getComment());
			}
		    }
		    break;
		case TASK:
		    qry = em.createQuery("select Object(c) from"
			+ " Tasks t JOIN t.comments tc "
			+ " JOIN tc.comment c"
			+ " where t = :task"
			+ " order by c.createdAt desc",
			CrmComments.class);
		    qry.setParameter("task", TasksDAO.getTask(targetId));
		    comments.addAll(qry.getResultList());
		    /*
		    for (TaskComments c :
			TasksDAO.getTask(targetId).getComments()) {
			comments.add(c.getComment());
		    }
		     *
		     */
		    break;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}
	return comments;
    }

    public static List<CrmComments> searchComments(CommentsTarget target,
	Long targetId, CrmUser user, String icriteria) {
	List<CrmComments> list = new ArrayList<CrmComments>();

	if (targetId == null) {
	    return list;
	}

	String criteria = icriteria == null ? "" : icriteria;

	System.out.println("DEBUG: searchComments t=" + target
	    + ", i=" + targetId + ", u=" + user + ", c=" + criteria);

	EntityManager em = DBUtil.getEM();
	try {
	    Query q;
	    switch (target) {
		case CONTACTS:
		    if (user == null || user.getUserId() == 0) {
			q = em.createQuery("select Object(c) "
			    + " from ContactComments cc JOIN cc.comment c "
			    //+ " JOIN Attachments a.comment"
			    + " where "
			    + " cc.company = :company and"
			    + " c.commentText like :criteria ");
			    //+ " a.attachmentName like :criteria)");
			q.setParameter("company",
			    CompaniesDAO.getCompany(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
		    } else {
			q = em.createQuery("select Object(c) "
			    + " from ContactComments cc JOIN cc.comment c "
			    //+ " JOIN c.attachments a"
			    + " where "
			    + " cc.company = :company and"
			    + " c.createdBy = :user and"
			    + " c.commentText like :criteria ");
			    //+ " or a.attachmentName like :criteria");
			q.setParameter("company",
			    CompaniesDAO.getCompany(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
			q.setParameter("user", user);
		    }

		    list.addAll((List<CrmComments>) q.getResultList());

		    break;
		case LEADS:
		    if (user == null || user.getUserId() == 0) {
			q = em.createQuery("select Object(c) "
			    + " from LeadComments cc JOIN cc.comment c "
			    //+ " JOIN Attachments a.comment"
			    + " where "
			    + " cc.lead = :lead and"
			    + " c.commentText like :criteria ");
			    //+ " a.attachmentName like :criteria)");
			q.setParameter("lead", LeadsDAO.getLead(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
		    } else {
			q = em.createQuery("select Object(c) "
			    + " from LeadComments cc JOIN cc.comment c "
			    //+ " JOIN c.attachments a"
			    + " where "
			    + " cc.lead = :lead and"
			    + " c.createdBy = :user and"
			    + " c.commentText like :criteria ");
			    //+ " or a.attachmentName like :criteria");
			q.setParameter("lead", LeadsDAO.getLead(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
			q.setParameter("user", user);
		    }

		    list.addAll((List<CrmComments>) q.getResultList());

		    break;
		case PROJECT:
		    if (user == null || user.getUserId() == 0) {
			q = em.createQuery("select Object(c) "
			    + " from ProjectComments cc JOIN cc.comment c "
			    //+ " JOIN Attachments a.comment"
			    + " where "
			    + " cc.project = :project and"
			    + " c.commentText like :criteria ");
			    //+ " a.attachmentName like :criteria)");
			q.setParameter("project", ProjectDAO.getProject(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
		    } else {
			q = em.createQuery("select Object(c) "
			    + " from ProjectComments cc JOIN cc.comment c "
			    //+ " JOIN c.attachments a"
			    + " where "
			    + " cc.project = :project and"
			    + " c.createdBy = :user and"
			    + " c.commentText like :criteria ");
			    //+ " or a.attachmentName like :criteria");
			q.setParameter("project", ProjectDAO.getProject(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
			q.setParameter("user", user);
		    }

		    list.addAll((List<CrmComments>) q.getResultList());

		    break;
		case ENQUIRY:
		    if (user == null || user.getUserId() == 0) {
			q = em.createQuery("select Object(c) "
			    + " from EnquiryComments cc JOIN cc.comment c "
			    //+ " JOIN Attachments a.comment"
			    + " where "
			    + " cc.enquiry = :enquiry and"
			    + " c.commentText like :criteria ");
			    //+ " a.attachmentName like :criteria)");
			q.setParameter("enquiry", EnquiryDAO.getEnquiry(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
		    } else {
			q = em.createQuery("select Object(c) "
			    + " from EnquiryComments cc JOIN cc.comment c "
			    //+ " JOIN c.attachments a"
			    + " where "
			    + " cc.enquiry = :enquiry and"
			    + " c.createdBy = :user and"
			    + " c.commentText like :criteria ");
			    //+ " or a.attachmentName like :criteria");
			q.setParameter("enquiry", EnquiryDAO.getEnquiry(targetId));
			    //ContactsDAO.getContact(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
			q.setParameter("user", user);
		    }

		    list.addAll((List<CrmComments>) q.getResultList());

		    break;
		case TASK:
		    if (user == null || user.getUserId() == 0) {
			q = em.createQuery("select Object(c) "
			    + " from TaskComments cc JOIN cc.comment c "
			    //+ " JOIN Attachments a.comment"
			    + " where "
			    + " cc.task = :task and"
			    + " c.commentText like :criteria ");
			    //+ " a.attachmentName like :criteria)");
			q.setParameter("task",
			    TasksDAO.getTask(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
		    } else {
			q = em.createQuery("select Object(c) "
			    + " from TaskComments cc JOIN cc.comment c "
			    //+ " JOIN c.attachments a"
			    + " where "
			    + " cc.task = :task and"
			    + " c.createdBy = :user and"
			    + " c.commentText like :criteria ");
			    //+ " or a.attachmentName like :criteria");
			q.setParameter("task",
			    TasksDAO.getTask(targetId));
			q.setParameter("criteria", "%" + criteria + "%");
			q.setParameter("user", user);
		    }

		    list.addAll((List<CrmComments>) q.getResultList());

		    break;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    em.close();
	}

	return list;
    }
}