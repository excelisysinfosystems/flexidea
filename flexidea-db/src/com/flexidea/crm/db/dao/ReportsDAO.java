package com.flexidea.crm.db.dao;

import com.flexidea.crm.db.dto.ReportModel;
import com.flexidea.crm.db.dto.ReportsSchedule;
import com.flexidea.crm.db.util.DBUtil;
import com.flexidea.crm.db.util.ReminderService;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 *
 * @author akshay
 */
public class ReportsDAO {

    public static ReportModel getReportModel(Long id) {
	return (ReportModel) DBUtil.getEntity(ReportModel.class, id);
    }

    public static ReportsSchedule getReportSchedule(Long id) {
	return (ReportsSchedule)
	    DBUtil.getEntity(ReportsSchedule.class, id);
    }

    public static void save(ReportModel model,
	List<ReportsSchedule> schedules) {
	EntityManager em = null;

	try {
	    em = DBUtil.getEM();

	    em.getTransaction().begin();

	    if (model.getId() == null) {
		em.persist(model);
	    } else {
		em.merge(model);
	    }

	    for (ReportsSchedule schedule : schedules) {
		if (schedule.getTo() == null
		    || schedule.getDuration() == null
		    || schedule.getStartDate() == null
		    ) {
		    continue;
		}

		if (schedule.getId() == null && schedule.isActive()) {
		    schedule.setModel(model);
		    em.persist(schedule);
		} else if (schedule.getId() != null) {
		    schedule.setModel(model);
		    em.merge(schedule);
		}
	    }

	    em.getTransaction().commit();

	    ReportModel m = em.find(ReportModel.class, model.getId());
	    for (ReportsSchedule schedule: m.getReportsSchedule()) {
		ReminderService.removeSchedule(schedule);

		if (schedule.isActive()) {
		    ReminderService.schedule(schedule);
		}
	    }
	    model = em.find(ReportModel.class, model.getId());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
    }

    public static List<ReportModel> getReports() {
	List<ReportModel> list = new ArrayList<ReportModel>();
	EntityManager em = null;
	try {
	    em = DBUtil.getEM();
	    Query q = em.createQuery("select object(r) from"
		+ " ReportModel r");
	    list.addAll(q.getResultList());
	} catch (Exception e) {
	    throw new RuntimeException(e);
	} finally {
	    if (em != null) {
		em.close();
	    }
	}
	return list;
    }

    public static void export(HashMap map,
	InputStream report, File outputFile) {
	try {
	    Properties props = new Properties();
	    props.load(ReportsDAO.class
		.getClassLoader()
		.getResourceAsStream("META-INF/connection.properties"));
	    Class.forName(props.getProperty("jdbc.driver"));
	    Connection connection = DriverManager.getConnection(
		props.getProperty("jdbc.url"),
		props.getProperty("jdbc.username"),
		props.getProperty("jdbc.password"));
	    JasperPrint print = JasperFillManager.fillReport(report, map,
		connection);
	    JRExporter exporter = new JRPdfExporter();
	    exporter.setParameter(JRExporterParameter.OUTPUT_FILE, outputFile);
	    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
	    System.out.println("DEBUG: exporting Report");
	    exporter.exportReport();
	    System.out.println("DEBUG: Report exported to=" + outputFile);
	} catch (Exception e) {
	    e.printStackTrace(System.out);
	    throw new RuntimeException(e);
	}
    }

    public static File export(ReportModel model) {
	File tmp = null;
	try {
	    tmp = File.createTempFile("report.", ".pdf");
	    tmp.createNewFile();

	    HashMap map = null;
	    map = new HashMap();

	    map.put("SUBREPORT_DIR", ReportsDAO.class.getClassLoader().getResource("META-INF/").toString());
	    map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	    map.put("groupByLevel1_Consultant", Boolean.FALSE);
	    map.put("groupByLevel1_Contractor", Boolean.FALSE);
	    map.put("groupByLevel1_Branch", Boolean.FALSE);
	    map.put("groupByLevel1_Project", Boolean.FALSE);
	    map.put("groupByLevel2_Value", Boolean.FALSE);
	    map.put("groupByLevel3_Weekly", Boolean.FALSE);
	    map.put("groupByLevel3_Monthly", Boolean.FALSE);
	    map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	    map.put("groupByLevel3_Annual", Boolean.FALSE);
	    map.put("stepValueFunction","" +
                "    CASE" +
                "        WHEN enq.basic_value <= 100000.00 THEN 1000000.00" +
                "        WHEN enq.basic_value <= 500000.00 THEN 500000.00" +
                "        WHEN enq.basic_value <= 1000000.00 THEN 1000000.00" +
                "        WHEN enq.basic_value <= 5000000.00 THEN 5000000.00" +
                "        WHEN enq.basic_value <= 100000000.00 THEN 100000000.00" +
                "        WHEN enq.basic_value > 100000000.00 THEN 100000000.00" +
                "        ELSE 1000000000.00" +
                "    END ");
	    map.put("stepFunction", "" +
                "    CASE" +
                "        WHEN enq.basic_value <= 100000.00 THEN '1L'" +
                "        WHEN enq.basic_value <= 500000.00 THEN '5L'" +
                "        WHEN enq.basic_value <= 1000000.00 THEN '10L'" +
                "        WHEN enq.basic_value <= 5000000.00 THEN '50L'" +
                "        WHEN enq.basic_value <= 100000000.00 THEN '100L'" +
                "        WHEN enq.basic_value > 100000000.00 THEN '1000L'" +
                "        ELSE 'above 1000L'" +
                "    END ");

	    if (model.getLevel1Group() != null) {
		switch (model.getLevel1Group()) {
		    case Branch:
			map.put("groupByLevel1_Branch", Boolean.TRUE);
			break;
		    case Consultant:
			map.put("groupByLevel1_Consultant", Boolean.TRUE);
			break;
		    case Contractor:
			map.put("groupByLevel1_Contractor", Boolean.TRUE);
			break;
		    case Job_Status:
			break;
		    case Project:
			map.put("groupByLevel1_Project", Boolean.TRUE);
			break;
		    case Sales_Representative:
			map.put("groupByLevel1_SalesRep", Boolean.TRUE);
			break;
		}
	    }

	    if (model.getLevel2Group() != null) {
		map.put("groupByLevel2_Value", Boolean.TRUE);
		switch (model.getLevel2Group()) {
		    case _10L:
			map.put("stepValueFunction","" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 1000000.00 THEN 1000000.00" +
			    "        WHEN enq.basic_value <= 2000000.00 THEN 2000000.00" +
			    "        WHEN enq.basic_value <= 3000000.00 THEN 3000000.00" +
			    "        WHEN enq.basic_value <= 4000000.00 THEN 4000000.00" +
			    "        WHEN enq.basic_value <= 5000000.00 THEN 5000000.00" +
			    "        WHEN enq.basic_value <= 6000000.00 THEN 6000000.00" +
			    "        WHEN enq.basic_value <= 7000000.00 THEN 7000000.00" +
			    "        WHEN enq.basic_value <= 8000000.00 THEN 8000000.00" +
			    "        WHEN enq.basic_value <= 9000000.00 THEN 9000000.00" +
			    "        WHEN enq.basic_value < 10000000.00 THEN 10000000.00" +
			    "        ELSE 100000000.00" +
			    "    END ");
			map.put("stepFunction", "" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 1000000.00 THEN '10L'" +
			    "        WHEN enq.basic_value <= 2000000.00 THEN '20L'" +
			    "        WHEN enq.basic_value <= 3000000.00 THEN '30L'" +
			    "        WHEN enq.basic_value <= 4000000.00 THEN '40L'" +
			    "        WHEN enq.basic_value <= 5000000.00 THEN '50L'" +
			    "        WHEN enq.basic_value <= 6000000.00 THEN '60L'" +
			    "        WHEN enq.basic_value <= 7000000.00 THEN '70L'" +
			    "        WHEN enq.basic_value <= 8000000.00 THEN '80L'" +
			    "        WHEN enq.basic_value <= 9000000.00 THEN '90L'" +
			    "        WHEN enq.basic_value < 10000000.00 THEN '100L'" +
			    "        ELSE 'above 100L'" +
			    "    END ");
			break;
		    case _25L:
			map.put("stepValueFunction","" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 2500000.00 THEN 2500000.00" +
			    "        WHEN enq.basic_value <= 5000000.00 THEN 5000000.00" +
			    "        WHEN enq.basic_value <= 7500000.00 THEN 7500000.00" +
			    "        WHEN enq.basic_value < 10000000.00 THEN 10000000.00" +
			    "        ELSE 100000000.00" +
			    "    END ");
			map.put("stepFunction", "" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 2500000.00 THEN '25L'" +
			    "        WHEN enq.basic_value <= 5000000.00 THEN '50L'" +
			    "        WHEN enq.basic_value <= 7500000.00 THEN '75L'" +
			    "        WHEN enq.basic_value < 10000000.00 THEN '100L'" +
			    "        ELSE 'above 100L'" +
			    "    END ");
			break;
		    case _50L:
			map.put("stepValueFunction","" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 5000000.00 THEN 5000000.00" +
			    "        WHEN enq.basic_value <= 10000000.00 THEN 10000000.00" +
			    "        WHEN enq.basic_value <= 15000000.00 THEN 15000000.00" +
			    "        WHEN enq.basic_value < 20000000.00 THEN 20000000.00" +
			    "        ELSE 100000000.00" +
			    "    END ");
			map.put("stepFunction", "" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 5000000.00 THEN '50L'" +
			    "        WHEN enq.basic_value <= 10000000.00 THEN '100L'" +
			    "        WHEN enq.basic_value <= 15000000.00 THEN '150L'" +
			    "        WHEN enq.basic_value < 20000000.00 THEN '200L'" +
			    "        ELSE 'above 200L'" +
			    "    END ");
			break;
		    case _100L:
			map.put("stepValueFunction","" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 10000000.00 THEN 10000000.00" +
			    "        WHEN enq.basic_value <= 20000000.00 THEN 20000000.00" +
			    "        WHEN enq.basic_value <= 30000000.00 THEN 30000000.00" +
			    "        WHEN enq.basic_value <= 40000000.00 THEN 40000000.00" +
			    "        WHEN enq.basic_value <= 50000000.00 THEN 50000000.00" +
			    "        WHEN enq.basic_value <= 60000000.00 THEN 60000000.00" +
			    "        WHEN enq.basic_value <= 70000000.00 THEN 70000000.00" +
			    "        WHEN enq.basic_value <= 80000000.00 THEN 80000000.00" +
			    "        WHEN enq.basic_value <= 90000000.00 THEN 90000000.00" +
			    "        WHEN enq.basic_value < 100000000.00 THEN 100000000.00" +
			    "        ELSE 100000000.00" +
			    "    END ");
			map.put("stepFunction", "" +
			    "    CASE" +
			    "        WHEN enq.basic_value <= 10000000.00 THEN '100L'" +
			    "        WHEN enq.basic_value <= 20000000.00 THEN '200L'" +
			    "        WHEN enq.basic_value <= 30000000.00 THEN '300L'" +
			    "        WHEN enq.basic_value <= 40000000.00 THEN '400L'" +
			    "        WHEN enq.basic_value <= 50000000.00 THEN '500L'" +
			    "        WHEN enq.basic_value <= 60000000.00 THEN '600L'" +
			    "        WHEN enq.basic_value <= 70000000.00 THEN '700L'" +
			    "        WHEN enq.basic_value <= 80000000.00 THEN '800L'" +
			    "        WHEN enq.basic_value <= 90000000.00 THEN '900L'" +
			    "        WHEN enq.basic_value < 100000000.00 THEN '1000L'" +
			    "        ELSE 'above 1000L'" +
			    "    END ");
			break;
		}
	    }

	    if (model.getLevel3Group() != null) {
		switch (model.getLevel3Group()) {
		    case Annual:
			map.put("groupByLevel3_Annual", Boolean.TRUE);
			break;
		    case Monthly:
			map.put("groupByLevel3_Monthly", Boolean.TRUE);
			break;
		    case Quarterly:
			map.put("groupByLevel3_Quarterly", Boolean.TRUE);
			break;
		    case Weekly:
			map.put("groupByLevel3_Weekly", Boolean.TRUE);
			break;
		}
	    }

	    if (model.isFilterByContractor()
		&& model.getFilterContractor() != null) {
		map.put("filterByContractor", " and contComp.ID = "
		    + model.getFilterContractor().getId());
	    }

	    if (model.isFilterBySalesRep()
		&& model.getFilterSalesRep() != null) {
		map.put("filterBySalesRep", " and sr.ID = "
		    + model.getFilterSalesRep().getId());
	    }

	    if (model.isFilterByConsultant()
		&& model.getFilterConsultant() != null) {
		map.put("filterByConsultant",
		    " and _consultant.company_ID = "
		    + model.getFilterConsultant().getId());
	    }

	    if (model.isFilterByBranch()
		&& model.getFilterBranch() != null) {
		map.put("filterByBranch",
		    " and cty.ID = "
		    + model.getFilterBranch().getId());
	    }

	    if (model.getFilterByValue()) {
		if (model.getFilterValue1() == null) {
		    model.setFilterValue1(0.00);
		}

		if (model.getFilterValue2() == null) {
		    model.setFilterValue2(0.00);
		}
	    }

	    if (model.getFilterByValue()
		&& model.getFilterValue1() != null) {
		if ("=".equals(model.getFilterValueOperator())) {
		    map.put("filterByValue", " and enq.basic_value = "
			+ model.getFilterValue1());
		} else if ("<".equals(model.getFilterValueOperator())) {
		    map.put("filterByValue", " and enq.basic_value < "
			+ model.getFilterValue1());
		} else if (">".equals(model.getFilterValueOperator())) {
		    map.put("filterByValue", " and enq.basic_value > "
			+ model.getFilterValue1());
		} else if ("between".equals(
		    model.getFilterValueOperator())
		    && model.getFilterValue2() != null) {
		    map.put("filterByValue", 
			" and enq.basic_value between "
			+ model.getFilterValue1()
			+ " and "
			+ model.getFilterValue2()
			);
		}
	    }

	    if (model.isFilterByJobStatus()
		&& model.getFilterJobStatus() != null) {
		map.put("filterByJobStatus",
		    " and enq.enquiry_status = '"
		    + model.getFilterJobStatus().toString()
		    + "'");
	    }

	    if (model.isFilterByDateRange()) {
		if (model.getFilterEndDate() == null) {
		    model.setFilterEndDate(new Date());
		}

		SimpleDateFormat sdf =
		    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (model.getFilterStartDate() != null) {
		    map.put("filterByTimePeriod",
			" and enq.modified_at between "
			+ "'" 
			+ sdf.format(model.getFilterStartDate())
			+ "' and '"
			+ sdf.format(model.getFilterEndDate())
			+ "'");
		}
	    }

	    if (model.isFilterByContactType()
		&& model.getFilterContactType() != null) {
		map.put("filterByContactType", 
		    " and company.contact_type = "
		    + model.getFilterContactType().getId());
	    }

	    InputStream fin = null;
	    if (model.isProjectSummaryReport()) {
		fin = ReportsDAO.class.getClassLoader()
		    .getResourceAsStream(
		    "META-INF/project-summary-report.jasper");
	    } else {
		fin = ReportsDAO.class.getClassLoader()
		    .getResourceAsStream(
		    "META-INF/report-contact.jasper");
	    }

	    export(map, fin, tmp);
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
	return tmp;
    }

    public static void main(String[] args) throws Exception {
	/*
	ReportModel model = new  ReportModel();
	export(model,  new FileInputStream(
	    "/home/akshay/NetBeansProjects/flexidea/"
	    + "flexidea-web/web/WEB-INF/classes/TestReport.jasper"),
	    File.createTempFile("report.", ".pdf"));
	 *
	 */
	FileInputStream input = null;
	HashMap map = null;
	File output = null;

	output = new File("/var/tmp/r1.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();

	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.TRUE);
	map.put("orderByContractor", "contractor ASC, ");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.FALSE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);

	export(map, input, output);

	output = new File("/var/tmp/r11.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();

	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.TRUE);
	map.put("orderByContractor", "contractor ASC, ");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.TRUE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);

	export(map, input, output);

	output = new File("/var/tmp/r12.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();
	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.TRUE);
	map.put("orderByContractor", "contractor ASC, ");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.FALSE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.TRUE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);
	export(map, input, output);

	output = new File("/var/tmp/r13.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();
	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.TRUE);
	map.put("orderByContractor", "contractor ASC, ");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.TRUE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.TRUE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);
	export(map, input, output);

	output = new File("/var/tmp/r2.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();

	map.put("groupByLevel1_SalesRep", Boolean.TRUE);
	map.put("orderBySalesRep", "sales_rep ASC, ");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.FALSE);
	map.put("orderByContractor", "");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.FALSE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);

	export(map, input, output);

	output = new File("/var/tmp/r3.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();

	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.TRUE);
	map.put("orderByConsultant", "consultant ASC, ");
	map.put("groupByLevel1_Contractor", Boolean.FALSE);
	map.put("orderByContractor", "");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.FALSE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);

	export(map, input, output);

	output = new File("/var/tmp/r4.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();

	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.FALSE);
	map.put("orderByContractor", "");
	map.put("groupByLevel1_Branch", Boolean.TRUE);
	map.put("orderByBranch", "contractor_branch ASC, ");
	map.put("groupByLevel1_Project", Boolean.FALSE);
	map.put("orderByProject", "");
	map.put("groupByLevel2_Value", Boolean.FALSE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);

	export(map, input, output);

	output = new File("/var/tmp/r5.pdf");
	input = new FileInputStream(
	    "/home/akshay/stuff/iReport-3.7.6/reports/test.jasper");
	map = new HashMap();

	map.put("groupByLevel1_SalesRep", Boolean.FALSE);
	map.put("orderBySalesRep", "");
	map.put("groupByLevel1_Consultant", Boolean.FALSE);
	map.put("orderByConsultant", "");
	map.put("groupByLevel1_Contractor", Boolean.FALSE);
	map.put("orderByContractor", "");
	map.put("groupByLevel1_Branch", Boolean.FALSE);
	map.put("orderByBranch", "");
	map.put("groupByLevel1_Project", Boolean.TRUE);
	map.put("orderByProject", "project_name ASC, ");
	map.put("groupByLevel2_Value", Boolean.FALSE);
	map.put("groupByLevel3_Weekly", Boolean.FALSE);
	map.put("groupByLevel3_Monthly", Boolean.FALSE);
	map.put("groupByLevel3_Quarterly", Boolean.FALSE);
	map.put("groupByLevel3_Annual", Boolean.FALSE);

	export(map, input, output);

    }
}
