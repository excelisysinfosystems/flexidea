package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum AuthAction {
    ALLOW,
    DENY
}
