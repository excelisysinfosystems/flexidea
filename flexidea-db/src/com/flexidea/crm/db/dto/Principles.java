package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum Principles {
    CONTACT,
    LEAD,
    PROJECT,
    ENQUIRY,
    TASK
}
