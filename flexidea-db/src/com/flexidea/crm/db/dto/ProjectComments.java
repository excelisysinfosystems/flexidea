/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "project_comments", catalog = "flexidea_crm", schema = "")
public class ProjectComments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="project")
    private Project project;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="project_comment")
    private CrmComments comment;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof ProjectComments)) {
	    return false;
	}
	ProjectComments other = (ProjectComments) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.ProjectComments[id=" + id + "]";
    }

    public CrmComments getComment() {
	return comment;
    }

    public void setComment(CrmComments comment) {
	this.comment = comment;
    }

    public Project getProject() {
	return project;
    }

    public void setProject(Project project) {
	this.project = project;
    }

}
