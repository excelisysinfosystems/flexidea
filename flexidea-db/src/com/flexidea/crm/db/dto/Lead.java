package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "lead", catalog = "flexidea_crm", schema = "")
public class Lead implements Serializable {
    @Transient
    private String comment;

    @OneToOne(mappedBy = "lead", fetch=FetchType.LAZY)
    @JoinColumn(name="project", nullable=true)
    private Project project;
    @OneToMany(mappedBy = "lead", fetch=FetchType.LAZY)
    private List<LeadComments> leadComments;
    @OneToMany(mappedBy = "lead", fetch=FetchType.LAZY)
    private List<LeadContacts> leadContacts;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name = "lead_name", nullable = false, length=128)
    private String leadName;

    @Basic(optional = true)
    @Column(name = "is_converted", nullable = true)
    private boolean isConverted;
    @Basic(optional = true)
    @Column(name = "is_dead", nullable = true)
    private boolean isDead;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    public Long getId() {
	return id;
    }

    public Lead setId(Long id) {
	this.id = id;
	return this;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Lead)) {
	    return false;
	}
	Lead other = (Lead) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.Leads[id=" + id + "]";
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public Lead setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
	return this;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public Lead setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public boolean isIsConverted() {
	return isConverted;
    }

    public Lead setIsConverted(boolean isConverted) {
	this.isConverted = isConverted;
	return this;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public Lead setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
	return this;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public Lead setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
	return this;
    }

    public boolean isIsDead() {
	return isDead;
    }

    public void setIsDead(boolean isDead) {
	this.isDead = isDead;
    }

    public String getLeadName() {
	return leadName;
    }

    public Lead setLeadName(String leadName) {
	this.leadName = leadName;
	return this;
    }

    public List<LeadContacts> getLeadContacts() {
	return leadContacts;
    }

    public Lead setLeadContacts(List<LeadContacts> leadContacts) {
	this.leadContacts = leadContacts;
	return this;
    }

    public List<LeadComments> getLeadComments() {
	return leadComments;
    }

    public Lead setLeadComments(List<LeadComments> leadComments) {
	this.leadComments = leadComments;
	return this;
    }

    public Project getProject() {
	return project;
    }

    public Lead setProject(Project project) {
	this.project = project;
	return this;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public int getStatus() {
	if (isConverted && ! isDead)
	    return 3;
	else if(isDead && ! isConverted)
	    return 1;
	else if(isDead && isConverted)
	    return 2;

	return 0;
    }
}
