package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "user_roles", catalog = "flexidea_crm", schema = "")
public class UserRoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="crm_user")
    private CrmUser user;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user_role")
    private AuthRoles role;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt = new Date();
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof UserRoles)) {
	    return false;
	}
	UserRoles other = (UserRoles) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.UserRoles[id=" + id + "]";
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public void setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    public AuthRoles getRole() {
	return role;
    }

    public void setRole(AuthRoles role) {
	this.role = role;
    }

    public CrmUser getUser() {
	return user;
    }

    public void setUser(CrmUser user) {
	this.user = user;
    }
}
