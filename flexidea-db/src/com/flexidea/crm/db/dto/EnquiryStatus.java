package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum EnquiryStatus {
    HOT,
    WARM,
    COLD,
    AWARDED,
    RECEIVED,
    LOST,
    LOST_BY_CONTRACTOR
}
