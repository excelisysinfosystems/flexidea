/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 *
 * @author akshay
 */
@Entity
@Table(name = "associated_contacts", catalog = "flexidea_crm", schema = "")
public class AssociatedContacts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /*
    @OneToOne()
    @JoinColumn(name="parent")
    private Contact parent;
     *
     */

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="company")
    private Company company;

    @ManyToOne()
    @JoinColumn(name="child")
    private Contact child;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof AssociatedContacts)) {
	    return false;
	}
	AssociatedContacts other = (AssociatedContacts) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.AssociatedContacts[id=" + id + "]";
    }

    public Contact getChild() {
	return child;
    }

    public void setChild(Contact child) {
	this.child = child;
    }

    /*
    public Contact getParent() {
	return parent;
    }

    public void setParent(Contact parent) {
	this.parent = parent;
    }
     *
     */

    public Company getCompany() {
	return company;
    }

    public void setCompany(Company company) {
	this.company = company;
    }
}
