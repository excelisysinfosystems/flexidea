package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "enquiry", catalog = "flexidea_crm", schema = "")
public class Enquiry implements Serializable {
    @Transient
    private String comment;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="project")
    private Project project;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional=true)
    @Column(name="enquiry_stage", nullable=true)
    @Enumerated(EnumType.STRING)
    private EnquiryStage stage;
    @Basic(optional=true)
    @Column(name="enquiry_status", nullable=true)
    @Enumerated(EnumType.STRING)
    private EnquiryStatus status;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="contractor")
    private Contact contractor;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="sales_rep")
    private Contact salesRep;

    @Basic(optional = true)
    @Column(name="basic_value", nullable = true, scale=12, precision=2)
    private Double basicValue = 0.0;

    @Basic(optional = true)
    @Column(name="gross_value", nullable = true, scale=12, precision=2)
    private Double grossValue = 0.0;

    @OneToMany(mappedBy = "enquiry", fetch=FetchType.LAZY)
    private List<EnquiryContacts> contacts;

    @OneToMany(mappedBy = "enquiry", fetch=FetchType.LAZY)
    @OrderBy("id ASC")
    private List<EnquiryDocs> enquiryDocs;

    @OneToMany(mappedBy = "enquiry", fetch=FetchType.LAZY)
    private List<EnquiryComments> comments;

    @Basic(optional=true)
    @Column(name="is_dead", nullable=true)
    private boolean dead;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Enquiry)) {
	    return false;
	}
	Enquiry other = (Enquiry) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Enquiry{" + "id=" + id + "stage=" + stage + "status=" + status + "contractor=" + contractor + "salesRep=" + salesRep + "contacts=" + contacts + "enquiryDocs=" + enquiryDocs + "comments=" + comments + '}';
    }

    public List<EnquiryComments> getComments() {
	return comments;
    }

    public void setComments(List<EnquiryComments> comments) {
	this.comments = comments;
    }

    public List<EnquiryContacts> getContacts() {
	return contacts;
    }

    public void setContacts(List<EnquiryContacts> contacts) {
	this.contacts = contacts;
    }

    public Contact getContractor() {
	return contractor;
    }

    public void setContractor(Contact contractor) {
	this.contractor = contractor;
    }

    public List<EnquiryDocs> getEnquiryDocs() {
	return enquiryDocs;
    }

    public void setEnquiryDocs(List<EnquiryDocs> enquiryDocs) {
	this.enquiryDocs = enquiryDocs;
    }

    public Contact getSalesRep() {
	return salesRep;
    }

    public void setSalesRep(Contact salesRep) {
	this.salesRep = salesRep;
    }

    public EnquiryStage getStage() {
	return stage;
    }

    public void setStage(EnquiryStage stage) {
	this.stage = stage;
    }

    public EnquiryStatus getStatus() {
	return status;
    }

    public void setStatus(EnquiryStatus status) {
	this.status = status;
    }

    public Project getProject() {
	return project;
    }

    public void setProject(Project project) {
	this.project = project;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public void setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    public String getEnquiryName() {
	return this.getContractor().getCompany().getCompanyName();
    }

    public boolean isDead() {
	return dead;
    }

    public void setDead(boolean dead) {
	this.dead = dead;
    }

    public Double getBasicValue() {
	return basicValue;
    }

    public void setBasicValue(Double basicValue) {
	this.basicValue = basicValue;
    }

    public Double getGrossValue() {
	return grossValue;
    }

    public void setGrossValue(Double grossValue) {
	this.grossValue = grossValue;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }
}
