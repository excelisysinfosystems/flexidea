/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum EnquiryStage {
    NOT_QUOTED,
    QUOTED,
    APPROVED,
    DISAPPROVED,
    HOLD
}
