package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "crm_comments", catalog = "flexidea_crm", schema = "")
@NamedQueries({
    @NamedQuery(name = "CrmComments.findAll", query = "SELECT c FROM CrmComments c")})
public class CrmComments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "comment_id", nullable = false)
    private Long commentId;

    @Basic(optional = true)
    @Column(name = "associated_with", nullable = true, length=20)
    private String associatedWith;

    @Lob
    @Column(name = "comment_text", length=512)
    private byte[] commentText;
    @Basic(optional = false)
    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;

    @OneToMany(mappedBy = "comment", fetch=FetchType.LAZY)
    private List<Attachments> attachments;

    public CrmComments() {
    }

    public CrmComments(Long commentId) {
	this.commentId = commentId;
    }

    public CrmComments(Long commentId, Date createdAt) {
	this.commentId = commentId;
	this.createdAt = createdAt;
    }

    public Long getCommentId() {
	return commentId;
    }

    public void setCommentId(Long commentId) {
	this.commentId = commentId;
    }

    public byte[] getCommentText() {
	return commentText;
    }

    public void setCommentText(byte[] commentText) {
	this.commentText = commentText;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (commentId != null ? commentId.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof CrmComments)) {
	    return false;
	}
	CrmComments other = (CrmComments) object;
	if ((this.commentId == null && other.commentId != null) || (this.commentId != null && !this.commentId.equals(other.commentId))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.CrmComments[commentId=" + commentId + "]";
    }

    public List<Attachments> getAttachments() {
	return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
	this.attachments = attachments;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

    public String getAssociatedWith() {
	return associatedWith;
    }

    public CrmComments setAssociatedWith(String associatedWith) {
	this.associatedWith = associatedWith;
	return this;
    }
}
