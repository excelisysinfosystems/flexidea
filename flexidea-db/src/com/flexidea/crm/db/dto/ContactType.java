package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "contact_type", catalog = "flexidea_crm", schema = "")
public class ContactType implements Serializable {
    @OneToMany(mappedBy = "contactType", fetch=FetchType.LAZY)
    private List<Contact> contacts;

    @OneToMany(mappedBy = "contactType")
    private List<Company> companies;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Basic(optional = true)
    @Column(name = "type_name", nullable = true, length=128)
    private String typeName;
    @Basic(optional = true)
    @Column(name = "is_internal", nullable = true)
    private boolean internal;
    @Basic(optional = true)
    @Column(name = "is_for_contact", nullable = true)
    private boolean forContact = true;
    @Basic(optional = true)
    @Column(name = "is_for_company", nullable = true)
    private boolean forCompany = true;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof ContactType)) {
	    return false;
	}
	ContactType other = (ContactType) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return this.getTypeName();
    }

    public boolean isInternal() {
	return internal;
    }

    public void setInternal(boolean internal) {
	this.internal = internal;
    }

    public String getTypeName() {
	return typeName;
    }

    public void setTypeName(String typeName) {
	this.typeName = typeName;
    }

    public List<Company> getCompanies() {
	return companies;
    }

    public void setCompanies(List<Company> companies) {
	this.companies = companies;
    }

    public List<Contact> getContacts() {
	return contacts;
    }

    public void setContacts(List<Contact> contacts) {
	this.contacts = contacts;
    }

    public boolean isForCompany() {
	return forCompany;
    }

    public void setForCompany(boolean forCompany) {
	this.forCompany = forCompany;
    }

    public boolean isForContact() {
	return forContact;
    }

    public void setForContact(boolean forContact) {
	this.forContact = forContact;
    }
}
