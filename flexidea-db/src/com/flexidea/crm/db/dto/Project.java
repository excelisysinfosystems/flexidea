package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "project", catalog = "flexidea_crm", schema = "")
public class Project implements Serializable {
    @Transient
    private String comment;

    @Transient
    private int status = 0;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name = "project_name", nullable = false, length=128)
    private String projectName;

    @Basic(optional = true)
    @Column(name = "is_dead", nullable = true)
    private boolean isDead;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="lead", nullable=true)
    private Lead lead;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="project")
    private List<ProjectComments> projectComments;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="project")
    private List<ProjectContacts> projectContacts;

    @OneToMany(mappedBy = "project", fetch=FetchType.LAZY)
    private List<Enquiry> enquiries;

    public Long getId() {
	return id;
    }

    public Project setId(Long id) {
	this.id = id;
	return this;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Project)) {
	    return false;
	}
	Project other = (Project) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.Project[id=" + id + "]";
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public Project setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
	return this;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public Project setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public boolean isIsDead() {
	return isDead;
    }

    public Project setIsDead(boolean isDead) {
	this.isDead = isDead;
	return this;
    }

    public Lead getLead() {
	return lead;
    }

    public Project setLead(Lead lead) {
	this.lead = lead;
	return this;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public Project setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
	return this;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public Project setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
	return this;
    }

    public String getProjectName() {
	return projectName;
    }

    public Project setProjectName(String projectName) {
	this.projectName = projectName;
	return this;
    }

    public List<ProjectComments> getProjectComments() {
	return projectComments;
    }

    public Project setProjectComments(List<ProjectComments> projectComments) {
	this.projectComments = projectComments;
	return this;
    }

    public List<ProjectContacts> getProjectContacts() {
	return projectContacts;
    }

    public Project setProjectContacts(List<ProjectContacts> projectContacts) {
	this.projectContacts = projectContacts;
	return this;
    }

    public List<Enquiry> getEnquiries() {
	return enquiries;
    }

    public void setEnquiries(List<Enquiry> enquiries) {
	this.enquiries = enquiries;
    }

    public int getStatus() {
	return status;
    }

    public void setStatus(int status) {
	this.status = status;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }
}
