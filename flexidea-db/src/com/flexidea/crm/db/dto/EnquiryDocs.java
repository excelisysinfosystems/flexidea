/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "enquiry_docs", catalog = "flexidea_crm", schema = "")
public class EnquiryDocs implements Serializable {
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="enquiry")
    private Enquiry enquiry;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional=false)
    @Column(name="revesion_no")
    private Integer revesionNo;

    @OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.REMOVE, mappedBy="enquiry")
    private List<Attachments> attachments;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by", nullable=true)
    private CrmUser createdBy;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof EnquiryDocs)) {
	    return false;
	}
	EnquiryDocs other = (EnquiryDocs) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "id=" + this.getId() + ", revision=" + this.getRevesionNo();
    }

    public List<Attachments> getAttachments() {
	return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
	this.attachments = attachments;
    }

    public Enquiry getEnquiry() {
	return enquiry;
    }

    public void setEnquiry(Enquiry enquiry) {
	this.enquiry = enquiry;
    }

    public Integer getRevesionNo() {
	return revesionNo;
    }

    public void setRevesionNo(Integer revesionNo) {
	this.revesionNo = revesionNo;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

}
