/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import com.flexidea.crm.db.dao.TasksDAO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "tasks", catalog = "flexidea_crm", schema = "")
public class Tasks implements Serializable {
    @OneToMany(mappedBy = "task", fetch=FetchType.LAZY)
    private List<TaskComments> comments;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
	return id;
    }

    public Tasks setId(Long id) {
	this.id = id;
	return this;
    }

    @Basic(optional = false)
    @Column(name = "title", nullable = false, length=256)
    private String title;
    @Basic(optional = false, fetch=FetchType.LAZY)
    @Column(name = "description", nullable = false, length=1024)
    private String description;
    @Basic(optional = true)
    @Column(name = "start_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date start;
    @Basic(optional = true)
    @Column(name = "end_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date end;
    @Basic(optional = false)
    @Column(name = "priority", nullable = false)
    private Integer priority;
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private TaskStatus status;
    @Basic(optional = true)
    @Column(name = "source", nullable = true)
    private String source;
    @Basic(optional = true)
    @Column(name = "is_active", nullable = true)
    private boolean active;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="assigned_by", nullable=true)
    private CrmUser assignedBy;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="owner")
    private CrmUser owner;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="delegated_to", nullable=true)
    private CrmUser delegatedTo;

    @OneToOne(optional=true, fetch=FetchType.LAZY)
    @JoinColumn(name="target_contact", nullable=true)
    private Contact targetContact;
    @OneToOne(optional=true, fetch=FetchType.LAZY)
    @JoinColumn(name="target_lead", nullable=true)
    private Lead targetLead;
    @OneToOne(optional=true, fetch=FetchType.LAZY)
    @JoinColumn(name="target_project", nullable=true)
    private Project targetProject;
    @OneToOne(optional=true, fetch=FetchType.LAZY)
    @JoinColumn(name="target_enquiry", nullable=true)
    private Enquiry targetEnquiry;

    @OneToMany(mappedBy = "task")
    private List<TaskReminders> reminders;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Tasks)) {
	    return false;
	}
	Tasks other = (Tasks) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.Tasks[id=" + id + "]";
    }

    public CrmUser getAssignedBy() {
	return assignedBy;
    }

    public Tasks setAssignedBy(CrmUser assigned_by) {
	this.assignedBy = assigned_by;
	return this;
    }

    public List<TaskComments> getComments() {
	return comments;
    }

    public Tasks setComments(List<TaskComments> comments) {
	this.comments = comments;
	return this;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public Tasks setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
	return this;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public Tasks setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public CrmUser getDelegatedTo() {
	return delegatedTo;
    }

    public Tasks setDelegatedTo(CrmUser delegatedTo) {
	this.delegatedTo = delegatedTo;
	return this;
    }

    public String getDescription() {
	return description;
    }

    public Tasks setDescription(String description) {
	this.description = description;
	return this;
    }

    public Date getEnd() {
	return end;
    }

    public Tasks setEnd(Date end) {
	this.end = end;
	return this;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public Tasks setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
	return this;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public Tasks setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
	return this;
    }

    public CrmUser getOwner() {
	return owner;
    }

    public Tasks setOwner(CrmUser owner) {
	this.owner = owner;
	return this;
    }

    public Integer getPriority() {
	return priority;
    }

    public Tasks setPriority(Integer priority) {
	this.priority = priority;
	return this;
    }

    public List<TaskReminders> getReminders() {
	return reminders;
    }

    public Tasks setReminders(List<TaskReminders> reminders) {
	this.reminders = reminders;
	return this;
    }

    public String getSource() {
	return source;
    }

    public Tasks setSource(String source) {
	this.source = source;
	return this;
    }

    public Date getStart() {
	return start;
    }

    public Tasks setStart(Date start) {
	this.start = start;
	return this;
    }

    public TaskStatus getStatus() {
	return status;
    }

    public Tasks setStatus(TaskStatus status) {
	this.status = status;
	return this;
    }

    public String getTitle() {
	return title;
    }

    public Tasks setTitle(String title) {
	this.title = title;
	return this;
    }

    public boolean isActive() {
	return active;
    }

    public Tasks setActive(boolean active) {
	this.active = active;
	return this;
    }

    public Contact getTargetContact() {
	return targetContact;
    }

    public void setTargetContact(Contact targetContact) {
	this.targetContact = targetContact;
    }

    public Enquiry getTargetEnquiry() {
	return targetEnquiry;
    }

    public void setTargetEnquiry(Enquiry targetEnquiry) {
	this.targetEnquiry = targetEnquiry;
    }

    public Lead getTargetLead() {
	return targetLead;
    }

    public void setTargetLead(Lead targetLead) {
	this.targetLead = targetLead;
    }

    public Project getTargetProject() {
	return targetProject;
    }

    public void setTargetProject(Project targetProject) {
	this.targetProject = targetProject;
    }

    public String getStatusString() {
	String status = null;
	Tasks t = this;
	long nDays = 0;
	switch (t.getStatus()) {
	    case OPEN:
		nDays = TasksDAO.getDaysToGo(t);
		if (nDays > 0) {
		    status = nDays + " Days to go.";
		} else if (nDays < 0) {
		    status = (-nDays) + " days overdue";
		} else {
		    status = "Needs to be done Today";
		}
		break;
	    case REJECTED:
		status = "Rejected";
		break;
	    case COMPLETE:
		nDays = TasksDAO.getCompletionDays(t);
		if (nDays > 0) {
		    status = "Finished after " + nDays + " Days.";
		} else if (nDays < 0) {
		    status = "Finished " + (-nDays) + " days earlier";
		} else {
		    status = "Finished on time";
		}
		break;
	}
	return status;
    }

    public Integer getCommentsCount() {
	return this.getComments().size();
    }

    public String getCreatorName() {
	return this.getAssignedBy().getFullName();
    }
}
