package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "task_reminders", catalog = "flexidea_crm", schema = "")
public class TaskReminders implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
	return id;
    }

    public TaskReminders setId(Long id) {
	this.id = id;
	return this;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="associated_task")
    private Tasks task;

    @Basic(optional = false)
    @Column(name = "reminder_type", nullable = false)
    private TaskReminderType reminderType;
    @Basic(optional = false)
    @Column(name = "duration_type", nullable = false)
    private DurationMeasure durationMeasure;
    @Basic(optional = false)
    @Column(name = "duration", nullable = false)
    private Integer duration;
    @Basic(optional = false)
    @Column(name = "is_active", nullable = false)
    private boolean active;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof TaskReminders)) {
	    return false;
	}
	TaskReminders other = (TaskReminders) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.TaskReminders[id=" + id + "]";
    }

    public boolean isActive() {
	return active;
    }

    public TaskReminders setActive(boolean active) {
	this.active = active;
	return this;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public TaskReminders setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
	return this;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public TaskReminders setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public Integer getDuration() {
	return duration;
    }

    public TaskReminders setDuration(Integer duration) {
	this.duration = duration;
	return this;
    }

    public DurationMeasure getDurationMeasure() {
	return durationMeasure;
    }

    public TaskReminders setDurationMeasure(DurationMeasure durationMeasure) {
	this.durationMeasure = durationMeasure;
	return this;
    }

    public TaskReminderType getReminderType() {
	return reminderType;
    }

    public TaskReminders setReminderType(TaskReminderType reminderType) {
	this.reminderType = reminderType;
	return this;
    }

    public Tasks getTask() {
	return task;
    }

    public TaskReminders setTask(Tasks task) {
	this.task = task;
	return this;
    }

}
