package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "reports_schedule", catalog = "flexidea_crm", schema = "")
public class ReportsSchedule implements Serializable {
    @Transient
    private int index;

    public static enum ScheduleDuration {
	Hourly,
	Daily,
	Weekly,
	Monthly
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "report_model")
    private ReportModel model;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name="start_date", nullable = false, length = 128)
    private Date startDate = new Date();

    @Basic(optional = false)
    @Column(name="report_interval", nullable = false)
    private Integer interval = 1;

    @Enumerated(EnumType.STRING)
    @Column(name="duration_measure", nullable = false)
    private ScheduleDuration duration;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="send_to")
    private Contact to;

    @Basic(optional = false)
    @Column(name="is_active", nullable = false)
    private boolean active = true;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof ReportsSchedule)) {
	    return false;
	}
	ReportsSchedule other = (ReportsSchedule) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.ReportsSchedule[id=" + id + "]";
    }

    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    public ScheduleDuration getDuration() {
	return duration;
    }

    public void setDuration(ScheduleDuration duration) {
	this.duration = duration;
    }

    public Integer getInterval() {
	return interval;
    }

    public void setInterval(Integer interval) {
	this.interval = interval;
    }

    public ReportModel getModel() {
	return model;
    }

    public void setModel(ReportModel model) {
	this.model = model;
    }

    public Date getStartDate() {
	return startDate;
    }

    public void setStartDate(Date startDate) {
	this.startDate = startDate;
    }

    public Contact getTo() {
	return to;
    }

    public void setTo(Contact to) {
	this.to = to;
    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }
}
