package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum Operations {
    NEW,
    EDIT,
    DELETE,
    COMMENT,
    VIEW,
    REPORT,
    ASSOCIATE_CONTACT,
    DISSOCIATE_CONTACT,
    FOLLOW_UP
}
