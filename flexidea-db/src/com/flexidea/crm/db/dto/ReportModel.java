package com.flexidea.crm.db.dto;

import com.flexidea.crm.db.dao.ContactsDAO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "saved_reports", catalog = "flexidea_crm", schema = "")
public class ReportModel implements Serializable {
    @OneToMany(mappedBy = "model", fetch=FetchType.LAZY)
    private List<ReportsSchedule> reportsSchedule;

    public static enum Level1Group {
	Contractor,
	Sales_Representative,
	Consultant,
	Branch,
	Project,
	Job_Status;
    }

    public static enum Level2Group {
	_10L,
	_25L,
	_50L,
	_100L
    }

    public static enum Level3Group {
	Weekly,
	Monthly,
	Quarterly,
	Annual
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name="report_name", nullable=false, length=255)
    private String reportName;

    @Enumerated(EnumType.STRING)
    @Column(name="level1_group", nullable=true, length=64)
    private Level1Group level1Group;

    @Enumerated(EnumType.STRING)
    @Column(name="level2_group", nullable=true, length=64)
    private Level2Group level2Group;

    @Enumerated(EnumType.STRING)
    @Column(name="level3_group", nullable=true, length=64)
    private Level3Group level3Group;

    @Basic(optional=true)
    @Column(name="project_summary_report", nullable=true)
    private boolean projectSummaryReport = true;

    @Basic(optional=true)
    @Column(name="filter_by_contact_type", nullable=true)
    private boolean filterByContactType = false;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="filter_contact_type", nullable=true)
    private ContactType filterContactType = ContactsDAO
	.getContactType("Contractor");

    @Basic(optional=true)
    @Column(name="filter_by_job_status", nullable=true)
    private boolean filterByJobStatus = false;

    @Enumerated(EnumType.STRING)
    @Column(name="filter_job_status", nullable=true, length=20)
    private EnquiryStatus filterJobStatus;

    @Basic(optional = true)
    @Column(name="filter_by_value")
    private Boolean filterByValue = false;
    @Basic(optional = true)
    @Column(name="filter_value_1")
    private Double filterValue1 = 0.00;
    @Basic(optional = true)
    @Column(name="filter_value_2")
    private Double filterValue2 = 0.00;
    @Basic(optional = true)
    @Column(name="filter_value_operator", length=20)
    private String filterValueOperator = null;

    @Basic(optional=true)
    @Column(name="filter_by_branch", nullable=true)
    private boolean filterByBranch = false;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="filter_branch", nullable=true)
    private City filterBranch;
    @Basic(optional=true)
    @Column(name="filter_by_project", nullable=true)
    private boolean filterByProject = false;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="filter_project", nullable=true)
    private Project filterProject;
    @Basic(optional=true)
    @Column(name="filter_by_company", nullable=true)
    private boolean filterByCompany = false;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="filter_company", nullable=true)
    private Company filterCompany;

    @Basic(optional=true)
    @Column(name="filter_by_contractor", nullable=true)
    private boolean filterByContractor = false;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="filter_contractor", nullable=true)
    private Company filterContractor;
    @Basic(optional=true)
    @Column(name="filter_by_sales_rep", nullable=true)
    private boolean filterBySalesRep = false;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="filter_sales_rep", nullable=true)
    private Contact filterSalesRep;
    @Basic(optional=true)
    @Column(name="filter_by_consultant", nullable=true)
    private boolean filterByConsultant = false;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="filter_consultant", nullable=true)
    private Company filterConsultant;

    @Basic(optional=true)
    @Column(name="filter_by_date_range", nullable=true)
    private boolean filterByDateRange = false;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name="filter_start_date", nullable=true)
    private Date filterStartDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name="filter_end_date", nullable=true)
    private Date filterEndDate;

    @Transient
    private String filtersDD = null;

    public City getFilterBranch() {
	return filterBranch;
    }

    public void setFilterBranch(City filterBranch) {
	this.filterBranch = filterBranch;
    }

    public Boolean getFilterByValue() {
	return filterByValue;
    }

    public void setFilterByValue(Boolean filterByValue) {
	this.filterByValue = filterByValue;
    }

    public Company getFilterCompany() {
	return filterCompany;
    }

    public void setFilterCompany(Company filterCompany) {
	this.filterCompany = filterCompany;
    }

    public ContactType getFilterContactType() {
	return filterContactType;
    }

    public void setFilterContactType(ContactType filterContactType) {
	this.filterContactType = filterContactType;
    }

    public EnquiryStatus getFilterJobStatus() {
	return filterJobStatus;
    }

    public void setFilterJobStatus(EnquiryStatus filterJobStatus) {
	this.filterJobStatus = filterJobStatus;
    }

    public Project getFilterProject() {
	return filterProject;
    }

    public void setFilterProject(Project filterProject) {
	this.filterProject = filterProject;
    }

    public Double getFilterValue2() {
	return filterValue2;
    }

    public void setFilterValue2(Double filterValue2) {
	this.filterValue2 = filterValue2;
    }

    public String getFilterValueOperator() {
	return filterValueOperator;
    }

    public void setFilterValueOperator(String filterValueOperator) {
	this.filterValueOperator = filterValueOperator;
    }

    public Double getFilterValue1() {
	return filterValue1;
    }

    public void setFilterValue1(Double filterValue1) {
	this.filterValue1 = filterValue1;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Level1Group getLevel1Group() {
	return level1Group;
    }

    public void setLevel1Group(Level1Group level1Group) {
	this.level1Group = level1Group;
    }

    public Level2Group getLevel2Group() {
	return level2Group;
    }

    public void setLevel2Group(Level2Group level2Group) {
	this.level2Group = level2Group;
    }

    public Level3Group getLevel3Group() {
	return level3Group;
    }

    public void setLevel3Group(Level3Group level3Group) {
	this.level3Group = level3Group;
    }

    public String getReportName() {
	return reportName;
    }

    public void setReportName(String reportName) {
	this.reportName = reportName;
    }

    public boolean isFilterByBranch() {
	return filterByBranch;
    }

    public void setFilterByBranch(boolean filterByBranch) {
	this.filterByBranch = filterByBranch;
    }

    public boolean isFilterByCompany() {
	return filterByCompany;
    }

    public void setFilterByCompany(boolean filterByCompany) {
	this.filterByCompany = filterByCompany;
    }

    public boolean isFilterByContactType() {
	return filterByContactType;
    }

    public void setFilterByContactType(boolean filterByContactType) {
	this.filterByContactType = filterByContactType;
    }

    public boolean isFilterByJobStatus() {
	return filterByJobStatus;
    }

    public void setFilterByJobStatus(boolean filterByJobStatus) {
	this.filterByJobStatus = filterByJobStatus;
    }

    public boolean isFilterByProject() {
	return filterByProject;
    }

    public void setFilterByProject(boolean filterByProject) {
	this.filterByProject = filterByProject;
    }

    public Date getFilterEndDate() {
	return filterEndDate;
    }

    public void setFilterEndDate(Date filterEndDate) {
	this.filterEndDate = filterEndDate;
    }

    public Contact getFilterSalesRep() {
	return filterSalesRep;
    }

    public void setFilterSalesRep(Contact filterSalesRep) {
	this.filterSalesRep = filterSalesRep;
    }

    public Date getFilterStartDate() {
	return filterStartDate;
    }

    public void setFilterStartDate(Date filterStartDate) {
	this.filterStartDate = filterStartDate;
    }


    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final ReportModel other = (ReportModel) obj;
	if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public int hashCode() {
	int hash = 7;
	hash = 61 * hash + (this.id != null ? this.id.hashCode() : 0);
	return hash;
    }

    public boolean isFilterByConsultant() {
	return filterByConsultant;
    }

    public void setFilterByConsultant(boolean filterByConsultant) {
	this.filterByConsultant = filterByConsultant;
    }

    public boolean isFilterByContractor() {
	return filterByContractor;
    }

    public void setFilterByContractor(boolean filterByContractor) {
	this.filterByContractor = filterByContractor;
    }

    public boolean isFilterByDateRange() {
	return filterByDateRange;
    }

    public void setFilterByDateRange(boolean filterByDateRange) {
	this.filterByDateRange = filterByDateRange;
    }

    public boolean isFilterBySalesRep() {
	return filterBySalesRep;
    }

    public void setFilterBySalesRep(boolean filterBySalesRep) {
	this.filterBySalesRep = filterBySalesRep;
    }

    public Company getFilterConsultant() {
	return filterConsultant;
    }

    public void setFilterConsultant(Company filterConsultant) {
	this.filterConsultant = filterConsultant;
    }

    public Company getFilterContractor() {
	return filterContractor;
    }

    public void setFilterContractor(Company filterContractor) {
	this.filterContractor = filterContractor;
    }

    public boolean isProjectSummaryReport() {
	return projectSummaryReport;
    }

    public void setProjectSummaryReport(boolean projectSummaryReport) {
	this.projectSummaryReport = projectSummaryReport;
    }

    public String getFiltersDD() {
	return filtersDD;
    }

    public void setFiltersDD(String filtersDD) {
	this.filtersDD = filtersDD;
    }

    public List<ReportsSchedule> getReportsSchedule() {
	return reportsSchedule;
    }

    public void setReportsSchedule(List<ReportsSchedule> reportsSchedule) {
	this.reportsSchedule = reportsSchedule;
    }
}