package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "contact", catalog = "flexidea_crm", schema = "")
public class Contact implements Serializable {
    // TODO: REMOVE
    //@OneToMany(mappedBy = "parent", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
    //private List<AssociatedContacts> associatedContacts;

    /*
    @OneToMany(mappedBy = "contact", fetch=FetchType.LAZY)
    private List<ContactComments> contactComments;
     *
     */

    @Transient
    private String comment;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = true)
    @Column(name = "first_name", nullable = true, length=128)
    private String firstName;
    @Basic(optional = true)
    @Column(name = "last_name", nullable = true, length=128)
    private String lastName;
    @Basic(optional = true)
    @Column(name = "job_title", nullable = true, length=128)
    private String jobTitle;
    @Basic(optional = true)
    @Column(name = "address", nullable = true, length=512)
    private String address;
    @Basic(optional = true)
    @Column(name = "phone", nullable = true, length=20)
    private String phone;
    @Basic(optional = true)
    @Column(name = "cell", nullable = true, length=20)
    private String cell;
    @Basic(optional = true)
    @Column(name = "email", nullable = true, length=255)
    private String email;

    @Basic(optional = true)
    @Column(name = "dob", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dob;

    @Basic(optional = true)
    @Column(name = "doa", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date doa;

    @Basic(optional = true)
    @Column(name = "gift", nullable = true, length=255)
    private String gift;

    @Basic(optional = true)
    @Column(name = "credit_terms", nullable = true, length=255)
    private String creditTerms;

    @Basic(optional = true)
    @Column(name = "general_note", nullable = true, length=512)
    private String generalNote;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="company")
    private Company company;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="contact_type")
    private ContactType contactType;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="city")
    private City city;

    @Basic(optional = true)
    @Column(name = "is_deleted", nullable = true)
    private boolean isDeleted;

    @OneToOne(mappedBy="contact", fetch=FetchType.EAGER, optional=true)
    private CrmUser user;

    public Long getId() {
	return id;
    }

    public Contact setId(Long id) {
	this.id = id;
	return this;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Contact)) {
	    return false;
	}
	Contact other = (Contact) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return this.getFirstName() + " " + this.getLastName();
    }

    public String getContactName() {
	return this.getFirstName() + " " + this.getLastName();
    }

    public String getAddress() {
	return address;
    }

    public Contact setAddress(String address) {
	this.address = address;
	return this;
    }

    public String getCell() {
	return cell;
    }

    public Contact setCell(String cell) {
	this.cell = cell;
	return this;
    }

    public Company getCompany() {
	return company;
    }

    public Contact setCompany(Company company) {
	this.company = company;
	return this;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public Contact setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
	return this;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public Contact setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public String getCreditTerms() {
	return creditTerms;
    }

    public Contact setCreditTerms(String credit_terms) {
	this.creditTerms = credit_terms;
	return this;
    }

    public Date getDoa() {
	return doa;
    }

    public Contact setDoa(Date doa) {
	this.doa = doa;
	return this;
    }

    public Date getDob() {
	return dob;
    }

    public Contact setDob(Date dob) {
	this.dob = dob;
	return this;
    }

    public String getEmail() {
	return email;
    }

    public Contact setEmail(String email) {
	this.email = email;
	return this;
    }

    public String getFirstName() {
	return firstName;
    }

    public Contact setFirstName(String firstName) {
	this.firstName = firstName;
	return this;
    }

    public String getGeneralNote() {
	return generalNote;
    }

    public Contact setGeneralNote(String generalNote) {
	this.generalNote = generalNote;
	return this;
    }

    public String getGift() {
	return gift;
    }

    public Contact setGift(String gift) {
	this.gift = gift;
	return this;
    }

    public String getJobTitle() {
	return jobTitle;
    }

    public Contact setJobTitle(String jobTitle) {
	this.jobTitle = jobTitle;
	return this;
    }

    public String getLastName() {
	return lastName;
    }

    public Contact setLastName(String lastName) {
	this.lastName = lastName;
	return this;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public Contact setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
	return this;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public Contact setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
	return this;
    }

    public String getPhone() {
	return phone;
    }

    public Contact setPhone(String phone) {
	this.phone = phone;
	return this;
    }

    /*
    public List<ContactComments> getContactComments() {
	return contactComments;
    }

    public Contact setContactComments(List<ContactComments> contactComments) {
	this.contactComments = contactComments;
	return this;
    }
     *
     */

    public ContactType getContactType() {
	return contactType;
    }

    public Contact setContactType(ContactType contactType) {
	this.contactType = contactType;
	return this;
    }

    public City getCity() {
	return city;
    }

    public Contact setCity(City city) {
	this.city = city;
	return this;
    }

    public boolean isIsDeleted() {
	return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
	this.isDeleted = isDeleted;
    }

    public CrmUser getUser() {
	return user;
    }

    public void setUser(CrmUser user) {
	this.user = user;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getFullName() {
	return firstName + " " + lastName;
    }
}
