/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "company", catalog = "flexidea_crm", schema = "")
public class Company implements Serializable {
    @OneToMany(mappedBy = "company", fetch=FetchType.LAZY,
    cascade=CascadeType.REMOVE)
    private List<ContactComments> comments;

    @OneToMany(mappedBy = "company", fetch=FetchType.LAZY,
	cascade=CascadeType.REMOVE)
    private List<AssociatedContacts> associatedContacts;

    @OneToMany(mappedBy = "company", fetch=FetchType.LAZY)
    private List<Contact> contacts;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name = "company_name", nullable = false, length=255, unique=true)
    private String companyName;

    @Basic(optional = false)
    @Column(name = "industry", nullable = false, length=255)
    private String industry;

    @Basic(optional = true)
    @Column(name = "address", nullable = true, length=512)
    private String companyAddress;

    @Basic(optional = true)
    @Column(name = "phone", nullable = true, length=20)
    private String phone;

    @Basic(optional = true)
    @Column(name = "phone1", nullable = true, length=20)
    private String phone1;

    @Basic(optional = true)
    @Column(name = "fax", nullable = true, length=20)
    private String fax;

    @Basic(optional = true)
    @Column(name = "email", nullable = true, length=255)
    private String email;

    @Basic(optional = true)
    @Column(name = "website", nullable = true, length=255)
    private String website;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="contact_type")
    private ContactType contactType;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="city")
    private City city;

    public Long getId() {
	return id;
    }

    public Company setId(Long id) {
	this.id = id;
	return this;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Company)) {
	    return false;
	}
	Company other = (Company) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	//return "com.flexidea.crm.db.dto.Company[id=" + id + "]";
	return this.companyName;
    }

    public String getCompanyAddress() {
	return companyAddress;
    }

    public Company setCompanyAddress(String companyAddress) {
	this.companyAddress = companyAddress;
	return this;
    }

    public String getCompanyName() {
	return companyName;
    }

    public Company setCompanyName(String companyName) {
	this.companyName = companyName;
	return this;
    }

    public List<Contact> getContacts() {
	return contacts;
    }

    public Company setContacts(List<Contact> contacts) {
	this.contacts = contacts;
	return this;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public Company setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
	return this;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public Company setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public String getEmail() {
	return email;
    }

    public Company setEmail(String email) {
	this.email = email;
	return this;
    }

    public String getFax() {
	return fax;
    }

    public Company setFax(String fax) {
	this.fax = fax;
	return this;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public Company setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
	return this;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public Company setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
	return this;
    }

    public String getPhone() {
	return phone;
    }

    public Company setPhone(String phone) {
	this.phone = phone;
	return this;
    }

    public String getPhone1() {
	return phone1;
    }

    public Company setPhone1(String phone1) {
	this.phone1 = phone1;
	return this;
    }

    public String getWebsite() {
	return website;
    }

    public Company setWebsite(String website) {
	this.website = website;
	return this;
    }

    public ContactType getContactType() {
	return contactType;
    }

    public Company setContactType(ContactType contactType) {
	this.contactType = contactType;
	return this;
    }

    public City getCity() {
	return city;
    }

    public Company setCity(City city) {
	this.city = city;
	return this;
    }

    public List<AssociatedContacts> getAssociatedContacts() {
	return associatedContacts;
    }

    public void setAssociatedContacts(List<AssociatedContacts> associatedContacts) {
	this.associatedContacts = associatedContacts;
    }

    public List<ContactComments> getComments() {
	return comments;
    }

    public void setComments(List<ContactComments> comments) {
	this.comments = comments;
    }

    public String getIndustry() {
	return industry;
    }

    public Company setIndustry(String industry) {
	this.industry = industry;
	return this;
    }

}
