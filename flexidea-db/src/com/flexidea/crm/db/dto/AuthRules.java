package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "auth_roles", catalog = "flexidea_crm", schema = "")
public class AuthRules implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="auth_role")
    private AuthRoles role;

    @Enumerated(EnumType.STRING)
    @Column(name = "auth_principle", nullable = false)
    private Principles principle;
    @Enumerated(EnumType.STRING)
    @Column(name = "auth_operation", nullable = false)
    private Operations operation;
    @Enumerated(EnumType.STRING)
    @Column(name = "auth_action", nullable = true)
    private AuthAction action = AuthAction.DENY;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt = new Date();
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof AuthRules)) {
	    return false;
	}
	AuthRules other = (AuthRules) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.AuthRules[id=" + id + "]";
    }

    public AuthAction getAction() {
	return action;
    }

    public void setAction(AuthAction action) {
	this.action = action;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public void setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    public Operations getOperation() {
	return operation;
    }

    public void setOperation(Operations operation) {
	this.operation = operation;
    }

    public Principles getPrinciple() {
	return principle;
    }

    public void setPrinciple(Principles principle) {
	this.principle = principle;
    }

    public AuthRoles getRole() {
	return role;
    }

    public void setRole(AuthRoles role) {
	this.role = role;
    }
}
