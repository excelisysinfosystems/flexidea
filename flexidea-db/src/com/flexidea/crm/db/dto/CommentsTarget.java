/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum CommentsTarget {
    CONTACTS,
    LEADS,
    ENQUIRY,
    PROJECT,
    TASK
}
