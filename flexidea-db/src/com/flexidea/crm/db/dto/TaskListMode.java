/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author akshay
 */
public enum TaskListMode {
    //ALL,
    TODAY,
    THIS_WEEK,
    THIS_MONTH,
    AFTER_THAT;

    public static Map<TaskListMode, String> displayValue =
	new  HashMap<TaskListMode, String>();

    static {
	//displayValue.put(ALL, "All");
	displayValue.put(TODAY, "Today");
	displayValue.put(THIS_MONTH, "This Month");
	displayValue.put(THIS_WEEK, "This Week");
	displayValue.put(AFTER_THAT, "After That");
    }
}
