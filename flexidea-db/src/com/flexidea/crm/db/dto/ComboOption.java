package com.flexidea.crm.db.dto;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "combooption")
@Table(name = "combo_options", catalog = "flexidea_crm", schema = "")
public class ComboOption implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long comboOptionId;

	@Basic(optional = false)
	@Column(name = "comboOptionName", nullable = false)
	private String comboOptionName;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "comboId")
	private Combo combo;

	public long getComboOptionId() {
		return comboOptionId;
	}

	public void setComboOptionId(long comboOptionId) {
		this.comboOptionId = comboOptionId;
	}

	public String getComboOptionName() {
		return comboOptionName;
	}

	public void setComboOptionName(String comboOptionName) {
		this.comboOptionName = comboOptionName;
	}

	public Combo getCombo() {
		return combo;
	}

	public void setCombo(Combo combo) {
		this.combo = combo;
	}

}
