/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

/**
 *
 * @author akshay
 */
public enum ListMode {
    ALPAHABATICAL,
    RECENT,
    COMPANY_WISE,
    INDUSTRY_WISE,
    SUPPLIER_WISE,
    CONSULTANT_WISE,
    CITY_WISE,

    SALES_REP_WISE,
    CONVERTED,
    Q1, Q2, Q3, Q4,
    ID
}