/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "enquiry_comments", catalog = "flexidea_crm", schema = "")
public class EnquiryComments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="enquiry")
    private Enquiry enquiry;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="enquiry_comment")
    private CrmComments comment;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof EnquiryComments)) {
	    return false;
	}
	EnquiryComments other = (EnquiryComments) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.EnquiryComments[id=" + id + "]";
    }

    public CrmComments getComment() {
	return comment;
    }

    public void setComment(CrmComments comment) {
	this.comment = comment;
    }

    public Enquiry getEnquiry() {
	return enquiry;
    }

    public void setEnquiry(Enquiry enquiry) {
	this.enquiry = enquiry;
    }

}
