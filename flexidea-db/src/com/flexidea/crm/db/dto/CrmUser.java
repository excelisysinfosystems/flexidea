package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "crm_user", catalog = "flexidea_crm", schema = "")
@NamedQueries({
    @NamedQuery(name = "CrmUser.findAll", query = "SELECT c FROM CrmUser c")})
public class CrmUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Basic(optional = false)
    @Column(name = "username", nullable = false, length = 20)
    private String username;
    @Basic(optional = false)
    @Column(name = "password", nullable = false, length = 32)
    private String password;
    @Basic(optional = false)
    @Column(name = "user_role", nullable = false, length = 20)
    private String userRole;

    @Basic(optional = false)
    @Column(name = "full_name", nullable = false, length = 128)
    private String fullName;

    @OneToOne(optional=true, fetch=FetchType.LAZY)
    @JoinColumn(name="contact", nullable=true)
    private Contact contact;

    @OneToMany(mappedBy = "user", fetch=FetchType.LAZY)
    private List<UserRoles> userRoles;

    public CrmUser() {
    }

    public CrmUser(Long userId) {
	this.userId = userId;
    }

    public CrmUser(Long userId, String username, String password, String userRole) {
	this.userId = userId;
	this.username = username;
	this.password = password;
	this.userRole = userRole;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getUserRole() {
	return userRole;
    }

    public void setUserRole(String userRole) {
	this.userRole = userRole;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (userId != null ? userId.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof CrmUser)) {
	    return false;
	}
	CrmUser other = (CrmUser) object;
	if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.CrmUser[userId=" + userId + "]";
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public Contact getContact() {
	return contact;
    }

    public void setContact(Contact contact) {
	this.contact = contact;
    }

    public List<UserRoles> getUserRoles() {
	return userRoles;
    }

    public void setUserRoles(List<UserRoles> userRoles) {
	this.userRoles = userRoles;
    }
}
