package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "combo", catalog = "flexidea_crm", schema = "")
public class Combo implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long comboId;

	@Basic(optional = false)
	@Column(name = "comboName", nullable = false)
	private String comboName;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "combo")
	private List<ComboOption> comboOptions;

	public long getComboId() {
		return comboId;
	}

	public void setComboId(long comboId) {
		this.comboId = comboId;
	}

	public String getComboName() {
		return comboName;
	}

	public void setComboName(String comboName) {
		this.comboName = comboName;
	}

	public String toString() {
		return comboName.toString();
	}

	public List<ComboOption> getComboOptions() {
		return comboOptions;
	}

	public void setComboOptions(List<ComboOption> comboOptions) {
		this.comboOptions = comboOptions;
	}

	
}
