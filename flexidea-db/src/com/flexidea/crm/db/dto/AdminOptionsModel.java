package com.flexidea.crm.db.dto;

import java.io.Serializable;

public final class AdminOptionsModel implements Serializable {

	private Combo comboName;
	private ComboOption comboOptions;

	public Combo getComboName() {
		return comboName;
	}

	public void setComboName(Combo comboName) {
		this.comboName = comboName;
	}

	public ComboOption getComboOptions() {
		return comboOptions;
	}

	public void setComboOptions(ComboOption comboOptions) {
		this.comboOptions = comboOptions;
	}

}
