package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "auth_roles", catalog = "flexidea_crm", schema = "")
public class AuthRoles implements Serializable {
    @OneToMany(mappedBy = "role", fetch=FetchType.LAZY)
    private List<AuthRules> authRules;

    @Transient
    private boolean selected = false;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic(optional = false)
    @Column(name = "role_name", nullable = false, length=255)
    private String roleName;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt = new Date();
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by")
    private CrmUser createdBy;
    @Basic(optional = true)
    @Column(name = "modified_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="modified_by")
    private CrmUser modifiedBy;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	if (!(object instanceof AuthRoles)) {
	    return false;
	}
	AuthRoles other = (AuthRoles) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.UserRoles[id=" + id + "]";
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
	return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
	this.modifiedAt = modifiedAt;
    }

    public CrmUser getModifiedBy() {
	return modifiedBy;
    }

    public void setModifiedBy(CrmUser modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    public String getRoleName() {
	return roleName;
    }

    public void setRoleName(String roleName) {
	this.roleName = roleName;
    }

    public List<AuthRules> getAuthRules() {
	return authRules;
    }

    public void setAuthRules(List<AuthRules> authRules) {
	this.authRules = authRules;
    }

    public boolean isSelected() {
	return selected;
    }

    public void setSelected(boolean selected) {
	this.selected = selected;
    }
}
