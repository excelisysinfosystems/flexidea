package com.flexidea.crm.db.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "attachments", catalog = "flexidea_crm", schema = "")
@NamedQueries({
    @NamedQuery(name = "Attachments.findAll", query = "SELECT a FROM Attachments a")})
public class Attachments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "attachment_id", nullable = false)
    private Long attachmentId;
    @Column(name = "content_type", length = 256)
    private String contentType;
    @Column(name = "attachment_name", length = 255)
    private String attachmentName;
    @Lob
    @Column(name = "attachment_content", length=10485760)
    private byte[] attachmentContent;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="crm_comment", nullable=true)
    private CrmComments comment;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="enquiry", nullable=true)
    private EnquiryDocs enquiry;

    @Basic(optional = true)
    @Column(name = "created_at", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="created_by", nullable=true)
    private CrmUser createdBy;

    public Attachments() {
    }

    public Attachments(Long attachmentId) {
	this.attachmentId = attachmentId;
    }

    public Long getAttachmentId() {
	return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
	this.attachmentId = attachmentId;
    }

    public String getContentType() {
	return contentType;
    }

    public void setContentType(String contentType) {
	this.contentType = contentType;
    }

    public String getAttachmentName() {
	return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
	this.attachmentName = attachmentName;
    }

    public byte[] getAttachmentContent() {
	return attachmentContent;
    }

    public void setAttachmentContent(byte[] attachmentContent) {
	this.attachmentContent = attachmentContent;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (attachmentId != null ? attachmentId.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof Attachments)) {
	    return false;
	}
	Attachments other = (Attachments) object;
	if ((this.attachmentId == null && other.attachmentId != null) || (this.attachmentId != null && !this.attachmentId.equals(other.attachmentId))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.Attachments[attachmentId=" + attachmentId + "]";
    }

    public CrmComments getComment() {
	return comment;
    }

    public void setComment(CrmComments comment) {
	this.comment = comment;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public CrmUser getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(CrmUser createdBy) {
	this.createdBy = createdBy;
    }

    public EnquiryDocs getEnquiry() {
	return enquiry;
    }

    public void setEnquiry(EnquiryDocs enquiry) {
	this.enquiry = enquiry;
    }
}
