/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.flexidea.crm.db.dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author akshay
 */
@Entity
@Table(name = "lead_contacts", catalog = "flexidea_crm", schema = "")
public class LeadContacts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="lead")
    private Lead lead;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "contact")
    private Contact contact;

    public Long getId() {
	return id;
    }

    public LeadContacts setId(Long id) {
	this.id = id;
	return this;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof LeadContacts)) {
	    return false;
	}
	LeadContacts other = (LeadContacts) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.flexidea.crm.db.dto.LeadContacts[id=" + id + "]";
    }

    public Contact getContact() {
	return contact;
    }

    public void setContact(Contact contact) {
	this.contact = contact;
    }

    public Lead getLead() {
	return lead;
    }

    public void setLead(Lead lead) {
	this.lead = lead;
    }

}
