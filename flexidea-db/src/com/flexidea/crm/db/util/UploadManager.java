package com.flexidea.crm.db.util;

import com.flexidea.crm.db.dao.CompaniesDAO;
import com.flexidea.crm.db.dao.ContactsDAO;
import com.flexidea.crm.db.dao.ListsDAO;
import com.flexidea.crm.db.dao.UserDAO;
import com.flexidea.crm.db.dto.City;
import com.flexidea.crm.db.dto.Company;
import com.flexidea.crm.db.dto.Contact;
import com.flexidea.crm.db.dto.ContactType;
import com.flexidea.crm.db.dto.CrmUser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.digest.DigestUtils;
/**
 *
 * @author akshay
 */
public class UploadManager {
    /** The rather involved pattern used to match CSV's consists of three
     * alternations: the first matches aquoted field, the second unquoted,
     * the third a null field.
     */
    public static final String CSV_PATTERN = "\"([^\"]+?)\",?|([^,]+),?|,";
    private static Pattern csvRE;
    private boolean createUser;
    private CrmUser user;

    public static void main(String[] argv) throws IOException {
	//System.out.println(CSV_PATTERN);
	//new UploadManager(false).process(new BufferedReader(new InputStreamReader(System.in)));
	UploadManager um = new UploadManager(UserDAO.getUser(1L), false);
	File el = new File("/var/tmp/errors.txt");
	el.createNewFile();
	File ecsv = new File("/var/tmp/rejected.csv");
	ecsv.createNewFile();
	File elog = new File("/var/tmp/error.log");
	elog.createNewFile();
	System.out.println(um.upload(new File("/var/tmp/test.csv"),
	    el, ecsv, elog));
    }

    public Map<String, Integer> upload(File inputFile, File errorListFile,
	File rejectedFile, File errorLogFile) {
	Map<String, Integer> stats = new HashMap<String, Integer>();
	stats.put("read", 0);
	stats.put("written", 0);
	stats.put("processed", 0);
	stats.put("rejected", 0);

	try {
	    List<List<String>> errors = new ArrayList<List<String>>();
	    List<List<String>> badLines = new ArrayList<List<String>>();
	    List<List<String>> lines = processList(
		new BufferedReader(new FileReader(inputFile)));
	    stats.put("read", lines.size());

	    List<Map<String, Object>> contacts =
		getContacts(lines, errors, badLines);

	    stats.put("processed", contacts.size());
	    stats.put("rejected", badLines.size());

	    PrintWriter ecsv = new PrintWriter(rejectedFile);
	    PrintWriter el = new PrintWriter(errorListFile);

	    int row = 0;
	    for (List<String> line : badLines) {
		int ncols = line.size() - 1;
		int i = 0;
		for (String col : line) {
		    ecsv.print("\"" + col + "\"");
		    if (i < ncols) {
			ecsv.print(",");
		    } else {
			ecsv.println();
		    }
		    i++;
		}

		el.println("ROW=" + row);
		for (String err : errors.get(row)) {
		    el.println(err);
		}
		row++;
	    }

	    el.close();
	    ecsv.close();

	    //EntityManager em = DBUtil.getEM();
	    //em.getTransaction().begin();
	    for (Map<String, Object> m : contacts) {
		Contact c = (Contact) m.get("contact");
		Company comp = companies.get(c.getCompany()
		    .getCompanyName().toUpperCase());
		comp.setModifiedAt(new Date());
		comp.setModifiedBy(user);
		c.setCompany(comp);
		ContactsDAO.updateContact(null, c, comp,
		    createUser, (String) m.get("password"),
		    (String) m.get("username"));
		companies.put(c.getCompany()
		    .getCompanyName().toUpperCase(),
		    c.getCompany());

		stats.put("written", stats.get("written") + 1);
	    }
	    //em.getTransaction().commit();
	} catch (Exception ex) {
	    try {
		PrintWriter fw = new PrintWriter(errorLogFile);
		ex.printStackTrace(fw);
		fw.close();
	    } catch (Exception e) {
		e.printStackTrace(System.out);
	    }
	}

	return stats;
    }

    public List<Map<String, Object>> getContacts(
	List<List<String>> lines,
	List<List<String>> ierrors,
	List<List<String>> badLines) throws Exception {
	List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

	int rownum = 0;
	for (List<String> row : lines) {
	    Map m = prepareObject(row, ++rownum);

	    List<String> e = (List<String>) m.get("errors");
	    Contact contact = (Contact) m.get("contact");
	    System.out.println("Row: " + rownum);
	    if (e.isEmpty() && contact != null) {
		Company company = contact.getCompany();
		System.out.print("cid=");
		System.out.print(company.getId());
		System.out.print(", cname=");
		System.out.print(company.getCompanyName());
		System.out.print(", cind=");
		System.out.print(company.getIndustry());
		System.out.print(", ctype=");
		System.out.print(company.getContactType().getTypeName());
		System.out.print(", ccity=");
		System.out.print(company.getCity().getCityName());
		System.out.print(", fname=");
		System.out.print(contact.getFirstName());
		System.out.print(", lname=");
		System.out.print(contact.getLastName());
		System.out.print(", type=");
		System.out.print(contact.getContactType().getTypeName());
		System.out.print(", city=");
		System.out.println(contact.getCity().getCityName());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("contact", contact);
		map.put("username", m.get("username"));
		map.put("password", m.get("password"));
		list.add(map);
	    } else {
		//System.err.println("ERRORS: " + e.toString());
		ierrors.add(e);
		badLines.add(row);
	    }
	}
	return list;
    }
    /** Construct a regex-based CSV parser. */
    public UploadManager(CrmUser user, boolean createUser) {
	csvRE = Pattern.compile(CSV_PATTERN);
	this.user = user;
	this.createUser = createUser;
    }

    /** Process one file. Delegates to parse() a line at a time */
    public void process(BufferedReader in) throws IOException {
	String line;

	// For each line...
	while ((line = in.readLine()) != null) {
	    System.out.println("line = `" + line + "'");
	    List l = parse(line);
	    System.out.println("Found " + l.size() + " items.");
	    for (int i = 0; i < l.size(); i++) {
		System.out.print(l.get(i) + ",");
	    }
	    System.out.println();
	}
    }

    private Map<String, Company> companies = new HashMap<String, Company>();
    private Map<String, ContactType> typeMap = new HashMap<String, ContactType>();
    private Map<String, City> cityMap = new HashMap<String, City>();
    private Map<String, Boolean> nameMap = new HashMap<String, Boolean>();

    public Map prepareObject(List<String> list, int rowNum) {
	Map map = new HashMap();
	List<String> errors = new ArrayList<String>();

	map.put("errors", errors);

	String companyName = clean(list.get(0));
	String ucompanyName = isEmpty(companyName)? "" :
	    companyName.trim().replace("\\s+", " ").toUpperCase();
	String industry = clean(list.get(1));
	String companyType = clean(list.get(2));
	String ucompanyType = isEmpty(companyType)? "" :
	    companyType.trim().replace("\\s+", " ").toUpperCase();
	String companyAddress = clean(list.get(3));
	String companyCity = clean(list.get(4));
	String ucompanyCity = isEmpty(companyCity)? "" :
	    companyCity.trim().replace("\\s+", " ").toUpperCase();
	String companyPhone = clean(list.get(5));
	String companyPhone1 = clean(list.get(6));
	String companyFax = clean(list.get(7));
	String companyEmail = clean(list.get(8));
	String companyWebsite = clean(list.get(9));

	String firstName = clean(list.get(10));
	String lastName = clean(list.get(11));
	String uFirstName = isEmpty(firstName)? "" :
	    firstName.trim().replace("\\s+", " ").toUpperCase();
	String uLastName = isEmpty(lastName)? "" :
	    lastName.trim().replace("\\s+", " ").toUpperCase();
	String jobTitle = clean(list.get(12));
	String address = clean(list.get(13));
	String type = clean(list.get(14));
	String uType = isEmpty(type)? "" :
	    type.trim().replace("\\s+", " ").toUpperCase();
	String city = clean(list.get(15));
	String ucity = isEmpty(city)? "" :
	    city.trim().replace("\\s+", " ").toUpperCase();
	String phone = clean(list.get(16));
	String mobile = clean(list.get(17));
	String email = clean(list.get(18));
	String username = null;
	String password = null;
	if (createUser) {
	    password = clean(list.get(20));
	    username = clean(list.get(19));
	}

	Company company = null;
	ContactType companyCtype = null;
	ContactType contactCtype = null;
	City ocCity = null;
	City ocomCity = null;

	if (! "".equals(ucompanyName) && companies.containsKey(ucompanyName)) {
	    company = companies.get(ucompanyName);
	} else {
	    Map m = validateCompany(companyName);
	    List<String> e = (List<String>) m.get("errors");
	    boolean found = (Boolean) m.get("found");

	    if (! e.isEmpty()) {
		errors.addAll(e);
	    } else {
		company = (Company) m.get("object");
		companies.put(ucompanyName, company);
	    }
	}

	if (company == null || company.getContactType() == null) {
	    if (! "".equals(ucompanyType)
		&& typeMap.containsKey(ucompanyType)) {
		companyCtype = typeMap.get(ucompanyType);
	    } else {
		Map m = validateContactType(companyType);
		List<String> e = (List<String>) m.get("errors");
		boolean found = (Boolean) m.get("found");

		if (! e.isEmpty()) {
		    errors.addAll(e);
		} else if (found) {
		    companyCtype = (ContactType) m.get("object");
		    typeMap.put(ucompanyType, companyCtype);
		}
	    }
	}

	if (! "".equals(uType) && typeMap.containsKey(uType)) {
	    contactCtype = typeMap.get(uType);
	} else {
	    Map m = validateContactType(type);
	    List<String> e = (List<String>) m.get("errors");
	    boolean found = (Boolean) m.get("found");

	    if (! e.isEmpty()) {
		errors.addAll(e);
	    } else if (found) {
		contactCtype = (ContactType) m.get("object");
		typeMap.put(uType, contactCtype);
	    }
	}

	if (! "".equals(ucity) && cityMap.containsKey(ucity)) {
	    ocCity = cityMap.get(ucity);
	} else {
	    Map m = validateCity(city);
	    List<String> e = (List<String>) m.get("errors");
	    boolean found = (Boolean) m.get("found");

	    if (! e.isEmpty()) {
		errors.addAll(e);
	    } else if (found) {
		ocCity = (City) m.get("object");
		cityMap.put(ucity, ocCity);
	    }
	}

	if (company == null || company.getCity() == null) {
	    if (! "".equals(ucompanyCity) && cityMap.containsKey(ucompanyCity)) {
		ocomCity = cityMap.get(ucompanyCity);
	    } else {
		Map m = validateCity(companyCity);
		List<String> e = (List<String>) m.get("errors");
		boolean found = (Boolean) m.get("found");

		if (! e.isEmpty()) {
		    errors.addAll(e);
		} else if (found) {
		    ocomCity = (City) m.get("object");
		    cityMap.put(ucompanyCity, ocomCity);
		}
	    }
	}

	if (contactCtype == null  || ocCity == null
	    ||
	    (company == null
		|| ((company.getCity() == null && ocomCity == null)
		|| (company.getContactType() == null && companyCtype == null))
	    )) {
	    return map;
	}

	boolean allReqFound = true;

	if (isEmpty(industry) && company.getIndustry() == null) {
	    errors.add("Industry can not be left blank");
	    allReqFound = false;
	} else if (! isEmpty(industry)) {
	    company.setIndustry(industry);
	}

	if (isEmpty(companyAddress) && company.getCompanyAddress() == null) {
	    errors.add("Company address can not be left blank");
	    allReqFound = false;
	} else if (! isEmpty(companyAddress)) {
	    company.setCompanyAddress(companyAddress);
	}

	if (isEmpty(companyEmail) && company.getEmail() == null) {
	    errors.add("Company email can not be left blank");
	    allReqFound = false;
	} else if (! isEmpty(companyEmail)){
	    company.setEmail(companyEmail);
	}

	if (isEmpty(companyPhone) && company.getPhone() == null) {
	    errors.add("Company phone can not be left blank");
	    allReqFound = false;
	} else if (! isEmpty(companyPhone)) {
	    company.setPhone(companyPhone);
	}

	if (isEmpty(companyWebsite) && company.getWebsite() == null) {
	    errors.add("Company website can not be left blank");
	    allReqFound = false;
	} else if (! isEmpty(companyWebsite)) {
	    company.setWebsite(companyWebsite);
	}

	if (company.getId() == null && company.getContactType() == null) {
	    company.setContactType(companyCtype);
	}

	if (company != null && nameMap.containsKey(
	    ("x" + firstName).toUpperCase()
	    + "."
	    + ("x" + lastName).toUpperCase()
	    + "." + ("x" + companyName).toUpperCase())) {
	    errors.add("Duplicate first/last name '"
		+ firstName + " " + lastName + "' for company "
		+ companyName);
	    return map;
	} else {
	    Map m = validateContactName(firstName, lastName, company);
	    List<String> e = (List<String>) m.get("errors");
	    if (! e.isEmpty()) {
		errors.addAll(e);
		return map;
	    }
	}

	nameMap.put(
	    ("x" + firstName).toUpperCase()
	    + "."
	    + ("x" + lastName).toUpperCase()
	    + "." + ("x" + companyName).toUpperCase(), true);

	if (isEmpty(company.getPhone1())) {
	    company.setPhone1(companyPhone1);
	}

	if (isEmpty(company.getFax())) {
	    company.setFax(companyFax);
	}

	if (company.getCity() == null) {
	    company.setCity(ocomCity);
	}

	if (company.getContactType() == null) {
	    company.setContactType(companyCtype);
	}

	Contact c = new Contact();

	if (isEmpty(jobTitle)) {
	    errors.add("Contact job title can not be left empty");
	    allReqFound = false;
	} else {
	    c.setJobTitle(jobTitle);
	}

	if (isEmpty(phone)) {
	    errors.add("Contact phone can not be left empty");
	    allReqFound = false;
	} else {
	    c.setPhone(phone);
	}

	if (isEmpty(mobile)) {
	    errors.add("Contact mobile can not be left empty");
	    allReqFound = false;
	} else {
	    c.setCell(mobile);
	}

	if (isEmpty(email)) {
	    errors.add("Contact email can not be left empty");
	    allReqFound = false;
	} else {
	    c.setEmail(email);
	}

	if (isEmpty(address)) {
	    errors.add("Contact address can not be left empty");
	    allReqFound = false;
	} else {
	    c.setAddress(address);
	}

	String encPassword = null;
	if (createUser && isEmpty(password)) {
	    errors.add("Contact password can not be left empty");
	    allReqFound = false;
	} else if (createUser) {
	    encPassword = DigestUtils.md5Hex(password);
	}

	map.put("password", encPassword);
	map.put("username", username);

	if (! allReqFound) {
	    return map;
	}

	if (company.getContactType().isInternal()) {
	    c.setContactType(contactCtype);
	} else {
	    c.setContactType(company.getContactType());
	}

	c.setFirstName(firstName);
	c.setLastName(lastName);
	c.setCell(mobile);
	c.setCity(ocCity);
	c.setCompany(company);
	c.setCreatedAt(new Date());
	c.setCreatedBy(user);
	c.setEmail(email);
	c.setIsDeleted(false);
	c.setJobTitle(jobTitle);
	c.setPhone(phone);

	map.put("contact", c);
	return map;
    }

    /** Process one file. Delegates to parse() a line at a time */
    public List<List<String>> processList(BufferedReader in) throws IOException {
	List<List<String>> list = new ArrayList<List<String>>();
	String line;

	// For each line...
	while ((line = in.readLine()) != null) {
//	    System.out.println("line = `" + line + "'");
	    list.add(parse(line));
	}
	return list;
    }

    /** Parse one line.
     * @return List of Strings, minus their double quotes
     */
    public List<String> parse(String line) {
	List<String> list = new ArrayList<String>();
	Matcher m = csvRE.matcher(line);
	// For each field
	while (m.find()) {
	    String match = m.group();
	    if (match == null)
		break;
	    if (match.endsWith(",")) {  // trim trailing ,
		match = match.substring(0, match.length() - 1);
	    }
	    if (match.startsWith("\"")) { // assume also ends with
		match = match.substring(1, match.length() - 1);
	    }
	    if (match.length() == 0)
		match = null;
	    list.add(match);
	}
	return list;
    }

    private String clean(String input) {
	if (input != null) {
	    return input.trim().replace("\\s+", "");
	}
	return null;
    }

    private boolean isEmpty(String value) {
	return (value == null || "".equals(value.trim()));
    }

    private Map validateCompany(String companyName) {
	Map map = new HashMap();
	List<String> errors = new ArrayList<String>();
	map.put("errors", errors);
	map.put("found", false);

	if (isEmpty(companyName)) {
	    errors.add("Company name can not be empty.");
	    return map;
	}

	String cName = companyName.trim().replace("\\s+", " ");

	map.put("clean", cName);

	Company company = CompaniesDAO.getCompany(companyName);

	if (company == null) {
	    map.put("found", false);
	    company = new Company();
	    company.setCreatedAt(new Date());
	    company.setCreatedBy(user);
	    company.setCompanyName(companyName);
	}

	map.put("found", true);
	map.put("object", company);

	return map;
    }

    private Map validateContactType(String contactType) {
	Map map = new HashMap();
	List<String> errors = new ArrayList<String>();
	map.put("errors", errors);
	map.put("found", false);

	if (isEmpty(contactType)) {
	    errors.add("Type can not be empty.");
	    return map;
	}

	String cName = contactType.trim().replace("\\s+", " ");
	map.put("clean", cName);

	ContactType cType = ContactsDAO.getContactType(cName);

	if (cType == null) {
	    map.put("found", false);
	    errors.add("Cannot find contact type '" + cName + "'");
	    return map;
	}

	map.put("found", true);
	map.put("object", cType);

	return map;
    }

    private Map validateCity(String cityName) {
	Map map = new HashMap();
	List<String> errors = new ArrayList<String>();
	map.put("errors", errors);
	map.put("found", false);

	if (isEmpty(cityName)) {
	    errors.add("City can not be empty.");
	    return map;
	}

	String cName = cityName.trim().replace("\\s+", " ");

	City city = ListsDAO.getCity(cName);
	map.put("clean", cName);

	if (city == null) {
	    map.put("found", false);
	    errors.add("Cannot find city '" + cName + "'");
	    return map;
	}

	map.put("found", true);
	map.put("object", city);

	return map;
    }

    private Map validateContactName(String firstName, String lastName,
	Company company) {
	Map map = new HashMap();
	List<String> errors = new ArrayList<String>();
	map.put("errors", errors);

	if (isEmpty(firstName) || isEmpty(lastName)) {
	    errors.add("First/Last name can not be empty.");
	    return map;
	}

	String fName = firstName.trim().replace("\\s+", " ");
	String lName = lastName.trim().replace("\\s+", " ");

	map.put("cleanF", fName);
	map.put("cleanL", lName);

	if (ContactsDAO.checkExists(firstName, lastName, company)) {
	    errors.add("Duplicate contact '" + fName + "' '" + lName + "'");
	    return map;
	}

	return map;
    }
}
