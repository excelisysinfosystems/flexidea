package com.flexidea.crm.db.util;

import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.Tasks;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import in.flexidea.commons.sms.Sms;
import in.flexidea.commons.sms.exception.SMSException;
import in.flexidea.commons.sms.model.MessageStatusDetail;
import in.flexidea.commons.sms.model.SMSData;
import in.flexidea.commons.sms.util.*;
import java.util.Properties;
import java.util.ResourceBundle;
/**
 *
 * @author akshay
 */
public class SMSJob implements Job {

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
	JobDataMap map = jec.getJobDetail().getJobDataMap();
	Long reminderId = map.getLong("reminder");
	TaskReminders reminder = TasksDAO.getReminder(reminderId);
	Tasks task = reminder.getTask();
	if (reminder.isActive()) {
	    try {
		CrmUser user = null;
		if (task.getDelegatedTo() == null) {
		    user = task.getOwner();
		} else {
		    user = task.getDelegatedTo();
		}
		Sms smsObj= new Sms();
		Properties smsProperties=new Properties();
		ResourceBundle rs= ResourceBundle.getBundle("SMSProperties");
		smsProperties=PropertiesConverterUtil.getProperties(rs);
		smsObj.init(smsProperties);
		SMSData smsData =new SMSData();

		smsData.setMsg(task.getTitle());
		smsData.setPhoneNumber(user.getContact().getCell());
		smsData.setFlash("1");
		/* by using send method the user can send sms which take object of SmsData*/
		MessageStatusDetail msd = null;
		if ("true".equals(smsProperties.getProperty("sms.enabled"))) {
		    msd = smsObj.send(smsData);
		} else {
		    System.out.println("DEBUG: sending message to="
			+ smsData.getPhoneNumber()
			+ ", msg=" + smsData.getMsg());
		}
		/*MessageStatusDetail shows the status of sms */
		/* if shows the staus of sms, remaining credit and message id*/
		if (msd != null) {
		    System.out.println("Message Status:"
			+msd.getMessageStatus()+"\n	RemainningCr:"
			+msd.getCreditsRemaining()+"\n MessageId:"
			+msd.getMessageId());
		}
	    } catch (Exception ex) {
		Logger.getLogger(EmailJob.class.getName())
		    .log(Level.SEVERE, null, ex);
		throw new RuntimeException(ex);
	    }
	}
    }

}
