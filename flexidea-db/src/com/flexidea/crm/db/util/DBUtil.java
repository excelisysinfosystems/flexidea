package com.flexidea.crm.db.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author akshay
 */
public class DBUtil {
    public static EntityManager getEM() {
	return Persistence
		.createEntityManagerFactory("flexidea-dbPU")
		.createEntityManager();
    }

    public static List<String> compareModels(Object oldEntity,
	Object newEntity, String[] properties, String[] displayNames) {
	List<String> changed = new ArrayList<String>();

	try {
	    int index = 0;
	    for(String property : properties) {
		Method oldObjGetter = oldEntity.getClass()
		    .getMethod("get" 
		    + property.substring(0, 1).toUpperCase()
		    + property.substring(1),
		    new Class[] {}
		    );
		Method newObjGetter = newEntity.getClass()
		    .getMethod("get"
		    + property.substring(0, 1).toUpperCase()
		    + property.substring(1)
		    );
		Object oldValue = oldObjGetter.invoke(oldEntity);
		Object newValue = newObjGetter.invoke(newEntity);

		if ((oldValue == null && newValue == null)
		    || (oldValue == null && newValue != null)
		    || (oldValue != null && newValue == null)
		    || (oldValue != null && newValue != null
			&& ! oldValue.equals(newValue))
		    ) {
		    changed.add(displayNames[index]);
		}
		index++;
	    }
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}

	return changed;
    }

    private static char[] chars = new char[] {
	'p',  '6',  'y',  't',  '8',  'x',  '5',  'n',  '0',  'e',  'w',  'j',
	'q',  'h',  'c',  'f',  'd',  '9',  'v',  'o',  's',  'l',  'g',  '7',
	'b',  'k',  '4',  '2',  '1',  'm',  'i',  'u',  'a',  '3',  'z',  'r'
    };

    public static String getRandomString(int length) {
	StringBuilder sb = new StringBuilder();
	Random random = new Random();

	for (int i = 0; i < length; i++) {
	    sb.append(chars[random.nextInt(chars.length)]);
	}

	return sb.toString();
    }

    public static void main(String[] args) {
	for (int i = 0; i < 10; i++)
	    System.out.println(getRandomString(8));
    }

    public static Object getEntity(Class entityClass, Object id) {
	if (id == null) {
	    return null;
	}

	Object entity = null;
	EntityManager em = getEM();
	entity = em.find(entityClass, id);
	em.close();
	return entity;
    }
}
