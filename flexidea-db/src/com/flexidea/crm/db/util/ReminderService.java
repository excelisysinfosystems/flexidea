package com.flexidea.crm.db.util;

import com.flexidea.crm.db.dto.ReportsSchedule;
import com.flexidea.crm.db.dto.TaskReminders;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.DateIntervalTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author akshay
 */
public class ReminderService {
    public static void start() {
	try {
	    Scheduler schedular = StdSchedulerFactory.getDefaultScheduler();
	    schedular.start();
	} catch (SchedulerException ex) {
	    Logger.getLogger(ReminderService.class.getName())
		    .log(Level.SEVERE, null, ex);
	    throw new RuntimeException(ex);
	}
    }

    public static void stop() {
	try {
	    Scheduler schedular = StdSchedulerFactory.getDefaultScheduler();
	    schedular.shutdown();
	} catch (SchedulerException ex) {
	    Logger.getLogger(ReminderService.class.getName())
		    .log(Level.SEVERE, null, ex);
	    throw new RuntimeException(ex);
	}
    }

    public static void removeSchedule(TaskReminders reminder) {
	try {
	    Scheduler schedular = StdSchedulerFactory.getDefaultScheduler();
	    schedular.deleteJob(Long.toString(reminder.getId()),
		"Flexidea-CRM");
	    //TasksDAO.removeReminder(reminder);
	} catch (SchedulerException ex) {
	    Logger.getLogger(ReminderService.class.getName())
		    .log(Level.SEVERE, null, ex);
	    throw new RuntimeException(ex);
	}
    }

    public static void removeSchedule(ReportsSchedule schedule) {
	try {
	    Scheduler schedular = StdSchedulerFactory.getDefaultScheduler();
	    schedular.deleteJob(Long.toString(schedule.getId()),
		"Flexidea-CRM-Reports");
	    //TasksDAO.removeReminder(reminder);
	} catch (SchedulerException ex) {
	    Logger.getLogger(ReminderService.class.getName())
		    .log(Level.SEVERE, null, ex);
	    throw new RuntimeException(ex);
	}
    }

    public static void schedule(TaskReminders reminder) {
	try {
	    Scheduler schedular = StdSchedulerFactory.getDefaultScheduler();
	    JobDetail job = null;

	    switch(reminder.getReminderType()) {
		case EMAIL:
		    job = new JobDetail(Long.toString(reminder.getId()), 
			"Flexidea-CRM", EmailJob.class,
			false, false, true);
		    break;
		case SMS:
		    job = new JobDetail(Long.toString(reminder.getId()),
			"Flexidea-CRM", SMSJob.class,
			false, false, true);
		    break;
	    }

	    job.getJobDataMap().put("reminder", Long.toString(reminder.getId()));

	    Calendar cal = Calendar.getInstance();
	    cal.setTime(reminder.getTask().getEnd());
	    DateIntervalTrigger.IntervalUnit iu = null;
	    switch(reminder.getDurationMeasure()) {
		case DAYS:
		    cal.add(Calendar.DAY_OF_YEAR, 0 - reminder.getDuration());
		    iu = DateIntervalTrigger.IntervalUnit.DAY;
		    break;
		case HOURS:
		    cal.add(Calendar.HOUR, 0 - reminder.getDuration());
		    iu = DateIntervalTrigger.IntervalUnit.HOUR;
		    break;
		case MINUTES:
		    cal.add(Calendar.MINUTE, 0 - reminder.getDuration());
		    iu = DateIntervalTrigger.IntervalUnit.MINUTE;
		    break;
		case MONTHS:
		    cal.add(Calendar.MONTH, 0 - reminder.getDuration());
		    iu = DateIntervalTrigger.IntervalUnit.MONTH;
		    break;
		case WEEKS:
		    cal.add(Calendar.WEEK_OF_YEAR, 0 - reminder.getDuration());
		    iu = DateIntervalTrigger.IntervalUnit.WEEK;
		    break;
	    }

	    if (new Date().before(cal.getTime())) {
		Trigger trigger =
		    new DateIntervalTrigger(
		    Long.toString(reminder.getId()),
		    "Flexidea-CRM", 
		    new Date(),
		    null,//reminder.getTask().getEnd(),
		    iu,
		    reminder.getDuration()
		);

		schedular.scheduleJob(job, trigger);
	    }
	} catch (SchedulerException ex) {
	    Logger.getLogger(ReminderService.class.getName())
		    .log(Level.SEVERE, null, ex);
	    throw new RuntimeException(ex);
	}
    }

    public static void schedule(ReportsSchedule reminder) {
	try {
	    Scheduler schedular = StdSchedulerFactory.getDefaultScheduler();
	    JobDetail job = null;

	    job = new JobDetail(Long.toString(reminder.getId()),
		"Flexidea-CRM-Reports", EmailJob.class,
		false, false, true);

	    job.getJobDataMap().put("schedule",
		Long.toString(reminder.getId()));

	    Trigger trigger = null;

	    Calendar cal = Calendar.getInstance();
	    cal.setTime(reminder.getStartDate());

	    switch (reminder.getDuration()) {
		case Hourly:
		    trigger = new DateIntervalTrigger(
			Long.toString(reminder.getId()),
			"Flexidea-CRM-Reports",
			reminder.getStartDate(),
			null,//reminder.getTask().getEnd(),
			DateIntervalTrigger.IntervalUnit.HOUR,
			reminder.getInterval()
		    );
		    break;
		case Daily:
		    trigger = new DateIntervalTrigger(
			Long.toString(reminder.getId()),
			"Flexidea-CRM-Reports",
			reminder.getStartDate(),
			null,//reminder.getTask().getEnd(),
			DateIntervalTrigger.IntervalUnit.DAY,
			reminder.getInterval()
		    );
		    break;
		case Monthly:
		    trigger = new DateIntervalTrigger(
			Long.toString(reminder.getId()),
			"Flexidea-CRM-Reports",
			reminder.getStartDate(),
			null,//reminder.getTask().getEnd(),
			DateIntervalTrigger.IntervalUnit.MONTH,
			reminder.getInterval()
		    );
		    break;
		case Weekly:
		    trigger = new DateIntervalTrigger(
			Long.toString(reminder.getId()),
			"Flexidea-CRM-Reports",
			reminder.getStartDate(),
			null,//reminder.getTask().getEnd(),
			DateIntervalTrigger.IntervalUnit.WEEK,
			reminder.getInterval()
		    );
		    break;
	    }

	    if (trigger != null) {
		schedular.scheduleJob(job, trigger);
	    }
	} catch (SchedulerException ex) {
	    Logger.getLogger(ReminderService.class.getName())
		    .log(Level.SEVERE, null, ex);
	    throw new RuntimeException(ex);
	}
    }
}
