package com.flexidea.crm.db.util;

import com.flexidea.crm.db.dao.ReportsDAO;
import com.flexidea.crm.db.dao.TasksDAO;
import com.flexidea.crm.db.dto.CrmUser;
import com.flexidea.crm.db.dto.ReportModel;
import com.flexidea.crm.db.dto.ReportsSchedule;
import com.flexidea.crm.db.dto.TaskReminders;
import com.flexidea.crm.db.dto.Tasks;
import in.flexidea.commons.sendmail.Exception.SendMailException;
import in.flexidea.commons.sendmail.SendEmail;
import in.flexidea.commons.sendmail.data.AttachmentData;
import in.flexidea.commons.sendmail.data.MailData;
import in.flexidea.commons.sendmail.settings.MailSettings;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author akshay
 */
public class EmailJob implements Job {

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
	JobDataMap map = jec.getJobDetail().getJobDataMap();
	String remId = map.getString("reminder");
	String scheId = map.getString("scheduleId");
	Long reminderId = remId == null ? null : new Long(remId);
	Long scheduleId = scheId == null ? null : new Long(scheId);

	if (reminderId != null) {
	    sendReminder(reminderId);
	} else if (scheduleId != null) {
	    sendReport(scheduleId);
	}
    }

    private void sendReport(Long scheduleId) {
	ReportsSchedule schedule = 
	    ReportsDAO.getReportSchedule(scheduleId);
	ReportModel model = schedule.getModel();

	if (schedule.isActive()) {
	    try {
		SendEmail sender = new SendEmail();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(bos);
		writer.write(model.getReportName());
		writer.close();
		bos.close();

		MailData data = new MailData();
		List<String> rec = new ArrayList<String>();
		rec.add(schedule.getTo().getEmail());
		data.setTo(rec);
		data.setSubject("Report: " + model.getReportName());
		data.setBody(bos.toString());

		List<AttachmentData> attachments =
		    new ArrayList<AttachmentData>();
		AttachmentData att = new AttachmentData();
		att.setAttachFileStream(
		    new FileInputStream(ReportsDAO.export(model)));
		att.setContentType("application/pdf");
		att.setFileName("report.pdf");
		attachments.add(att);
		data.setAttachData(attachments);

		MailSettings settings = new MailSettings();
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader()
		    .getResourceAsStream(
		    "reminder-config.properties"));
		settings.setUsername(prop.getProperty("mailer.smtp.username"));
		settings.setPassword(prop.getProperty("mailer.smtp.password"));
		settings.setSmtpPort(prop.getProperty("mailer.smtp.port"));
		settings.setSmtpServer(prop.getProperty("mailer.smtp.server"));
		settings.setSender(prop.getProperty("mailer.sender"));
		settings.setProtocol("smtp");
		settings.setSmtpAuth("true");

		if ("true".equals(prop.getProperty("mailer.enabled"))) {
		    sender.init(settings);
		    sender.send(data);
		} else {
		    System.out.println("Sending Email reminder to=" + rec);
		}
	    } catch (Exception ex) {
		Logger.getLogger(EmailJob.class.getName())
		    .log(Level.SEVERE, null, ex);
		throw new RuntimeException(ex);
	    }
	}
    }
    private void sendReminder(Long reminderId) {
	TaskReminders reminder = TasksDAO.getReminder(reminderId);
	Tasks task = reminder.getTask();
	if (reminder.isActive()) {
	    try {
		CrmUser user = null;
		if (task.getDelegatedTo() == null) {
		    user = task.getOwner();
		} else {
		    user = task.getDelegatedTo();
		}

		SendEmail sender = new SendEmail();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(bos);
		writer.write(task.getTitle() + "\n");
		writer.write(task.getDescription());
		writer.close();
		bos.close();

		MailData data = new MailData();
		List<String> rec = new ArrayList<String>();
		rec.add(user.getContact().getEmail());
		data.setTo(rec);
		data.setSubject("Reminder: " + task.getTitle());
		data.setBody(bos.toString());

		MailSettings settings = new MailSettings();
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader()
		    .getResourceAsStream(
		    "reminder-config.properties"));
		settings.setUsername(prop.getProperty("mailer.smtp.username"));
		settings.setPassword(prop.getProperty("mailer.smtp.password"));
		settings.setSmtpPort(prop.getProperty("mailer.smtp.port"));
		settings.setSmtpServer(prop.getProperty("mailer.smtp.server"));
		settings.setSender(prop.getProperty("mailer.sender"));
		settings.setProtocol("smtp");
		settings.setSmtpAuth("true");

		if ("true".equals(prop.getProperty("mailer.enabled"))) {
		    sender.init(settings);
		    sender.send(data);
		} else {
		    System.out.println("Sending Email reminder to=" + rec);
		}
	    } catch (SendMailException ex) {
		Logger.getLogger(EmailJob.class.getName())
		    .log(Level.SEVERE, null, ex);
		throw new RuntimeException(ex);
	    } catch (IOException ex) {
		Logger.getLogger(EmailJob.class.getName())
		    .log(Level.SEVERE, null, ex);
		throw new RuntimeException(ex);
	    }
	}
    }
}
